import com.sun.spot.io.j2me.radiogram.*;

import com.sun.spot.peripheral.ota.OTACommandServer;

import java.awt.EventQueue;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.DateFormat;
import java.util.Date;
import javax.microedition.io.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class BasestationGatewayHost extends Thread {

	private JFrame frame;
	private JPanel contentPane;
	private JTextField tx_server;
	private JTextField tx_port;

	// Broadcast port on which we listen for sensor samples
	private static final int HOST_PORT = 67;

	BasestationGatewayHost() {
		frame = new JFrame("Base Station Gateway");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 192, 141);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblServer = new JLabel("Server:");
		lblServer.setBounds(24, 11, 46, 14);
		contentPane.add(lblServer);

		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(24, 36, 46, 14);
		contentPane.add(lblPort);

		tx_server = new JTextField();
		tx_server.setText("localhost");
		tx_server.setBounds(66, 8, 86, 20);
		contentPane.add(tx_server);
		tx_server.setColumns(10);

		tx_port = new JTextField();
		tx_port.setText("8000");
		tx_port.setBounds(66, 36, 86, 20);
		contentPane.add(tx_port);
		tx_port.setColumns(10);
	}

	public void run() {
		RadiogramConnection rCon = null;
		Datagram dg = null;
		DateFormat fmt = DateFormat.getTimeInstance();

		try {
			// Open up a server-side broadcast radiogram connection
			// to listen for sensor readings being sent by different SPOTs
			rCon = (RadiogramConnection) Connector.open("radiogram://:"
					+ HOST_PORT);
			dg = rCon.newDatagram(rCon.getMaximumLength());
		} catch (Exception e) {
			System.err.println("setUp caught " + e.getMessage());
		}

		// Main data collection loop
		while (true) {
			try {
				// Read sensor sample received over the radio
				rCon.receive(dg);
				String addr = dg.getAddress(); // read sender's Id
				long time = dg.readLong(); // read time of the reading
				int val = dg.readInt(); // read the sensor value

				String log = fmt.format(new Date(time)) + "  from: " + addr
						+ "   value = " + val;
				System.out.println(log);
				try {
					String servidor = tx_server.getText();
					int port = Integer.parseInt(tx_port.getText());
					InetAddress serverAddress = InetAddress.getByName(servidor);

					DatagramSocket socket = new DatagramSocket();

					String msg = addr +"|"+val;
					byte[] requestData = msg.getBytes();

					DatagramPacket echoRequest = new DatagramPacket(
							requestData, requestData.length);

					echoRequest.setAddress(serverAddress);
					echoRequest.setPort(port);

					socket.send(echoRequest);

					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				System.err.println("Caught " + e
						+ " while reading sensor samples.");
			}
		}
	}

	/**
	 * Start up the host application.
	 * 
	 * @param args
	 *            any command line arguments
	 */
	public static void main(String[] args) throws Exception {
		// register the application's name with the OTA Command server & start
		// OTA running
		OTACommandServer.start("SendDataDemo");

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BasestationGatewayHost app = new BasestationGatewayHost();
					app.frame.setVisible(true);
					app.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}
