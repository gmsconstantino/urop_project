import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class SendMessage extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tx_server;
	private JTextField tx_port;
	private boolean config = true;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SendMessage frame = new SendMessage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SendMessage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 192, 141);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblServer = new JLabel("Server:");
		lblServer.setBounds(24, 11, 46, 14);
		contentPane.add(lblServer);

		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(24, 36, 46, 14);
		contentPane.add(lblPort);

		tx_server = new JTextField();
		tx_server.setText("localhost");
		tx_server.setBounds(66, 8, 86, 20);
		contentPane.add(tx_server);
		tx_server.setColumns(10);

		tx_port = new JTextField();
		tx_port.setText("8000");
		tx_port.setBounds(66, 36, 86, 20);
		contentPane.add(tx_port);
		tx_port.setColumns(10);

		JButton btnNewButton = new JButton("Send Message");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (config)
					config = false;
				try {
					String servidor = tx_server.getText();
					int port = Integer.parseInt(tx_port.getText());
					InetAddress serverAddress = InetAddress.getByName(servidor);

					DatagramSocket socket = new DatagramSocket();
					String request = "HELLO";
					byte[] requestData = request.getBytes();

					DatagramPacket echoRequest = new DatagramPacket(
							requestData, requestData.length);

					echoRequest.setAddress(serverAddress);
					echoRequest.setPort(port);

					socket.send(echoRequest);
					
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(10, 67, 144, 23);
		contentPane.add(btnNewButton);
	}
}
