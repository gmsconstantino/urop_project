/*
 **  Wireless Sensor Network Simulator
 *  The next generation for WSN Simulations
 */
package org.wisenet.protocols.insens.basestation.dijkstra.engine;

/**
 *
 * @author Pedro Marques da Silva <MSc Student @di.fct.unl.pt>
 */
public class CalculatorException extends Exception {

    /**
     *
     * @param msg
     */
    public CalculatorException(String msg) {
        super(msg);
    }
}
