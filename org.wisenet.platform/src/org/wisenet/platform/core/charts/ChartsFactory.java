/*
 *     Wireless Sensor Network Simulator
 *   The next generation for WSN Simulations
 */
package org.wisenet.platform.core.charts;

import java.awt.Font;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.Dataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.wisenet.simulator.components.evaluation.MessageDatabase;
import org.wisenet.simulator.core.energy.GlobalEnergyDatabase;

/**
 *
 * @author Andr� Guerreiro
 */
public class ChartsFactory {

	
	public static SimulationChart createRouteBalancementbyEventChart(final MessageDatabase database){
        SimulationChart chart = new SimulationChart() {
        	 @Override
             protected Dataset createDataset() {
        		 
        		 List<Integer> list = database.getMessageCountByHops();
        		 
//        		 final double[][] data = new double[1][];
//        		 data[0] = new double[list.size()];
//        		 for( int i = 0; i < list.size(); i++){
//        			data[0][i] = list.get(i);
//        		 }
//
//        		 CategoryDataset dataset = DatasetUtilities.createCategoryDataset("Balancement", "HopCount", data);
        		 
        		 
        		 DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        		 for( int i = 0; i < list.size(); i++){
        			 dataset.setValue(list.get(i), "count",(i+1)+" hop ");
        		 }

        		 return dataset;
        		
             }
        	 @Override
             protected JFreeChart createChart() {

        		 
        		  final ChartPanel chartPanel = new ChartPanel(chart);        		 
                 JFreeChart c = null;
        
                 if (dataset != null) {
                	 
                	 c = ChartFactory.createBarChart(
                			 "Message Count by Hops",
                			 "Hops",
                			 "number of messages ",
                			 (CategoryDataset) dataset,
                			 PlotOrientation.HORIZONTAL,
                			 true,
                			 true,
                			 false);
                	 
                	 CategoryPlot categoryplot = (CategoryPlot) c.getPlot();
                	 BarRenderer br = (BarRenderer)categoryplot.getRenderer();
                     return c;
                 }
                 return c;
             }
         };
         chart.setTitle("Route Balancement Test");
         chart.create();
         return chart;
    }
	
	public static SimulationChart createRouteDisjointnesstbyEventChart(final Hashtable database){
        SimulationChart chart = new SimulationChart() {
        	 @Override
             protected Dataset createDataset() {
        		 //order dataset
        		 LinkedList<SimpleEntry<Short,Integer>> ordered = new LinkedList<SimpleEntry<Short,Integer>>();
        	        for(Object o : database.keySet()){
        	        	Short key = (Short)o;
        	        	ordered.add(new SimpleEntry<Short,Integer>(key,(Integer)database.get(key)));
        	        }
        	        
        	        Comparator<SimpleEntry<Short,Integer>> c = new Comparator<SimpleEntry<Short,Integer>>() {

        	            public int compare(SimpleEntry<Short,Integer> o1, SimpleEntry<Short,Integer> o2) {
        	                return Integer.valueOf(o1.getValue()).compareTo(Integer.valueOf(o2.getValue()));
        	            }
        	        };
        	        
        	        Collections.sort(ordered,c);
        		 
        		 
        		 DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        		 for(SimpleEntry<Short,Integer> s : ordered){
        			 Short id = s.getKey();
        			 dataset.setValue((Integer)s.getValue(), "routes", "node "+id);
        		 }
        		 

        		 return dataset;
        		
             }
        	 @Override
             protected JFreeChart createChart() {

        		 
        		  final ChartPanel chartPanel = new ChartPanel(chart);        		 
                 JFreeChart c = null;
        
                 if (dataset != null) {
                	 
                	 c = ChartFactory.createBarChart(
                			 "Number of Routes per Node",
                			 "Node",
                			 "number of routes ",
                			 (CategoryDataset) dataset,
                			 PlotOrientation.VERTICAL,
                			 true,
                			 true,
                			 false);
                	 
                	 CategoryPlot categoryplot = (CategoryPlot) c.getPlot();
                	 BarRenderer br = (BarRenderer)categoryplot.getRenderer();
                     return c;
                 }
                 return c;
             }
         };
         chart.setTitle("Route Balancement Test");
         chart.create();
         return chart;
    }
	
    /**
     * Creates a energyByEventChart
     * @param test
     * @return
     */
    public static SimulationChart createEnergybyEventChart(final GlobalEnergyDatabase database, String title) {

        SimulationChart chart = new SimulationChart() {

            @Override
            protected Dataset createDataset() {
                DefaultPieDataset ds = new DefaultPieDataset();
                Double value = 0.0;
                if (database != null) {
                    Hashtable<String, Double> ee = database.getEventsEnergy();
                    for (String key : ee.keySet()) {
                        value = ee.get(key);
                        ds.setValue(key, value);
                    }
                } else {
                    ds = null;
                }
                return ds;
            }

            @Override
            protected JFreeChart createChart() {

                JFreeChart c = null;
                if (dataset != null) {
                    c = ChartFactory.createPieChart(
                            getTitle(), (PieDataset) dataset, // data
                            true, // include legend
                            true,
                            false);

                    PiePlot plot = (PiePlot) c.getPlot();
                    plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 7));
                    plot.setNoDataMessage("No data available");
                    plot.setCircular(false);
                    plot.setLabelGap(0.02);
                    return c;
                }
                return c;
            }
        };
        chart.setTitle(title);
        chart.create();
        return chart;
    }
}
