/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */

package org.wisenet.platform;

/**
 *
 * @author Pedro Marques da Silva
 */
public class PlatformConstants {
    public static String PLATFORM_CONFIGURATION_FILE="Platform.properties";
    public static String MSG_UNKNOWNED_ERROR="Unknowned Error Occurred!";
    public static String MSG_SIMULATION_IS_NULL="Simulation is NULL";
    public static String SIMULATION_FILE_DESCRIPTION="*.sim - Simulation file";
    public static String SIMULATION_FILE_EXTENSION=".sim";
    public static String MSG_SAVE_ERROR_OR_CANCEL="Simulation save failed by user action or error";
    public static String MSG_OPEN_ERROR_OR_CANCEL="Simulation open failed by user action or error";

}
