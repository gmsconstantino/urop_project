package org.wisenet.platform.gui.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.wisenet.platform.common.ui.PlatformPanel;
import org.wisenet.platform.core.charts.ChartsFactory;
import org.wisenet.platform.core.charts.SimulationChart;
import org.wisenet.simulator.components.simulation.Simulation;
import org.wisenet.simulator.core.node.Node;

import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;

public class RouteDisjointTestResultsPanel extends PlatformPanel {
	
	private Dimension panelDimension = new Dimension(300, 250);
	private Dimension chartDimenseion = new Dimension(450, 200);
	protected Simulation simulation;
	
	
	private JLabel lblNumberOfRoutes ;
	private JLabel lblNumberOfDisjoint;
	private JLabel lblOfDisjoint;
	private JLabel lblNumberOfShared ;
	private JLabel lblNumberOfPrimary ;
	private JLabel lblNumberOfSecundary ;
	private JLabel lblNumberOfSinks;
	private JLabel lblTotalNumberOf;
	private JFormattedTextField totalNodesField;
	private JFormattedTextField stableNodesField;
	private JFormattedTextField totalRoutesField ;
	private JFormattedTextField nodesWithAlternativePercentageField;
	private JFormattedTextField avgRoutesPerNodeField ;
	private JFormattedTextField percentageDisjointRoutesField;
	private JFormattedTextField primaryRoutesField ;
	private JFormattedTextField alternativeRoutesField ;
	private JFormattedTextField sinkNumberField;
	private JPanel charPanel;
	
	private Integer numberOfSinks;
	private Integer totalNumberOfRoutes;
	private Integer numberOfPrimaryRoutes;
	private Integer numberOfAlternativaRoutes;
	private Integer numberOfDisjointRoutes;
	private Integer numberOfSharedRoutes;
	private Integer percentOfDisjointRoutes;
	
	private Hashtable routesPerNode;
	
	
	public RouteDisjointTestResultsPanel(Simulation s) {
		simulation = s;
		initComponents();
	}
		
	private void initComponents(){
		setLayout(null);
		
		numberOfSinks = 0;
		totalNumberOfRoutes = 0;
		numberOfPrimaryRoutes = 0;
		numberOfAlternativaRoutes = 0;
		numberOfDisjointRoutes = 0;
		numberOfSharedRoutes = 0;
		percentOfDisjointRoutes = 0;
		routesPerNode = new Hashtable();
		
		this.setPreferredSize(new Dimension(455, 335));
		
		lblNumberOfRoutes = new JLabel("Total routes:");
		lblNumberOfRoutes.setBounds(296, 8, 88, 16);
		add(lblNumberOfRoutes);
		
		lblNumberOfDisjoint = new JLabel("% nodes w/alternative routes");
		lblNumberOfDisjoint.setBounds(6, 64, 192, 16);
		add(lblNumberOfDisjoint);
		
		lblOfDisjoint = new JLabel("disjointness (%):");
		lblOfDisjoint.setBounds(282, 93, 102, 16);
		add(lblOfDisjoint);
		
		lblNumberOfShared = new JLabel("Average routes p/Node:");
		lblNumberOfShared.setBounds(6, 93, 192, 16);
		add(lblNumberOfShared);
		
		totalRoutesField = new JFormattedTextField();
		totalRoutesField.setEditable(false);
		totalRoutesField.setBounds(381, 2, 56, 28);
		add(totalRoutesField);
		
		nodesWithAlternativePercentageField = new JFormattedTextField();
		nodesWithAlternativePercentageField.setEditable(false);
		nodesWithAlternativePercentageField.setBounds(190, 58, 56, 28);
		add(nodesWithAlternativePercentageField);
		
		avgRoutesPerNodeField = new JFormattedTextField();
		avgRoutesPerNodeField.setEditable(false);
		avgRoutesPerNodeField.setBounds(190, 87, 56, 28);
		add(avgRoutesPerNodeField);
		
		percentageDisjointRoutesField = new JFormattedTextField();
		percentageDisjointRoutesField.setEditable(false);
		percentageDisjointRoutesField.setBounds(381, 87, 56, 28);
		add(percentageDisjointRoutesField);
		
		lblNumberOfPrimary = new JLabel("Primary routes:");
		lblNumberOfPrimary.setBounds(289, 36, 95, 16);
		add(lblNumberOfPrimary);
		
		lblNumberOfSecundary = new JLabel("Alternative routes:");
		lblNumberOfSecundary.setBounds(271, 64, 116, 17);
		add(lblNumberOfSecundary);
		
		primaryRoutesField = new JFormattedTextField();
		primaryRoutesField.setEditable(false);
		primaryRoutesField.setBounds(381, 30, 56, 28);
		add(primaryRoutesField);
		
		alternativeRoutesField = new JFormattedTextField();
		alternativeRoutesField.setEditable(false);
		alternativeRoutesField.setBounds(381, 58, 56, 28);
		add(alternativeRoutesField);
		
		lblNumberOfSinks = new JLabel("Sinks:");
		lblNumberOfSinks.setBounds(6, 8, 56, 16);
		add(lblNumberOfSinks);
		
		sinkNumberField = new JFormattedTextField();
		sinkNumberField.setEditable(false);
		sinkNumberField.setBounds(190, 2, 56, 28);
		add(sinkNumberField);
		
		lblTotalNumberOf = new JLabel("Total Nodes/Stable:");
		lblTotalNumberOf.setBounds(6, 36, 133, 16);
		add(lblTotalNumberOf);
		
		stableNodesField = new JFormattedTextField();
		stableNodesField.setEditable(false);
		stableNodesField.setBounds(190, 30, 56, 28);
		add(stableNodesField);
		
		charPanel = new JPanel();
		charPanel.setBounds(6, 129, 450, 200);
		charPanel.setPreferredSize(chartDimenseion);
		add(charPanel);
		
		totalNodesField = new JFormattedTextField();
		totalNodesField.setEditable(false);
		totalNodesField.setBounds(136, 30, 56, 28);
		add(totalNodesField);
	}

	@Override
	 public void beforeStart() {
	        super.beforeStart();
	        LinkedList<Short> sinks = (LinkedList<Short>)simulation.getSimulator().getSinkNodesIds(); 
	        Node sink = simulation.getSimulator().getNode(sinks.getFirst());
	        if(sink.getRoutingLayer().usesConsensus()){ // total information is in every BS...so only needs to read one
	        	updateRoutesInfoConsensus(sink);
	        }else{ // no consensus, needs to go through all BS
	        	
	        	//for every sinkNode, retrieve info on routes and update
		        for( Node n : simulation.getSimulator().getNodes() ){
		        	if (n.isSinkNode())
		        		updateRoutesInfo(n);
		        	else if( !n.getRoutingLayer().isStable()){
		        		routesPerNode.put(n.getId(), 0);
		        	}
		        }
	        }
	        
	        
	        SimulationChart c = ChartsFactory.createRouteDisjointnesstbyEventChart(routesPerNode);
	        ChartFrame cf = new ChartFrame("Test", c.getChart());
	        cf.setSize(450, 240);
	        ChartPanel cp = c.getPanel();
	        cp.setPreferredSize(chartDimenseion);
	        charPanel.add(cp, BorderLayout.CENTER);
	        
	        percentOfDisjointRoutes = numberOfDisjointRoutes*100/(numberOfDisjointRoutes+numberOfSharedRoutes);
	        
	        Integer stableNodes = ((Integer)simulation.getSimulator().getNumberOfStableNodes());
	        
	        totalRoutesField.setText(totalNumberOfRoutes.toString());
	        double perc = calculatePercentageOfNodesWithAlternativeRotues(routesPerNode);
	        nodesWithAlternativePercentageField.setText(((Double)perc).toString());
	        
	        int totalNodes = (Integer)simulation.getSimulator().getNodes().size() ;
	        
	        double avg = (double)(numberOfPrimaryRoutes+numberOfAlternativaRoutes)/(double)(totalNodes - numberOfSinks);
	        DecimalFormat twoDForm = new DecimalFormat("#.##");
			Double toShow = Double.valueOf(twoDForm.format(avg));
	        avgRoutesPerNodeField.setText(toShow.toString()) ;
	    	
	        percentageDisjointRoutesField.setText(percentOfDisjointRoutes.toString());
	    	primaryRoutesField.setText(numberOfPrimaryRoutes.toString()) ;
	    	alternativeRoutesField.setText(numberOfAlternativaRoutes.toString()) ;
	    	sinkNumberField.setText(numberOfSinks.toString());
	    	stableNodesField.setText( stableNodes.toString());
	    	totalNodesField.setText(((Integer)totalNodes).toString());
	}
	
	private double calculatePercentageOfNodesWithAlternativeRotues(Hashtable<Short,Integer> routesperNode){
		int count = 0;
		for(Short nodeId : routesperNode.keySet()){
			int routeCount = routesperNode.get(nodeId);
			if(routeCount > 1)
				count++;
		}
		return count*100/routesperNode.size();
	}
	
	
	private void updateRoutesInfoConsensus(Node sink){
		numberOfSinks = simulation.getSimulator().getSinkNodesIds().size();
		Vector allPaths = sink.getRoutingLayer().getBaseStationController().getAllPaths();

		Vector allPathsUpdated = (Vector)allPaths.clone();
		
		for(Object r: allPaths){
			ArrayList route = (ArrayList)r;
			Short destNodeId = (Short)route.get(route.size()-1);
			
			
			if(allPathsUpdated.contains(route)){//if not then it was already counted..
				totalNumberOfRoutes++;
				numberOfPrimaryRoutes++;
				
				allPathsUpdated.remove(route);
				
				
				Integer count = (Integer)routesPerNode.get(destNodeId);

				if (count == null){
					count = 1;
				}else{
					count++;
				}
				routesPerNode.put(destNodeId, count);
				ArrayList alternativeRoute = findAndRemoveAlternativeRoute(allPathsUpdated,route);
				while( alternativeRoute != null){
					totalNumberOfRoutes++;
					numberOfAlternativaRoutes++;
					
					count = (Integer)routesPerNode.get(destNodeId);
					count++;
					routesPerNode.put(destNodeId, count);
					
					boolean disjoint = checkDisjointNess(route,alternativeRoute);
					if( disjoint){
						numberOfDisjointRoutes++;
					}else{
						numberOfSharedRoutes++;
					}
					
					alternativeRoute = findAndRemoveAlternativeRoute(allPathsUpdated,route);
				}
			}
		}
		//to consider non-stable nodes
		for(Node n : simulation.getSimulator().getNodes()){
			Short nodeId = n.getId(); 
//			if (routesPerNode.get(nodeId) == null)
			if (!n.getRoutingLayer().isStable())	
				routesPerNode.put(nodeId, 0);
		}
			
		
	}
	
	private ArrayList findAndRemoveAlternativeRoute(Vector paths, ArrayList originalRoute){
		Short originalDestination = (Short)originalRoute.get(originalRoute.size()-1);
		//search every routes...
		for(Object r : paths){
			ArrayList route = (ArrayList)r;
			Short destination = (Short)route.get(route.size()-1);
			if( originalDestination == destination){
				paths.remove(route);
				return route;
			}
		}
		return null;
	}
	
	

	private void updateRoutesInfo(Node sink){
		numberOfSinks++;
		List<List<List<Short>>> paths = sink.getRoutingLayer().getBaseStationController().getPaths();
//		System.out.println("yahoo! "+paths.size());
		ArrayList firstPaths = (ArrayList)paths.get(0);
		//for each route on original set of paths
		for(Object r : firstPaths){
			ArrayList route = (ArrayList)r;
			
			int jj=0;
			if(route.size()-1 <= 0)
				 jj++;//debug...TODO:
			else{
				totalNumberOfRoutes++;
				numberOfPrimaryRoutes++;
				
				Short destNodeId = (Short)route.get(route.size()-1);
				Integer count = (Integer)routesPerNode.get(destNodeId);
				if (count == null){
					count = 1;
				}else{
					count++;
				}
				routesPerNode.put(destNodeId, count);
				
				//search for alternative on other sets of paths
				for(int i = 1; i < paths.size(); i++){
					ArrayList alternativePaths = (ArrayList)paths.get(i);
					ArrayList routeAlternative = findAlternativeRoute(alternativePaths,route);
					if (routeAlternative != null){
						totalNumberOfRoutes++;
						numberOfAlternativaRoutes++;
						
						count = (Integer)routesPerNode.get(destNodeId);
						count++;
						routesPerNode.put(destNodeId, count);
						
						boolean disjoint = checkDisjointNess(route,routeAlternative);
						if( disjoint){
							numberOfDisjointRoutes++;
						}else{
							numberOfSharedRoutes++;
						}
					}
				}
			}
			
			
		}
		
	}
	
	
	private ArrayList findAlternativeRoute(ArrayList paths, ArrayList originalRoute){
		Short originalDestination = (Short)originalRoute.get(originalRoute.size()-1);
		//search every routes...
		for(Object r : paths){
			ArrayList route = (ArrayList)r;
			Short destination = (Short)route.get(route.size()-1);
			if( originalDestination == destination)
				return route;
		}
		return null;
	}
	
	private boolean checkDisjointNess(ArrayList route1, ArrayList route2){
		//goes from 1 until size-2 because first item is source and last item is destination, which should the same
		for(int i = 1; i < route1.size()-2; i++){
			if (route2.contains(route1.get(i)))
				return false;
		}
		return true;
	}
	
	
	
	@Override
	public boolean onCancel() {
		return true;
	}

	@Override
	public boolean onOK() {
		return true;
	}

	@Override
	public boolean onApply() {
		return true;
	}

	@Override
	protected boolean isDataValid() {
		return true;
	}

	@Override
	public void beforeClose() {
		// TODO Auto-generated method stub

	}
}
