package org.wisenet.platform.gui.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.wisenet.platform.common.ui.PlatformPanel;
import org.wisenet.platform.core.charts.ChartsFactory;
import org.wisenet.platform.core.charts.SimulationChart;
import org.wisenet.simulator.components.evaluation.MessagePassageEntry;
import org.wisenet.simulator.components.evaluation.tests.AbstractTest;
import org.wisenet.simulator.core.node.Node;
import org.wisenet.simulator.core.ui.ISimulationDisplay;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;

public class RoutingBalancementResultsPanel extends PlatformPanel  {

	protected AbstractTest test;
	private Dimension panelDimension = new Dimension(600, 350);
	private Dimension chartDimenseion = new Dimension(600, 250);
	
	JPanel hopBalancementPanel;
	JPanel charPanel;
	JLabel lblSenderNodes; 
	JLabel lblMessagesSent; 
	JLabel lblMessagesGenerated;
	JLabel lblMessagesReceived; 
	JLabel lblResuts;
	JFormattedTextField senderNodesField;
	JFormattedTextField messagesSentField;
	JFormattedTextField messagesReceivedField;
	JFormattedTextField messagesGeneratedField;
	JComboBox messagesComboBox;
	private JLabel lblMessageToView;
	
	private LinkedList<Short> nodesToClean;
	
	
	public RoutingBalancementResultsPanel(){
		
		initComponents();
	}
	
	
	private void initComponents(){
		
		nodesToClean = new LinkedList<Short>();
		this.setPreferredSize(panelDimension);
		hopBalancementPanel = new JPanel();
		hopBalancementPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
		hopBalancementPanel.setSize(panelDimension);
		add(hopBalancementPanel);
		hopBalancementPanel.setLayout(null);
		
		charPanel = new JPanel();
		charPanel.setBounds(6, 63, 528, 281);
		charPanel.setPreferredSize(chartDimenseion);
		hopBalancementPanel.add(charPanel);
		
		lblSenderNodes = new JLabel("Sender Nodes:");
		lblSenderNodes.setBounds(234, 12, 100, 16);
		hopBalancementPanel.add(lblSenderNodes);
		
		lblMessagesSent = new JLabel("Messages sent:");
		lblMessagesSent.setBounds(6, 12, 100, 16);
		hopBalancementPanel.add(lblMessagesSent);
		
		senderNodesField = new JFormattedTextField();
		senderNodesField.setEditable(false);
		senderNodesField.setBounds(330, 6, 58, 28);
		hopBalancementPanel.add(senderNodesField);
		
		messagesSentField = new JFormattedTextField();
		messagesSentField.setEditable(false);
		messagesSentField.setBounds(141, 6, 43, 28);
		hopBalancementPanel.add(messagesSentField);
		
		lblMessagesGenerated = new JLabel("Messages generated:");
		lblMessagesGenerated.setBounds(196, 35, 138, 16);
		hopBalancementPanel.add(lblMessagesGenerated);
		
		lblMessagesReceived = new JLabel("Messages Received");
		lblMessagesReceived.setBounds(6, 35, 138, 16);
		hopBalancementPanel.add(lblMessagesReceived);
		
		messagesReceivedField = new JFormattedTextField();
		messagesReceivedField.setEditable(false);
		messagesReceivedField.setBounds(141, 29, 43, 28);
		hopBalancementPanel.add(messagesReceivedField);
		
		messagesGeneratedField = new JFormattedTextField();
		messagesGeneratedField.setEditable(false);
		messagesGeneratedField.setBounds(330, 29, 58, 28);
		hopBalancementPanel.add(messagesGeneratedField);
		
		lblMessageToView = new JLabel("Message to view route:");
		lblMessageToView.setBounds(400, 12, 149, 16);
		hopBalancementPanel.add(lblMessageToView);
		
		messagesComboBox = new JComboBox();
		messagesComboBox.setBounds(388, 30, 161, 27);
		messagesComboBox.addActionListener( new java.awt.event.ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox)e.getSource();
		        Integer messageId = (Integer)cb.getSelectedItem();
		        drawMessageRoutes(messageId);
			}
		}	
		);
		
		hopBalancementPanel.add(messagesComboBox);
		
		lblResuts = new JLabel("Results");
		lblResuts.setBounds(6, 22, 68, 30);
		add(lblResuts);
		lblResuts.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
	}
	
	@Override
	public boolean onCancel() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onOK() {
		Node toClean;
		for(Short id: nodesToClean){
			toClean = test.getSimulation().getSimulator().findNode(id);
			toClean.getNextHops().clear();
			toClean.getGraphicNode().unmarkStable();
		}
		nodesToClean.clear();
		
		return true;
	}

	@Override
	public boolean onApply() {
		return true;
	}

	@Override
	protected boolean isDataValid() {
		return true;
	}
	
	private void drawMessageRoutes(int messageId){
		System.out.println("it works! "+messageId);
		
		//start by erasing last message path
		Node toClean;
		for(Short id: nodesToClean){
			toClean = test.getSimulation().getSimulator().findNode(id);
			toClean.getNextHops().clear();
			toClean.getGraphicNode().unmarkStable();
		}
		nodesToClean.clear();
		
		List<MessagePassageEntry> entries =  test.getEvaluationManager().getMessageDatabase().getSeenMessageNodes().get(messageId);
		
		boolean firstNode = true;
		//build the path of nextHops for this message..
		for(int i = 0; i < entries.size(); i++){
			MessagePassageEntry m = (MessagePassageEntry)entries.get(i);
			Node currentNode = test.getSimulation().getSimulator().findNode(m.getReportingNode());
			if(firstNode){
				currentNode.getGraphicNode().markStable();
				firstNode = false;
			}
			LinkedList<SimpleEntry> nextHops = new LinkedList<SimpleEntry>();
			//find nodes who received message from currentNode
			for(int j = i+1; j < entries.size(); j++){
				MessagePassageEntry m2 = (MessagePassageEntry)entries.get(j);
				if(m2.getPreviousHopId() == currentNode.getId()){
					ISimulationDisplay disp = test.getSimulation().getDisplay();
					
					Node node2 = test.getSimulation().getSimulator().findNode(m2.getReportingNode());
					SimpleEntry s = new SimpleEntry(node2.getGraphicNode().getX(),node2.getGraphicNode().getY());
					nextHops.addLast(s);

				}
					
			}
			currentNode.setNextHops(nextHops);
			nodesToClean.addLast(currentNode.getId());
		}
		
	}
	
    private void updateResults() {
    	senderNodesField.setText( ((Integer)test.getEvaluationManager().getMessageDatabase().getTotalSenderNodes()).toString()) ;
    	messagesSentField.setText( ((Long)test.getEvaluationManager().getMessageDatabase().getTotalNumberOfUniqueMessagesSent()).toString());
    	messagesReceivedField.setText( ((Long)test.getEvaluationManager().getMessageDatabase().getTotalMessagesReceived()).toString());
    	messagesGeneratedField.setText( ((Long)test.getEvaluationManager().getMessageDatabase().getTotalNumberOfMessagesSent()).toString());
    	
    	Set<Long> messages =  test.getEvaluationManager().getMessageDatabase().getSeenMessageNodes().keySet();
    	for(Object m :messages){
    		Integer messageId = (Integer)m;
    		messagesComboBox.addItem(messageId);
    	}
    }
	
	 @Override
	    public void beforeStart() {
	        super.beforeStart();
	        if (test != null) {
	            updateResults();
	            SimulationChart c = ChartsFactory.createRouteBalancementbyEventChart(test.getEvaluationManager().getMessageDatabase());
	            ChartFrame cf = new ChartFrame("Test", c.getChart());
	            cf.setSize(400, 300);
//	            cf.setVisible(true);
//	            this.add(cf);
	            
	            cf.setSize(hopBalancementPanel.getSize());
	            ChartPanel cp = c.getPanel();
//	            cp.setPreferredSize(new Dimension(300, 200));
	            cp.setPreferredSize(chartDimenseion);
//	            cp.setSize(hopBalancementPanel.getSize());
	            charPanel.add(cp, BorderLayout.CENTER);
//	            hopBalancementPanel.setSize(c.getPanel().getSize());
	        }
	    }

	@Override
	public void beforeClose() {
		
		
	}
	
	public AbstractTest getTest() {
        return test;
    }

    public void setTest(AbstractTest test) {
        this.test = test;
    }
}
