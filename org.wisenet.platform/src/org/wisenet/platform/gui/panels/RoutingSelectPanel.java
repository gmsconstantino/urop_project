package org.wisenet.platform.gui.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import org.wisenet.platform.PlatformView;
import org.wisenet.platform.common.conf.ConfigurationUtils;
import org.wisenet.platform.common.conf.ClassConfigReader.ClassDefinitions;
import org.wisenet.platform.common.ui.PlatformDialog;
import org.wisenet.platform.common.ui.PlatformFrame;
import org.wisenet.platform.common.ui.PlatformPanel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class RoutingSelectPanel extends PlatformPanel {
	
	private static final long serialVersionUID = 1L;
	HashSet<ClassDefinitions> conf;
	
	private JComboBox comboBox;
	private Object selected;
	private JPanel panel;
	
	public RoutingSelectPanel(HashSet<ClassDefinitions> _conf){
		conf = _conf;
		initComponents();
		
		setLayout(new BorderLayout(0, 0));
		add(panel);
		comboBox = new JComboBox();
		comboBox.setBounds(10, 45, 354, 34);
		panel.add(comboBox);
		setPreferredSize(new java.awt.Dimension(400, 90));
		
		addObjectsComboBax();
		addListener();
	}
	
	private void addListener() {
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				JComboBox cb = (JComboBox) ev.getSource();
				selected = cb.getSelectedItem();
			}
		});
	}

	private void addObjectsComboBax() {
		for(ClassDefinitions c : conf){
			comboBox.addItem(c);
		}
		if(conf.size()>0)
			comboBox.setSelectedIndex(0);
	}

	private void initComponents() {
		panel = new JPanel();
		panel.setLayout(null);
		
		JLabel lblSelectNewRouting = new JLabel("Select new Routing Layer:");
		lblSelectNewRouting.setBounds(10, 11, 177, 34);
		panel.add(lblSelectNewRouting);
	}

	@Override
	public boolean onCancel() {
		return true;
	}

	@Override
	public boolean onOK() {
		selected = comboBox.getSelectedItem();
		return true;
	}

	@Override
	public boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean isDataValid() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void beforeClose() {
		// TODO Auto-generated method stub
		
	}

	public boolean routingHasChanged() {
		return (selected == null) ? false : true;
	}

	public Object getSelected() {
		return selected;
	}
	
	 public static void main(String[] args) {
	        PlatformView.applyLookAndFeel();
	        
	        HashSet<ClassDefinitions> conf = ConfigurationUtils.loadConfigurationClasses(ConfigurationUtils.CONF_ROUTING_CLASSES_PROPERTIES);
	    	
	       	RoutingSelectPanel rsp = new RoutingSelectPanel(conf);     	
	    	PlatformDialog.display(rsp, "Select Gateway Routing", PlatformFrame.OKCANCEL_MODE);
    }
}
