package org.wisenet.platform.gui.panels;

import org.wisenet.platform.common.ui.PlatformPanel;
import org.wisenet.simulator.components.evaluation.tests.AbstractTest;
import org.wisenet.simulator.components.evaluation.tests.AdHocTest;
import org.wisenet.simulator.components.evaluation.tests.RouteBalancementTest;
import org.wisenet.simulator.components.evaluation.tests.TestInputParameters;
import org.wisenet.simulator.components.simulation.Simulation;

import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFormattedTextField;
import javax.swing.SwingConstants;
import javax.swing.JPanel;
import javax.swing.Box;
import java.awt.Component;

public class RoutingBalanceTestParametersPanel extends PlatformPanel  {
	
	
	private final int NUMBER_OF_MESSAGES = 1;
	private final int INTERVAL_BETWEEN_MESSAGES = 10;
	private final int NUMBER_OF_RETRANSMISSIONS = 1;
	

    private JFormattedTextField percentageSenderNodes;
    private JFormattedTextField numberOfMessages;
    private JFormattedTextField numberOfRetransmissions ;
    protected boolean addToSim;
    protected Simulation simulation = null;
    AbstractTest test;
    TestInputParameters inputParameters = new TestInputParameters();
	

	public RoutingBalanceTestParametersPanel() {
		initComponents();
	}
	
		
	private void initComponents() {	
		this.setPreferredSize(new Dimension(300, 200));
		setLayout(null);
		JLabel lblOfSender = new JLabel("% of sender nodes");
		lblOfSender.setBounds(6, 35, 114, 16);
		add(lblOfSender);
		
		JLabel lblNumberOfMessages = new JLabel("Number of Messages");
		lblNumberOfMessages.setBounds(6, 63, 132, 16);
		add(lblNumberOfMessages);
		
		JLabel lblNumberOfRetransmissions = new JLabel("Number of retransmissions");
		lblNumberOfRetransmissions.setBounds(6, 91, 182, 16);
		add(lblNumberOfRetransmissions);
		
		percentageSenderNodes = new JFormattedTextField();
		percentageSenderNodes.setBounds(200, 21, 43, 28);
		add(percentageSenderNodes);
		percentageSenderNodes.setHorizontalAlignment(SwingConstants.RIGHT);
		percentageSenderNodes.setText("0");
		
		numberOfMessages = new JFormattedTextField();
		numberOfMessages.setBounds(200, 54, 43, 28);
		add(numberOfMessages);
		numberOfMessages.setHorizontalAlignment(SwingConstants.RIGHT);
		numberOfMessages.setText(Integer.toString(NUMBER_OF_MESSAGES));
		
		numberOfRetransmissions = new JFormattedTextField();
		numberOfRetransmissions.setBounds(200, 85, 43, 28);
		add(numberOfRetransmissions);
		numberOfRetransmissions.setText(Integer.toString(NUMBER_OF_RETRANSMISSIONS));
		numberOfRetransmissions.setHorizontalAlignment(SwingConstants.RIGHT);
	}
	
	
	 public void setCurrentTest(AbstractTest currentTest) {
	        test = currentTest;
	 }
	 
	 public void setSimulation(Simulation simulation) {
	        this.simulation = simulation;
	 }

	@Override
	public boolean onCancel() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onOK() {
		if (isDataValid()) {
            updateTest();
            result = test;
            return true;
        } else {
            return false;
        }
	}

	@Override
	public boolean onApply() {
		return true;
	}

	@Override
	protected boolean isDataValid() {
		if(INT(percentageSenderNodes) > 0)
			return true;
		else
			return false;
	}

	@Override
	public void beforeClose() {
		// TODO Auto-generated method stub
		
	}

	
	protected void updateTest() {
		int stableNodes = simulation.getNumberOfStableNodes();
		int percentage = (INT(percentageSenderNodes));
		int senderNodesNumber =  percentage * stableNodes / 100 ;
		
        inputParameters.setNumberOfSenderNodes(senderNodesNumber);
        inputParameters.setNumberOfReceiverNodes(1);
        inputParameters.setNumberOfAttackNodes(0);
        inputParameters.setPercentOfSenderNodes(false);
        inputParameters.setPercentOfReceiverNodes(false);
        inputParameters.setPercentOfAttackNodes(false);
        inputParameters.setOnlyConsiderToSenderStableNodes(false);
        inputParameters.setOnlyConsiderToAttackStableNodes(false);
        inputParameters.setOnlyConsiderToReceiverSinkNodes(false);
        //
        inputParameters.setNumberOfMessagesPerNode(INT(numberOfMessages));
        inputParameters.setIntervalBetweenMessagesSent(INTERVAL_BETWEEN_MESSAGES);
        inputParameters.setNumberOfRetransmissions(INT(numberOfRetransmissions));

        
        test = new RouteBalancementTest(inputParameters);
        test.setName("Topolgy Balance Test");
        test.setDescription("Test routing balancement");
        test.setDebugEnabled(false);
        test.setBatchMode(false);
        test.setTimesToRun(1);
    }
	
	private int INT(JTextField text) throws NumberFormatException {
        return Integer.parseInt(text.getText());
    }
}
