package org.wisenet.protocols.MAC.tinysec;


/**
 * O TinySec permite as seguintes parametriza?�es:
 * 
 * @author Tiago Araujo
 *
 */
public class ConstantsTinySec {
	
	//Modo de utiliza?�o do TinySec
	public static int TINYSEC_AUTH_ONLY = 1;
	public static int TINYSEC_ENCRYPT_AND_AUTH = 2;
	public static int TINYSEC_DISABLED = 3;
	
	//Cifra
	public static String CIPHERSKIPJACK = "SKIPJACK";
	public static String CIPHERRC5 = "RC5";
	public static String CIPHERMODE = "CBC";
	public static String PADDING = "PKCS7Padding";
	
	//MAC
	public static String MACALGORITHM = "SKIPJACKMAC";
	
	//HMAC  
	public static String MSGDIGESTALGORITHM = "SHA1";
	
	//80 bits para o keysize
	public static int KEYSIZE = 80;
	
	//IVector size
	public static int IVECTORSIZE = 8;
	
	//Provedor utilizado
	public static String PROVIDER = "BC";		
}