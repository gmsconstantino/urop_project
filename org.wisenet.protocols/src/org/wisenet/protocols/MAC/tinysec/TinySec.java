package org.wisenet.protocols.MAC.tinysec;


import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;

import org.wisenet.protocols.common.ByteArrayDataInputStream;
import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.protocols.common.Utils;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.node.Node;


/**
 * *******************  Camada de seguran?a TinySec ****************
 * 
 * - Macs:
 * 		 - senders e receivers partilham uma chave secreta
 * 		 - Mac faz parte do pacote
 * 
 * - IVector:
 * 		- 8 Bytes
 * 		- Composto por: dst(2)||AM(1)||l(1)||src(2)||ctr(2)  = 8 Bytes 
 * 		- ctr � um counter que incrementa por cada mensagem enviada
 * 		- enviado em claro no pacote
 * 
 *  - Autentica?�o:
 *  	- TinySec-AE -> Cifra autenticada (cifra do payload + MAC(aplicado � cifra e header do pacote))
 *  	- TInySec-Auth -> Cifra nao autenticada (MAC aplicado a todo o pacote mas payload n�o est� cifrado)
 *  	- CBC-MAC (4 bytes)
 * 
 * 
 * - Cifra:		
 * 		- Algoritmo de cifra por defeito utilizado � o Skipjack
 * 
 * - TinySec Key = Chave Skipjack para cifra + Chave Skipjack para macs
 * 
 * - A distribui?�o de chaves: considera-se que os n�s s�o carregados com uma chave antes do deployment
 * 
 * ***************************************************************************
 * 
 * @author Tiago Araujo
 *
 */
public class TinySec {  //Classe estatica, muda conforme as configura�oes q lhe sao dadas
	
	/**
	 * Modo de tinysec definido
	 */
	private int tinysecmode;
	
	/**
	 * Algoritmo de cifra 
	 */
	private static String cipheralgorithm;
	private static String macalgorithm;
	
	private static Key cipherKey = null;
	private static Key macKey = null;
	
	protected Node node;
	
	
	public TinySec(Node n) {
		this.node = n;
		defaultConfig();
	}
	
	
	// -------------- M�todos de Base -------------------
	
		
	
	
	
	/**
	 * Uma chave TinySec � composta pela chave para cifrar
	 * e a chave para autenticar. Cria uma chave TinySec
	 * @return
	 */
	public static void createTinySecKey() {
		if(macKey==null && macKey == null){
			cipherKey = createKey(cipheralgorithm, ConstantsTinySec.KEYSIZE);
			macKey = createKey(cipheralgorithm, ConstantsTinySec.KEYSIZE);		
		}
	}
	
	/**
	 * Obter a chave para autentica?�o
	 * @return
	 */
	public Key getMacKey() {
		return macKey;
	}
	
	/**
	 * Obter a chave para cifra (simetrica)
	 * @return
	 */
	public Key getCipherKey(){
		return cipherKey;
	}
	
	
	/**
	 * Obter um vector de 8 bytes
	 * Simula a cria?�o de um vector do tipo dst(2)||AM(1)||l(1)||src(2)||ctr(2)
	 * @return
	 */
	public byte[] getIVector(int size){
		SecureRandom random = new SecureRandom();
		long seed = System.nanoTime() ^Runtime.getRuntime().freeMemory();
		random.setSeed(seed);
		byte ivector[] = new byte[ConstantsTinySec.IVECTORSIZE];
		random.nextBytes(ivector);
		return ivector;
	}
	
	
	/**
	 * Gerar um pacote seguro de acordo com a especifica?�o TinySec
	 * @param bm
	 * @param type
	 * @return
	 */
	public TinySecPacket securePacket(Message bm){
		if(tinysecmode == ConstantsTinySec.TINYSEC_ENCRYPT_AND_AUTH)
			return createPacketEncrAuth(bm);
		else if(tinysecmode == ConstantsTinySec.TINYSEC_AUTH_ONLY)
			return createPacketAuth(bm);
		else if(tinysecmode == ConstantsTinySec.TINYSEC_DISABLED)
			return null;
		return null;
	}
	
	/**
	 * Cria pacote TinySec com cifra e autentica?�o
	 * @param bm
	 * @return
	 */
	private TinySecPacket createPacketEncrAuth(Message bm){
		try{
			//Cria IVector
			ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
			bados.writeShort((Short) bm.getDestinationId());
			bados.writeByte(bm.getType());
			bados.writeByte(bm.getPayload().length);
			bados.writeShort((Short) bm.getSourceId());
			bados.writeShort(-1); //Simula o CTR
			byte[] ivector = bados.toByteArray(); 
			bados.close();
			
			//
			String[] cipherprop = {cipheralgorithm,ConstantsTinySec.CIPHERMODE,ConstantsTinySec.PADDING};
			byte[] cipherdata = encrypt(bm.getPayload(), cipherKey, cipherprop, ConstantsTinySec.PROVIDER, ivector);
			byte[] aux = Utils.appendByteArrays(ivector, cipherdata);
			byte[] mac = createMac(aux, macKey, macalgorithm, ConstantsTinySec.PROVIDER);
			TinySecPacket tsp = new TinySecPacket(mac,cipherdata,ivector);
			
			//Auxiliar
			tsp.setUniqueId(bm.getUniqueId());
			tsp.setForwardBy(bm.getForwardBy());
			return tsp;
			
		}catch(Exception e){
			//System.err.println("ERROR - Problema ao cifrar pacote");
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * Cria pacote TinySec com apenas autentica?�o
	 * @param bm
	 * @return
	 */
	private TinySecPacket createPacketAuth(Message bm){
		try {
			ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
			bados.writeShort((Short) bm.getDestinationId());
			bados.writeByte(bm.getType());
			bados.writeByte(bm.getPayload().length);
			byte[] ivector = bados.toByteArray(); 
			bados.close();
			
			
			byte[] aux = Utils.appendByteArrays(ivector, bm.getPayload());
			byte[] mac = createMac(aux, macKey, macalgorithm, ConstantsTinySec.PROVIDER);
			TinySecPacket tsp = new TinySecPacket(mac,bm.getPayload(),ivector);
			
			//Auxiliar
			tsp.setUniqueId(bm.getUniqueId());
			tsp.setForwardBy(bm.getForwardBy());
			return tsp;
			
		}catch(Exception e){
			System.err.println("ERROR");	
		}
		return null;
	}
	
	
	/**
	 * Verificar as propriedades de seguran?a do pacote recebido
	 * Transforma o pacote TinySec num pacote sem seguran?a,
	 * pois j� ocorreu a verifica?�o
	 * 
	 * @param packet
	 * @return Retorna o os dados do pacote decifrados
	 */
	public Message verifySecurePacket(TinySecPacket packet){
		if(tinysecmode == ConstantsTinySec.TINYSEC_ENCRYPT_AND_AUTH)
			return verifySecurePacketMode1(packet);
		else if(tinysecmode == ConstantsTinySec.TINYSEC_AUTH_ONLY)
			return verifySecurePacketMode2(packet);
		//else if(tinysecmode == ConstantsTinySec.TINYSEC_DISABLED)
			//return null;
		return null;
	}
	
	/**
	 * Verifica?�o no modo Cifra + Autentica?�o e transforma pacote num pacote sem seguran?a
	 * @param packet
	 * @return
	 */
	private Message verifySecurePacketMode1(TinySecPacket packet){
		byte[] aux = Utils.appendByteArrays(packet.getHeader(),packet.getPayloadOrCiphered());
		boolean vermac = verifyMAC(packet.getMac(), aux, getMacKey(), macalgorithm, ConstantsTinySec.PROVIDER);
		if(vermac){
			try {
				String[] cipherprop = {cipheralgorithm,ConstantsTinySec.CIPHERMODE,ConstantsTinySec.PADDING};
				byte[] payload = decrypt(packet.getPayloadOrCiphered(), getCipherKey(), cipherprop, ConstantsTinySec.PROVIDER, packet.getHeader());
				ByteArrayDataInputStream badis = new ByteArrayDataInputStream(packet.getHeader());
				short dest = badis.readShort();
				byte type = badis.readByte();
				badis.readByte(); //length
				short source = badis.readShort();
				badis.readShort(); //ctr
				Message bm = new Message(payload);
				bm.setDestinationId(dest);
				bm.setType(type);
				bm.setSourceId(source);
				bm.packetSecurity(true);
				
				//Auxiliar
				bm.setForwardBy(packet.getForwardBy());
				bm.setUniqueId(packet.getMessageNumber());
				
				return bm;
			}catch(Exception e){
				return null;
			}
		}
		return null;
	}
	
	/**
	 * Verifica?�o no modo apenas de Autentica?�o e transforma pacote num pacote sem seguran?a
	 * @param packet
	 * @return
	 */
	private Message verifySecurePacketMode2(TinySecPacket packet){
		byte[] aux = Utils.appendByteArrays(packet.getHeader(),packet.getPayloadOrCiphered());
		if(verifyMAC(packet.getMac(), aux, getMacKey(), macalgorithm, ConstantsTinySec.PROVIDER)){
			try {
				ByteArrayDataInputStream bados = new ByteArrayDataInputStream(packet.getHeader());
				short dest = bados.readShort();
				byte type = bados.readByte();
				bados.readByte(); //length
				Message bm = new Message(packet.getPayloadOrCiphered());
				bm.setDestinationId(dest);
				bm.setType(type);
				bm.packetSecurity(true);
				
				//Auxiliar
				bm.setForwardBy(packet.getForwardBy());
				bm.setUniqueId(packet.getMessageNumber());
				return bm;
			}catch(Exception e){
				return null;
			}
		}
		return null;
	}
	
	
	
	
	//Setters and getters
	
	public void setMode(int mode){
		tinysecmode = mode;
	}
	
	public void setCipherAlgorithm(String cipheralg){
		cipheralgorithm = cipheralg;
	}
	
	public String getCipherAlgorithm() {
		return cipheralgorithm;
	}
	
	public void setMacAlgorithm(String macalg){
		macalgorithm = macalg;
	}
	
	/**
	 * Configura?�o por defeito
	 * @return
	 */
	public String getMacAlgorithm(){
		return macalgorithm;
	}
	
	
	public void defaultConfig(){
		tinysecmode = ConstantsTinySec.TINYSEC_ENCRYPT_AND_AUTH;
		cipheralgorithm = ConstantsTinySec.CIPHERSKIPJACK;
		macalgorithm = ConstantsTinySec.MACALGORITHM;
	}
	
	
	
	
	// ---------  Outros m�todos de suporte -----------
	
	
    /**
     * Cifrar mensagem utilizando
     * a chave em argumento, o possivel vector
     * de inicializacao e a configuracao da cifra 
     */
    public byte[] encrypt (byte[] plainText, Key key, String[] transf, String provider, byte[] ivector) throws Exception {
    	String transformation = transf[0] + "/" + transf[1] + "/" + transf[2];
		Cipher cipher = Cipher.getInstance(transformation,provider);
    	if(ivector !=null)
    		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(ivector));
    	else
    		cipher.init(Cipher.ENCRYPT_MODE, key);
    	byte[] res = cipher.doFinal(plainText);
    	node.getBateryEnergy().consumeEncryption(plainText.length);
    	return res;
    }
	
	
    /**
     * Decifrar mensagem utilizando
     * a chave em argumento, o possivel vector
     * de inicializacao e a configuracao da cifra 
     */
    public byte[] decrypt (byte[] cipherText, Key key, String[] transf, String provider, byte[] ivector) throws Exception {
    	String transformation = transf[0] + "/" + transf[1] + "/" + transf[2]; 
		Cipher cipher = Cipher.getInstance(transformation,provider);
		if (ivector != null)
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(ivector));
		else
			cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] res = cipher.doFinal(cipherText);
		node.getBateryEnergy().consumeDecryption(cipherText.length);
		return res; 
    }
    
    
    
	/**
     * Cria o MAC de uma mensagem passada em parametro
     * Utiliza uma determinada chave para o MAC 
     */
    public byte[] createMac (byte[] msg, Key macKey, String macAlgorithm, String provider)  {
    	try {
    		Mac mac = Mac.getInstance(macAlgorithm,provider);
    		mac.init(macKey);
    		byte[] res = mac.doFinal(msg);
    		node.getBateryEnergy().consumeMAC(msg.length);
    		return res;
    		
    	} catch(Exception e) {
    		System.err.println(e.getMessage());
    	}
    	return null;
    }
    
    
    /**
     * Verifica se os MAC's sao iguais
     */
    public boolean verifyMAC(byte[] oldmac, byte[] msg, Key macKey, String macAlgorithm, String provider) {
    	byte[] newhmac = createMac(msg,macKey,macAlgorithm, provider);
    	node.getBateryEnergy().consumeMACVerification(msg.length);
    	if(MessageDigest.isEqual(oldmac, newhmac))
    		return true;
    	return false;
    }
    
    
    
    /**
     * Cria uma chave
     */
    public static Key createKey(String alg, int sizeKey){
		try {
			KeyGenerator generator = KeyGenerator.getInstance(alg);
			generator.init(sizeKey);
			return generator.generateKey();
		} catch (NoSuchAlgorithmException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}
    
    public String[] getCipherProperties() {
    	String[] str = {cipheralgorithm,ConstantsTinySec.CIPHERMODE,ConstantsTinySec.PADDING};
    	return str;
    }
}

