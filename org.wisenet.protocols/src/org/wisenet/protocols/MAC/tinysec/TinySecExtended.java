package org.wisenet.protocols.MAC.tinysec;

import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import org.wisenet.protocols.common.Utils;
import org.wisenet.simulator.core.node.Node;



/**
 * Esta contem metodos que n�o est�o incluidos na camada de base TinySec
 * Isto porque alguns protocolos exigem outros m�todos de seguran?a
 * @author Tiago Araujo
 *
 */
public class TinySecExtended extends TinySec {
	
	
	public TinySecExtended(Node n) {
		super(n);
	}
	
	/**
	 * Cria um par de chaves assim�tricas
	 * @param alg
	 * @param sizeKey
	 * @param provider
	 * @return
	 */
    public KeyPair createKeyPair(String alg, int sizeKey, String provider) {
		try {
			KeyPairGenerator generator = KeyPairGenerator.getInstance(alg,provider);
	    	generator.initialize(sizeKey);
	    	return generator.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			System.err.println(e.getMessage());
		} catch (NoSuchProviderException e) {
			System.err.println(e.getMessage());
		}
		return null;
    }
	
    
    
    
    /**
     * Criar assinatura, atrav�s de message digests (hash)
     * @return
     */
    public byte[] createSignature(byte[] msg, String signAlgorithm, String provider){
		try {
	    	MessageDigest md = MessageDigest.getInstance(signAlgorithm,provider);
			node.getBateryEnergy().consumeSignature(msg.length);
	    	return md.digest(msg);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		return null;
    }
    
    
    
    /**
     * MessageDigest passado em parametro
     * Criar assinatura, atrav�s de message digests
     * @return
     */
    public byte[] createSignature(byte[] msg, MessageDigest md){
		node.getBateryEnergy().consumeSignature(msg.length);
    	return md.digest(msg);
    }
    
    
    /**
     * Verificar assinatura, atrav�s de message digests
     * @param oldsign
     * @param msg
     * @param signAlgorithm
     * @param provider
     * @return
     */
    public boolean verifySignature(byte[] oldsign, byte[] msg, String signAlgorithm, String provider){
    	byte[] newsign = createSignature(msg,signAlgorithm, provider);
    	node.getBateryEnergy().consumeSignatureVerify(msg.length);
    	if(MessageDigest.isEqual(oldsign, newsign))
    		return true;
    	return false;
    }
    
   
   
    /**
     * Verificar assinatura, atrav�s de message digests.
     * Passado em parametro o MessageDigest
     * @param oldsign
     * @param msg
     * @param md
     * @return
     */
    public boolean verifySignature(byte[] oldsign, byte[] msg, MessageDigest md){
    	byte[] newsign = createSignature(msg,md);
    	node.getBateryEnergy().consumeSignatureVerify(msg.length);
    	if(MessageDigest.isEqual(oldsign, newsign))
    		return true;
    	return false;
    }
    
	
    /**
     * Gera uma one-way hash sequence
     * @param md
     * @param n1
     * @param k
     * @return
     * @throws IOException
     */
    public int[] generateOneWaySeq(MessageDigest md, int n1, int k) throws IOException{
    	
    	int[] array = new int[k];
    	int nk=n1;
    	for(int i=0; i<k-1; i++){
	    	array[i] = nk;
			byte[] aux = Utils.intToByteArray(nk);
			nk = Utils.byteArrayToInt(createSignature(aux, md));
		}
    	array[k-1] = nk;
    	return array;
    }
    
   
    
    // Contabilizar energia?
    /**
     * Verifica uma one-way hash sequence
     */
    public boolean verifyOWS(MessageDigest md, int owsi, int owsfresh, int iterations) throws IOException{
    	int ni=owsi;
    	for(int i=0; i<iterations; i++){
			byte[] aux = Utils.intToByteArray(ni);
			ni = Utils.byteArrayToInt(md.digest(aux));
			if(ni == owsfresh)
				return true;
		}
    	return ni==owsi;
    }	
}