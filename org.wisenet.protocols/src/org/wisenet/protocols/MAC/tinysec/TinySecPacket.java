package org.wisenet.protocols.MAC.tinysec;

import org.wisenet.simulator.core.Message;


/**
 * 
 * @author Tiago Ara�jo
 *
 * Pacote TinySec
 */
public class TinySecPacket extends Message{
	
	private byte[] mac;
	private byte[] payload;
	private byte[] header; //Pode conter o IV de 8 bytes ou sem IV mas tem ainda 4 bytes
	//private int type;
	
	public TinySecPacket(byte[] mac, byte[] payload, byte[] header){
		super();
		this.mac = mac;
		this.payload = payload;
		this.header = header;
	}
	
	public byte[] getPayloadOrCiphered(){
		return payload;
	}
	
	public byte[] getMac(){
		return mac;
	}
	
	public byte[] getHeader(){
		return header;
	}
	
	
	/**
	 * Tamanho da mensagem
	 */
	@Override
	public int size(){
		return header.length + payload.length + mac.length;
	}
}