package org.wisenet.protocols.minsens;

import org.wisenet.protocols.gateway.Gateway;
import org.wisenet.protocols.gateway.GatewayServer;
import org.wisenet.simulator.core.Message;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class MINSENSGateway extends MINSENSRoutingLayer implements Gateway{

	GatewayServer s; 
	
	public MINSENSGateway() {
		super();
		s = new GatewayServer(this);
	}
	
	@Override
	public void onReceiveMessageGateway(byte[] message) {
		Message msg = new Message(message);
		
		onSendMessage(msg, getNode().getApplication());
	}

}
