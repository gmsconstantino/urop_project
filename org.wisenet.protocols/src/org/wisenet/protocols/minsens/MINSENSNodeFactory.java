package org.wisenet.protocols.minsens;

import org.wisenet.protocols.minsens.EvaluateMINSENSApplication;
import org.wisenet.protocols.minsens.INSENSNode;
import org.wisenet.simulator.core.node.factories.AbstractNodeFactory;
import org.wisenet.simulator.core.node.layers.mac.Mica2MACLayer;

public class MINSENSNodeFactory extends AbstractNodeFactory  {

	@Override
	public void setup() {
		setApplicationClass(EvaluateMINSENSApplication.class);
        setRoutingLayerClass(MINSENSRoutingLayer.class);
        setNodeClass(INSENSNode.class);
        setMacLayer(Mica2MACLayer.class);
        setSetup(true);
		
	}

}
