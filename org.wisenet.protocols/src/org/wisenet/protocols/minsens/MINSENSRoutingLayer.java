package org.wisenet.protocols.minsens;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.protocols.common.routingattacks.BlackholeRoutingAttack;
import org.wisenet.protocols.common.routingattacks.HelloFloodingRountingAttack;
import org.wisenet.protocols.minsens.basestation.BaseStationController;
import org.wisenet.protocols.minsens.basestation.MInsensForwardingTable;
import org.wisenet.protocols.minsens.basestation.InsensForwardingTable;
import org.wisenet.protocols.minsens.messages.INSENSMessage;
import org.wisenet.protocols.minsens.messages.MINSENSMessagePayloadFactory;
import org.wisenet.protocols.minsens.messages.data.DATAPayload;
import org.wisenet.protocols.minsens.messages.data.FDBKPayload;
import org.wisenet.protocols.minsens.messages.data.RREQPayload;
import org.wisenet.protocols.minsens.messages.data.RUPDAckPayload;
import org.wisenet.protocols.minsens.messages.data.RUPDPayload;
import org.wisenet.protocols.minsens.utils.NeighborInfo;
import org.wisenet.protocols.minsens.utils.NetworkKeyStore;
import org.wisenet.protocols.minsens.utils.OneWaySequenceNumbersChain;
import org.wisenet.protocols.myflooding.messages.MyFloodMessage;
import org.wisenet.simulator.components.evaluation.tests.AbstractTest;
import org.wisenet.simulator.components.evaluation.tests.RouteBalancementTest;
import org.wisenet.simulator.components.instruments.IInstrumentHandler;
import org.wisenet.simulator.core.Application;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.events.DelayedMessageEvent;
import org.wisenet.simulator.core.events.Timer;
import org.wisenet.simulator.core.node.Node;
import org.wisenet.simulator.core.node.layers.routing.RoutingLayer;
import org.wisenet.simulator.core.node.layers.routing.attacks.AttacksEntry;
import org.wisenet.simulator.utilities.CryptoFunctions;

public class MINSENSRoutingLayer extends RoutingLayer implements IInstrumentHandler  {

  
	/**
	 * Routing scaling configuration
	 */
	public static final int K = 3;
	public static final int K_RANDOM_ROUTES = 0;
	public static final int K_BALANCED_ROUTES = 1;
	public static final int ONE_RANDOM_ROUTE = 2;
	public static final int ONE_ROUND_ROBIN = 3;
	public static final int ALL = 4;
	
	public static final int FORWARDING_TYPE = 4;
	
	
	/**
	 * Route disjointness configuration
	 */
	public static final boolean USE_CONSENSUS_ON_ROUTES = true;
	
	
	/**
	 * Data consensus data structures and configuration
	 */
	public static final boolean USE_LOCAL_BYZANTINE_AGREEMENT = false;
	public static final boolean USE_GLOBAL_BYZANTINE_AGREEMENT = true;
	
	/**
	 * the number of equal replicas necessary to accept the value locally
	 * 
	 */
	public static final int LOCAL_BYZANTINE_AGREEMENT_COPIES_NUMBER = 6; 
	/**
	 *  the number of global votes por message value necessary to agree on the value
	 */
	public static final int GLOBAL_BYZANTINE_AGREEMENT_VOTES_NUMBER = 6;
	
	private Hashtable<Object, List<Message>> messagesTable = new Hashtable<Object, List<Message>>(); //key is messageId, value is a linkedList of all message replicas received
//	private Hashtable<Object, Object> votesTable = new Hashtable<Object, Object>(); // key is messageId, value is the count of votes for that message
	
 
	
	/**
	 * route setup phases
	 */
   public static final String PHASE_SETUP = "SETUP";
   public static final String PHASE_ROUTE_REQUEST = "ROUTE_REQUEST";
   public static final String PHASE_ROUTE_FEEDBACK = "ROUTE_FEEDBACK";
   public static final String PHASE_FORWARD_DATA = "FORWARD_DATA";
   public static final String PHASE_ROUTE_UPDATE = "ROUTE_UPDATE";
   /**
    * Node info attributes
    */
   private Set<Short> baseStations;//each entry is a BS id
   private Hashtable<Short,byte[]> privateKey = new Hashtable<Short,byte[]>(); //one for each BS 
   private Hashtable<Short,Long> OWS = new Hashtable<Short,Long>(); 
   private Hashtable<Short,Long> roundOWS = new Hashtable<Short,Long>();
   private Hashtable<Short,Integer> roundNumber = new Hashtable<Short,Integer>();
   private Hashtable<Short,NeighborInfo> neighborInfo = new Hashtable<Short,NeighborInfo>();
   private Hashtable<Short,byte[]> myRoundMAC = new Hashtable<Short,byte[]>();
   /**
    * Timers for actions control
    */
   private Hashtable<Short,Timer> feedbackMessageStartTimer = new Hashtable<Short,Timer>();
   private Timer queueMessageDispatchTimer;
   private Timer startForwardTablesCalculesTimer;
   private Timer forwardingTablesDispatchTimer;
   
   
   /**
    * Control structures
    */
//   private BaseStationController baseStationController = null;
   private LinkedList<Message> messagesQueue = new LinkedList<Message>();
   private boolean sendingMessage;
   
	/**
     * Current phase
     */
    protected Hashtable<Short,String> currentPhase = new Hashtable<Short,String>();
	
    /**
     * Counters for control protocol
     */
    private short feedbackMessagesReceived = 0;
    private short lastFeedbackMessagesReceivedCheck = 0;
    
    private byte feedbackMessageRetries;
    
    private Hashtable<Integer,List<Short>> tableOfNodesByHops;
    private boolean reliableMode = true;
    private Hashtable<Integer,byte[]> forwardingTablesRepository = null;
    
    private List<Integer> orderedByHops;
    private Iterator<Integer> orderedByHopsIterator;
    
    private List<Short> orderedByHopsNodes;
    private Iterator<Short> orderedByHopsNodesIterator;
    
    private int replyToRUPDCounter;
    private Iterator<byte[]> forwardingTablesRepositoryIter;
    private boolean first = true;
    private int retry = 0;
    
    
    
    
    public BaseStationController getBaseStationController(){
    	return (BaseStationController)baseStationController;
    }
    
    @Override
	protected void onStartUp() {
		setCurrentPhase(PHASE_SETUP);
		setBaseStations(getNode().getSimulator().getSinkNodesIds());
		
		if (getNode().isSinkNode()) {
            baseStationController = new BaseStationController(this);
            getBaseStationController().setUSE_CONSENSUS_ON_ROUTES(USE_CONSENSUS_ON_ROUTES);
            roundNumber.put(getNode().getId(), 0);
            roundOWS.put(getNode().getId(), new Long(0));
            createNodeKeys(getNode().getId());
			initiateSequenceNumber(getNode().getId());
			createTimers(getNode().getId());
        }else{
        	for(Short id : baseStations){
        		roundNumber.put(id, 0);
        		roundOWS.put(id, new Long(0));
        		createNodeKeys(id);
    			initiateSequenceNumber(id);
    			createTimers(id);
    		}	
        }
		
		
		startProtocol();
		
	}
    
    @Override
	public void newRound() {
		if (getNode().isSinkNode()) {
            newRoutingDiscover();
        }
		
	}
    
	@Override
	protected String getRoutingTable() {
		return getForwardingTable().toString();
	}
	

	
	/**
     * Receive a message from the MAC Layer
     * @param message
     */
	@Override
	protected void onReceiveMessage(Object message) {
		if (message instanceof INSENSMessage) {
            try {
                INSENSMessage m = (INSENSMessage) message;
                byte type = MINSENSFunctions.getMessageType(m);
                if (MINSENSFunctions.verifyMAC(m.getPayload(), type)) {
                    switch (type) {
                        case INSENSConstants.MSG_ROUTE_REQUEST:
                            setCurrentPhase(PHASE_ROUTE_REQUEST);
                            getController().addMessageReceivedCounter(INSENSConstants.MSG_ROUTE_REQUEST);
                            processRREQMessage(m);
                            break;
                        case INSENSConstants.MSG_FEEDBACK:
                            setCurrentPhase(PHASE_ROUTE_FEEDBACK);
                            getController().addMessageReceivedCounter(INSENSConstants.MSG_FEEDBACK);
                            processFDBKMessage(m);
                            break;
                        case INSENSConstants.MSG_ROUTE_UPDATE:
                            setCurrentPhase(PHASE_ROUTE_UPDATE);
                            getController().addMessageReceivedCounter(INSENSConstants.MSG_ROUTE_UPDATE);
                            processRUPDMessage(m);
                            break;
                        case INSENSConstants.MSG_ROUTE_UPDATE_ACK:
                            getController().addMessageReceivedCounter(INSENSConstants.MSG_ROUTE_UPDATE_ACK);
                            processRUPDACKMessage(m);
                            break;
                        case INSENSConstants.MSG_DATA:
                            setCurrentPhase(PHASE_FORWARD_DATA);
                            getController().addMessageReceivedCounter(INSENSConstants.MSG_DATA);
                            processDATAMessage(m);
                            break;
                    }
                }
            } catch (INSENSException ex) {
                log(ex);
            }
        }
		
	}

	@Override
	protected boolean onSendMessage(Object message, Application app) {
		if (isStable()) {
            return sendDATAMessage((Message) message);
        } else {
        	System.out.println(getNode().getId()+" routing not stable, message not sent");
            return false;
        }
	}
	
    

	@Override
	protected void onRouteMessage(Object message) {
		 try {
	            INSENSMessage m = (INSENSMessage) message;
	            byte type = MINSENSFunctions.getMessageType(m);
	            byte[] new_payload = null;
	            short source = -1;
	            short destination = -1;
	            switch (type) {
	                case INSENSConstants.MSG_ROUTE_UPDATE:
	                    RUPDPayload payload = new RUPDPayload(m.getPayload());
	                    if (getForwardingTable().haveRoute(payload.destination, payload.source, payload.immediate, payload.routeIds)) {
	                        new_payload = MINSENSMessagePayloadFactory.updateRUPDPayload(payload.source, payload.destination, getNode().getId(), payload.routeIds, payload.ows, payload.forwardingTable, payload.mac, this.getNode());
	                        source = payload.source;
	                        destination = payload.destination;
	                    }
	                    break;
	                case INSENSConstants.MSG_ROUTE_UPDATE_ACK:
	                    RUPDAckPayload payloadAck = new RUPDAckPayload(m.getPayload());
	                    if (getForwardingTable().haveRoute(payloadAck.destination, payloadAck.source, payloadAck.immediate, payloadAck.routeIds)) {
	                        new_payload = MINSENSMessagePayloadFactory.updateRUPDAckPayload(payloadAck.source, payloadAck.destination, getNode().getId(), payloadAck.routeIds, payloadAck.ftUID, payloadAck.ows, payloadAck.mac, this.getNode());
	                        source = payloadAck.source;
	                        destination = payloadAck.destination;
	                    }
	                    break;
	                case INSENSConstants.MSG_DATA:
	                    DATAPayload payloadData = new DATAPayload(m.getPayload());
	                    lastMessageSenderId = payloadData.immediate;
	                    if (getForwardingTable().haveRoute(payloadData.destination, payloadData.source, payloadData.immediate, payloadData.routeIds)) {
	                        new_payload = MINSENSMessagePayloadFactory.updateDATAPayload(payloadData.source, payloadData.destination, getNode().getId(), payloadData.routeIds, payloadData.data, payloadData.mac, this.getNode());
	                        source = payloadData.source;
	                        destination = payloadData.destination;
	                    }
	                    break;
	                default:
	                    break;
	            }
	            if (new_payload != null) {
                    m.setPayload(new_payload);
                    getController().addMessageSentCounter(type);
                    send((Message) m.clone());
                    log("Forward Message from " + source + " to " + destination);
                }
	        } catch (CloneNotSupportedException ex) {
	            log(ex);
	        } catch (INSENSException ex) {
	            log(ex);
	        }
		
	}

	@Override
	protected void startupAttacks() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void sendMessageToAir(Object message) {
		messagesQueue.addLast((Message) message);
        if (queueMessageDispatchTimer.isStop()) {
            sendingMessage = false;
            queueMessageDispatchTimer.start();
        }
		
	}

	

	@Override
	public void sendMessageDone() {
		sendingMessage = false;
        messagesQueue.poll();
	}

	

	@Override
	protected void onStable(boolean oldValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initAttacks() {
		AttacksEntry entry = new AttacksEntry(true, "Blackhole Attack", new BlackholeRoutingAttack(this));
        attacks.addEntry(entry);
        AttacksEntry entry2 = new AttacksEntry(false, "Hello Flooding Attack", new HelloFloodingRountingAttack(this));
        attacks.addEntry(entry2);
		
	}

	
	 private void processRREQMessage(INSENSMessage m) throws INSENSException {
	        boolean isParent = false;
	        RREQPayload payload = new RREQPayload(m.getPayload());
	        if (!getNode().isSinkNode()) {
	
	            if (isFirstTime(payload)) {
	                if (getNode().getMacLayer().getSignalStrength() > INSENSConstants.SIGNAL_STRENGH_THRESHOLD && getNode().getMacLayer().getNoiseStrength() < INSENSConstants.SIGNAL_NOISE_THRESHOLD) {
	                    if (owsIsValid(payload)) {
	                        isParent = true;
	                        
//	                        roundOWS = payload.ows; // updates the round ows
	                        OWS.put(payload.bsId, payload.ows); // updates the round ows
	                        
	                        log("Received RREQ From " + m.getSourceId());
	                        rebroadcastRREQMessage(payload);
	                        feedbackMessageStartTimer.get(payload.bsId).start();//start this baseStation feedbackTimer..
	                    } else {
	                        log("OWS is invalid");
	                        return;
	                    }
	                } else {
	                    log("SIGNAL STRENGH = " + getNode().getMacLayer().getSignalStrength());
	                }
	            }
	        }
	        NeighborInfo n = neighborInfo.get(payload.bsId);
	        if( n== null)
	        	n = new NeighborInfo();
	        n.addNeighbor(payload.sourceId, payload.mac, isParent);
	        neighborInfo.put(payload.bsId,n);
	 }
	
	 
	 /**
     * Process a Feedback message message
     * @param m
     */
    private void processFDBKMessage(INSENSMessage m) throws INSENSException {
        FDBKPayload payload = new FDBKPayload(m.getPayload());
        Short destBs = (Short)m.getDestinationId();
        if (Arrays.equals(myRoundMAC.get(destBs), payload.parent_mac)) { // is from my child
            if (getNode().isSinkNode()) { // if i'm a sink node keep the information
            	getBaseStationController().addFeedbackMessages(payload);
                MINSENSFunctions.decryptData(getNode(), payload.neighborInfo, null);
                feedbackMessagesReceived++;
            } else { // forward the message if is from my child
            	byte[] bsParentMac = neighborInfo.get(destBs).getParentMac();
                byte[] new_payload = modifiedParentMAC(payload,bsParentMac);
                getController().addMessageSentCounter(INSENSConstants.MSG_FEEDBACK);
                INSENSMessage m2 = new INSENSMessage(new_payload);
                m2.setDestinationId(m.getDestinationId());
                send(m2);
//	                sendACKFeedbackMessage(payload);
                log("Forward FDBK Message From Child " + payload.sourceId);
            }
        }// else drop it
    }
    
    /**
     * Process a Route Update Message
     * @param m
     */
    private void processRUPDMessage(INSENSMessage m) {
        try {
            if (getNode().isSinkNode()) {
                return;
            }
            RUPDPayload payload = new RUPDPayload(m.getPayload());
            if (payload.destination == getNode().getId()) { // It's for me :)
                if (isStable() && getForwardingTable().isStableOnBs(payload.source)) {
                    replyToRUPDMessage(payload);
                } else {
                    updateRoutingStatus(payload);
                }
            } else {
                if (isStable()) {
                    routeMessage(m);
                }
            }
        } catch (INSENSException ex) {
            log(ex);
        }
    }
    
    /**
     * Process a RUPD message
     * With this method we can get some reliable RouteUpdate messages
     * by controlling the message acknowledge
     * @param m
     */
    private void processRUPDACKMessage(INSENSMessage m) {
        if (getNode().isSinkNode()){
        	if (itsForMe(m)) 
        		ackRouteUpdate(m);   
        } else {
            if (isStable()) {
                routeMessage(m);
            }
        }

    }
    
    /**
     * When routing is stable we can process a data message
     * this messages are application specific 
     * @param m
     *          the message
     */
    private void processDATAMessage(INSENSMessage m) {
        if (isStable()) {
            if (!itsForMe(m) && !getNode().isSinkNode()) {
                routeMessage(m);
            } else {
                try {
                    DATAPayload payload = new DATAPayload(m.getPayload());
                    lastMessageSenderId = payload.immediate;
                    if (getController().isTesting() && getNode().isSinkNode()) {//sink node only
                        AbstractTest test = getController().getActiveTest();
                        if( test instanceof RouteBalancementTest){
                        	test.getEvaluationManager().registerMessagePassage(m, this);
                        }   
                    }
                    MINSENSFunctions.decryptData(getNode(), payload.data, null);
                    getNode().getApplication().receiveMessage(payload.data);
                    if( USE_LOCAL_BYZANTINE_AGREEMENT || USE_GLOBAL_BYZANTINE_AGREEMENT){
                    	List<Message> replicas = messagesTable.get(m.getUniqueId());
                    	if( replicas == null)
                    		replicas = new LinkedList<Message>();
                    	((LinkedList<Message>) replicas).addLast(m);
                    	messagesTable.put(m.getUniqueId(), replicas);
                    }
                    done(m);
                } catch (INSENSException ex) {
                    log(ex);
                }
            }
        }
    }
    
    
    /**
     * Verify if the message its for me 
     * @param m
     * @return
     */
    private boolean itsForMe(INSENSMessage m) {
        try {
        	 byte type = MINSENSFunctions.getMessageType(m);
        	 switch (type) {
             case INSENSConstants.MSG_ROUTE_UPDATE:
            	  RUPDPayload payload2 = new RUPDPayload(m.getPayload());
            	  if (payload2.destination == getNode().getId()) {
                      return true;
             	 }else
                      return false;
             case INSENSConstants.MSG_ROUTE_UPDATE_ACK:
            	 RUPDAckPayload ackPayload = new RUPDAckPayload(m.getPayload());
            	 if (ackPayload.destination == getNode().getId()) {
                     return true;
            	 }else
                     return false;
             case INSENSConstants.MSG_DATA:
            	 DATAPayload payload = new DATAPayload(m.getPayload());
            	 if (payload.destination == getNode().getId()) {
                     return true;
            	 }else
                     return false;
        	 }	
        } catch (INSENSException ex) {
            log(ex);
        }
        return false;

    }
    private void ackRouteUpdate(INSENSMessage m) {
        RUPDAckPayload ackPayload;
        try {
            ackPayload = new RUPDAckPayload(m.getPayload());
            forwardingTablesRepository.remove(ackPayload.ftUID);
        } catch (INSENSException ex) {
            Logger.getLogger(MINSENSRoutingLayer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    /**
     * Send a route update message acknowledge to base station
     * @param payload
     */
    private void replyToRUPDMessage(RUPDPayload payload) {
        if (replyToRUPDCounter < 3) {
            replyToRUPDCounter++;
            
            Short bsId = payload.source;
            Long ows = OWS.get(bsId);
            byte[] privateKey = this.privateKey.get(bsId);
            byte[] payloadResponse = MINSENSMessagePayloadFactory.createRUPDReplyPayload(getNode().getId(), (Short) payload.source, getNode().getId(), payload.routeIds, payload.forwardingTable.getUniqueId(), ows, privateKey, this.getNode());
            getController().addMessageSentCounter(INSENSConstants.MSG_ROUTE_UPDATE_ACK);
            send(new INSENSMessage(payloadResponse));
            log("Replying to RUPD Message");
        }
    }
    
    /**
     * When receive a route update message, we must update the routing status
     * of the node, ie. change status to stable (which can route messages)
     * @param payload
     */
    private void updateRoutingStatus(RUPDPayload payload) {
    	if(forwardingTable == null)
    		forwardingTable = new MInsensForwardingTable(getNode().getId());
    	
//        forwardingTable.setHopDistanceToBS(payload.forwardingTable.getHopDistanceToBS());
//        getForwardingTable().addAll(payload.forwardingTable);
//        
        getForwardingTable().add(payload.source, payload.forwardingTable);
        
        setStable(true);
        replyToRUPDMessage(payload);
    }
    
    /**
     * Modify the parentMAC
     * @param old_payload
     * @return
     */
    private byte[] modifiedParentMAC(FDBKPayload old_payload, byte[] parentMac) {
        try {
            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
            bados.writeByte(old_payload.type);
            bados.writeShort(old_payload.sourceId);
            bados.writeInt(old_payload.neighborInfoSize);
            bados.write(old_payload.neighborInfo);
            bados.write(parentMac);
            bados.write(old_payload.mac);
            return bados.toByteArray();
        } catch (IOException ex) {
            log(ex);
        }
        return null;
    }
	 
	 /**
     * Verify if ows is valid
     * @param payload
     * @return
     */
    private boolean owsIsValid(RREQPayload payload) {
    	Short bsId = payload.bsId;
    	Long bsOWS = OWS.get(bsId);
        return OneWaySequenceNumbersChain.verifySequenceNumber(bsOWS, payload.ows);
    }
 
 /**
     * Verify if is the first time this node receives a RREQ in current round
     * @param payload
     * @return
     */
    private boolean isFirstTime(RREQPayload payload) {
        Short bsId = payload.bsId;
        Long bsOWS = roundOWS.get(bsId);
    	if (bsOWS == payload.ows) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Broadcast the RREQ message updating important fieds
     * @param payload
     */
    private void rebroadcastRREQMessage(RREQPayload rreqPayload) throws INSENSException {
    	byte[] privateKey = this.privateKey.get(rreqPayload.bsId);
        byte[] payload = MINSENSMessagePayloadFactory.createREQPayload(rreqPayload.bsId,getNode().getId(), rreqPayload.ows, privateKey, rreqPayload.mac, this.getNode());
        INSENSMessage message = new INSENSMessage(payload);
        RREQPayload dummy = new RREQPayload(payload);
        
        byte[] roundMAC = Arrays.copyOf(dummy.mac, dummy.mac.length);
        myRoundMAC.put(rreqPayload.bsId, roundMAC);
        
        getController().addMessageSentCounter(INSENSConstants.MSG_ROUTE_REQUEST);
        send(message);
    }   
	 
	/**
     * Send a DATA message 
     * @param message
     * @return
     */
    private boolean sendDATAMessage(Message message) {
    	
    	/*
    	 * Choose which routes to use here
    	 * 
    	 * For a given message to be sent, the routing layer sets which
    	 * routes it should travel by creating messages to base-stations
    	 * and assigning route ids to each message
    	 * 
    	 */
    	try{
    		Hashtable<Short,LinkedList<Integer>> routes;
        	SimpleEntry<Short,Integer> route;
        	switch(FORWARDING_TYPE){
        		case K_BALANCED_ROUTES:
        			routes = getForwardingTable().getKRoutesBalancedByBS(K);
        			processRoutes(message, routes);
        			break;
        		case K_RANDOM_ROUTES:
        			routes = getForwardingTable().getKRandomRoutes(K);
        			processRoutes(message, routes);
        			break;
        		case ALL:
        			routes = getForwardingTable().getAllRoutes();
        			processRoutes(message, routes);
        			break;
        		case ONE_RANDOM_ROUTE:
        			route = getForwardingTable().getRandomRoute();
        			processOneRoute(message, route);
        			break;
        		case ONE_ROUND_ROBIN:
        			 route = getForwardingTable().getNextRouteRoundRobin();
        			processOneRoute(message, route);
        	}
    		return true;
    	}catch(CloneNotSupportedException e){
    		return false;
    	}
    	
    	
//        INSENSMessage m = (INSENSMessage) encapsulateMessage(message);
//        send((Message) m);
//        return true;
    }
    
    private void processRoutes(Message message, Hashtable<Short,LinkedList<Integer>> routes) throws CloneNotSupportedException{
    	for( Short bsId : routes.keySet()){
    		LinkedList<Integer> routeIds = routes.get(bsId);
    		INSENSMessage m = (INSENSMessage) createRoutingMessage(message, bsId, routeIds);
    		getForwardingTable().registerLastUsedBs(bsId);
    		send((Message) m);
    	}
    }
    
    private void processOneRoute(Message message, SimpleEntry<Short,Integer> route) throws CloneNotSupportedException{
    	LinkedList<Integer> routes = new LinkedList<Integer>();
    	routes.add(route.getValue());
    	INSENSMessage m = (INSENSMessage) createRoutingMessage(message,route.getKey(),routes);
    	getForwardingTable().registerLastUsedBs(route.getKey());
        send((Message) m);
    }
    
    private INSENSMessage createRoutingMessage(Message message, short destBS, LinkedList<Integer> routes) throws CloneNotSupportedException{
    	
			Message toSend = (Message)message.clone();
			toSend.setDestinationId(destBS);
	    	INSENSMessage m = new INSENSMessage();
	        m.setUniqueId(toSend.getUniqueId());
	        m.setSourceId(toSend.getSourceId());
	        m.setDestinationId(toSend.getDestinationId());
	  	
	    	byte[] new_payload = encapsulateDataPayload(toSend,routes);
	        m.setPayload(new_payload);
	        
	        getController().addMessageSentCounter(INSENSConstants.MSG_DATA);
	        return m;
	
    }
    
    @Override
	protected Message encapsulateMessage(Message message) {
		
		INSENSMessage m = new INSENSMessage();
        m.setUniqueId(message.getUniqueId());
        m.setSourceId(message.getSourceId());
        m.setDestinationId(message.getDestinationId());
        
        getController().addMessageSentCounter(INSENSConstants.MSG_DATA);
        return m;
    }
	
	/**
     * Create a DATA message payload
     * @param message
     * @return
     */
    private byte[] encapsulateDataPayload(Message message, LinkedList<Integer> routes) {
    	byte[] privateKey = this.privateKey.get(message.getDestinationId());
        return MINSENSMessagePayloadFactory.createDATAPayload((Short) message.getSourceId(), (Short) message.getDestinationId(), getNode().getId(), routes, message.getPayload(), privateKey, this.getNode());
    }
	
	
	/**
     * Sets the current phase of the routing protocol
     * @param currentPhase
     */
    public void setCurrentPhase(short bsId, String currentPhase) {
        this.currentPhase.put(bsId,  currentPhase);
    }
    
    public void setCurrentPhaseToAll(String currentPhase){
    	for(Short s : this.currentPhase.keySet()){
    		this.currentPhase.put(s, currentPhase);
    	}
    	
    }

    /**
     * Get neighbor info
     * @return the neighbor information
     */
    public NeighborInfo getNeighborInfo(short bsId) {
        return neighborInfo.get(bsId);
    }
    
    public Set<Short> getBaseStations() {
		return baseStations;
	}

	public void setBaseStations(Collection<Short> baseStations) {
		this.baseStations = new HashSet<Short>();
		
		for(Short bs: baseStations){
			this.baseStations.add(bs);
		}
	}

	public String toString() {
        return forwardingTable.toString();
    }

    public short getUniqueId() {
        return getNode().getId();
    }
    
    public MInsensForwardingTable getForwardingTable(){
    	return (MInsensForwardingTable)forwardingTable;
    }

    public Hashtable<Object, List<Message>> getMessagesTable() {
		return messagesTable;
	}

	/**
     * Start the protocol execution, must be initiated by a node a base station
     */
    private void startProtocol() {
        setCurrentPhase(getNode().getId(),PHASE_ROUTE_REQUEST);
        queueMessageDispatchTimer.start();
        if (getNode().isSinkNode()) {
            newRoutingDiscover();
        }
    }
    
    /**
     * Begins a new network organization
     */
    private void newRoutingDiscover() {
        int currRoundNumber = roundNumber.get(getNode().getId());
        currRoundNumber++;
        roundNumber.put(getNode().getId(), currRoundNumber);
        long nextOWS = MINSENSFunctions.getNextOWS(getNode().getId());
        roundOWS.put(getNode().getId(), nextOWS); 
        log("BS: "+getNode().getId()+" Round " + roundNumber + " OWS: " + roundOWS);
        startNetworkOrganization();
    }
    
    
    /**
     * Create the initial request message for network organization
     */
    private void startNetworkOrganization() {
        try {
            /* create a initial route request message */
            byte[] payload = MINSENSMessagePayloadFactory.createREQPayload(getNode().getId(),getNode().getId(), roundOWS.get(getNode().getId()), privateKey.get(getNode().getId()), null, this.getNode());
            INSENSMessage m = new INSENSMessage(payload);
            RREQPayload dummy = new RREQPayload(payload);
            myRoundMAC.put(getNode().getId(), dummy.mac);
            getController().addMessageSentCounter(INSENSConstants.MSG_ROUTE_REQUEST);
            send(m);
            startForwardTablesCalculesTimer.start();
        } catch (INSENSException ex) {
            log(ex);
        }
    }
    /**
     * Create a node private key
     */
    private void createNodeKeys(Short bsId) {
    	byte[] key = CryptoFunctions.createSkipjackKey();
        privateKey.put(bsId, key);
        NetworkKeyStore.getInstance().registerKey(getNode().getId(),bsId, key);
    }
    
    /**
     * Load initial sequence number from the one way hash chain
     */
    private void initiateSequenceNumber(Short bsId) {
        OWS.put(bsId, MINSENSFunctions.getInitialSequenceNumber(bsId));
    }
    

    /**
     * Create the timers needed for operation
     */
    private void createTimers(short bsId) {
    	
    	feedbackMessageStartTimer.put(bsId, 
    	new Timer(getNode().getSimulator(), 1, INSENSConstants.FEEDBACK_START_TIME_BOUND + getNode().getId() * 100,bsId) {

            @Override
            public void executeAction() {
                sendFeedbackMessageInfo((Short)param);

            }
        });
        
    	startForwardTablesCalculesTimer = new Timer(getNode().getSimulator(), INSENSConstants.FEEDBACK_MSG_RECEIVED_TIME_BOUND) {

            @Override
            public void executeAction() {
                startComputeRoutingInfo();
            }
        };
        queueMessageDispatchTimer =  new Timer(getNode().getSimulator(), INSENSConstants.MESSAGE_DISPATCH_RATE) {

            @Override
            public void executeAction() {
                dispatchNextMessage();
            }
        };
        
        
        forwardingTablesDispatchTimer =  new Timer(getNode().getSimulator(), INSENSConstants.MESSAGE_DISPATCH_RATE *2) {
            private int maxRetries;

            
            /*
             * (non-Javadoc)
             * TODO:Esta implementa�‹o n est‡ descrita no paper...as tentativas de reenvio n‹o s‹o explicitadas la.
             * @see org.wisenet.simulator.core.events.Timer#executeAction()
             */
            @Override
            public void executeAction() {
                if (forwardingTablesRepository.isEmpty()) {
                    System.out.println("No more forwarding tables to send");
                    forwardingTablesDispatchTimer.stop();
                    
                    //to be used by registerMessagePassage...
                    forwardingTable = new MInsensForwardingTable(getNode().getId());
                    forwardingTable.setHopDistanceToBS(0);
                    getForwardingTable().setAsSink();
                    
                } else {
                    if (orderedByHopsNodesIterator.hasNext()) {
                        Short nodeId = (Short) orderedByHopsNodesIterator.next();
                        sendForwardTable(getBaseStationController().getForwardingTables(), nodeId);
                        System.out.println(getNode().getId()+" : sending ft to "+nodeId);
                    } else {
                        if (orderedByHopsIterator.hasNext()) {
                            orderedByHopsNodes = tableOfNodesByHops.get(orderedByHopsIterator.next());
                            orderedByHopsNodesIterator = orderedByHopsNodes.iterator();
                            System.out.println(getNode().getId()+" : moving to next hop level "+orderedByHopsNodes.size()
                            		+" nodes");
                        } else {

                            if (first) {
                                forwardingTablesRepositoryIter = new HashSet<byte[]>(forwardingTablesRepository.values()).iterator();
                                first = false;
                            }
                            if (forwardingTablesRepositoryIter.hasNext()) {
                                sendUpdateRouteMessage((byte[]) forwardingTablesRepositoryIter.next());
                            }
                            if (!forwardingTablesRepositoryIter.hasNext() && !forwardingTablesRepository.isEmpty() && retry==0){
//                                maxRetries=forwardingTablesRepository.size(); //*3;
                                maxRetries=1; //*3;
                            }
                            if (!forwardingTablesRepositoryIter.hasNext() && !forwardingTablesRepository.isEmpty() && retry < maxRetries) {
                                forwardingTablesRepositoryIter = new HashSet<byte[]>(forwardingTablesRepository.values()).iterator();
                                retry++;
                            }else{
                                 System.out.println("Stop sending forwarding tables: " + forwardingTablesRepository.size()+ " left");
                                 forwardingTablesDispatchTimer.stop();
                                 
                               //to be used by registerMessagePassage...
                                 forwardingTable = new MInsensForwardingTable(getNode().getId());
                                 forwardingTable.setHopDistanceToBS(0);
                                 getForwardingTable().setAsSink();
                            }
                        }
                    }
                }
            }
        };
    }
    
    
    /**
     * Start building routing info
     */
    private void startComputeRoutingInfo() {
        if (canStartComputeRoutingInfo()) {
//            new Thread(new Runnable() {
//                public void run() {

            log("Started to compute routing info");
            getBaseStationController().calculateForwardingTables();
            log("Number of Forwarding Tables:  " + getBaseStationController().getForwardingTables().size());
            if(USE_CONSENSUS_ON_ROUTES){
            	usesConsensus = true;
            	makeConsensusOnRoutes();
            }else{
            	sendRouteUpdateMessages(getBaseStationController().getForwardingTables());
            }
            
            
            
            
//                }
//            }).start();
        }
    }
    
    private void makeConsensusOnRoutes(){
//    	Collection<Short> ids = getNode().getSimulator().getSinkNodesIds();
    	
    }
    
    
    public void registerSinkFeedback(List<List<List<Short>>> paths){
    	/*
    	 * dar a baseStationController. 
    	 * Se ela tiver calculado o novo allpaths, 
    	 * 		->chamar buildForwardingTables na baseStationController
    	 * 		->sendRouteUpdateMessages(getBaseStationController().getForwardingTables());
    	 */
    	boolean consensusDone = this.getBaseStationController().receiveRoutesFromOtherBaseStations(paths);
    	if(consensusDone){
    		getBaseStationController().buildForwardingTables();
    		sendRouteUpdateMessages(getBaseStationController().getForwardingTables());
    	}
    }
    
    /**
     * Verifies if can begin to compute the routing info
     * @return
     */
    boolean canStartComputeRoutingInfo() {
        /**
         * Devo verificar se o numero de mensagens alterou
         * se alterou entao actualizo e reinicio as tentativas
         * se nao alterou depois de 3 checks entÃƒÂ£o posso iniciar a recepÃƒÂ§ÃƒÂ£o
         */
        if (feedbackMessagesReceived <= lastFeedbackMessagesReceivedCheck) {
            if (feedbackMessageRetries >= INSENSConstants.FEEDBACK_MSG_RECEIVED_RETRIES) {
                startForwardTablesCalculesTimer.stop();
                return true;
            } else {
                feedbackMessageRetries++;
                return false;
            }
        } else {
            lastFeedbackMessagesReceivedCheck = feedbackMessagesReceived;
            feedbackMessageRetries = 0;
            return false;
        }
    }
    
    
    public void clearDataConsensusStructures(){
		this.messagesTable.clear();
    }
    
    
    public double getLocalDataConsensusResult(){
    	
        int consensualizedMessages = 0;
        int replicaCount = 0;
        for(Object o : messagesTable.keySet()){
        	List<Message> messages = messagesTable.get(o);
        	replicaCount = 0;
        	if( messages.size() >= LOCAL_BYZANTINE_AGREEMENT_COPIES_NUMBER){
        		INSENSMessage originalMessage = (INSENSMessage)getController().getActiveTest().getEvaluationManager().getOriginalMessage((Long)o);
        		if(originalMessage == null)
        			break; // if no original message is found...(not suposed to happen)
        		for(Object m2 : messages){
        			INSENSMessage m = (INSENSMessage)m2;
        			DATAPayload originalPayloadData;
					try {
						originalPayloadData = new DATAPayload(originalMessage.getPayload());
						DATAPayload mPayloadData = new DATAPayload(m.getPayload());
						if( Arrays.equals(originalPayloadData.data, mPayloadData.data) ){
	        				replicaCount++;
	        				if(replicaCount == LOCAL_BYZANTINE_AGREEMENT_COPIES_NUMBER){
	        					consensualizedMessages++;
	        					replicaCount = 0; //no need to continue searching in this message
	        					break;
	        				}
	        			}
					} catch (INSENSException e) {
						e.printStackTrace();
					}
        			
        				
            	}
        	}
        }
        return consensualizedMessages*100/messagesTable.size();
    }
    
    
    public Hashtable<Object,Integer> getGlobalConsensusVotes(){
    	Hashtable<Object,Integer> votes = new Hashtable<Object,Integer>();//key is messageId, value is correct replica count
    	
//    	int consensualizedMessages = 0;
        int replicaCount = 0;
        for(Object o : messagesTable.keySet()){
        	List<Message> messages = messagesTable.get(o);
        	replicaCount = 0;
    		INSENSMessage originalMessage = (INSENSMessage)getController().getActiveTest().getEvaluationManager().getOriginalMessage((Long)o);
    		if(originalMessage != null){
    			for(Object m2 : messages){
        			INSENSMessage m = (INSENSMessage)m2;
        			DATAPayload originalPayloadData;
    				try {
    					originalPayloadData = new DATAPayload(originalMessage.getPayload());
    					DATAPayload mPayloadData = new DATAPayload(m.getPayload());
    					if( Arrays.equals(originalPayloadData.data, mPayloadData.data) ){
            				replicaCount++;
            			}
    				} catch (INSENSException e) {
    					e.printStackTrace();
    				}
            	}
        		votes.put(o, replicaCount);
    		}
        }
        return votes;
    	
    }
    
    public double getGlobalDataConsensusResult(){
    	//get other base stations votes
    	Set<Short> sinks = getController().getActiveTest().getSimulation().getSimulator().getSinkNodesIds();
    	Hashtable<Object,Integer> totalVotes = this.getGlobalConsensusVotes();//initialize with my values..
        for(Short id: sinks){
        	if(id != getNode().getId()){//if not me...
        		Node sink = getController().getActiveTest().getSimulation().getSimulator().getNode(id);
        		Hashtable<Object,Integer> otherBSVotes = ((MINSENSRoutingLayer)sink.getRoutingLayer()).getGlobalConsensusVotes();
        		totalVotes = mergeVotes(totalVotes,otherBSVotes);
        	}
        }
        
    	return getPercentageConsensualized(totalVotes);
    }
    
    private Hashtable<Object,Integer> mergeVotes(Hashtable<Object,Integer> votes, Hashtable<Object,Integer> votesToAdd){
    	Hashtable<Object,Integer> updatedVotes =  new Hashtable<Object,Integer>();
    	for(Object id : votes.keySet()){
    		if(votesToAdd.containsKey(id)){
    			int originalCount = votes.get(id);
        		updatedVotes.put(id, originalCount + votesToAdd.get(id));
    		}else
    			updatedVotes.put(id, votes.get(id));
    	}
    	return updatedVotes;
    }
    
    private double getPercentageConsensualized(Hashtable<Object,Integer> votes){
    	int count = 0;
    	for(Object id: votes.keySet()){
    		if(votes.get(id) > GLOBAL_BYZANTINE_AGREEMENT_VOTES_NUMBER)
    			count++;
    	}
    	return (count*100)/votes.size();
    }
    
    public boolean routingUsingLocalDataConsensus(){
    	return USE_LOCAL_BYZANTINE_AGREEMENT;
    }
    
    public boolean routingUsingGlobalDataConsensus(){
    	return USE_GLOBAL_BYZANTINE_AGREEMENT;
    }
    
    /**
     * Prepare the feedback message info to send to parent nodes
     */
    private void sendFeedbackMessageInfo(short bsId) {
        log("Send FeedBack Message ");
        byte[] payload = MINSENSMessagePayloadFactory.createFBKPayload(getNode().getId(),
                privateKey.get(bsId), neighborInfo.get(bsId), neighborInfo.get(bsId).getParentMac(), this.getNode());
        INSENSMessage message = new INSENSMessage(payload);
        message.setDestinationId(bsId);
        getController().addMessageSentCounter(INSENSConstants.MSG_FEEDBACK);
        send(message);
//        feedbackMessageStartTimer.reschedule();
    }
    
    private void sendForwardTable(Hashtable<Short, InsensForwardingTable> forwardingTables, Short id) {
        if (forwardingTables.containsKey(id)) {
            InsensForwardingTable ft = forwardingTables.get(id);
            byte[] payload = MINSENSMessagePayloadFactory.createRUPDPayload(getNode().getId(), (Short) id, getNode().getId(),ft.getRouteIdsToBS() , OWS.get(getNode().getId()), ft, privateKey.get(getNode().getId()), this.getNode());
            sendUpdateRouteMessage(payload);
        }
    }

    private void sendUpdateRouteMessage(byte[] payload) {
        getController().addMessageSentCounter(INSENSConstants.MSG_ROUTE_UPDATE);
        send(new INSENSMessage(payload));
    }
    

    /**
     * Dispatchs a message from the message queue
     */
    private void dispatchNextMessage() {
        if (!sendingMessage) {
            if (!messagesQueue.isEmpty()) {
                /**
                 * Este teste permite diminuir a densidade da rede em virtude de
                 * dimunir as colisoes na rede
                 */
//              if(!getNode().getMacLayer().isReceiving() &&  !getNode().getMacLayer().isTransmitting()){
                broadcastMessage((Message) messagesQueue.peek());
//              }
            } else {
                queueMessageDispatchTimer.stop();
            }
        }
    }
    
    /**
     * Broadcast a message
     * @param message
     * @param reliable
     */
    private void broadcastMessage(Message message) {
        sendingMessage = true;
        long delay = (long) Simulator.randomGenerator.nextDoubleBetween((int) INSENSConstants.MIN_DELAYED_MESSAGE_BOUND, (int) INSENSConstants.MAX_DELAYED_MESSAGE_BOUND);
        long time = (long) (Simulator.getSimulationTime());
        DelayedMessageEvent delayMessageEvent = new DelayedMessageEvent(time, delay, message, getNode(), reliableMode);
        delayMessageEvent.setReliable(reliableMode);
        getNode().getSimulator().addEvent(delayMessageEvent);
    }
    
    /**
     * Send messages with pre-calculated routing tables
     * @param forwardingTables
     */
    public void sendRouteUpdateMessages(Hashtable<Short, InsensForwardingTable> forwardingTables) {
        Vector<List<Short>> allpaths = getBaseStationController().getAllPaths();
        Comparator<List<Short>> c = new Comparator<List<Short>>() {

            public int compare(List<Short> o1, List<Short> o2) {
                return Integer.valueOf(o1.size()).compareTo(Integer.valueOf(o2.size()));
            }
        };
        Collections.sort(allpaths, c);
        tableOfNodesByHops = new Hashtable<Integer,List<Short>>();
        tableOfNodesByHops = buildListOfNodestByHops(allpaths);
        sendForwardingTablesByHops(forwardingTables, tableOfNodesByHops);
        setStable(true);
    }
    
    /**
     * Auxiliary function to create a list of nodes joined by number of hops
     * @param allpaths
     * @return
     */
    private Hashtable<Integer,List<Short>> buildListOfNodestByHops(Vector<List<Short>> allpaths) {
        Hashtable<Integer,List<Short>> t = new Hashtable<Integer,List<Short>>();
        
        for (List<Short> path : allpaths) {
            if (path.size() > 0) {
                List<Short> lst2 = t.get(path.size());
                if (lst2 == null) {
                    lst2 = new ArrayList<Short>();
                }
                lst2.add(path.get(path.size() - 1));
                t.put(path.size(), lst2);
            }
        }
        
        return t;
    }
    
    /**
     * For each hop number based nodes send a Forwarding table
     * @param forwardingTables
     * @param tableOfNodesByHops
     */
    private void sendForwardingTablesByHops(Hashtable<Short, InsensForwardingTable> forwardingTables, Hashtable<Integer,List<Short>> tableOfNodesByHops) {
        forwardingTablesRepository = new Hashtable<Integer, byte[]>();
        orderedByHops = new LinkedList<Integer>(tableOfNodesByHops.keySet());
        Collections.sort(orderedByHops);
        byte[] payload;
        Set<Short> differentNodes = new HashSet<Short>();
        InsensForwardingTable ft = null;
        int i = 0;
        for (Integer key : orderedByHops) {
            List<Short> nodes = tableOfNodesByHops.get(key);
            for (Short id : nodes) {
                differentNodes.add(id);
                
                if (forwardingTables.containsKey(id)) {
                    ft = forwardingTables.get(id);
//                    ft.setHopDistanceToBS((Integer)key-1);//paths contain sinkNode, so hopdistance is size-1
//                    System.out.println("node "+id+" at "+((Integer)key-1)+" hops");
                    payload = MINSENSMessagePayloadFactory.createRUPDPayload(getNode().getId(), id, getNode().getId(),ft.getRouteIdsToBS(), OWS.get(getNode().getId()), ft, privateKey.get(getNode().getId()), this.getNode());
                    forwardingTablesRepository.put(new Integer(ft.getUniqueId()), payload);
                    i++;
//                    sendUpdateRouteMessage(payload);
                }
            }

        }
        System.out.println("Total Forwarding Tables: " + i);
        System.out.println("Total nodes having ft: " + differentNodes.size());

        if(orderedByHops.isEmpty()){
        	System.out.println("Error: No network possible.");
        	return;
        }
        	
        orderedByHopsIterator = orderedByHops.iterator();
        orderedByHopsNodes = tableOfNodesByHops.get(orderedByHopsIterator.next());
        orderedByHopsNodesIterator = orderedByHopsNodes.iterator();
        forwardingTablesDispatchTimer.start();



    }
    
//    private void startAllTimers(Hashtable<Short,Timer> timers){
//    	for(Short s: timers.keySet()){
//    		timers.get(s).start();
//    	}
//    }
    
	protected void sendDataToAnalyse(Object message,String operation, long time, long simtime){
		try {
			String servidor = "localhost";
			int port = 8888;
			InetAddress serverAddress = InetAddress.getByName(servidor);
			DatagramSocket socket = new DatagramSocket();
			String data = "";
			
			// Merge data to send
			MyFloodMessage m = (MyFloodMessage) message;
			data+="operation="+operation+"&";
			data+="id="+m.getUniqueId()+"&";
			data+="hops="+m.getTotalHops()+"&";
			data+="size="+m.size()+"&";
			data+="time="+Long.toString(time)+"&";
			data+="simtime="+simtime;
			
			byte[] requestData = data.getBytes();
			DatagramPacket echoRequest = new DatagramPacket(requestData,
					requestData.length);
			echoRequest.setAddress(serverAddress);
			echoRequest.setPort(port);
			socket.send(echoRequest);
			socket.close();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
