/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.minsens.utils;


import java.util.Hashtable;
import java.util.Map;

/**
 * Key Store for global access 
* 
 */
public class NetworkKeyStore extends Hashtable<Short, Hashtable<Short,byte[]>> {

	private static final long serialVersionUID = 1L;

	
    protected static NetworkKeyStore instance;

    /**
     *
     * @return
     */
    public static NetworkKeyStore getInstance() {
        if (instance == null) {
            instance = new NetworkKeyStore();
        }
        return instance;
    }

    /**
     *
     * @param t
     */
    public NetworkKeyStore(Map<? extends Short, ? extends Hashtable<Short,byte[]>> t) {
        super(t);
    }

    /**
     *
     */
    public NetworkKeyStore() {
        super();
    }

    /**
     *
     * @param initialCapacity
     */
    public NetworkKeyStore(int initialCapacity) {
        super(initialCapacity);
    }

    /**
     *
     * @param initialCapacity
     * @param loadFactor
     */
    public NetworkKeyStore(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    /**
     * Get the registred key for node identified by id
     * @param id
     * @return
     * @throws NetworkKeyStoreException
     */
    public byte[] getNodeKey(short id,short bsId) throws NetworkKeyStoreException {
        
    	Hashtable<Short,byte[]> nodeKeys = get(id);
        byte[] key = nodeKeys.get(bsId);
        if (key == null) {
            throw new NetworkKeyStoreException("Cannot be found a key for this id: " + id);
        }
        return key;
    }

    /**
     *  Register a key for a node identified by id
     * @param id
     * @param key
     */
    public void registerKey(short id,short bsId, byte[] key) {
    	Hashtable<Short,byte[]> nodeKeys = get(id);
    	if(nodeKeys == null)
    		nodeKeys = new Hashtable<Short,byte[]>();
    	nodeKeys.put(bsId, key);
    	put(id,nodeKeys);
    }
}
