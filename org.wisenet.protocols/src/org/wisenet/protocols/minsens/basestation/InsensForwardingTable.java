/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.minsens.basestation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wisenet.protocols.common.ByteArrayDataInputStream;
import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.simulator.core.node.layers.routing.ForwardingTableInterface;

/**
 *
 * @author Andr� Guerreiro
 */
public class InsensForwardingTable implements ForwardingTableInterface {

    static int counter = 0;
    private int hopDistanceToBS ;//by default, 1hop nodes don't get their
    private int hopDistanceOnLastRoute;// only used to measure route balancement
    int uniqueId = counter++;
    short nodeId;
    short bsId;
    int routesToBs;
    Set<RoutingTableEntry> entries = new HashSet<RoutingTableEntry>();

    /**
     *
     * @param nodeId
     */
    public InsensForwardingTable(short nodeId,short bsId) {
        this.nodeId = nodeId;
        this.hopDistanceToBS = Integer.MAX_VALUE;
        this.routesToBs = 0;
        this.bsId = bsId;
    }

    /**
     *
     */
    public InsensForwardingTable() {
    }

    /**
     *
     * @return
     */
    public short getNodeId() {
        return nodeId;
    }

    /**
     *
     * @param nodeId
     */
    public void setNodeId(short nodeId) {
        this.nodeId = nodeId;
    }
    
    public void increaseRoutesNumber(){
    	this.routesToBs++;
    }
    
    public int getNumberOfRoutesToBS(){
    	return this.routesToBs;
    }
    
    public LinkedList<Integer> getRouteIdsToBS(){
    	LinkedList<Integer> lst = new LinkedList<Integer>();
    	for(int i = 0; i< this.routesToBs; i++)
    		lst.add(i);
    	return lst;
    }

    /**
     *
     * @return
     */
    public Set<RoutingTableEntry> getEntries() {
        return entries;
    }

    /**
     *
     * @param badis
     */
    public void read(ByteArrayDataInputStream badis) {
        try {
            uniqueId = badis.readInt();
            hopDistanceToBS = badis.readInt();
            bsId = badis.readShort();
            routesToBs = badis.readInt();
            int nrEntries = badis.readInt();
            for (int i = 0; i < nrEntries; i++) {
                RoutingTableEntry entry = new RoutingTableEntry();
                entry.read(badis);
                entries.add(entry);
            }
        } catch (Exception ex) {
            Logger.getLogger(InsensForwardingTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param bados
     */
    public void write(ByteArrayDataOutputStream bados) {
        try {
            bados.writeInt(uniqueId);
            bados.writeInt(hopDistanceToBS);
            bados.writeShort(bsId);
            bados.writeInt(routesToBs);
            bados.writeInt(entries.size());
            for (RoutingTableEntry routingTableEntry : entries) {
                routingTableEntry.write(bados);
            }
        } catch (Exception ex) {
            Logger.getLogger(InsensForwardingTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param source
     * @param destination
     * @return
     */
    public short getImmediate(short source, short destination) {
        for (RoutingTableEntry routingTableEntry : entries) {
            if (routingTableEntry.getSource() == source && routingTableEntry.getDestination() == destination) {
                return routingTableEntry.getImmediate();
            }
        }
        return -1;
    }

    /**
     *
     * @param entry
     */
    public void add(RoutingTableEntry entry) {
        entries.add(entry);
    }

    /**
     *
     * @param source
     * @param destination
     * @param immediate
     */
    public void add(short source, short destination, short immediate) {
        entries.add(new RoutingTableEntry(source, destination, immediate));
    }

    /**
     *
     * @param entry
     */
    public void remove(RoutingTableEntry entry) {
        entries.remove(entry);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String out = "";
        out += "ForwardingTable[" + uniqueId + "] : " + nodeId + "\n";
        out += "-----------------------------\n";
        for (RoutingTableEntry routingTableEntry : entries) {
            out += routingTableEntry + "\n";
        }
        out += "-----------------------------\n";
        return out;
    }

    /**
     *
     * @param forwardingTable
     */
    public void addAll(InsensForwardingTable forwardingTable) {
        entries.addAll(forwardingTable.getEntries());
    }

    /**
     * Evaluate if a route exists in this forwing table
     * @param destination
     * @param source
     * @param immediate
     * @return
     */
    public boolean haveRoute(short destination, short source, short immediate) {
        for (RoutingTableEntry routingTableEntry : entries) {
            if (routingTableEntry.getSource() == source && routingTableEntry.getDestination() == destination && routingTableEntry.getImmediate() == immediate) {
            	hopDistanceOnLastRoute = routingTableEntry.getHopDistance();
                return true;
            }
        }
        return false;
    }

    public int getUniqueId() {
        return uniqueId;
    }

	public int getHopDistanceToBS() {
		return hopDistanceToBS;
	}

	public void setHopDistanceToBS(int hopDistanceToBS) {
		this.hopDistanceToBS = hopDistanceToBS;
	}

	@Override
	public int getHopDistanceOnLastRoute() {
		return hopDistanceOnLastRoute;
	}

	public short getBsId() {
		return bsId;
	}
}
