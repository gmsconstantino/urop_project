/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.minsens.basestation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.LinkedList;

import java.util.List;
import java.util.Vector;
import org.jgrapht.graph.DefaultEdge;
import org.wisenet.protocols.minsens.basestation.RoutingTableEntry;
import org.wisenet.protocols.minsens.MINSENSRoutingLayer;
import org.wisenet.protocols.minsens.basestation.jgrapht.NetworkGraph;
import org.wisenet.protocols.minsens.messages.data.FDBKPayload;
import org.wisenet.protocols.minsens.utils.NeighborInfo;
import org.wisenet.simulator.core.node.layers.routing.BaseStationControllerInterface;

/**
 *
 * 
 */
public class BaseStationController implements BaseStationControllerInterface {
    //TODO: Test other implementation

    NetworkGraph graph = new NetworkGraph(DefaultEdge.class);
    Hashtable<Short, List<List<Short>>> firstPaths = new Hashtable<Short, List<List<Short>>>();
    Hashtable<Short, List<List<Short>>> secondPaths = new Hashtable<Short, List<List<Short>>>();
    
    //each entry is a list of paths
    LinkedList<List<List<Short>>> paths = new LinkedList<List<List<Short>>>();
    Hashtable<Short, FDBKPayload> feedbackMessagesTable = new Hashtable<Short, FDBKPayload>();
    Hashtable<Short, NeighborInfo> networkNeighborsTable = new Hashtable<Short, NeighborInfo>();
    Hashtable<Short, InsensForwardingTable> forwardingTables = new Hashtable<Short, InsensForwardingTable>();
    PathsFinder pathsFinder;
    private Vector<List<Short>> allPaths;
    private final MINSENSRoutingLayer basestation;
    
    private boolean USE_CONSENSUS_ON_ROUTES;
    private int sinksFeedbackCounter; 

    /**
     *
     * @return
     */
    public MINSENSRoutingLayer getBasestation() {
        return basestation;
    }

    /**
     *
     * @param basestation
     */
    public BaseStationController(MINSENSRoutingLayer basestation) {
        this.basestation = basestation;

    }
    

    /**
     *
     * @return
     */
    public Vector<List<Short>> getAllPaths() {
        return allPaths;
    }

    /**
     *
     * @return
     */
    public Hashtable<Short, List<List<Short>>> getFirstPaths() {
        return firstPaths;
    }

    /**
     *
     * @return
     */
    public Hashtable<Short, NeighborInfo> getNetworkNeighborsTable() {
        return networkNeighborsTable;
    }

    /**
     *
     * @return
     */
    public Hashtable<Short, List<List<Short>>> getSecondPaths() {
        return secondPaths;
    }

    public LinkedList<List<List<Short>>> getPaths() {
		return paths;
	}

	/**
     *
     * @return
     */
    public Hashtable<Short, FDBKPayload> getFeedbackMessagesTable() {
        return feedbackMessagesTable;
    }

    public void setUSE_CONSENSUS_ON_ROUTES(boolean uSE_CONSENSUS_ON_ROUTES) {
		USE_CONSENSUS_ON_ROUTES = uSE_CONSENSUS_ON_ROUTES;
		if(USE_CONSENSUS_ON_ROUTES)
			sinksFeedbackCounter = basestation.getNode().getSimulator().getSinkNodesIds().size();
	}

	/**
     * Adds a message to the feedback messages
     * @param message
     */
    public void addFeedbackMessages(FDBKPayload message) {
        if (verifyFeedbackMessage(message)) {
            feedbackMessagesTable.put(message.sourceId, message);
            processFeedbackMessage(message);
        }
    }

    /**
     * Makes all topology verification based on nested MAC's
     * @return
     */
    public boolean verifyTopology() {
        return true; //TODO: Verify Topology
    }

    /**
     * Verify if the MAC is correct
     * @param message
     * @return
     */
    public boolean verifyFeedbackMessage(FDBKPayload message) {
        return true; // TODO: verifyFeedbackMessage
    }

    /**
     * Save the message for each source Node
     * @param message
     */
    public void processFeedbackMessage(FDBKPayload message) {
        short src = message.sourceId;
        NeighborInfo neighborInfo = new NeighborInfo();
        neighborInfo.fromByteArray(message.neighborInfo);
        networkNeighborsTable.put(src, neighborInfo);
    }

    /**
     * Utility function to print Forwarding tables
     */
    protected void printFwTables() {
        for (InsensForwardingTable forwardingTable : forwardingTables.values()) {
            System.out.println(forwardingTable);
        }
    }

    /**
     * Utility functions to print paths
     * @param allPaths
     */
    protected void printPaths(Vector<List<Short>> allPaths) {
        for (List<Short> list : allPaths) {
            System.out.print("[");
            for (Object object1 : list) {
                System.out.print(" " + object1 + " ");
            }
            System.out.println("]");
        }
    }

//    private List callPathFinder(NetworkGraph graph) {
//        short start = (Short) getBasestation().getUniqueId();
//        pathsFinder = new PathsFinder(graph, start);
//        return pathsFinder.findFirstPaths();
//    }
    
    int counter = 0;

    private synchronized void saveTable(Hashtable<Short,List<RoutingTableEntry>> table, Short destinationId, int hopDistance) {
        for (Short node : table.keySet()) {
            InsensForwardingTable fwt = forwardingTables.get(node);
            if (fwt == null) {
                fwt = new InsensForwardingTable(node,basestation.getNode().getId());
            }
            
            if(node == destinationId){
            	fwt.increaseRoutesNumber();
            	if( fwt.getHopDistanceToBS() > hopDistance){
            	fwt.setHopDistanceToBS(hopDistance);//node should use the shortest distance to BS
            	}
            }
        	
            
            List<RoutingTableEntry> t = table.get(node);
            if (t.size() > 0) {
                fwt.add(t.get(0));
            }
            if (t.size() > 1) {
                fwt.add(t.get(1));
            }
            forwardingTables.put(node, fwt);
        }
    }

    /**
     * Prepare the base station adding the base station info 
     * @param baseStation
     */
    private void prepareBaseStation(MINSENSRoutingLayer baseStation) {
        NeighborInfo n = new NeighborInfo();
        n.fromByteArray(baseStation.getNeighborInfo(baseStation.getNode().getId()).toByteArray());
        networkNeighborsTable.put(baseStation.getNode().getId(), n);
    }

    /**
     *  Calculate the paths
     * @param path
     * @return
     */
    private Hashtable<Short, List<RoutingTableEntry>> path2RoutingTableEntryTable(List<Short> path, Hashtable<Short,Integer> routeCount ) {
        Short destination;
        Short source = path.get(0);
        if(source != basestation.getNode().getId()) //if it's not a route from this base-Station..
        	return null;
        
        Hashtable<Short, List<RoutingTableEntry>> table = new Hashtable<Short, List<RoutingTableEntry>>();
        destination = path.get(path.size() - 1);
        Integer routeId = routeCount.get(destination);
        
        if(routeId == null)
        	routeId = 0;
        else//if it exists, this is another route to it..
        	routeId++;
        
        routeCount.put(destination, routeId);
        
        for (int i = 1; i < path.size(); i++) {
            Short node = path.get(i);
            List<RoutingTableEntry> t = new ArrayList<RoutingTableEntry>();
            t.add(new RoutingTableEntry(source, destination, path.get(i - 1),i,routeId));
            
            if (i < path.size() - 1) {
                t.add(new RoutingTableEntry(destination, source, path.get(i + 1),i,routeId));
            }
            
            table.put(node, t);
        }
        
        return table;
    }

    /**
     * Gets the Forwarding Tables
     * @return
     */
    public Hashtable<Short, InsensForwardingTable> getForwardingTables() {
        return forwardingTables;
    }

    /**
     ******************************************************************************************
     ******************************************************************************************
     ******************************************************************************************
     ******************************************************************************************
     ******************************************************************************************
     */
    /**
     * Perform the calculations related with building forwarding tables
     */
    public void calculateForwardingTables() {
        int numOfFDBKMessages = feedbackMessagesTable.entrySet().size();
                System.out.println("RECEIVED " + numOfFDBKMessages + " FDBK MESSAGES AT BASESTATION");
                long start = System.currentTimeMillis();
                long partial = start;
                int totalRoutes =0;
                System.out.println("INITIATED FORWARDING TABLES CALCULATION");
                prepareBaseStation(basestation);

                System.out.println("prepared base station IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");
                partial = System.currentTimeMillis();
                createGraph();
                System.out.println("created network graph IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");
                partial = System.currentTimeMillis();
                Short startId = (Short) getBasestation().getUniqueId();
                pathsFinder = new PathsFinder(graph, startId);
                List<List<Short>> paths1 = pathsFinder.findFirstPaths();
                paths.add(paths1);
                totalRoutes+=paths1.size();
                
                System.out.println("first path calculated IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");
                partial = System.currentTimeMillis();
                List<List<Short>> paths2 = pathsFinder.findOtherPaths(paths1);
                paths.add(paths2);
                totalRoutes+=paths2.size();
                System.out.println("second path calculated IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");
                
                partial = System.currentTimeMillis();
                List<List<Short>> paths3 = pathsFinder.findOtherPaths(paths2);
                paths.add(paths3);
                totalRoutes+=paths3.size();
                System.out.println("third path calculated IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");
                
                mergePaths(paths);
                if(USE_CONSENSUS_ON_ROUTES){
                	
                	this.sinksFeedbackCounter--;//this bs is ready..
                	if(sinksFeedbackCounter == 0){ //this is the last Bs sending the routes..
                		discardNonDisjointRoutes();
                		buildForwardingTables();
                		basestation.sendRouteUpdateMessages(getForwardingTables());
                	}
                	System.out.println("send "+totalRoutes+" routes to other basestations..");
                	basestation.getNode().getSimulator().sendRoutesToOtherSinks(basestation.getNode().getId(), paths);
                	
                	
                }else{
                	partial = System.currentTimeMillis();
                    buildForwardingTables();
                    System.out.println("builded tables IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");

                    System.out.println("ENDED FORWARDING TABLES CALCULATION IN " + (System.currentTimeMillis() - start) / 1000 + " SECONDS");
	
                }
   }
    
    /*
     * returns true when it receives the routes from the last baseStation
     * and calculates the new totally disjoint routes
     */
    public boolean receiveRoutesFromOtherBaseStations(List<List<List<Short>>> receivedPaths){
    	
    	boolean result = false;
    	this.sinksFeedbackCounter--;
    	mergePaths(receivedPaths);
    	if( sinksFeedbackCounter == 0){ //received routes from all the other baseStations
    		result = true;
    		discardNonDisjointRoutes();
    	}
    	return result;
    }
    
    private void mergePaths(List<List<List<Short>>> newPaths){
    	if(allPaths == null){
    		allPaths = new Vector<List<Short>>();
    	}
    	
    	for(List<List<Short>> paths: newPaths){
    		for( List<Short> e : paths){
    			allPaths.add(e);
    		}
    	}
    }
    
    private void discardNonDisjointRoutes(){
    	Comparator<List<Short>> c = new Comparator<List<Short>>() {

            public int compare(List<Short> o1, List<Short> o2) {
                return Integer.valueOf(o1.size()).compareTo(Integer.valueOf(o2.size()));
            }
        };
        Collections.sort(allPaths, c);
        
        System.out.println("Executing consensus on "+allPaths.size()+" routes");
        
    	Vector<List<Short>> disjointPaths = new Vector<List<Short>>();
    	Hashtable<Short,List<Short>> nodesUsedToDestination = new Hashtable<Short,List<Short>>();
    	
    	for (int i = 0; i < allPaths.size(); i++) {
            List<Short> path = allPaths.get(i);
            
            if(path.size() > 2){
            	Short destination = (Short)path.get(path.size()-1);
            	if(!nodesUsedToDestination.containsKey(destination))
            		nodesUsedToDestination.put(destination, new LinkedList<Short>());
            	
            	List<Short> alreadyUsed = nodesUsedToDestination.get(destination);
            	for(int j = 1; j < path.size()-1;j++){//all elements excluding first and last
            		Short id = path.get(j);
            		 
            		if(alreadyUsed.contains(id)){
            			break;
            		}else{
            			//alreadyUsed.addLast(id);
            			alreadyUsed.add(alreadyUsed.size(), id);
            			if(j == path.size()-2)//if it's the last element except the destination..
            				disjointPaths.add(path);
            		}
            		
            	}
            }else if (path.size() == 2){
            	disjointPaths.add(path); 
            }
    	}
    	allPaths = disjointPaths;
    	System.out.println("CONSENSUS DONE: "+allPaths.size()+" routes ");
    }

    public void createGraph() {
        for (Object e1 : this.networkNeighborsTable.keySet()) { // por cada nó que enviou com sucesso feedback message
            Short edge1 = (Short) e1; // Nó com feedback message recebida
            NeighborInfo neighborInfo1 = (NeighborInfo) networkNeighborsTable.get(edge1); // ler a tabela de vizinhos
            if (neighborInfo1 != null) {
                for (Object e2 : neighborInfo1.keySet()) {
                    Short edge2 = (Short) e2;
                    NeighborInfo neighborInfo2 = (NeighborInfo) networkNeighborsTable.get(e2);
                    if (neighborInfo2 != null) {
                        if (neighborInfo2.containsKey(edge1)) {
                            graph.addVertex(edge1);
                            graph.addVertex(edge2);
                            if (!graph.containsEdge(edge2, edge1) && !graph.containsEdge(edge1, edge2)) {
                                graph.addEdge(edge1, edge2);
                                graph.addEdge(edge2, edge1);
                            }
                        }
                    }
                }
            }
        }
    }

    public void buildForwardingTables(){
    	 Comparator<List<Short>> c = new Comparator<List<Short>>() {

             public int compare(List<Short> o1, List<Short> o2) {
                 return Integer.valueOf(o1.size()).compareTo(Integer.valueOf(o2.size()));
             }
         };
         Collections.sort(allPaths, c);
         
         //key is nodeId(destination), and value is a counter for number of routes
         Hashtable<Short,Integer> routeCount = new Hashtable<Short,Integer>();
         for (int i = 0; i < allPaths.size(); i++) {
             List<Short> path = allPaths.get(i);
             
             if (path != null && path.size() >= 2) {
                 Hashtable<Short,List<RoutingTableEntry>> table = path2RoutingTableEntryTable(path, routeCount);
                 
                 if(table != null){//it is null if this path is from other base-station..
                	 int hopDistance = path.size()-1;
                     Short destination = (Short)path.get(hopDistance);
                     saveTable(table,destination,hopDistance);
                 }
             }
         }
    }
    
//    private void buildForwardingTables(List<List<Short>> firstPathsL, List<List<Short>> secondPathsL) {
//        allPaths = new Vector<List<Short>>();
//        allPaths.addAll(firstPathsL);
//        allPaths.addAll(secondPathsL);
//        
//        buildForwardingTables();
//    }
}
