/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.minsens.basestation;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wisenet.protocols.common.ByteArrayDataInputStream;
import org.wisenet.protocols.common.ByteArrayDataOutputStream;

/**
 *
* 
 */
public class RoutingTableEntry {

    short source;
    short destination;
    short immediate;
    int hopDistance = -1;//distance to BS on this route
    int routeId = -1;

    /**
     *
     */
    public RoutingTableEntry() {
    }

    /**
     *
     * @param source
     * @param destination
     * @param immediate
     */
    public RoutingTableEntry(short source, short destination, short immediate) {
        this.source = source;
        this.destination = destination;
        this.immediate = immediate;
    }
    
    public RoutingTableEntry(short source, short destination, short immediate,int hopDistance) {
        this.source = source;
        this.destination = destination;
        this.immediate = immediate;
        this.hopDistance = hopDistance;
    }
    
    public RoutingTableEntry(short source, short destination, short immediate,int hopDistance, int routeId) {
        this.source = source;
        this.destination = destination;
        this.immediate = immediate;
        this.hopDistance = hopDistance;
        this.routeId = routeId;
    }

    /**
     *
     * @return
     */
    public short getDestination() {
        return destination;
    }

    /**
     *
     * @return
     */
    public short getImmediate() {
        return immediate;
    }

    /**
     *
     * @return
     */
    public short getSource() {
        return source;
    }

    public int getHopDistance() {
		return hopDistance;
	}


	public int getRouteId() {
		return routeId;
	}

	/**
     *
     * @param badis
     */
    public void read(ByteArrayDataInputStream badis) {
        try {
            
            source = badis.readShort();
            destination = badis.readShort();
            immediate = badis.readShort();
            hopDistance = badis.readInt();
            routeId = badis.readInt();
        } catch (IOException ex) {
            Logger.getLogger(RoutingTableEntry.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void write(ByteArrayDataOutputStream bados) {
        try {
            
            bados.writeShort(source);
            bados.writeShort(destination);
            bados.writeShort(immediate);
            bados.writeInt(hopDistance);
            bados.writeInt(routeId);
        } catch (IOException ex) {
            Logger.getLogger(RoutingTableEntry.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RoutingTableEntry){
            RoutingTableEntry o = (RoutingTableEntry) obj;
            return o.source==source && o.destination==destination && o.immediate==immediate;
        }
        return false;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.source;
        hash = 97 * hash + this.destination;
        hash = 97 * hash + this.immediate;
        return hash;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return destination+ " , " + source + " , " + immediate;
    }


}
