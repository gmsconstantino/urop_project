/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.minsens.basestation;

import java.util.AbstractMap.SimpleEntry;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Set;
import java.util.Random;
import org.wisenet.simulator.core.node.layers.routing.ForwardingTableInterface;

/**
 *
 * @author Andr� Guerreiro
 */
public class MInsensForwardingTable implements ForwardingTableInterface {

    static int counter = 0;
    private int hopDistanceOnLastRoute;// only used to measure route balancement
    int uniqueId = counter++;
    short nodeId;
    Hashtable<Short,InsensForwardingTable> fTables = new Hashtable<Short,InsensForwardingTable>();
    int totalRoutesToBs = 0;
    int nextRRRoute = 0;
    short lastUsedBs;
    boolean isSink = false;

    /**
     *
     * @param nodeId
     */
    public MInsensForwardingTable(short nodeId) {
        this.nodeId = nodeId;
    }

    /**
     *
     */
    public MInsensForwardingTable() {
    }

    /**
     *
     * @return
     */
    public short getNodeId() {
        return nodeId;
    }

    /**
     *
     * @param nodeId
     */
    public void setNodeId(short nodeId) {
        this.nodeId = nodeId;
    }

    /**
     *
     * @return
     */
    public Set<RoutingTableEntry> getEntries(short bsId) {
        return fTables.get(bsId).getEntries();
    }


    /**
     *
     * @param source
     * @param destination
     * @return
     */
    public short getImmediate(short source, short destination) {
    	for(Short id : fTables.keySet()){
    		Set<RoutingTableEntry> entries = fTables.get(id).getEntries();
	        for (RoutingTableEntry routingTableEntry : entries) {
	            if (routingTableEntry.getSource() == source && routingTableEntry.getDestination() == destination) {
	                return routingTableEntry.getImmediate();
	            }
	        }
    	}
        return -1;
    }

    /**
     *
     * @param entry
     */
    public void add(Short bsId, InsensForwardingTable table) {
       fTables.put(bsId, table);
       this.totalRoutesToBs+=table.getNumberOfRoutesToBS();
    }

    public boolean isStableOnBs(Short bsId){
    	return fTables.keySet().contains(bsId);
    }

    /**
     *
     * @param entry
     */
    public void remove(short bsId,RoutingTableEntry entry) {
        InsensForwardingTable ft = fTables.get(bsId);
        if(ft != null){
        	ft.getEntries().remove(entry);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String out = "";
        out += "ForwardingTable[" + uniqueId + "] : " + nodeId + "\n";
        out += "-----------------------------\n";
        for(Short id : fTables.keySet()){
    		Set<RoutingTableEntry> entries = fTables.get(id).getEntries();
	        for (RoutingTableEntry routingTableEntry : entries) {
	            out += routingTableEntry + "\n";
	        }
        }
	   out += "-----------------------------\n";
        return out;
    }


    /**
     * Evaluate if a route exists in this forwing table
     * @param destination
     * @param source
     * @param immediate
     * @return
     */
    public boolean haveRoute(short destination, short source, short immediate, LinkedList<Integer> routeIds) {
    	for(Short id : fTables.keySet()){
    		Set<RoutingTableEntry> entries = fTables.get(id).getEntries();
	    	for (RoutingTableEntry routingTableEntry : entries) {
	            if (routingTableEntry.getSource() == source && routingTableEntry.getDestination() == destination 
	            		&& routingTableEntry.getImmediate() == immediate
	            		&& routeIds.contains(routingTableEntry.routeId))//my routeId must be in the designated ones in order to forward
	            {
	            	hopDistanceOnLastRoute = routingTableEntry.getHopDistance();
	            	lastUsedBs = id;
	                return true;
	            }
	        }
    	}
        return false;
    }
    
    
    public Hashtable<Short,LinkedList<Integer>> getKRandomRoutes(int k){
    	Hashtable<Short,LinkedList<Integer>> result = new Hashtable<Short,LinkedList<Integer>>();
    	for(int i = 0; i < k; i++){
    		SimpleEntry<Short,Integer> route = getRandomRoute();
    		LinkedList<Integer> r = result.get(route.getKey());
    		if( r == null)
    			r = new LinkedList<Integer>();
    		r.add(route.getValue());
    		result.put(route.getKey(), r);
    	}
    	return result;
    }

    public Hashtable<Short,LinkedList<Integer>>  getKRoutesBalancedByBS(int k){
    	Hashtable<Short,LinkedList<Integer>> result = new Hashtable<Short,LinkedList<Integer>>();
    	int perBS = k/fTables.size();
    	for( InsensForwardingTable ft: fTables.values() ){
    		LinkedList<Integer> routes = new LinkedList<Integer>();
    		for(int i = 0; i < perBS; i++){
    			if (i == ft.entries.size())
    				break;
    			routes.add(ft.getRouteIdsToBS().get(i));
    		}
    		result.put(ft.getBsId(), routes);
    	}
    	return result;
    }
    
    public Hashtable<Short,LinkedList<Integer>>  getAllRoutes(){
    	Hashtable<Short,LinkedList<Integer>> routes = new Hashtable<Short,LinkedList<Integer>>();
    	for( InsensForwardingTable ft: fTables.values() ){
    		routes.put(ft.getBsId(),ft.getRouteIdsToBS());
    	}
    	return routes;
    }
    
    public SimpleEntry<Short,Integer> getRandomRoute(){
    	Random r = new Random();
    	int routeNumber = 0 ;
    	if(totalRoutesToBs > 1){
    		routeNumber = r.nextInt(totalRoutesToBs-1);
    	}
    	SimpleEntry<Short,Integer> result = null;
    	for( InsensForwardingTable ft: fTables.values() ){
    		if( routeNumber < ft.routesToBs){
    			result = new SimpleEntry<Short,Integer>(ft.getBsId(),ft.getRouteIdsToBS().get(routeNumber));
    			break;
    		}else
    			routeNumber-= ft.routesToBs;
    	}
    	return result;
    	
    }
    
    public SimpleEntry<Short,Integer> getNextRouteRoundRobin(){
    	SimpleEntry<Short,Integer> result = null;
    	int routeToFind = nextRRRoute;
    	for( InsensForwardingTable ft: fTables.values() ){
    		if( routeToFind <= ft.routesToBs){
    			result = new SimpleEntry<Short,Integer>(ft.getBsId(),ft.getRouteIdsToBS().get(routeToFind));
    			break;
    		}else
    			routeToFind-= ft.routesToBs;
    	}
    	nextRRRoute = (nextRRRoute++)%(totalRoutesToBs);
    	return result;
    }
    
    public int getUniqueId() {
        return uniqueId;
    }

	public int getHopDistanceToBS(short bsId) {
		return fTables.get(bsId).getHopDistanceToBS();
	}


	@Override
	public int getHopDistanceOnLastRoute() {
		return hopDistanceOnLastRoute;
	}
	
	public  void registerLastUsedBs(short bsId){
		lastUsedBs  = bsId;
	}

	@Override
	public int getHopDistanceToBS() {
		if(isSink)
			return 0;
		else
			return getHopDistanceToBS(lastUsedBs); // assumes registerLastUsedBs is used before a send()
	}

	@Override
	public void setHopDistanceToBS(int hopDistanceToBS) {
		
	}
	
	public void setAsSink(){
		this.isSink = true;
	}
}
