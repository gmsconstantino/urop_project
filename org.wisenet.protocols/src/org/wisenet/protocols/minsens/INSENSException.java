/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */

package org.wisenet.protocols.minsens;

/**
 *
* @author  <MSc Student @di.fct.unl.pt>
 */
public class INSENSException extends Exception{

	private static final long serialVersionUID = 1L;

	/**
     *
     * @param cause
     */
    public INSENSException(Throwable cause) {
        super(cause);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public INSENSException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     *
     * @param message
     */
    public INSENSException(String message) {
        super(message);
    }

    /**
     *
     */
    public INSENSException() {
    }


}
