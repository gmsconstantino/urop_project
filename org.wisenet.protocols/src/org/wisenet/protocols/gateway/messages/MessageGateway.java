package org.wisenet.protocols.gateway.messages;

import java.io.Serializable;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class MessageGateway implements Serializable {

	private static final long serialVersionUID = -3822851192064478103L;

	private Object source;
	private Object destination;
	private byte[] payload;
	
	public MessageGateway() {
		this.source = null;
		this.destination = null;
		this.payload = null;
	}

	public MessageGateway(Object source, Object destination, byte[] payload) {
		this.source = source;
		this.destination = destination;
		this.payload = payload;
	}

	public Object getSource() {
		return source;
	}

	public void setSource(Object source) {
		this.source = source;
	}

	public Object getDestination() {
		return destination;
	}

	public void setDestination(Object destination) {
		this.destination = destination;
	}

	public byte[] getPayload() {
		return payload;
	}

	public void setPayload(byte[] payload) {
		this.payload = payload;
	}

}
