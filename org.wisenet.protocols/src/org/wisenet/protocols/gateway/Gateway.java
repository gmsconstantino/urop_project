package org.wisenet.protocols.gateway;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public interface Gateway {
	
	/*
	 * Message passed from sensor to node
	 */
	void onReceiveMessageGateway(byte[] message);

}
