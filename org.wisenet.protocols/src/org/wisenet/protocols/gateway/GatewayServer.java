package org.wisenet.protocols.gateway;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class GatewayServer extends Thread {
	
		public final int PORT = 8000;
		DatagramSocket socket;
		Gateway gateway;
		
		public GatewayServer(Gateway gt) {
			gateway = gt;
			try {
				socket = new DatagramSocket(PORT);
			} catch (SocketException e) {
				e.printStackTrace();
			}
			start();
		}

		public void run() {
			for (;;) {
				byte[] buffer = new byte[65536];
				DatagramPacket echoRequest = new DatagramPacket(buffer,
						buffer.length);
				try {
					socket.receive(echoRequest);
					byte[] data = echoRequest.getData();
					//int length = echoRequest.getLength();

					//System.out.println(new String(echoRequestData, 0, echoRequestLength));
					gateway.onReceiveMessageGateway(data);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

}
