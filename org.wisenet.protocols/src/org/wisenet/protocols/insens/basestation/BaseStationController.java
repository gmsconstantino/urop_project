package org.wisenet.protocols.insens.basestation;

import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.jgraph.graph.DefaultEdge;
import org.wisenet.protocols.insens.INSENSRoutingLayer;
import org.wisenet.protocols.insens.basestation.jgrapht.NetworkGraph;
import org.wisenet.protocols.insens.messages.data.FDBKPayload;
import org.wisenet.protocols.insens.utils.NeighborInfo;
import org.wisenet.simulator.core.node.layers.routing.BaseStationControllerInterface;

public class BaseStationController implements BaseStationControllerInterface{

	//TODO: Test other implementation

    NetworkGraph graph = new NetworkGraph(DefaultEdge.class);
    Hashtable<Short, List<List<Short>>> firstPaths = new Hashtable<Short, List<List<Short>>>();
    Hashtable<Short, List<List<Short>>> secondPaths = new Hashtable<Short, List<List<Short>>>();
    
    //each entry is a list of paths
    List<List<List<Short>>> paths = new LinkedList<List<List<Short>>>();
    
    Hashtable<Short, FDBKPayload> feedbackMessagesTable = new Hashtable<Short, FDBKPayload>();
    Hashtable<Short, NeighborInfo> networkNeighborsTable = new Hashtable<Short, NeighborInfo>();
    Hashtable<Short, INSENSForwardingTable> forwardingTables = new Hashtable<Short, INSENSForwardingTable>();
    
    PathsFinder pathsFinder;
    
    private Vector<List<Short>> allPaths;
    private final INSENSRoutingLayer basestation;

    /**
     * @return
     */
    public INSENSRoutingLayer getBasestation() {
        return basestation;
    }

    /**
     * @param basestation
     */
    public BaseStationController(INSENSRoutingLayer basestation) {
        this.basestation = basestation;
    }

    /**
     * @return
     */
    public Vector<List<Short>> getAllPaths() {
        return allPaths;
    }

    /**
     * @return
     */
    public Hashtable<Short,List<List<Short>>> getFirstPaths() {
        return firstPaths;
    }

    /**
     * @return
     */
    public Hashtable<Short,List<List<Short>>> getSecondPaths() {
        return secondPaths;
    }
    
    /**
     * @return
     */
    public Hashtable<Short,NeighborInfo> getNetworkNeighborsTable() {
        return networkNeighborsTable;
    }

    public List<List<List<Short>>> getPaths() {
		return paths;
	}

	/**
     * @return
     */
    public Hashtable<Short,FDBKPayload> getFeedbackMessagesTable() {
        return feedbackMessagesTable;
    }

    /**
     * Adds a message to the feedback messages
     * @param message
     */
    public void addFeedbackMessages(FDBKPayload message) {
        if (verifyFeedbackMessage(message)) {
            feedbackMessagesTable.put(message.sourceId, message);
            processFeedbackMessage(message);
        }
    }

    /**
     * Makes all topology verification based on nested MAC's
     * @return
     */
    public boolean verifyTopology() {
        return true; //TODO: Verify Topology
    }

    /**
     * Verify if the MAC is correct
     * @param message
     * @return
     */
    public boolean verifyFeedbackMessage(FDBKPayload message) {
        return true; // TODO: verifyFeedbackMessage
    }

    /**
     * Save the message for each source Node
     * @param message
     */
    public void processFeedbackMessage(FDBKPayload message) {
        short src = message.sourceId;
        NeighborInfo neighborInfo = new NeighborInfo();
        neighborInfo.fromByteArray(message.neighborInfo);
        networkNeighborsTable.put(src, neighborInfo);
    }

    /**
     * Utility function to print Forwarding tables
     */
    protected void printFwTables() {
        for (INSENSForwardingTable forwardingTable : forwardingTables.values()) {
            System.out.println(forwardingTable);
        }
    }

    /**
     * Utility functions to print paths
     * @param allPaths
     */
    protected void printPaths(Vector<List<Short>> allPaths) {
        for (List<Short> list : allPaths) {
           System.out.print("[");
           for (Object object1 : list) {
        	   System.out.print(" " + object1 + " ");
           }
           System.out.println("]");
        }
    }
    
    int counter = 0;

    private synchronized void saveTable(Hashtable<Short,List<RoutingTableEntry>> table, Short destinationId, int hopDistance) {
        for (Short node : table.keySet()) {
            INSENSForwardingTable fwt = forwardingTables.get(node);
            
            if (fwt == null) {
                fwt = new INSENSForwardingTable(node);
            }
            
            if(node == destinationId && fwt.getHopDistanceToBS() > hopDistance){
            	fwt.setHopDistanceToBS(hopDistance);//node should use the shortest distance to BS
            }
            
            List<RoutingTableEntry> t = table.get(node);
            
            if (t.size() > 0) {
                fwt.add(t.get(0));
            }
            if (t.size() > 1) {
                fwt.add(t.get(1));
            }
            
            forwardingTables.put(node, fwt);
        }
    }

    /**
     * Prepare the base station adding the base station info 
     * @param baseStation
     */
    private void prepareBaseStation(INSENSRoutingLayer baseStation) {
        NeighborInfo n = new NeighborInfo();
        n.fromByteArray(baseStation.getNeighborInfo().toByteArray());
        networkNeighborsTable.put(baseStation.getNode().getId(), n);
    }

    /**
     *  Calculate the paths
     * @param path
     * @return
     */
    private Hashtable<Short,List<RoutingTableEntry>> path2RoutingTableEntryTable(List<Short> path) {
        short destination;
        short source;
        Hashtable<Short,List<RoutingTableEntry>> table = 
        		new Hashtable<Short,List<RoutingTableEntry>>();
        
        destination = path.get(path.size() - 1);
        source = path.get(0);
        
        for (int i = 1; i < path.size(); i++) {
            Short node = (Short) path.get(i);
            List<RoutingTableEntry> t = new LinkedList<RoutingTableEntry>();
            
            t.add(new RoutingTableEntry(source, destination, (Short) path.get(i - 1),i));
            
            if (i < path.size() - 1) {
                t.add(new RoutingTableEntry(destination, source, (Short) path.get(i + 1),i));
            }
            
            table.put(node, t);
        }
        
        return table;
    }

    /**
     * Gets the Forwarding Tables
     * @return
     */
    public Hashtable<Short, INSENSForwardingTable> getForwardingTables() {
        return forwardingTables;
    }

    /**
     ******************************************************************************************
     ******************************************************************************************
     ******************************************************************************************
     ******************************************************************************************
     ******************************************************************************************
     */
    /**
     * Perform the calculations related with building forwarding tables
     */
    public void calculateForwardingTables() {
        int numOfFDBKMessages = feedbackMessagesTable.entrySet().size();
                System.out.println("RECEIVED " + numOfFDBKMessages + " FDBK MESSAGES AT BASESTATION");
                long start = System.currentTimeMillis();
                long partial = start;
                
                System.out.println("INITIATED FORWARDING TABLES CALCULATION");
                prepareBaseStation(basestation);

                System.out.println("prepared base station IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");
                partial = System.currentTimeMillis();
                createGraph();
                System.out.println("created network graph IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");
                
                partial = System.currentTimeMillis();
                Short startId = (Short) getBasestation().getUniqueId();
                pathsFinder = new PathsFinder(graph, startId);
                List<List<Short>> paths1 = pathsFinder.findFirstPaths();
                paths.add(paths1);
                
                System.out.println("first path calculated IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");
                partial = System.currentTimeMillis();
                List<List<Short>> paths2 = pathsFinder.findOtherPaths(paths1);
                paths.add(paths2);
                System.out.println("second path calculated IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");

                partial = System.currentTimeMillis();
                buildForwardingTables(paths1, paths2);
                
                System.out.println("builded tables IN " + (System.currentTimeMillis() - partial) / 1000 + " SECONDS");
                System.out.println("ENDED FORWARDING TABLES CALCULATION IN " + (System.currentTimeMillis() - start) / 1000 + " SECONDS");
    }

    public void createGraph() {
        for (Short e1 : this.networkNeighborsTable.keySet()) { // por cada n� que enviou com sucesso feedback message
            NeighborInfo neighborInfo1 = networkNeighborsTable.get(e1); // ler a tabela de vizinhos
            
            if (neighborInfo1 != null) {
                for (Short e2 : neighborInfo1.keySet()) {
                    NeighborInfo neighborInfo2 = networkNeighborsTable.get(e2);
                    
                    if (neighborInfo2 != null) {
                        if (neighborInfo2.containsKey(e1)) {
                            graph.addVertex(e1);
                            graph.addVertex(e2);
                            
                            if (!graph.containsEdge(e2, e1) && !graph.containsEdge(e1, e2)) {
                                graph.addEdge(e1, e2);
                                graph.addEdge(e2, e1);
                            }
                        }
                    }
                }
            }
        }
    }

    private void buildForwardingTables(List<List<Short>> firstPathsL, List<List<Short>> secondPathsL) {
        allPaths = new Vector<List<Short>>();
        allPaths.addAll(firstPathsL);
        allPaths.addAll(secondPathsL);
        
        Comparator<List<Short>> c = new Comparator<List<Short>>() {

            public int compare(List<Short> o1, List<Short> o2) {
                return Integer.valueOf(o1.size()).compareTo(Integer.valueOf(o2.size()));
            }
        };
        Collections.sort(allPaths, c);
        
        
        for (int i = 0; i < allPaths.size(); i++) {
            List<Short> path = allPaths.get(i);
            
            if (path != null && path.size() >= 2) {
                Hashtable<Short,List<RoutingTableEntry>> table = path2RoutingTableEntryTable(path);
                int hopDistance = path.size()-1;
                Short destination = (Short)path.get(hopDistance);
                
                saveTable(table,destination,hopDistance);
            }
        }

    }
}
