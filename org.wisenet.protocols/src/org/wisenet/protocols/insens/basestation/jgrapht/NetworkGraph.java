package org.wisenet.protocols.insens.basestation.jgrapht;

import org.jgraph.graph.DefaultEdge;
import org.jgrapht.EdgeFactory;
import org.jgrapht.graph.DefaultDirectedGraph;

public class NetworkGraph extends DefaultDirectedGraph<Short, DefaultEdge>{

	private static final long serialVersionUID = 1L;

	
	public NetworkGraph(Class type) {
        super(type);
    }

    public NetworkGraph(EdgeFactory<Short, DefaultEdge> ef) {
        super(ef);
    }

    @Override
    public Object clone() {
        return super.clone();
    }
}
