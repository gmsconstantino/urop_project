package org.wisenet.protocols.insens.messages.evaluation;

import org.wisenet.protocols.insens.messages.INSENSDATAMessage;
import org.wisenet.simulator.components.instruments.IInstrumentMessage;

public class EvaluationINSENSDATAMessage extends INSENSDATAMessage implements IInstrumentMessage{

	
   public EvaluationINSENSDATAMessage() {
       super("A".getBytes());
   }

   /**
    * @param payload
    */
   public EvaluationINSENSDATAMessage(byte[] payload) {
       super(payload);
   }

   public short getSourceId() {
       return getSource();
   }

   public short getDestinationId() {
       return getDestination();
   }

   public void setSourceId(Object id) {
       setSource((Short) id);
   }

   public void setDestinationId(Object id) {
       setDestination((Short) id);
   }

   public void setUniqueId(Object id) {
       setID((Long) id);
   }

   public long getUniqueId() {
       return getID();
   }

   /**
    * @param id
    */
   public void setUniqueId(long id) {
       setID(id);
   }
}
