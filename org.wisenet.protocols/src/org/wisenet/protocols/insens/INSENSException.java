package org.wisenet.protocols.insens;

public class INSENSException extends Exception{

	private static final long serialVersionUID = 1L;

	
	/**
    * @param cause
    */
   public INSENSException(Throwable cause) {
       super(cause);
   }

   /**
    * @param message
    * @param cause
    */
   public INSENSException(String message, Throwable cause) {
       super(message, cause);
   }

   /**
    * @param message
    */
   public INSENSException(String message) {
       super(message);
   }

   public INSENSException() {
   }
}
