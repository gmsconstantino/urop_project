package org.wisenet.protocols.insens;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.protocols.common.routingattacks.BlackholeRoutingAttack;
import org.wisenet.protocols.common.routingattacks.HelloFloodingRountingAttack;
import org.wisenet.protocols.insens.basestation.BaseStationController;
import org.wisenet.protocols.insens.basestation.INSENSForwardingTable;
import org.wisenet.protocols.insens.messages.INSENSMessage;
import org.wisenet.protocols.insens.messages.INSENSMessagePayloadFactory;
import org.wisenet.protocols.insens.messages.data.DATAPayload;
import org.wisenet.protocols.insens.messages.data.FDBKPayload;
import org.wisenet.protocols.insens.messages.data.RREQPayload;
import org.wisenet.protocols.insens.messages.data.RUPDAckPayload;
import org.wisenet.protocols.insens.messages.data.RUPDPayload;
import org.wisenet.protocols.insens.utils.NeighborInfo;
import org.wisenet.protocols.insens.utils.NetworkKeyStore;
import org.wisenet.protocols.insens.utils.OneWaySequenceNumbersChain;
import org.wisenet.simulator.components.evaluation.tests.AbstractTest;
import org.wisenet.simulator.components.evaluation.tests.RouteBalancementTest;
import org.wisenet.simulator.components.instruments.IInstrumentHandler;
import org.wisenet.simulator.core.Application;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.events.DelayedMessageEvent;
import org.wisenet.simulator.core.events.Timer;
import org.wisenet.simulator.core.node.layers.routing.RoutingLayer;
import org.wisenet.simulator.core.node.layers.routing.attacks.AttacksEntry;
import org.wisenet.simulator.utilities.CryptoFunctions;

public class INSENSRoutingLayer extends RoutingLayer implements IInstrumentHandler{

	public static final String PHASE_SETUP = "SETUP";
	public static final String PHASE_ROUTE_REQUEST = "ROUTE_REQUEST";
	public static final String PHASE_ROUTE_FEEDBACK = "ROUTE_FEEDBACK";
	public static final String PHASE_FORWARD_DATA = "FORWARD_DATA";
	public static final String PHASE_ROUTE_UPDATE = "ROUTE_UPDATE";
	
	/**
	* Node info attributes
	*/
	private byte[] privateKey;
	private long OWS;
	private long roundOWS;
	private int roundNumber = 0;
	private NeighborInfo neighborInfo = new NeighborInfo();
	private byte[] myRoundMAC = null;
	private INSENSForwardingTable forwardingTable = new INSENSForwardingTable();
   
	/**
	 * Timers for actions control
	 */
	private Timer feedbackMessageStartTimer = null;
	private Timer queueMessageDispatchTimer = null;
	private Timer startForwardTablesCalculesTimer = null;
	private Timer forwardingTablesDispatchTimer = null;
	   
	/**
	 * Control structures
	 */
	//private BaseStationController baseStationController = null;
	private LinkedList<Message> messagesQueue = new LinkedList<Message>();
	private boolean sendingMessage;
	   
	/**
	 * Current phase
	 */
	protected String currentPhase = "NOPHASE_DEFINED";
		
	/**
	 * Counters for control protocol
	 */
	private short feedbackMessagesReceived = 0;
	private short lastFeedbackMessagesReceivedCheck = 0;
	    
	private boolean reliableMode = true;
	private boolean first = true;
	    
	private byte feedbackMessageRetries;
	    
	private Map<Integer,List<Short>> tableOfNodesByHops;
	private Map<Integer,byte[]> forwardingTablesRepository = null;
	    
	private List<Integer> orderedByHops;
	private List<Short> orderedByHopsNodes;
	    
	private Iterator<Integer> orderedByHopsIterator;
	private Iterator<Short> orderedByHopsNodesIterator;
	private Iterator<byte[]> forwardingTablesRepositoryIter;
	  
	private int replyToRUPDCounter;
	private int retry = 0;
	    
	private BaseStationController baseStationController;
	    
	
	public BaseStationController getBaseStationController(){
		return baseStationController;
	}
	    
    @Override
	protected void onStartUp() {
		setCurrentPhase(PHASE_SETUP);
		if (getNode().isSinkNode()) {
            baseStationController = new BaseStationController(this);
        }
		createNodeKeys();
		initiateSequenceNumber();
		createTimers();
		startProtocol();
		
	}
	    
    @Override
	public void newRound() {
		if (getNode().isSinkNode()) {
            newRoutingDiscover();
        }
	}
	    
	@Override
	protected String getRoutingTable() {
		return getForwardingTable().toString();
	}
		
	/**
     * Receive a message from the MAC Layer
     * @param message
     */
	@Override
	protected void onReceiveMessage(Object message) {
		if (message instanceof INSENSMessage) {
            try {
                INSENSMessage m = (INSENSMessage) message;
                byte type = INSENSFunctions.getMessageType(m);
                if (INSENSFunctions.verifyMAC(m.getPayload(), type)) {
                	switch (type) {
                        case INSENSConstants.MSG_ROUTE_REQUEST:
                            setCurrentPhase(PHASE_ROUTE_REQUEST);
                            getController().addMessageReceivedCounter(INSENSConstants.MSG_ROUTE_REQUEST);
                            processRREQMessage(m);
                            break;
                        case INSENSConstants.MSG_FEEDBACK:
                            setCurrentPhase(PHASE_ROUTE_FEEDBACK);
                            getController().addMessageReceivedCounter(INSENSConstants.MSG_FEEDBACK);
                            processFDBKMessage(m);
                            break;
                        case INSENSConstants.MSG_ROUTE_UPDATE:
                            setCurrentPhase(PHASE_ROUTE_UPDATE);
                            getController().addMessageReceivedCounter(INSENSConstants.MSG_ROUTE_UPDATE);
                            processRUPDMessage(m);
                            break;
                        case INSENSConstants.MSG_ROUTE_UPDATE_ACK:
                            getController().addMessageReceivedCounter(INSENSConstants.MSG_ROUTE_UPDATE_ACK);
                            processRUPDACKMessage(m);
                            break;
                        case INSENSConstants.MSG_DATA:
                            setCurrentPhase(PHASE_FORWARD_DATA);
                            getController().addMessageReceivedCounter(INSENSConstants.MSG_DATA);
                            processDATAMessage(m);
                            break;
                    }
                }
            } catch (INSENSException ex) {
                log(ex);
            }
        }
		
	}

		@Override
		protected boolean onSendMessage(Object message, Application app) {
			if (isStable()) {
	            return sendDATAMessage((Message) message);
	        } else {
	        	System.out.println(getNode().getId()+" routing not stable, message not sent");
	            return false;
	        }
		}
		
	    

		@Override
		protected void onRouteMessage(Object message) {
			 try {
		            INSENSMessage m = (INSENSMessage) message;
		            byte type = INSENSFunctions.getMessageType(m);
		            byte[] new_payload = null;
		            short source = -1;
		            short destination = -1;
		            switch (type) {
		                case INSENSConstants.MSG_ROUTE_UPDATE:
		                    RUPDPayload payload = new RUPDPayload(m.getPayload());
		                    if (getForwardingTable().haveRoute(payload.destination, payload.source, payload.immediate)) {
		                        new_payload = INSENSMessagePayloadFactory.updateRUPDPayload(payload.source, payload.destination, getNode().getId(), payload.ows, payload.forwardingTable, payload.mac, this.getNode());
		                        source = payload.source;
		                        destination = payload.destination;
		                    }
		                    break;
		                case INSENSConstants.MSG_ROUTE_UPDATE_ACK:
		                    RUPDAckPayload payloadAck = new RUPDAckPayload(m.getPayload());
		                    if (getForwardingTable().haveRoute(payloadAck.destination, payloadAck.source, payloadAck.immediate)) {
		                        new_payload = INSENSMessagePayloadFactory.updateRUPDAckPayload(payloadAck.source, payloadAck.destination, getNode().getId(), payloadAck.ftUID, payloadAck.ows, payloadAck.mac, this.getNode());
		                        source = payloadAck.source;
		                        destination = payloadAck.destination;
		                    }
		                    break;
		                case INSENSConstants.MSG_DATA:
		                    DATAPayload payloadData = new DATAPayload(m.getPayload());
		                    lastMessageSenderId = payloadData.immediate;
		                    if (getForwardingTable().haveRoute(payloadData.destination, payloadData.source, payloadData.immediate)) {
		                        new_payload = INSENSMessagePayloadFactory.updateDATAPayload(payloadData.source, payloadData.destination, getNode().getId(), payloadData.data, payloadData.mac, this.getNode());
		                        source = payloadData.source;
		                        destination = payloadData.destination;
		                    }
		                    break;
		                default:
		                    break;
		            }
		            if (new_payload != null) {
	                    m.setPayload(new_payload);
	                    getController().addMessageSentCounter(type);
	                    send((Message) m.clone());
	                    log("Forward Message from " + source + " to " + destination);
	                }
		        } catch (CloneNotSupportedException ex) {
		            log(ex);
		        } catch (INSENSException ex) {
		            log(ex);
		        }
			
		}

		@Override
		protected void startupAttacks() {
			// TODO Auto-generated method stub
		}

		@Override
		protected void sendMessageToAir(Object message) {
			messagesQueue.addLast((Message) message);
	        if (queueMessageDispatchTimer.isStop()) {
	            sendingMessage = false;
	            queueMessageDispatchTimer.start();
	        }
			
		}

		@Override
		public void sendMessageDone() {
			sendingMessage = false;
	        messagesQueue.poll();
		}

		@Override
		protected void onStable(boolean oldValue) {
			// TODO Auto-generated method stub
		}

		@Override
		protected void initAttacks() {
			AttacksEntry entry = new AttacksEntry(true, "Blackhole Attack", new BlackholeRoutingAttack(this));
	        attacks.addEntry(entry);
	        AttacksEntry entry2 = new AttacksEntry(false, "Hello Flooding Attack", new HelloFloodingRountingAttack(this));
	        attacks.addEntry(entry2);
			
		}

		
		 private void processRREQMessage(INSENSMessage m) throws INSENSException {
		        boolean isParent = false;
		        RREQPayload payload = new RREQPayload(m.getPayload());
		        if (!getNode().isSinkNode()) {
		
		            if (isFirstTime(payload)) {
		                if (getNode().getMacLayer().getSignalStrength() > INSENSConstants.SIGNAL_STRENGH_THRESHOLD && getNode().getMacLayer().getNoiseStrength() < INSENSConstants.SIGNAL_NOISE_THRESHOLD) {
		                    if (owsIsValid(payload)) {
		                        isParent = true;
		                        roundOWS = payload.ows; // updates the round ows
		                        log("Received RREQ From " + m.getSourceId());
		                        rebroadcastRREQMessage(payload);
		                        feedbackMessageStartTimer.start();
		                    } else {
		                        log("OWS is invalid");
		                        return;
		                    }
		                } else {
		                    log("SIGNAL STRENGH = " + getNode().getMacLayer().getSignalStrength());
		                }
		            }
		        }
		        neighborInfo.addNeighbor(payload.sourceId, payload.mac, isParent);
		 }
		
		 
		 /**
	     * Process a Feedback message message
	     * @param m
	     */
	    private void processFDBKMessage(INSENSMessage m) throws INSENSException {
	        FDBKPayload payload = new FDBKPayload(m.getPayload());
	        if (Arrays.equals(myRoundMAC, payload.parent_mac)) { // is from my child
	            if (getNode().isSinkNode()) { // if i'm a sink node keep the information
	            	getBaseStationController().addFeedbackMessages(payload);
	                INSENSFunctions.decryptData(getNode(), payload.neighborInfo, null);
	                feedbackMessagesReceived++;
	            } else { // forward the message if is from my child
	                byte[] new_payload = modifiedParentMAC(payload);
	                getController().addMessageSentCounter(INSENSConstants.MSG_FEEDBACK);
	                send(new INSENSMessage(new_payload));
//		                sendACKFeedbackMessage(payload);
	                log("Forward FDBK Message From Child " + payload.sourceId);
	            }
	        }// else drop it
	    }
	    
	    /**
	     * Process a Route Update Message
	     * @param m
	     */
	    private void processRUPDMessage(INSENSMessage m) {
	        try {
	            if (getNode().isSinkNode()) {
	                return;
	            }
	            RUPDPayload payload = new RUPDPayload(m.getPayload());
	            if (payload.destination == getNode().getId()) { // It's for me :)
	                if (isStable()) {
	                    replyToRUPDMessage(payload);
	                } else {
	                    updateRoutingStatus(payload);
	                }
	            } else {
	                if (isStable()) {
	                    routeMessage(m);
	                }
	            }
	        } catch (INSENSException ex) {
	            log(ex);
	        }
	    }
	    
	    /**
	     * Process a RUPD message
	     * With this method we can get some reliable RouteUpdate messages
	     * by controlling the message acknowledge
	     * @param m
	     */
	    private void processRUPDACKMessage(INSENSMessage m) {
	        if (getNode().isSinkNode()) {
	            ackRouteUpdate(m);
	        } else {
	            if (isStable()) {
	                routeMessage(m);
	            }
	        }

	    }
	    
	    /**
	     * When routing is stable we can process a data message
	     * this messages are application specific 
	     * @param m
	     *          the message
	     */
	    private void processDATAMessage(INSENSMessage m) {
	        if (isStable()) {
	            if (!itsForMe(m)) {
	                routeMessage(m);
	            } else {
	                try {
	                    DATAPayload payload = new DATAPayload(m.getPayload());
	                    lastMessageSenderId = payload.immediate;
	                    if (getController().isTesting() && getNode().isSinkNode()) {//sink node only
	                        AbstractTest test = getController().getActiveTest();
	                        
	                        if(test instanceof RouteBalancementTest){
	                        	(test.getEvaluationManager()).registerMessagePassage(m, this);
	                        }   
	                    }
	                    INSENSFunctions.decryptData(getNode(), payload.data, null);
	                    getNode().getApplication().receiveMessage(payload.data);
	                    done(m);
	                } catch (INSENSException ex) {
	                    log(ex);
	                }
	            }
	        }
	    }
	    
	    
	    /**
	     * Verify if the message its for me 
	     * @param m
	     * @return
	     */
	    private boolean itsForMe(INSENSMessage m) {
	        try {
	            DATAPayload payload = new DATAPayload(m.getPayload());
	            if (payload.destination == getNode().getId()) {
	                return true;
	            }
	        } catch (INSENSException ex) {
	            log(ex);
	        }
	        return false;

	    }
	    private void ackRouteUpdate(INSENSMessage m) {
	        RUPDAckPayload ackPayload;
	        try {
	            ackPayload = new RUPDAckPayload(m.getPayload());
	            forwardingTablesRepository.remove(ackPayload.ftUID);
	        } catch (INSENSException ex) {
	            Logger.getLogger(INSENSRoutingLayer.class.getName()).log(Level.SEVERE, null, ex);
	        }

	    }
	    
	    /**
	     * Send a route update message acknowledge to base station
	     * @param payload
	     */
	    private void replyToRUPDMessage(RUPDPayload payload) {
	        if (replyToRUPDCounter < 3) {
	            replyToRUPDCounter++;
	            byte[] payloadResponse = INSENSMessagePayloadFactory.createRUPDReplyPayload(getNode().getId(), (Short) payload.source, getNode().getId(), payload.forwardingTable.getUniqueId(), OWS, privateKey, this.getNode());
	            getController().addMessageSentCounter(INSENSConstants.MSG_ROUTE_UPDATE_ACK);
	            send(new INSENSMessage(payloadResponse));
	            log("Replying to RUPD Message");
	        }
	    }
	    
	    /**
	     * When receive a route update message, we must update the routing status
	     * of the node, ie. change status to stable (which can route messages)
	     * @param payload
	     */
	    private void updateRoutingStatus(RUPDPayload payload) {
	        forwardingTable = new INSENSForwardingTable(getNode().getId());
	        forwardingTable.setHopDistanceToBS(payload.forwardingTable.getHopDistanceToBS());
	        getForwardingTable().addAll(payload.forwardingTable);
	        setStable(true);
	        replyToRUPDMessage(payload);
	    }
	    
	    /**
	     * Modify the parentMAC
	     * @param old_payload
	     * @return
	     */
	    private byte[] modifiedParentMAC(FDBKPayload old_payload) {
	        try {
	            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
	            bados.writeByte(old_payload.type);
	            bados.writeShort(old_payload.sourceId);
	            bados.writeInt(old_payload.neighborInfoSize);
	            bados.write(old_payload.neighborInfo);
	            bados.write(neighborInfo.getParentMac());
	            bados.write(old_payload.mac);
	            return bados.toByteArray();
	        } catch (IOException ex) {
	            log(ex);
	        }
	        return null;
	    }
		 
		 /**
	     * Verify if ows is valid
	     * @param payload
	     * @return
	     */
	    private boolean owsIsValid(RREQPayload payload) {
	        return OneWaySequenceNumbersChain.verifySequenceNumber(OWS, payload.ows);
	    }
	 
	 /**
	     * Verify if is the first time this node receives a RREQ in current round
	     * @param payload
	     * @return
	     */
	    private boolean isFirstTime(RREQPayload payload) {
	        return roundOWS != payload.ows;
	    }
	    
	    /**
	     * Broadcast the RREQ message updating important fields
	     * @param payload
	     */
	    private void rebroadcastRREQMessage(RREQPayload rreqPayload) throws INSENSException {
	        byte[] payload = INSENSMessagePayloadFactory.createREQPayload(getNode().getId(), rreqPayload.ows, privateKey, rreqPayload.mac, this.getNode());
	        INSENSMessage message = new INSENSMessage(payload);
	        RREQPayload dummy = new RREQPayload(payload);
	        myRoundMAC = Arrays.copyOf(dummy.mac, dummy.mac.length);
	        getController().addMessageSentCounter(INSENSConstants.MSG_ROUTE_REQUEST);
	        send(message);
	    }   
		 
		/**
	     * Send a DATA message 
	     * @param message
	     * @return
	     */
	    private boolean sendDATAMessage(Message message) {
	        INSENSMessage m = (INSENSMessage) encapsulateMessage(message);
	        send((Message) m);
	        return true;
	    }
	    
	    @Override
		protected Message encapsulateMessage(Message message) {
			
			INSENSMessage m = new INSENSMessage();
	        m.setUniqueId(message.getUniqueId());
	        m.setSourceId(message.getSourceId());
	        m.setDestinationId(message.getDestinationId());
	        byte[] new_payload = encapsulateDataPayload(message);
	        m.setPayload(new_payload);
	        getController().addMessageSentCounter(INSENSConstants.MSG_DATA);
	        
	        return m;
	    }
		
		/**
	     * Create a DATA message payload
	     * @param message
	     * @return
	     */
	    private byte[] encapsulateDataPayload(Message message) {
	        return INSENSMessagePayloadFactory.createDATAPayload((Short) message.getSourceId(), (Short) message.getDestinationId(), getNode().getId(), message.getPayload(), privateKey, this.getNode());
	    }
		
		
		/**
	     * Sets the current phase of the routing protocol
	     * @param currentPhase
	     */
	    public void setCurrentPhase(String currentPhase) {
	        this.currentPhase = currentPhase;
	    }

	    /**
	     * Get neighbor info
	     * @return the neighbor information
	     */
	    public NeighborInfo getNeighborInfo() {
	        return neighborInfo;
	    }
	    
	    public String toString() {
	        return forwardingTable.toString();
	    }

	    public short getUniqueId() {
	        return getNode().getId();
	    }
	    
	    public INSENSForwardingTable getForwardingTable(){
	    	return forwardingTable;
	    }

	    /**
	     * Start the protocol execution, must be initiated by a node a base station
	     */
	    private void startProtocol() {
	        setCurrentPhase(PHASE_ROUTE_REQUEST);
	        queueMessageDispatchTimer.start();
	        if (getNode().isSinkNode()) {
	            newRoutingDiscover();
	        }
	    }
	    
	    /**
	     * Begins a new network organization
	     */
	    private void newRoutingDiscover() {
	        roundNumber++;
	        roundOWS = INSENSFunctions.getNextOWS();
	        log("Round " + roundNumber + " OWS: " + roundOWS);
	        startNetworkOrganization();
	    }
	    
	    
	    /**
	     * Create the initial request message for network organization
	     */
	    private void startNetworkOrganization() {
	        try {
	            /* create a initial route request message */
	            byte[] payload = INSENSMessagePayloadFactory.createREQPayload(getNode().getId(), roundOWS, privateKey, null, this.getNode());
	            INSENSMessage m = new INSENSMessage(payload);
	            RREQPayload dummy = new RREQPayload(payload);
	            myRoundMAC = dummy.mac;
	            getController().addMessageSentCounter(INSENSConstants.MSG_ROUTE_REQUEST);
	            send(m);
	            startForwardTablesCalculesTimer.start();
	        } catch (INSENSException ex) {
	            log(ex);
	        }
	    }
	    /**
	     * Create a node private key
	     */
	    private void createNodeKeys() {
	        privateKey = CryptoFunctions.createSkipjackKey();
	        NetworkKeyStore.getInstance().registerKey(getNode().getId(), privateKey);
	    }
	    
	    /**
	     * Load initial sequence number from the one way hash chain
	     */
	    private void initiateSequenceNumber() {
	        OWS = INSENSFunctions.getInitialSequenceNumber();
	    }
	    

	    /**
	     * Create the timers needed for operation
	     */
	    private void createTimers() {
	    	this.feedbackMessageStartTimer = new Timer(getNode().getSimulator(), 1, INSENSConstants.FEEDBACK_START_TIME_BOUND + getNode().getId() * 100) {

	            @Override
	            public void executeAction() {
	                sendFeedbackMessageInfo();

	            }
	        };
	        this.startForwardTablesCalculesTimer = new Timer(getNode().getSimulator(), INSENSConstants.FEEDBACK_MSG_RECEIVED_TIME_BOUND) {

	            @Override
	            public void executeAction() {
	                startComputeRoutingInfo();
	            }
	        };
	        this.queueMessageDispatchTimer = new Timer(getNode().getSimulator(), INSENSConstants.MESSAGE_DISPATCH_RATE) {

	            @Override
	            public void executeAction() {
	                dispatchNextMessage();
	            }
	        };
	        
	        
	        this.forwardingTablesDispatchTimer = new Timer(getNode().getSimulator(), INSENSConstants.MESSAGE_DISPATCH_RATE *2) {
	            private int maxRetries;

	            
	            /*
	             * (non-Javadoc)
	             * TODO:Esta implementa�?‹o n est‡ descrita no paper...as tentativas de reenvio n‹o s‹o explicitadas la.
	             * @see org.wisenet.simulator.core.events.Timer#executeAction()
	             */
	            @Override
	            public void executeAction() {
	                if (forwardingTablesRepository.isEmpty()) {
	                    System.out.println("No more forwarding tables to send");
	                    forwardingTablesDispatchTimer.stop();
	                    
	                    //to be used by registerMessagePassage...
	                    forwardingTable = new INSENSForwardingTable(getNode().getId());
	                    forwardingTable.setHopDistanceToBS(0);
	                    
	                } else {
	                    if (orderedByHopsNodesIterator.hasNext()) {
	                        Short nodeId = orderedByHopsNodesIterator.next();
	                        sendForwardTable(getBaseStationController().getForwardingTables(), nodeId);
	                    } else {
	                        if (orderedByHopsIterator.hasNext()) {
	                            orderedByHopsNodes = tableOfNodesByHops.get(orderedByHopsIterator.next());
	                            orderedByHopsNodesIterator = orderedByHopsNodes.iterator();
	                        } else {

	                            if (first) {
	                                forwardingTablesRepositoryIter = new HashSet<byte[]>(forwardingTablesRepository.values()).iterator();
	                                first = false;
	                            }
	                            if (forwardingTablesRepositoryIter.hasNext()) {
	                                sendUpdateRouteMessage(forwardingTablesRepositoryIter.next());
	                            }
	                            if (!forwardingTablesRepositoryIter.hasNext() && !forwardingTablesRepository.isEmpty() && retry==0){
	                                maxRetries=forwardingTablesRepository.size()*3;
	                            }
	                            if (!forwardingTablesRepositoryIter.hasNext() && !forwardingTablesRepository.isEmpty() && retry < maxRetries) {
	                                forwardingTablesRepositoryIter = new HashSet<byte[]>(forwardingTablesRepository.values()).iterator();
	                                retry++;
	                            }else{
	                                 System.out.println("Stop sending forwarding tables: " + forwardingTablesRepository.size()+ " left");
	                                 forwardingTablesDispatchTimer.stop();
	                                 
	                               //to be used by registerMessagePassage...
	                                 forwardingTable = new INSENSForwardingTable(getNode().getId());
	                                 forwardingTable.setHopDistanceToBS(0);
	                            }
	                        }
	                    }
	                }
	            }
	        };
	    }
	    
	    
	    /**
	     * Start building routing info
	     */
	    private void startComputeRoutingInfo() {
	        if (canStartComputeRoutingInfo()) {
//	            new Thread(new Runnable() {
//	                public void run() {

	            log("Started to compute routing info");
	            getBaseStationController().calculateForwardingTables();
	            log("Number of Forwarding Tables:  " + getBaseStationController().getForwardingTables().size());
	            sendRouteUpdateMessages(getBaseStationController().getForwardingTables());
//	                }
//	            }).start();
	        }
	    }
	    
	    /**
	     * Verifies if can begin to compute the routing info
	     * @return
	     */
	    boolean canStartComputeRoutingInfo() {
	        /**
	         * Devo verificar se o numero de mensagens alterou
	         * se alterou entao actualizo e reinicio as tentativas
	         * se nao alterou depois de 3 checks entÃƒÂ£o posso iniciar a recepÃƒÂ§ÃƒÂ£o
	         */
	        if (feedbackMessagesReceived <= lastFeedbackMessagesReceivedCheck) {
	            if (feedbackMessageRetries >= INSENSConstants.FEEDBACK_MSG_RECEIVED_RETRIES) {
	                startForwardTablesCalculesTimer.stop();
	                return true;
	            } else {
	                feedbackMessageRetries++;
	                return false;
	            }
	        } else {
	            lastFeedbackMessagesReceivedCheck = feedbackMessagesReceived;
	            feedbackMessageRetries = 0;
	            return false;
	        }
	    }

	    
	    /**
	     * Prepare the feedback message info to send to parent nodes
	     */
	    private void sendFeedbackMessageInfo() {
	        log("Send FeedBack Message ");
	        byte[] payload = INSENSMessagePayloadFactory.createFBKPayload(getNode().getId(),
	                privateKey, neighborInfo, neighborInfo.getParentMac(), this.getNode());
	        INSENSMessage message = new INSENSMessage(payload);
	        getController().addMessageSentCounter(INSENSConstants.MSG_FEEDBACK);
	        send(message);
//	        feedbackMessageStartTimer.reschedule();
	    }
	    
	    private void sendForwardTable(Hashtable<Short, INSENSForwardingTable> forwardingTables, Short id) {
	        if (forwardingTables.containsKey(id)) {
	            INSENSForwardingTable ft = forwardingTables.get(id);
	            byte[] payload = INSENSMessagePayloadFactory.createRUPDPayload(getNode().getId(), (Short) id, getNode().getId(), OWS, ft, privateKey, this.getNode());
	            sendUpdateRouteMessage(payload);
	        }
	    }

	    private void sendUpdateRouteMessage(byte[] payload) {
	        getController().addMessageSentCounter(INSENSConstants.MSG_ROUTE_UPDATE);
	        send(new INSENSMessage(payload));
	    }
	    

	    /**
	     * Dispatchs a message from the message queue
	     */
	    private void dispatchNextMessage() {
	        if (!sendingMessage) {
	            if (!messagesQueue.isEmpty()) {
	                /**
	                 * Este teste permite diminuir a densidade da rede em virtude de
	                 * dimunir as colisoes na rede
	                 */
//	              if(!getNode().getMacLayer().isReceiving() &&  !getNode().getMacLayer().isTransmitting()){
	                broadcastMessage((Message) messagesQueue.peek());
//	              }
	            } else {
	                queueMessageDispatchTimer.stop();
	            }
	        }
	    }
	    
	    /**
	     * Broadcast a message
	     * @param message
	     * @param reliable
	     */
	    private void broadcastMessage(Message message) {
	        sendingMessage = true;
	        long delay = (long) Simulator.randomGenerator.nextDoubleBetween((int) INSENSConstants.MIN_DELAYED_MESSAGE_BOUND, (int) INSENSConstants.MAX_DELAYED_MESSAGE_BOUND);
	        long time = (long) (Simulator.getSimulationTime());
	        DelayedMessageEvent delayMessageEvent = new DelayedMessageEvent(time, delay, message, getNode(), reliableMode);
	        delayMessageEvent.setReliable(reliableMode);
	        getNode().getSimulator().addEvent(delayMessageEvent);
	    }
	    
	    /**
	     * Send messages with pre-calculated routing tables
	     * @param forwardingTables
	     */
	    private void sendRouteUpdateMessages(Hashtable<Short, INSENSForwardingTable> forwardingTables) {
	        Vector<List<Short>> allpaths = getBaseStationController().getAllPaths();
	        Comparator<List<Short>> c = new Comparator<List<Short>>() {

	            public int compare(List<Short> o1, List<Short> o2) {
	                return Integer.valueOf(o1.size()).compareTo(Integer.valueOf(o2.size()));
	            }
	        };
	        Collections.sort(allpaths, c);
	        tableOfNodesByHops = new Hashtable<Integer,List<Short>>();
	        tableOfNodesByHops = buildListOfNodestByHops(allpaths);
	        sendForwardingTablesByHops(forwardingTables, tableOfNodesByHops);
	        setStable(true);
	    }
	    
	    /**
	     * Auxiliary function to create a list of nodes joined by number of hops
	     * @param allpaths
	     * @return
	     */
	    private Hashtable<Integer,List<Short>> buildListOfNodestByHops(Vector<List<Short>> allpaths) {
	        Hashtable<Integer,List<Short>> t = new Hashtable<Integer,List<Short>>();
	        for (List<Short> list : allpaths) {
	            if (list.size() > 0) {
	                List<Short> list2 = t.get(list.size());
	                
	                if (list2 == null) {
	                    list2 = new LinkedList<Short>();
	                }
	                
	                list2.add(list.get(list.size() - 1));
	                t.put(list.size(), list2);
	            }
	        }
	        
	        return t;
	    }
	    
	    /**
	     * For each hop number based nodes send a Forwarding table
	     * @param forwardingTables
	     * @param tableOfNodesByHops
	     */
	    private void sendForwardingTablesByHops(Map<Short, INSENSForwardingTable> forwardingTables, Map<Integer,List<Short>> tableOfNodesByHops) {
	        forwardingTablesRepository = new Hashtable<Integer,byte[]>();
	        orderedByHops = new LinkedList<Integer>(tableOfNodesByHops.keySet());
	        
	        Collections.sort(orderedByHops);
	        byte[] payload;
	        Set<Short> differentNodes = new HashSet<Short>();
	        INSENSForwardingTable ft = null;
	        int i = 0;
	        
	        for (Integer key : orderedByHops) {
	            List<Short> nodes = tableOfNodesByHops.get(key);
	            
	            for (Short id : nodes) {
	                differentNodes.add(id);
	                
	                if (forwardingTables.containsKey(id)) {
	                    ft = forwardingTables.get(id);
//	                    ft.setHopDistanceToBS((Integer)key-1);//paths contain sinkNode, so hopdistance is size-1
//	                    System.out.println("node "+id+" at "+((Integer)key-1)+" hops");
	                    payload = INSENSMessagePayloadFactory.createRUPDPayload(getNode().getId(), id, getNode().getId(), OWS, ft, privateKey, this.getNode());
	                    forwardingTablesRepository.put(new Integer(ft.getUniqueId()), payload);
	                    i++;
//	                    sendUpdateRouteMessage(payload);
	                }
	            }

	        }
	        
	        System.out.println("Total Forwarding Tables: " + i);
	        System.out.println("Total nodes having ft: " + differentNodes.size());

	        if(orderedByHops.isEmpty()){
	        	System.out.println("Error: No network possible.");
	        	return;
	        }
	        	
	        orderedByHopsIterator = orderedByHops.iterator();
	        orderedByHopsNodes = tableOfNodesByHops.get(orderedByHopsIterator.next());
	        orderedByHopsNodesIterator = orderedByHopsNodes.iterator();
	        forwardingTablesDispatchTimer.start();
	    }
}
