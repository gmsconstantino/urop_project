package org.wisenet.protocols.insens;

import org.wisenet.simulator.core.node.factories.AbstractNodeFactory;
import org.wisenet.simulator.core.node.layers.mac.Mica2MACLayer;

public class INSENSNodeFactory extends AbstractNodeFactory{

	@Override
	public void setup() {
		setApplicationClass(EvaluateINSENSApplication.class);
        setRoutingLayerClass(INSENSRoutingLayer.class);
        setNodeClass(INSENSNode.class);
        setMacLayer(Mica2MACLayer.class);
        setSetup(true);
		
	}
}
