package org.wisenet.protocols.insens.utils;

import java.util.Hashtable;
import java.util.Map;


public class NetworkKeyStore extends Hashtable<Short, byte[]>{

	private static final long serialVersionUID = 1L;
	
	
	protected static NetworkKeyStore instance;

   /**
    * @return
    */
   public static NetworkKeyStore getInstance() {
       if (instance == null){
           instance = new NetworkKeyStore();
       }
       return instance;
   }

   /**
    * @param t
    */
   public NetworkKeyStore(Map<? extends Short, ? extends byte[]> t) {
       super(t);
   }


   public NetworkKeyStore() {
       super();
   }

   /**
    * @param initialCapacity
    */
   public NetworkKeyStore(int initialCapacity) {
       super(initialCapacity);
   }

   /**
    * @param initialCapacity
    * @param loadFactor
    */
   public NetworkKeyStore(int initialCapacity, float loadFactor) {
       super(initialCapacity, loadFactor);
   }

   /**
    * Get the registred key for node identified by id
    * @param id
    * @return
    * @throws NetworkKeyStoreException
    */
   public byte[] getNodeKey(short id) throws NetworkKeyStoreException {
       byte[] key = get(id);
       if (key == null) {
           throw new NetworkKeyStoreException("Cannot be found a key for this id: " + id);
       }
       return key;
   }

   /**
    *  Register a key for a node identified by id
    * @param id
    * @param key
    */
   public void registerKey(short id, byte[] key) {
       put(id, key);
   }
}
