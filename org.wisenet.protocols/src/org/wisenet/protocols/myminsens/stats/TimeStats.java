package org.wisenet.protocols.myminsens.stats;


public class TimeStats {
	
	private long routingTimeTotal = 0;
	private long consensusTimeTotal = 0;
	
	private int numRoutingEntries = 0;
	private int numConsensusEntries = 0;
	
	
	public TimeStats(){}


	public synchronized void addRoutingTime(long routingTime) {
		routingTimeTotal += routingTime;
		numRoutingEntries++;
	}

	public synchronized void addConsensusTime(long consensusTime) {
		consensusTimeTotal += consensusTime;
		numConsensusEntries++;
	}
	
	
	public double getRoutingTime(){
		if(numRoutingEntries == 0)
			return 0;
		else
			return routingTimeTotal / numRoutingEntries;
	}
	
	public double getConsensusTime(){
		if(numConsensusEntries == 0)
			return 0;
		else
			return consensusTimeTotal / numConsensusEntries;
	}
	
	public double getTotalTime(){
		if(numRoutingEntries == 0 && numConsensusEntries == 0)
			return 0;
		else
			return (routingTimeTotal + consensusTimeTotal) / (numRoutingEntries + numConsensusEntries);
	}
	
	public double getRoutingTimeRatio(){
		return (getRoutingTime() / getTotalTime()) * 100.0;
	}
	
	public double getConsensusTimeRatio(){
		return (getConsensusTime() / getTotalTime()) * 100.0;
	}
}
