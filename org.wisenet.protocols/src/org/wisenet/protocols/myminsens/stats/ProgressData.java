package org.wisenet.protocols.myminsens.stats;

import java.util.HashSet;
import java.util.Set;

public class ProgressData {

	private Set<Short> participants;
	private Set<Short> received;
	
	public ProgressData(Set<Short> participants){
		this.participants = participants;
		received = new HashSet<Short>();
	}
	
	
	public boolean addProgress(short p){
		if(participants.contains(p)){
			return received.add(p);
		}
		
		return false;
	}
	
	public int getMaxProgress(){
		return participants.size();
	}
	
	public int getCurrentProgress(){
		return received.size();
	}
	
	public boolean isProgressComplete(){
		return received.containsAll(participants);
	}
	
	public boolean isProgressStarted(){
		return received.size() > 0;
	}
	
	public boolean isProgressAboveThreshold(int v){
		return received.size() > v;
	}
	
	public Set<Short> getParticipants(){
		return participants;
	}
}
