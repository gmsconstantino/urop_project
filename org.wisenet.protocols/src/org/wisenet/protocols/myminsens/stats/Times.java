package org.wisenet.protocols.myminsens.stats;

public class Times {

	private long startTime = 0;
	private long consensusStartTime = 0;
	private long receivedTime = 0;
	
	
	public Times(){}
	
	public Times(long startTime, long consensusStartTime, long receivedTime){
		this.startTime = startTime;
		this.consensusStartTime = consensusStartTime;
		this.receivedTime = receivedTime;
	}

	
	public long getStartTime() {
		return startTime;
	}

	public long getConsensusStartTime() {
		return consensusStartTime;
	}

	public long getReceivedTime() {
		return receivedTime;
	}
}
