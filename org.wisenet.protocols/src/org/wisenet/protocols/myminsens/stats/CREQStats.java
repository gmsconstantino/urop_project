package org.wisenet.protocols.myminsens.stats;

import java.util.HashMap;
import java.util.Set;

public class CREQStats {

	private HashMap<Long, ProgressData> creqsStarted;
	private HashMap<Long, ProgressData> creqsFinished;
	
	
	public CREQStats(){
		creqsStarted = new HashMap<Long, ProgressData>();
		creqsFinished = new HashMap<Long, ProgressData>();
	}
	
	public synchronized boolean addCREQStarted(long msgId, short nodeId, Set<Short> participants){
		ProgressData prev = creqsStarted.get(msgId);
		
		if(prev == null){
			prev = new ProgressData(participants);
			creqsStarted.put(msgId, prev);
		}
		
		return prev.addProgress(nodeId);
	}
	
	public synchronized boolean addCREQFinished(long msgId, short nodeId){
		ProgressData prev = creqsFinished.get(msgId);
		
		if(prev == null){
			ProgressData started = creqsStarted.get(msgId);
			prev = new ProgressData(started.getParticipants());
			creqsFinished.put(msgId, prev);
		}
		
		return prev.addProgress(nodeId);
	}
	
	public int getCREQsStartedCount(){
		return creqsStarted.size();
	}
	
	public int getCREQsFinishedCount(){
		return creqsFinished.size();
	}
	
	public int getCREQsFinishedByFCount(){
		int count = 0;
		
		for(ProgressData pd: creqsFinished.values()){
			if(pd.getCurrentProgress() > getF(pd.getMaxProgress()))
				count++;
		}
		
		return count;
	}
	
	public double getCREQsFinishedPercentage(){
		return ((double) getCREQsFinishedCount() / (double) getCREQsStartedCount()) * 100.0;
	}
	
	public double getCREQsFinishedAboveFPercentage(){
		return ((double) getCREQsFinishedByFCount() / (double) getCREQsStartedCount()) * 100.0;
	}
	
	public double getCREQsStartedByAllCount(){			
		return getProgressCompleteCount(creqsStarted);
	}
	
	public int getCREQsFinishedByAllCount(){
		return getProgressCompleteCount(creqsFinished);
	}
	
	public double getCREQsFinishedByAllPercentage(){
		return ((double) getCREQsFinishedByAllCount() / (double) getCREQsStartedCount()) * 100.0;
	}
	
	
	private int getProgressCompleteCount(HashMap<Long,ProgressData> map){
		int count = 0;
		
		for(ProgressData p: map.values())
			if(p.isProgressComplete())
				count++;
		
		return count;
	}
	
	private int getF(int n){
		return (n + n / 3) / 2;
	}
}
