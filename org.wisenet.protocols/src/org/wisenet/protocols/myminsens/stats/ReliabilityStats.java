package org.wisenet.protocols.myminsens.stats;

import java.util.HashSet;
import java.util.Set;

public class ReliabilityStats {

	Set<Short> participants;
	Set<Short> receivedDests;
	
	public ReliabilityStats(Set<Short> dests){
		participants = dests;
		receivedDests = new HashSet<Short>();
	}
	
	
	public synchronized boolean add(short nodeId){
		if(participants.contains(nodeId) && !receivedDests.contains(nodeId)){
			receivedDests.add(nodeId);
			return true;
		}
		
		return false;
	}
	
	public int getNumDestinations(){
		return participants.size();
	}
	
	public int getReceivedSize(){
		return receivedDests.size();
	}
}
