package org.wisenet.protocols.myminsens.stats;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.wisenet.protocols.myminsens.utils.KillableThread;

public class StatsManager {

	private static StatsManager instance = new StatsManager();
	
	/* RELIABILITY DATA */
	private HashMap<Long, ReliabilityStats> receivedStats;
	private HashSet<Long> sentMessages;
	
	/* CREQ DATA */
	private CREQStats creqStats;
	
	/* TIMES */
	private HashMap<Long, TimeStats> timeStats;
	
	private ResultPrinter resultPrinter;
	
	private StatsManager(){
		receivedStats = new HashMap<Long, ReliabilityStats>();
		timeStats = new HashMap<Long, TimeStats>();
		sentMessages = new HashSet<Long>();
		
		creqStats = new CREQStats();
		
		resultPrinter = new ResultPrinter();
		resultPrinter.start();
	}
	
	public static StatsManager getInstance(){
		return instance;
	}
	
	
	public synchronized void addReceived(long msgId, Set<Short> participants, short nodeId, boolean consensus, long consensusTime){
		ReliabilityStats rs = receivedStats.get(msgId);
		
		if(rs == null){
			rs = new ReliabilityStats(participants);
			receivedStats.put(msgId, rs);
		}
		
		rs.add(nodeId);
		
		TimeStats ts = timeStats.get(msgId);
		if(ts == null){
			ts = new TimeStats();
			timeStats.put(msgId, ts);
		}
		
		ts.addConsensusTime(consensusTime);
	}
	
	public synchronized boolean addSentMsg(long msgId){		
		return sentMessages.add(msgId);
	}
	
	public int getNumReceivedMessages(){
		return receivedStats.size();
	}
	
	public int getNumReceivedByAllMessages(){
		int count = 0;
		
		for(ReliabilityStats rs: receivedStats.values()){
			if(rs.getNumDestinations() <= rs.getReceivedSize())
				count++;
		}
		
		return count;
	}
	
	public double getOneCopyReliabilityPercentage(){
		return ((double) getNumReceivedMessages() / (double) creqStats.getCREQsFinishedCount()) * 100;
	}
	
	public double getAboveThresholdReliabilityPercentage(){
		int numMsgs = 0;
		
		for(ReliabilityStats rs: receivedStats.values()){
			if(rs.getReceivedSize() > getF(rs.getNumDestinations()))
				numMsgs++;
		}
		
		return ((double) numMsgs / (double) creqStats.getCREQsFinishedCount()) * 100;
	}
	
	public double getAllCopiesReliabilityPercentage(){
		return ((double) getNumReceivedByAllMessages() / (double) creqStats.getCREQsFinishedCount()) * 100;
	}
	
	public synchronized boolean addCreqStarted(long msgId, short nodeId, Set<Short> participants, long routingTime){
		boolean res = creqStats.addCREQStarted(msgId, nodeId, participants);
		
		TimeStats ts = timeStats.get(msgId);
		if(ts == null){
			ts = new TimeStats();
			timeStats.put(msgId, ts);
		}
		ts.addRoutingTime(routingTime);
		
		return res;
	}
	
	public synchronized boolean addCreqFinished(long msgId, short nodeId){		
		return creqStats.addCREQFinished(msgId, nodeId);
	}
	
	public String getMeanRoutingAndConsensusTimes(){
		int numEntries = 0;
		long totalRoutingTime = 0;
		long totalConsensusTime = 0;
		
		for(TimeStats ts: timeStats.values()){
			totalRoutingTime += ts.getRoutingTime();
			totalConsensusTime += ts.getConsensusTime();
			numEntries++;
		}
		
		double meanRoutingTime = totalRoutingTime / numEntries;
		double meanConsensusTime = totalConsensusTime / numEntries;
		double meanTotalTime = meanConsensusTime + meanRoutingTime;
		
		return String.format("%.0f (%.2f) / %.0f (%.2f)", meanRoutingTime, (meanRoutingTime / meanTotalTime) * 100.0, meanConsensusTime, (meanConsensusTime / meanTotalTime) * 100.0);
	}
	
	public synchronized void printResults(){
		System.out.printf("" +
				"CREQs Started: %.2f \n" +
				"CREQs Finished(one/>f/all): %.2f / %.2f / %.2f \n" +
				"Reliability(one/>f/all): %.2f / %.2f / %.2f \n" +
				"Time(routing/consensus): %s \n",
				
				(double) creqStats.getCREQsStartedCount()/sentMessages.size()*100.0,
				creqStats.getCREQsFinishedPercentage(), creqStats.getCREQsFinishedAboveFPercentage(), creqStats.getCREQsFinishedByAllPercentage(),
				getOneCopyReliabilityPercentage(), getAboveThresholdReliabilityPercentage(), getAllCopiesReliabilityPercentage(),
				getMeanRoutingAndConsensusTimes());
	}
	
	private int getF(int n){
		return (n + n / 3) / 2;
	}
	
	
	private class ResultPrinter extends KillableThread{
		private long PERIOD = 10000;
		
		
		public ResultPrinter(){
			super();
		}
		
		
		public void run(){
			while(isRunning){
				if(!receivedStats.isEmpty())
					printResults();
				
				try {
					sleep(PERIOD);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
