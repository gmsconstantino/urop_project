package org.wisenet.protocols.myminsens;

import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.protocols.common.routingattacks.BlackholeRoutingAttack;
import org.wisenet.protocols.common.routingattacks.HelloFloodingRountingAttack;
import org.wisenet.protocols.myminsens.basestation.BaseStationController;
import org.wisenet.protocols.myminsens.basestation.MINSENSForwardingTable;
import org.wisenet.protocols.myminsens.basestation.INSENSForwardingTable;
import org.wisenet.protocols.myminsens.messages.MINSENSMessage;
import org.wisenet.protocols.myminsens.messages.MINSENSMessagePayloadFactory;
import org.wisenet.protocols.myminsens.messages.consensus.ConsensusMsg;
import org.wisenet.protocols.myminsens.messages.data.CREQPayload;
import org.wisenet.protocols.myminsens.messages.data.DATAPayload;
import org.wisenet.protocols.myminsens.messages.data.FDBKPayload;
import org.wisenet.protocols.myminsens.messages.data.RREQPayload;
import org.wisenet.protocols.myminsens.messages.data.RUPDAckPayload;
import org.wisenet.protocols.myminsens.messages.data.RUPDPayload;
import org.wisenet.protocols.myminsens.stats.StatsManager;
import org.wisenet.protocols.myminsens.utils.NeighborInfo;
import org.wisenet.protocols.myminsens.utils.NetworkKeyStore;
import org.wisenet.protocols.myminsens.utils.OneWaySequenceNumbersChain;
import org.wisenet.simulator.components.instruments.IInstrumentHandler;
import org.wisenet.simulator.core.Application;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.events.DelayedMessageEvent;
import org.wisenet.simulator.core.events.Timer;
import org.wisenet.simulator.core.node.layers.routing.RoutingLayer;
import org.wisenet.simulator.core.node.layers.routing.attacks.AttacksEntry;
import org.wisenet.simulator.utilities.CryptoFunctions;

public class MINSENSRoutingLayer extends RoutingLayer implements IInstrumentHandler  {

  
	/**
	 * Routing scaling configuration
	 */
	public static final int K = 3;
	public static final int K_RANDOM_ROUTES = 0;
	public static final int K_BALANCED_ROUTES = 1;
	public static final int ONE_RANDOM_ROUTE = 2;
	public static final int ONE_ROUND_ROBIN = 3;
	public static final int ALL = 4;
	
	public static final int FORWARDING_TYPE = 4;
	
	
	/**
	 * Route disjointness configuration
	 */
	public static final boolean USE_CONSENSUS_ON_ROUTES = false;
	
	
	/**
	 * Data consensus data structures and configuration
	 */
	public static final boolean USE_LOCAL_BYZANTINE_AGREEMENT = false;
	public static final boolean USE_GLOBAL_BYZANTINE_AGREEMENT = true;
	
	/**
	 * the number of equal replicas necessary to accept the value locally
	 * 
	 */
	public static final int LOCAL_BYZANTINE_AGREEMENT_COPIES_NUMBER = 6; 
	/**
	 *  the number of global votes por message value necessary to agree on the value
	 */
	public static final int GLOBAL_BYZANTINE_AGREEMENT_VOTES_NUMBER = 6;
	
	private Hashtable<Object, List<Message>> messagesTable = new Hashtable<Object, List<Message>>(); //key is messageId, value is a linkedList of all message replicas received
	
	
	/**
	 * route setup phases
	 */
   public static final String PHASE_SETUP = "SETUP";
   public static final String PHASE_ROUTE_REQUEST = "ROUTE_REQUEST";
   public static final String PHASE_ROUTE_FEEDBACK = "ROUTE_FEEDBACK";
   public static final String PHASE_FORWARD_DATA = "FORWARD_DATA";
   public static final String PHASE_ROUTE_UPDATE = "ROUTE_UPDATE";
   
   
   /**
    * Node info attributes
    */
   private Set<Short> baseStations;//each entry is a BS id
   private Hashtable<Short,byte[]> privateKey = new Hashtable<Short,byte[]>(); //one for each BS 
   private Hashtable<Short,Long> OWS = new Hashtable<Short,Long>(); 
   private Hashtable<Short,Long> roundOWS = new Hashtable<Short,Long>();
   private Hashtable<Short,Integer> roundNumber = new Hashtable<Short,Integer>();
   private Hashtable<Short,NeighborInfo> neighborInfo = new Hashtable<Short,NeighborInfo>();
   private Hashtable<Short,byte[]> myRoundMAC = new Hashtable<Short,byte[]>();
   /**
    * Timers for actions control
    */
   private Hashtable<Short,Timer> feedbackMessageStartTimer = new Hashtable<Short,Timer>();
   private Timer queueMessageDispatchTimer;
   private Timer startForwardTablesCalculesTimer;
   private Timer forwardingTablesDispatchTimer;
   
   
   /**
    * Control structures
    */
//   private BaseStationController baseStationController = null;
   private Queue<Message> messagesQueue = new LinkedList<Message>();
   private boolean sendingMessage;
   
	/**
     * Current phase
     */
    protected Hashtable<Short,String> currentPhase = new Hashtable<Short,String>();
	
    /**
     * Counters for control protocol
     */
    private short feedbackMessagesReceived = 0;
    private short lastFeedbackMessagesReceivedCheck = 0;
    
    private byte feedbackMessageRetries;
    
    private Hashtable<Integer,List<Short>> tableOfNodesByHops;
    private boolean reliableMode = true;
    private Hashtable<Integer,byte[]> forwardingTablesRepository = null;
    
    private List<Integer> orderedByHops;
    private Iterator<Integer> orderedByHopsIterator;
    
    private List<Short> orderedByHopsNodes;
    private Iterator<Short> orderedByHopsNodesIterator;
    
    private int replyToRUPDCounter;
    private Iterator<byte[]> forwardingTablesRepositoryIter;
    private boolean first = true;
    private int retry = 0;
    
    
    
    public BaseStationController getBaseStationController(){
    	return (BaseStationController)baseStationController;
    }
    
    @Override
	protected void onStartUp() {
		setCurrentPhase(PHASE_SETUP);
		setBaseStations(getNode().getSimulator().getSinkNodesIds());
		
		if (getNode().isSinkNode()) {
            baseStationController = new BaseStationController(this);
            getBaseStationController().setUSE_CONSENSUS_ON_ROUTES(USE_CONSENSUS_ON_ROUTES);
            roundNumber.put(getNode().getId(), 0);
            roundOWS.put(getNode().getId(), new Long(0));
            createNodeKeys(getNode().getId());
			initiateSequenceNumber(getNode().getId());
			createTimers(getNode().getId());
        }else{
        	for(Short id : baseStations){
        		roundNumber.put(id, 0);
        		roundOWS.put(id, new Long(0));
        		createNodeKeys(id);
    			initiateSequenceNumber(id);
    			createTimers(id);
    		}	
        }
		
		startProtocol();
	}
    
    @Override
	public void newRound() {
		if (getNode().isSinkNode()) {
            newRoutingDiscover();
        }
		
	}
    
	@Override
	protected String getRoutingTable() {
		return getForwardingTable().toString();
	}
	

	
	/**
     * Receive a message from the MAC Layer
     * @param message
     */
	@Override
	protected void onReceiveMessage(Object message) {
		if (message instanceof MINSENSMessage) {
            try {
                MINSENSMessage m = (MINSENSMessage) message;
                byte type = MINSENSFunctions.getMessageType(m);
                if (MINSENSFunctions.verifyMAC(m.getPayload(), type)) {
                    switch (type) {
                        case MINSENSConstants.MSG_ROUTE_REQUEST:
                            setCurrentPhase(PHASE_ROUTE_REQUEST);
                            getController().addMessageReceivedCounter(MINSENSConstants.MSG_ROUTE_REQUEST);
                            processRREQMessage(m);
                            break;
                        case MINSENSConstants.MSG_FEEDBACK:
                            setCurrentPhase(PHASE_ROUTE_FEEDBACK);
                            getController().addMessageReceivedCounter(MINSENSConstants.MSG_FEEDBACK);
                            processFDBKMessage(m);
                            break;
                        case MINSENSConstants.MSG_ROUTE_UPDATE:
                            setCurrentPhase(PHASE_ROUTE_UPDATE);
                            getController().addMessageReceivedCounter(MINSENSConstants.MSG_ROUTE_UPDATE);
                            processRUPDMessage(m);
                            break;
                        case MINSENSConstants.MSG_ROUTE_UPDATE_ACK:
                            getController().addMessageReceivedCounter(MINSENSConstants.MSG_ROUTE_UPDATE_ACK);
                            processRUPDACKMessage(m);
                            break;
                        case MINSENSConstants.MSG_DATA:
                            setCurrentPhase(PHASE_FORWARD_DATA);
                            getController().addMessageReceivedCounter(MINSENSConstants.MSG_DATA);
                            processDATAMessage(m);
                            break;
                        case MINSENSConstants.MSG_CONSENSUS_REQUEST:
                        	processCREQMessage(m);
                        	break;
                        case MINSENSConstants.MSG_CONSENSUS:
                        	processConsensusMessage(m);
                        	break;
                    }
                }
            } catch (MINSENSException ex) {
                log(ex);
            } catch (IOException e) {
				log(e);
			}
        }
		
	}

	@Override
	protected boolean onSendMessage(Object message, Application app) {
		if (isStable()) {
            return sendDATAMessage((Message) message);
        } else {
        	System.out.println(getNode().getId()+" routing not stable, message not sent");
            return false;
        }
	}
	
    

	@Override
	protected void onRouteMessage(Object message) {
		 try {
	            MINSENSMessage m = (MINSENSMessage) message;
	            byte type = MINSENSFunctions.getMessageType(m);
	            byte[] new_payload = null;
	            short source = -1;
	            short destination = -1;
	            switch (type) {
	                case MINSENSConstants.MSG_ROUTE_UPDATE:
	                    RUPDPayload payload = new RUPDPayload(m.getPayload());
	                    if (getForwardingTable().haveRoute(payload.destination, payload.source, payload.immediate, payload.routeIds)) {
	                        new_payload = MINSENSMessagePayloadFactory.updateRUPDPayload(payload.source, payload.destination, getNode().getId(), payload.routeIds, payload.ows, payload.forwardingTable, payload.mac, this.getNode());
	                        source = payload.source;
	                        destination = payload.destination;
	                    }
	                    break;
	                case MINSENSConstants.MSG_ROUTE_UPDATE_ACK:
	                    RUPDAckPayload payloadAck = new RUPDAckPayload(m.getPayload());
	                    if (getForwardingTable().haveRoute(payloadAck.destination, payloadAck.source, payloadAck.immediate, payloadAck.routeIds)) {
	                        new_payload = MINSENSMessagePayloadFactory.updateRUPDAckPayload(payloadAck.source, payloadAck.destination, getNode().getId(), payloadAck.routeIds, payloadAck.ftUID, payloadAck.ows, payloadAck.mac, this.getNode());
	                        source = payloadAck.source;
	                        destination = payloadAck.destination;
	                    }
	                    break;
	                case MINSENSConstants.MSG_DATA:
	                    DATAPayload payloadData = new DATAPayload(m.getPayload());
	                    lastMessageSenderId = payloadData.immediate;
	                    if (getForwardingTable().haveRoute(payloadData.destination, payloadData.source, payloadData.immediate, payloadData.routeIds)) {
	                        new_payload = MINSENSMessagePayloadFactory.updateDATAPayloadImmediate(payloadData, getNode().getId());
	                        source = payloadData.source;
	                        destination = payloadData.destination;
	                    }
	                    break;
	                default:
	                    break;
	            }
	            if (new_payload != null) {
                    m.setPayload(new_payload);
                    getController().addMessageSentCounter(type);
                    send(m);
                    log("Forward Message from " + source + " to " + destination);
                }
	        } catch (MINSENSException ex) {
	            log(ex);
	        }
		
	}

	@Override
	protected void startupAttacks() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void sendMessageToAir(Object message){		
		messagesQueue.add((Message) message);
		
        if (queueMessageDispatchTimer.isStop()) {
            sendingMessage = false;
            queueMessageDispatchTimer.start();
        }
	}

	@Override
	public void sendMessageDone() {
		sendingMessage = false;
		//messagesQueue.remove(0);
	}	

	@Override
	protected void onStable(boolean oldValue) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void initAttacks() {
		AttacksEntry entry = new AttacksEntry(true, "Blackhole Attack", new BlackholeRoutingAttack(this));
        attacks.addEntry(entry);
        AttacksEntry entry2 = new AttacksEntry(false, "Hello Flooding Attack", new HelloFloodingRountingAttack(this));
        attacks.addEntry(entry2);
		
	}

	private void processRREQMessage(MINSENSMessage m) throws MINSENSException {
		boolean isParent = false;
	    RREQPayload payload = new RREQPayload(m.getPayload());
	    if (!getNode().isSinkNode()) {
	    	if (isFirstTime(payload)) {
	    		if (getNode().getMacLayer().getSignalStrength() > MINSENSConstants.SIGNAL_STRENGH_THRESHOLD && getNode().getMacLayer().getNoiseStrength() < MINSENSConstants.SIGNAL_NOISE_THRESHOLD) {
	    			if (owsIsValid(payload)) {
	    				isParent = true;
//	                    roundOWS = payload.ows; // updates the round ows
	    				OWS.put(payload.bsId, payload.ows); // updates the round ows
	    				log("Received RREQ From " + m.getSourceId());
	    				rebroadcastRREQMessage(payload);
	    				feedbackMessageStartTimer.get(payload.bsId).start();//start this baseStation feedbackTimer..
	    				} else{
	    					log("OWS is invalid");
	    					return;
	                    }
	                } else {
	                	log("SIGNAL STRENGH = " + getNode().getMacLayer().getSignalStrength());
	                }
	    		}
	    	}
	        NeighborInfo n = neighborInfo.get(payload.bsId);
	        if( n== null)
	        	n = new NeighborInfo();
	        
	        n.addNeighbor(payload.sourceId, payload.mac, isParent);
	        neighborInfo.put(payload.bsId,n);
	 }
	
	 
	 /**
     * Process a Feedback message message
     * @param m
     */
    private void processFDBKMessage(MINSENSMessage m) throws MINSENSException {
        FDBKPayload payload = new FDBKPayload(m.getPayload());
        Short destBs = (Short)m.getDestinationId();
        if (Arrays.equals(myRoundMAC.get(destBs), payload.parent_mac)) { // is from my child
            if (getNode().isSinkNode()) { // if i'm a sink node keep the information
            	getBaseStationController().addFeedbackMessages(payload);
                MINSENSFunctions.decryptData(getNode(), payload.neighborInfo, null);
                feedbackMessagesReceived++;
            } else { // forward the message if is from my child
            	byte[] bsParentMac = neighborInfo.get(destBs).getParentMac();
                byte[] new_payload = modifiedParentMAC(payload,bsParentMac);
                getController().addMessageSentCounter(MINSENSConstants.MSG_FEEDBACK);
                MINSENSMessage m2 = new MINSENSMessage(new_payload);
                m2.setDestinationId(m.getDestinationId());
                send(m2);
//	                sendACKFeedbackMessage(payload);
                log("Forward FDBK Message From Child " + payload.sourceId);
            }
        }// else drop it
    }
    
    /**
     * Process a Route Update Message
     * @param m
     */
    private void processRUPDMessage(MINSENSMessage m) {
        try {
            if (getNode().isSinkNode()) {
                return;
            }
            RUPDPayload payload = new RUPDPayload(m.getPayload());
            if (payload.destination == getNode().getId()) { // It's for me :)
                if (isStable() && getForwardingTable().isStableOnBs(payload.source)) {
                    replyToRUPDMessage(payload);
                } else {
                    updateRoutingStatus(payload);
                }
            } else {
                if (isStable()) {
                    routeMessage(m);
                }
            }
        } catch (MINSENSException ex) {
            log(ex);
        }
    }
    
    /**
     * Process a RUPD message
     * With this method we can get some reliable RouteUpdate messages
     * by controlling the message acknowledge
     * @param m
     */
    private void processRUPDACKMessage(MINSENSMessage m) {
        if (getNode().isSinkNode()){
        	if (itsForMe(m)) 
        		ackRouteUpdate(m);   
        } else {
            if (isStable()) {
                routeMessage(m);
            }
        }
    }
    
    /**
     * When routing is stable we can process a data message
     * this messages are application specific 
     * @param m
     *          the message
     */
    private void processDATAMessage(MINSENSMessage m) {
        if (isStable()) {
            if (!itsForMe(m) && !getNode().isSinkNode()) {
            	try {
					MINSENSMessage msg = (MINSENSMessage) m.clone();
					DATAPayload dp = new DATAPayload(msg.getPayload());
					int routeId = ((MINSENSForwardingTable) forwardingTable).getRouteId(dp.source, dp.destination, dp.immediate);
					
					if(dp.routeIds.contains(routeId)){
						if(dp.routeId == -1){
							msg.setPayload(MINSENSMessagePayloadFactory.updateDATAPayloadRouteId(dp, routeId));
						}
						
						routeMessage(msg);
					}
				} catch (MINSENSException e) {
					e.printStackTrace();
				}
            } 
            else {
                try {
                    DATAPayload payload = new DATAPayload(m.getPayload());
                    lastMessageSenderId = payload.immediate;
                    getBaseStationController().receiveReplica(m);
                } catch (MINSENSException ex) {
                    log(ex);
                } catch (IOException e) {
					log(e);
				}
            }
        }
    }
    
    private void processCREQMessage(MINSENSMessage m) throws MINSENSException{
    	if(getNode().isSinkNode()){
    		getBaseStationController().receiveCREQMessage(new CREQPayload(m.getPayload()));
    	}
    }
    
    private void processConsensusMessage(MINSENSMessage m) throws MINSENSException, IOException{
    	if(getNode().isSinkNode()){
    		ConsensusMsg msg = new ConsensusMsg(m.getPayload());
    		
    		if(msg.getDestinations().contains(getNode().getId())){
    			getBaseStationController().receiveConsensusMessage(msg);
    		}
    	}
    }
    
    public void sendCREQMessage(byte[] creqPayloadBytes) throws IOException{
    	sendMessageToAir(new MINSENSMessage(creqPayloadBytes));
    }
    
    public void sendConsensusMessage(ConsensusMsg msg) throws IOException{
    	sendMessageToAir(new MINSENSMessage(msg.toByteArray()));
    }
    
    public void sendPEBMessage(byte[] pebPayload){
    	sendMessageToAir(new MINSENSMessage(pebPayload));
    }
    
    /**
     * Verify if the message its for me 
     * @param m
     * @return
     */
    private boolean itsForMe(MINSENSMessage m) {
        try {
        	 byte type = MINSENSFunctions.getMessageType(m);
        	 switch (type) {
             case MINSENSConstants.MSG_ROUTE_UPDATE:
            	  RUPDPayload payload2 = new RUPDPayload(m.getPayload());
            	  if (payload2.destination == getNode().getId()) {
                      return true;
             	 }else
                      return false;
             case MINSENSConstants.MSG_ROUTE_UPDATE_ACK:
            	 RUPDAckPayload ackPayload = new RUPDAckPayload(m.getPayload());
            	 if (ackPayload.destination == getNode().getId()) {
                     return true;
            	 }else
                     return false;
             case MINSENSConstants.MSG_DATA:
            	 DATAPayload payload = new DATAPayload(m.getPayload());
            	 if (payload.destination == getNode().getId()) {
                     return true;
            	 }else
                     return false;
        	 }	
        } catch (MINSENSException ex) {
            log(ex);
        }
        return false;

    }
    private void ackRouteUpdate(MINSENSMessage m) {
        RUPDAckPayload ackPayload;
        try {
            ackPayload = new RUPDAckPayload(m.getPayload());
            forwardingTablesRepository.remove(ackPayload.ftUID);
        } catch (MINSENSException ex) {
            Logger.getLogger(MINSENSRoutingLayer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    /**
     * Send a route update message acknowledge to base station
     * @param payload
     */
    private void replyToRUPDMessage(RUPDPayload payload) {
        if (replyToRUPDCounter < 3) {
            replyToRUPDCounter++;
            
            Short bsId = payload.source;
            Long ows = OWS.get(bsId);
            byte[] privateKey = this.privateKey.get(bsId);
            byte[] payloadResponse = MINSENSMessagePayloadFactory.createRUPDReplyPayload(getNode().getId(), (Short) payload.source, getNode().getId(), payload.routeIds, payload.forwardingTable.getUniqueId(), ows, privateKey, this.getNode());
            getController().addMessageSentCounter(MINSENSConstants.MSG_ROUTE_UPDATE_ACK);
            send(new MINSENSMessage(payloadResponse));
            log("Replying to RUPD Message");
        }
    }
    
    /**
     * When receive a route update message, we must update the routing status
     * of the node, ie. change status to stable (which can route messages)
     * @param payload
     */
    private void updateRoutingStatus(RUPDPayload payload) {
    	if(forwardingTable == null)
    		forwardingTable = new MINSENSForwardingTable(getNode().getId());
    	
//        forwardingTable.setHopDistanceToBS(payload.forwardingTable.getHopDistanceToBS());
//        getForwardingTable().addAll(payload.forwardingTable);
//        
        getForwardingTable().add(payload.source, payload.forwardingTable);
        
        setStable(true);
        replyToRUPDMessage(payload);
    }
    
    /**
     * Modify the parentMAC
     * @param old_payload
     * @return
     */
    private byte[] modifiedParentMAC(FDBKPayload old_payload, byte[] parentMac) {
        try {
            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
            bados.writeByte(old_payload.type);
            bados.writeShort(old_payload.sourceId);
            bados.writeInt(old_payload.neighborInfoSize);
            bados.write(old_payload.neighborInfo);
            bados.write(parentMac);
            bados.write(old_payload.mac);
            return bados.toByteArray();
        } catch (IOException ex) {
            log(ex);
        }
        return null;
    }
	 
	 /**
     * Verify if ows is valid
     * @param payload
     * @return
     */
    private boolean owsIsValid(RREQPayload payload) {
    	Short bsId = payload.bsId;
    	Long bsOWS = OWS.get(bsId);
        return OneWaySequenceNumbersChain.verifySequenceNumber(bsOWS, payload.ows);
    }
 
 /**
     * Verify if is the first time this node receives a RREQ in current round
     * @param payload
     * @return
     */
    private boolean isFirstTime(RREQPayload payload) {
        Short bsId = payload.bsId;
        Long bsOWS = roundOWS.get(bsId);
    	if (bsOWS == payload.ows) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Broadcast the RREQ message updating important fieds
     * @param payload
     */
    private void rebroadcastRREQMessage(RREQPayload rreqPayload) throws MINSENSException {
    	byte[] privateKey = this.privateKey.get(rreqPayload.bsId);
        byte[] payload = MINSENSMessagePayloadFactory.createREQPayload(rreqPayload.bsId,getNode().getId(), rreqPayload.ows, privateKey, rreqPayload.mac, this.getNode());
        MINSENSMessage message = new MINSENSMessage(payload);
        RREQPayload dummy = new RREQPayload(payload);
        
        byte[] roundMAC = Arrays.copyOf(dummy.mac, dummy.mac.length);
        myRoundMAC.put(rreqPayload.bsId, roundMAC);
        
        getController().addMessageSentCounter(MINSENSConstants.MSG_ROUTE_REQUEST);
        send(message);
    }   
	 
	/**
     * Send a DATA message 
     * @param message
     * @return
     */
    private boolean sendDATAMessage(Message message) {
    	
    	/*
    	 * Choose which routes to use here
    	 * 
    	 * For a given message to be sent, the routing layer sets which
    	 * routes it should travel by creating messages to base-stations
    	 * and assigning route ids to each message
    	 * 
    	 */
    	try{    		
    		Hashtable<Short,Set<Integer>> routes;
        	SimpleEntry<Short,Integer> route;
        	switch(FORWARDING_TYPE){
        		case K_BALANCED_ROUTES:
        			routes = getForwardingTable().getKRoutesBalancedByBS(K);
        			processRoutes(message, routes);
        			break;
        		case K_RANDOM_ROUTES:
        			routes = getForwardingTable().getKRandomRoutes(K);
        			processRoutes(message, routes);
        			break;
        		case ALL:
        			routes = getForwardingTable().getAllRoutes();
        			processRoutes(message, routes);
        			break;
        		case ONE_RANDOM_ROUTE:
        			route = getForwardingTable().getRandomRoute();
        			processOneRoute(message, route);
        			break;
        		case ONE_ROUND_ROBIN:
        			 route = getForwardingTable().getNextRouteRoundRobin();
        			processOneRoute(message, route);
        	}
    		return true;
    	}catch(CloneNotSupportedException e){
    		return false;
    	}
    }
    
    private void processRoutes(Message message, Hashtable<Short, Set<Integer>> routes) throws CloneNotSupportedException{
    	Set<Short> basestations = new HashSet<Short>();
    	for(Short s: routes.keySet())
    		basestations.add(s);
    	
    	for(Short bsId : basestations){
    		Set<Integer> routeIds = routes.get(bsId);
    		
    		MINSENSMessage m = createRoutingMessage(message, bsId, routeIds, basestations);
    		getForwardingTable().registerLastUsedBs(bsId);
    		send(m);
    	}
    }
    
    private void processOneRoute(Message message, SimpleEntry<Short,Integer> route) throws CloneNotSupportedException{
    	Short bsId = route.getKey();
    	
    	Set<Integer> routes = new HashSet<Integer>();
    	routes.add(route.getValue());
    	
    	Set<Short> basestation = new HashSet<Short>();
    	basestation.add(bsId);
    	
    	MINSENSMessage m = (MINSENSMessage) createRoutingMessage(message, bsId, routes, basestation);
    	getForwardingTable().registerLastUsedBs(route.getKey());
    	
        send(m);
    }
    
    private MINSENSMessage createRoutingMessage(Message message, short destBS, Set<Integer> routes, Set<Short> basestations) throws CloneNotSupportedException{
			Message toSend = (Message) message.clone();
			toSend.setDestinationId(destBS);
	    	MINSENSMessage m = new MINSENSMessage();
	        m.setUniqueId(toSend.getUniqueId());
	        m.setSourceId(toSend.getSourceId());
	        m.setDestinationId(toSend.getDestinationId());
	  	
	    	byte[] new_payload = encapsulateDataPayload(toSend, routes, basestations);
	        m.setPayload(new_payload);
	        StatsManager.getInstance().addSentMsg(m.getUniqueId());
	        
	        getController().addMessageSentCounter(MINSENSConstants.MSG_DATA);
	        return m;
    }
    
    @Override
	protected Message encapsulateMessage(Message message) {
		
		MINSENSMessage m = new MINSENSMessage();
        m.setUniqueId(message.getUniqueId());
        m.setSourceId(message.getSourceId());
        m.setDestinationId(message.getDestinationId());
        
        getController().addMessageSentCounter(MINSENSConstants.MSG_DATA);
        return m;
    }
	
	/**
     * Create a DATA message payload
     * @param message
     * @return
     */
    private byte[] encapsulateDataPayload(Message message, Set<Integer> routes, Set<Short> basestations) {
    	byte[] privateKey = this.privateKey.get(message.getDestinationId());
    	short source = message.getSourceId();
    	short dest = message.getDestinationId();
    	short immediate = getNode().getId();
    	Integer routeId = getForwardingTable().getRouteId(source, dest, (short) -1);
    	long startTime = getNode().getSimulator().getSimulation().getCurrentSimulationTime();
    	
        return MINSENSMessagePayloadFactory.createDATAPayload(source, dest, immediate, message.getUniqueId(), routeId, routes, basestations, startTime, message.getPayload(), privateKey, this.getNode());
    }
	
	
	/**
     * Sets the current phase of the routing protocol
     * @param currentPhase
     */
    public void setCurrentPhase(short bsId, String currentPhase) {
        this.currentPhase.put(bsId,  currentPhase);
    }
    
    public void setCurrentPhaseToAll(String currentPhase){
    	for(Short s : this.currentPhase.keySet()){
    		this.currentPhase.put(s, currentPhase);
    	}
    	
    }

    /**
     * Get neighbor info
     * @return the neighbor information
     */
    public NeighborInfo getNeighborInfo(short bsId) {
        return neighborInfo.get(bsId);
    }
    
    public Set<Short> getBaseStations() {
		return baseStations;
	}

	public void setBaseStations(Set<Short> baseStations) {
		this.baseStations = baseStations;
	}

	public String toString() {
        return forwardingTable.toString();
    }

    public short getUniqueId() {
        return getNode().getId();
    }
    
    public MINSENSForwardingTable getForwardingTable(){
    	return (MINSENSForwardingTable)forwardingTable;
    }

    public Hashtable<Object, List<Message>> getMessagesTable() {
		return messagesTable;
	}

	/**
     * Start the protocol execution, must be initiated by a node a base station
     */
    private void startProtocol() {
        setCurrentPhase(getNode().getId(),PHASE_ROUTE_REQUEST);
        queueMessageDispatchTimer.start();
        if (getNode().isSinkNode()) {
            newRoutingDiscover();
        }
    }
    
    /**
     * Begins a new network organization
     */
    private void newRoutingDiscover() {
        int currRoundNumber = roundNumber.get(getNode().getId());
        currRoundNumber++;
        roundNumber.put(getNode().getId(), currRoundNumber);
        long nextOWS = MINSENSFunctions.getNextOWS(getNode().getId());
        roundOWS.put(getNode().getId(), nextOWS); 
        log("BS: "+getNode().getId()+" Round " + roundNumber + " OWS: " + roundOWS);
        startNetworkOrganization();
    }
    
    
    /**
     * Create the initial request message for network organization
     */
    private void startNetworkOrganization() {
        try {
            /* create a initial route request message */
            byte[] payload = MINSENSMessagePayloadFactory.createREQPayload(getNode().getId(),getNode().getId(), roundOWS.get(getNode().getId()), privateKey.get(getNode().getId()), null, this.getNode());
            MINSENSMessage m = new MINSENSMessage(payload);
            RREQPayload dummy = new RREQPayload(payload);
            myRoundMAC.put(getNode().getId(), dummy.mac);
            getController().addMessageSentCounter(MINSENSConstants.MSG_ROUTE_REQUEST);
            send(m);
            startForwardTablesCalculesTimer.start();
        } catch (MINSENSException ex) {
            log(ex);
        }
    }
    /**
     * Create a node private key
     */
    private void createNodeKeys(Short bsId) {
    	byte[] key = CryptoFunctions.createSkipjackKey();
        privateKey.put(bsId, key);
        NetworkKeyStore.getInstance().registerKey(getNode().getId(),bsId, key);
    }
    
    /**
     * Load initial sequence number from the one way hash chain
     */
    private void initiateSequenceNumber(Short bsId) {
        OWS.put(bsId, MINSENSFunctions.getInitialSequenceNumber(bsId));
    }
    

    /**
     * Create the timers needed for operation
     */
    private void createTimers(short bsId) {
    	
    	feedbackMessageStartTimer.put(bsId, 
    	new Timer(getNode().getSimulator(), 1, MINSENSConstants.FEEDBACK_START_TIME_BOUND + getNode().getId() * 100,bsId) {

            @Override
            public void executeAction() {
                sendFeedbackMessageInfo((Short)param);

            }
        });
        
    	startForwardTablesCalculesTimer = new Timer(getNode().getSimulator(), MINSENSConstants.FEEDBACK_MSG_RECEIVED_TIME_BOUND) {

            @Override
            public void executeAction() {
                startComputeRoutingInfo();
            }
        };
        queueMessageDispatchTimer =  new Timer(getNode().getSimulator(), MINSENSConstants.MESSAGE_DISPATCH_RATE) {

            @Override
            public void executeAction() {
                dispatchNextMessage();
            }
        };
        
        
        forwardingTablesDispatchTimer =  new Timer(getNode().getSimulator(), MINSENSConstants.MESSAGE_DISPATCH_RATE *2) {
            private int maxRetries;

            
            /*
             * (non-Javadoc)
             * TODO:Esta implementa�‹o n est‡ descrita no paper...as tentativas de reenvio n‹o s‹o explicitadas la.
             * @see org.wisenet.simulator.core.events.Timer#executeAction()
             */
            @Override
            public void executeAction() {
                if (forwardingTablesRepository.isEmpty()) {
                    System.out.println("No more forwarding tables to send");
                    forwardingTablesDispatchTimer.stop();
                    
                    //to be used by registerMessagePassage...
                    forwardingTable = new MINSENSForwardingTable(getNode().getId());
                    forwardingTable.setHopDistanceToBS(0);
                    getForwardingTable().setAsSink();
                    
                } else {
                    if (orderedByHopsNodesIterator.hasNext()) {
                        Short nodeId = (Short) orderedByHopsNodesIterator.next();
                        sendForwardTable(getBaseStationController().getForwardingTables(), nodeId);
                        System.out.println(getNode().getId()+" : sending ft to "+nodeId);
                    } else {
                        if (orderedByHopsIterator.hasNext()) {
                            orderedByHopsNodes = tableOfNodesByHops.get(orderedByHopsIterator.next());
                            orderedByHopsNodesIterator = orderedByHopsNodes.iterator();
                            System.out.println(getNode().getId()+" : moving to next hop level "+orderedByHopsNodes.size()
                            		+" nodes");
                        } else {

                            if (first) {
                                forwardingTablesRepositoryIter = new HashSet<byte[]>(forwardingTablesRepository.values()).iterator();
                                first = false;
                            }
                            if (forwardingTablesRepositoryIter.hasNext()) {
                                sendUpdateRouteMessage((byte[]) forwardingTablesRepositoryIter.next());
                            }
                            if (!forwardingTablesRepositoryIter.hasNext() && !forwardingTablesRepository.isEmpty() && retry==0){
//                                maxRetries=forwardingTablesRepository.size(); //*3;
                                maxRetries=1; //*3;
                            }
                            if (!forwardingTablesRepositoryIter.hasNext() && !forwardingTablesRepository.isEmpty() && retry < maxRetries) {
                                forwardingTablesRepositoryIter = new HashSet<byte[]>(forwardingTablesRepository.values()).iterator();
                                retry++;
                            }else{
                                 System.out.println("Stop sending forwarding tables: " + forwardingTablesRepository.size()+ " left");
                                 forwardingTablesDispatchTimer.stop();
                                 
                               //to be used by registerMessagePassage...
                                 forwardingTable = new MINSENSForwardingTable(getNode().getId());
                                 forwardingTable.setHopDistanceToBS(0);
                                 getForwardingTable().setAsSink();
                            }
                        }
                    }
                }
            }
        };
    }
    
    
    /**
     * Start building routing info
     */
    private void startComputeRoutingInfo() {
        if (canStartComputeRoutingInfo()) {
//            new Thread(new Runnable() {
//                public void run() {

            log("Started to compute routing info");
            getBaseStationController().calculateForwardingTables();
            log("Number of Forwarding Tables:  " + getBaseStationController().getForwardingTables().size());
            if(USE_CONSENSUS_ON_ROUTES){
            	usesConsensus = true;
            	makeConsensusOnRoutes();
            }else{
            	sendRouteUpdateMessages(getBaseStationController().getForwardingTables());
            }
            
            
            
            
//                }
//            }).start();
        }
    }
    
    private void makeConsensusOnRoutes(){
//    	Collection<Short> ids = getNode().getSimulator().getSinkNodesIds();
    	
    }
    
    
    public void registerSinkFeedback(List<List<List<Short>>> paths){
    	/*
    	 * dar a baseStationController. 
    	 * Se ela tiver calculado o novo allpaths, 
    	 * 		->chamar buildForwardingTables na baseStationController
    	 * 		->sendRouteUpdateMessages(getBaseStationController().getForwardingTables());
    	 */
    	boolean consensusDone = this.getBaseStationController().receiveRoutesFromOtherBaseStations(paths);
    	if(consensusDone){
    		getBaseStationController().buildForwardingTables();
    		sendRouteUpdateMessages(getBaseStationController().getForwardingTables());
    	}
    }
    
    /**
     * Verifies if can begin to compute the routing info
     * @return
     */
    boolean canStartComputeRoutingInfo() {
        /**
         * Devo verificar se o numero de mensagens alterou
         * se alterou entao actualizo e reinicio as tentativas
         * se nao alterou depois de 3 checks entÃƒÂ£o posso iniciar a recepÃƒÂ§ÃƒÂ£o
         */
        if (feedbackMessagesReceived <= lastFeedbackMessagesReceivedCheck) {
            if (feedbackMessageRetries >= MINSENSConstants.FEEDBACK_MSG_RECEIVED_RETRIES) {
                startForwardTablesCalculesTimer.stop();
                return true;
            } else {
                feedbackMessageRetries++;
                return false;
            }
        } else {
            lastFeedbackMessagesReceivedCheck = feedbackMessagesReceived;
            feedbackMessageRetries = 0;
            return false;
        }
    }
    
    
    public void clearDataConsensusStructures(){
		this.messagesTable.clear();
    }
    
    public boolean routingUsingLocalDataConsensus(){
    	return USE_LOCAL_BYZANTINE_AGREEMENT;
    }
    
    public boolean routingUsingGlobalDataConsensus(){
    	return USE_GLOBAL_BYZANTINE_AGREEMENT;
    }
    
    /**
     * Prepare the feedback message info to send to parent nodes
     */
    private void sendFeedbackMessageInfo(short bsId) {
        log("Send FeedBack Message ");
        byte[] payload = MINSENSMessagePayloadFactory.createFBKPayload(getNode().getId(),
                privateKey.get(bsId), neighborInfo.get(bsId), neighborInfo.get(bsId).getParentMac(), this.getNode());
        MINSENSMessage message = new MINSENSMessage(payload);
        message.setDestinationId(bsId);
        getController().addMessageSentCounter(MINSENSConstants.MSG_FEEDBACK);
        send(message);
//        feedbackMessageStartTimer.reschedule();
    }
    
    private void sendForwardTable(Hashtable<Short, INSENSForwardingTable> forwardingTables, Short id) {
        if (forwardingTables.containsKey(id)) {
            INSENSForwardingTable ft = forwardingTables.get(id);
            byte[] payload = MINSENSMessagePayloadFactory.createRUPDPayload(getNode().getId(), (Short) id, getNode().getId(),ft.getRouteIdsToBS() , OWS.get(getNode().getId()), ft, privateKey.get(getNode().getId()), this.getNode());
            sendUpdateRouteMessage(payload);
        }
    }

    private void sendUpdateRouteMessage(byte[] payload) {
        getController().addMessageSentCounter(MINSENSConstants.MSG_ROUTE_UPDATE);
        send(new MINSENSMessage(payload));
    }
    

    /**
     * Dispatchs a message from the message queue
     */
    private void dispatchNextMessage() {
        if (!sendingMessage) {
            if (!messagesQueue.isEmpty()) {
                /**
                 * Este teste permite diminuir a densidade da rede em virtude de
                 * dimunir as colisoes na rede
                 */
//              if(!getNode().getMacLayer().isReceiving() && !getNode().getMacLayer().isTransmitting()){
            	Message m = messagesQueue.poll();
            	if(m != null)
            		broadcastMessage(m);
//              }
            } else {
                queueMessageDispatchTimer.stop();
            }
        }
    }
    
    /**
     * Broadcast a message
     * @param message
     * @param reliable
     */
    private void broadcastMessage(Message message) {
    	sendingMessage = true;
        long delay = (long) Simulator.randomGenerator.nextDoubleBetween((int) MINSENSConstants.MIN_DELAYED_MESSAGE_BOUND, (int) MINSENSConstants.MAX_DELAYED_MESSAGE_BOUND);
        long time = (long) (Simulator.getSimulationTime());
        DelayedMessageEvent delayMessageEvent = new DelayedMessageEvent(time, delay, message, getNode(), reliableMode);
        delayMessageEvent.setReliable(reliableMode);
        getNode().getSimulator().addEvent(delayMessageEvent);
    }
    
    /**
     * Send messages with pre-calculated routing tables
     * @param forwardingTables
     */
    public void sendRouteUpdateMessages(Hashtable<Short, INSENSForwardingTable> forwardingTables) {
        Vector<List<Short>> allpaths = getBaseStationController().getAllPaths();
        Comparator<List<Short>> c = new Comparator<List<Short>>() {

            public int compare(List<Short> o1, List<Short> o2) {
                return Integer.valueOf(o1.size()).compareTo(Integer.valueOf(o2.size()));
            }
        };
        Collections.sort(allpaths, c);
        tableOfNodesByHops = new Hashtable<Integer,List<Short>>();
        tableOfNodesByHops = buildListOfNodestByHops(allpaths);
        sendForwardingTablesByHops(forwardingTables, tableOfNodesByHops);
        setStable(true);
    }
    
    /**
     * Auxiliary function to create a list of nodes joined by number of hops
     * @param allpaths
     * @return
     */
    private Hashtable<Integer,List<Short>> buildListOfNodestByHops(Vector<List<Short>> allpaths) {
        Hashtable<Integer,List<Short>> t = new Hashtable<Integer,List<Short>>();
        
        for (List<Short> path : allpaths) {
            if (path.size() > 0) {
                List<Short> lst2 = t.get(path.size());
                if (lst2 == null) {
                    lst2 = new ArrayList<Short>();
                }
                lst2.add(path.get(path.size() - 1));
                t.put(path.size(), lst2);
            }
        }
        
        return t;
    }
    
    /**
     * For each hop number based nodes send a Forwarding table
     * @param forwardingTables
     * @param tableOfNodesByHops
     */
    private void sendForwardingTablesByHops(Hashtable<Short, INSENSForwardingTable> forwardingTables, Hashtable<Integer,List<Short>> tableOfNodesByHops) {
        forwardingTablesRepository = new Hashtable<Integer, byte[]>();
        orderedByHops = new LinkedList<Integer>(tableOfNodesByHops.keySet());
        Collections.sort(orderedByHops);
        byte[] payload;
        Set<Short> differentNodes = new HashSet<Short>();
        INSENSForwardingTable ft = null;
        int i = 0;
        for (Integer key : orderedByHops) {
            List<Short> nodes = tableOfNodesByHops.get(key);
            for (Short id : nodes) {
                differentNodes.add(id);
                
                if (forwardingTables.containsKey(id)) {
                    ft = forwardingTables.get(id);
//                  ft.setHopDistanceToBS((Integer)key-1);//paths contain sinkNode, so hopdistance is size-1
//                  System.out.println("node "+id+" at "+((Integer)key-1)+" hops");
                    payload = MINSENSMessagePayloadFactory.createRUPDPayload(getNode().getId(), id, getNode().getId(),ft.getRouteIdsToBS(), OWS.get(getNode().getId()), ft, privateKey.get(getNode().getId()), this.getNode());
                    forwardingTablesRepository.put(new Integer(ft.getUniqueId()), payload);
                    i++;
//                  sendUpdateRouteMessage(payload);
                }
            }

        }
        System.out.println("Total Forwarding Tables: " + i);
        System.out.println("Total nodes having ft: " + differentNodes.size());

        if(orderedByHops.isEmpty()){
        	System.out.println("Error: No network possible.");
        	return;
        }
        	
        orderedByHopsIterator = orderedByHops.iterator();
        orderedByHopsNodes = tableOfNodesByHops.get(orderedByHopsIterator.next());
        orderedByHopsNodesIterator = orderedByHopsNodes.iterator();
        forwardingTablesDispatchTimer.start();
    }
    
//    private void startAllTimers(Hashtable<Short,Timer> timers){
//    	for(Short s: timers.keySet()){
//    		timers.get(s).start();
//    	}
//    }
    
}
