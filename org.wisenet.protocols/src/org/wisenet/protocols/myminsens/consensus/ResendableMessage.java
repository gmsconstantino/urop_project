package org.wisenet.protocols.myminsens.consensus;

import org.wisenet.simulator.core.Message;

public class ResendableMessage extends Message{

	private boolean isResend = false;
	

	public ResendableMessage(){
		super();
	}
	
	
	public boolean isResend() {
		return isResend;
	}

	public void setResend(boolean isResend) {
		this.isResend = isResend;
	}
}
