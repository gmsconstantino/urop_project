package org.wisenet.protocols.myminsens.consensus.turquois;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.wisenet.protocols.myminsens.consensus.turquois.messages.TurquoisMessage;

public class TurquoisMessageReceivedBuffer {

	private HashMap<Integer, List<TurquoisMessage>> receivedMsgsPerPhase;
	
	
	public TurquoisMessageReceivedBuffer(){
		receivedMsgsPerPhase = new HashMap<Integer, List<TurquoisMessage>>();
	}
	
	
	public synchronized boolean addMessage(TurquoisMessage m){
		List<TurquoisMessage> l = receivedMsgsPerPhase.get(m.getPhase());
		
		if(l == null){
			l = new LinkedList<TurquoisMessage>();
			receivedMsgsPerPhase.put(m.getPhase(), l);
		}
		
		return l.add(m);
	}
	
	public synchronized List<TurquoisMessage> getMessagesOnPhase(int phase){
		return receivedMsgsPerPhase.remove(phase);
	}
}
