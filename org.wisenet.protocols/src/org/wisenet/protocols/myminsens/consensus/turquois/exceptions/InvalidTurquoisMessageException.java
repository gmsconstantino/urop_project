package org.wisenet.protocols.myminsens.consensus.turquois.exceptions;

public class InvalidTurquoisMessageException extends RuntimeException{


	private static final long serialVersionUID = 1L;

	
	public InvalidTurquoisMessageException(){
		super();
	}
	
	public InvalidTurquoisMessageException(String msg){
		super(msg);
	}
}
