package org.wisenet.protocols.myminsens.consensus.turquois;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

import org.wisenet.protocols.myminsens.basestation.communication.CommunicationChannel;
import org.wisenet.protocols.myminsens.consensus.BinaryConsensus;
import org.wisenet.protocols.myminsens.consensus.SentMessagesRecord;
import org.wisenet.protocols.myminsens.consensus.turquois.messages.TurquoisMessage;
import org.wisenet.protocols.myminsens.utils.KillableThread;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.Simulator;


public class Turquois implements BinaryConsensus{
	
	//TODO: mudar phase e value para byte nos m�todos e mensagens
	public static final int CONVERGE_PHASE = 1;
	public static final int LOCK_PHASE = 2;
	public static final int DECIDE_PHASE = 0;
	
	public static final int ZERO_VALUE = 0;
	public static final int ONE_VALUE = 1;
	public static final int UNDEFINED_VALUE = 2;
	
	private Short myId;
	private Set<Short> participants;
	private HashMap<Short, byte[]> participantsKeys;
	
	private boolean useMac = false;
	
	private int round;
	private int phase;
	private boolean decided;
	private int proposalValue;
	private int decisionValue = -1;
	
	private int broadcastsInPhase;
	private ValidMessagesList received;
	private CommunicationChannel network;
	
	private SenderTask senderTask;
	private ReceiverTask receiverTask;
	
	private SentMessagesRecord<Integer, TurquoisMessage> sentMsgsByPhase;
	private Queue<TurquoisMessage> messagesToDeliver;
	private final long SEND_PERIOD = 1000;
	
	
	public Turquois(Simulator sim, Short me, CommunicationChannel ch){
		myId = me;
		participants = new HashSet<Short>();
		participants.add(me);
		participantsKeys = new HashMap<Short, byte[]>();
		received = new ValidMessagesList();
		this.network = ch;
		messagesToDeliver = new LinkedList<TurquoisMessage>();
		sentMsgsByPhase = new SentMessagesRecord<Integer, TurquoisMessage>();
		
		phase = 1;
		round = 0;
		decided = false;
		this.proposalValue = UNDEFINED_VALUE;
		
		senderTask = new SenderTask(SEND_PERIOD);
		receiverTask = new ReceiverTask(50);
	}
	
	
	public void init(Boolean proposal){
		if(proposal == null){
			this.proposalValue = UNDEFINED_VALUE;
		}
		else{
			if(proposal)
				this.proposalValue = ONE_VALUE;
			else
				this.proposalValue = ZERO_VALUE;
		}
		
		receiverTask.start();
		senderTask.start();
	}
	
	public Boolean getDecision(){
		if(decided){
			if(decisionValue == ZERO_VALUE)
				return false;
			else if(decisionValue == ONE_VALUE)
				return true;
		}
		
		return null;
	}
	
	public synchronized void receiveMessage(Message m){
		if(m instanceof TurquoisMessage && m != null){
			
			if(participants.contains(m.getSourceId())){
				TurquoisMessage tm = (TurquoisMessage) m;
				
				// if message is a duplicate, discard it
				if(received.contains(tm))
					return;
				
				if(tm.getPhase() < phase){
					if(!tm.isResend()){
						TurquoisMessage toSend = sentMsgsByPhase.getMessage(phase);
						if(toSend != null){
							toSend.setResend(true);
							network.broadcastConsensusMsg(toSend, participants);
						}
					}
				}
				else
					messagesToDeliver.add(tm);
			}
		}
	}
	
	public void addParticipant(Short p, byte[] sharedKey){
		participants.add(p);
		participantsKeys.put(p, sharedKey);
	}
	
	public boolean removeParticipant(Short p){
		return participants.remove(p);
	}
	
	public void setParticipants(HashMap<Short, byte[]> participantsKeys) {
		this.participantsKeys = participantsKeys;
		participants.clear();
		
		for(Short s: participantsKeys.keySet())
			participants.add(s);
	}
	
	public Short getId(){
		return myId;
	}
	
	public int getNumRounds(){
		return round;
	}
	
	public void stop(){
		senderTask.kill();
		receiverTask.kill();
	}
	
	public Set<Short> getReceivedParticipants(){
		return received.getReceivedParticipants(phase);
	}
	
	
	private int coin(){
		if(new Random().nextDouble() <= 0.5)
			return ZERO_VALUE;
		else
			return ONE_VALUE;
	}
	
	private int getNumFaultyParticipantsAllowed(){
		return (participants.size()) / 3;
	}
	
	private double getNPlusFOverTwo(){
		return (participants.size() + getNumFaultyParticipantsAllowed()) / 2;
	}
	
	private void incPhase(){		
		phase++;
		senderTask.resetSentCounter();
	}
	
	
	
	private class SenderTask extends KillableThread{
		
		private static final short MAX_SENDS = 5;
		
		private long period;
		private int sends = 0;
		
		
		public SenderTask(long period){
			super();
			this.period = period;
		}
		
		
		public void run(){
			while(isRunning){
				if(sends < MAX_SENDS){
					broadcast();
					sends++;
				}
				
				try {
					sleep(period);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		public void resetSentCounter(){
			sends = 0;
		}
		
		private void broadcast(){
			List<TurquoisMessage> proofs = new LinkedList<TurquoisMessage>();
			
			if(broadcastsInPhase > 1 && phase > 1)
				proofs = received.getMsgsInPhase(phase - 1);
			
			receiveMessage(new TurquoisMessage(myId, myId, phase, proposalValue, decided, null, proofs));
			
			TurquoisMessage toSend = new TurquoisMessage(myId, (short)0, phase, proposalValue, decided, null, proofs);
			sentMsgsByPhase.add(phase, toSend);
			network.broadcastConsensusMsg(toSend, participants);
			
			broadcastsInPhase++;
		}
	}
	
	private class ReceiverTask extends KillableThread{
		
		private long delay;
		
		
		public ReceiverTask(long delay){
			this.delay = delay;
		}
		
		
		public void run(){
			while(isRunning){
				if(!messagesToDeliver.isEmpty()){
					treatMessage(messagesToDeliver.poll());
				}

				try {
					sleep(delay);
				} catch (InterruptedException e) {
					e.printStackTrace();
					continue;
				}
			}
		}
		
		private void treatMessage(TurquoisMessage m){
			if(!isValid(m)){
				messagesToDeliver.add(m);
				//throw new InvalidTurquoisMessageException();
			}
			else{
				received.add(m);
				
				if(m.getPhase() > phase){
					phase = m.getPhase();
					
					if(m.getPhase() % 3 == 1 && isValResultOfCoinFlip(m)){
						proposalValue = coin();
					}
					else{
						proposalValue = m.getVal();
					}
					
					decided = m.isDecided();
				}
				
				if(received.getPhaseCounter(phase) > getNPlusFOverTwo()){
					if(phase % 3 == 1){	/* phase CONVERGE */
						proposalValue = received.getMostReceivedValueInPhase(phase);
					}
					else if(phase % 3 == 2){	/* phase LOCK */
						if(received.valGreaterThan(phase, ZERO_VALUE, (int)getNPlusFOverTwo())){
							proposalValue = ZERO_VALUE;
						}
						else if(received.valGreaterThan(phase, ONE_VALUE, (int)getNPlusFOverTwo())){
							proposalValue = ONE_VALUE;
						}
						else{
							proposalValue = UNDEFINED_VALUE;
						}
					}
					else{	/* phase DECIDE */
						// if the number of messages with phase = phase and v={0,1} is greater than (n+f)/2
						if(received.valGreaterThan(phase, ZERO_VALUE, (int)getNPlusFOverTwo())
							|| received.valGreaterThan(phase, ONE_VALUE, (int)getNPlusFOverTwo()))
						{
							decided = true;
						}
						
						if(received.getValuePerPhaseCounter(phase, ZERO_VALUE) >= 1){
							proposalValue = ZERO_VALUE;
						}
						else if(received.getValuePerPhaseCounter(phase, ONE_VALUE) >= 1){
							proposalValue = ONE_VALUE;
						}
						else{
							proposalValue = coin();
						}
					}
					
					incPhase();
					broadcastsInPhase = 0;
				}
				
				if(decided){
					decisionValue = proposalValue;
					this.kill();
				}
				
				round++;
			}
		}
		
		private boolean isValid(TurquoisMessage m){			
			if(m == null)
				return false;
			
			try {
			// authenticity validation
				if(useMac){
					if(!m.macIsCorrect(participantsKeys.get(m.getSourceId()))){
						return false;
					}
				}		
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
				
			// semantic validation
			if(m.getPhase() > 1){
				
				// phase value
				if(received.getPhaseCounter(m.getPhase() - 1) < (int)getNPlusFOverTwo()){
					List<TurquoisMessage> proofs = m.getProofMessages();
					if(proofs != null){
//						System.out.println("Checking proofs...");
						
						if(proofs.size() <= getNPlusFOverTwo()){
//							System.err.println("Not enough proofs!");
							return false;
						}
						
						for(TurquoisMessage msg: proofs){
							if(!isValid(msg)){
//								System.err.println("Invalid proof detected!");
								return false;
							}
						}
					}
					else{
						//System.err.println(myId + ": Invalid phase value: MSG phase = " + m.getPhase() + "; Current phase = " + phase + "; Num. msgs with msgphase-1 = " + received.getPhaseCounter(m.getPhase()-1));
//						System.err.println("just re-added message to the list!");
						messagesToDeliver.add(m);
						return false;
					}
				}
				
				// proposal value
				if(m.getPhase() % 3 == 2){ // Phase LOCK
					if(received.getValuePerPhaseCounter(m.getPhase() - 1, m.getVal()) < (int)(getNPlusFOverTwo()/2)){
						//System.err.println("Invalid proposal value! (Phase LOCK)");
						return false;
					}
				}
				else if(m.getPhase() % 3 == 0){ // Phase DECIDE
					if(m.getVal() == UNDEFINED_VALUE){
						if(!(received.valGreaterThan(m.getPhase()-2, ONE_VALUE, (int)(getNPlusFOverTwo()/2))
							&& received.valGreaterThan(m.getPhase()-2, ZERO_VALUE, (int)(getNPlusFOverTwo()/2))))
						{
							System.err.println("Invalid proposal value: UNDEFINED! (Phase DECIDE)");
							return false;
						}
					}
					else{
						if(!received.valGreaterThan(m.getPhase()-1, m.getVal(), (int)getNPlusFOverTwo())){
							//System.err.println("Invalid proposal value! (Phase DECIDE)");
							return false;
						}
					}
				}
				else if(m.getPhase() % 3 == 1){ // Phase CONVERGE
					if(isValResultOfCoinFlip(m)){
						if(received.getValuePerPhaseCounter(m.getPhase()-1, UNDEFINED_VALUE) < getNPlusFOverTwo()){
							System.err.println("Invalid proposal value! (Phase CONVERGE)");
							return false;
						}
					}
					else{
						if(received.getValuePerPhaseCounter(m.getPhase()-2, m.getVal()) < getNPlusFOverTwo()){
							System.err.println("Invalid proposal value! (Phase CONVERGE)");
							return false;
						}
					}
				}
				else{
					return false;
				}
			}
			
			// status value
			if(m.getPhase() <= 3){
				if(m.isDecided()){
					System.err.println("Invalid status value!");
					return false;
				}	
			}
			else{
				if(m.isDecided()){
					//TODO
				}
				else{
					//TODO
				}
			}
			
			return true;
		}
		
		private boolean isValResultOfCoinFlip(TurquoisMessage m){
			if(m.getPhase() > 1){
				if(received.getMostReceivedValueInPhase(m.getPhase() - 1) == UNDEFINED_VALUE
						&& m.getVal() != UNDEFINED_VALUE)
				{
					return true;	
				}
			}
			
			return false;
		}
	}
}
