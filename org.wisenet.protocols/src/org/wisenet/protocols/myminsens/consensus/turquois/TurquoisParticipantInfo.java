package org.wisenet.protocols.myminsens.consensus.turquois;

public class TurquoisParticipantInfo {

	protected Short id;
	protected byte[] sharedKey;
	protected int distanceInHops;
	
	
	public TurquoisParticipantInfo(Short id, byte[] sharedKey, int distance){
		this.id = id;
		this.sharedKey = sharedKey;
		this.distanceInHops = distance;
	}


	public Short getId() {
		return id;
	}

	public void setId(Short id) {
		this.id = id;
	}

	public byte[] getSharedKey() {
		return sharedKey;
	}

	public void setSharedKey(byte[] sharedKey) {
		this.sharedKey = sharedKey;
	}

	public int getDistanceInHops() {
		return distanceInHops;
	}

	public void setDistanceInHops(int distanceInHops) {
		this.distanceInHops = distanceInHops;
	}
}
