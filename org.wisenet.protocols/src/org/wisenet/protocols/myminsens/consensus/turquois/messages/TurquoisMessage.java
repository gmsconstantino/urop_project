package org.wisenet.protocols.myminsens.consensus.turquois.messages;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.wisenet.protocols.common.ByteArrayDataInputStream;
import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.protocols.myminsens.consensus.ResendableMessage;
import org.wisenet.simulator.utilities.CryptoFunctions;

public class TurquoisMessage extends ResendableMessage{

	private short source;
	private short dest;
	
	private int phase;
	private int val;
	private boolean status;
	
	private byte[] authKey;
	private byte[] mac = new byte[0];
	private List<TurquoisMessage> proofMessages;
	
	
	public TurquoisMessage(short source, int phase, int val, boolean status, byte[] authKey, List<TurquoisMessage> proofMessages){
		this.source = source;		
		this.phase = phase;
		this.val = val;
		this.status = status;
		this.authKey = authKey;
		this.proofMessages = proofMessages;
		
		this.dest = 0;
		
		try {
			updateMac();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public TurquoisMessage(short source, short dest, int phase, int val, boolean status, byte[] authKey, List<TurquoisMessage> proofMessages){
		this.source = source;
		this.dest = dest;
		this.phase = phase;
		this.val = val;
		this.status = status;
		this.authKey = authKey;
		this.proofMessages = proofMessages;
		
		try {
			updateMac();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public TurquoisMessage(byte[] bytes) throws IOException{
		proofMessages = new LinkedList<TurquoisMessage>();
		
		ByteArrayDataInputStream badis = new ByteArrayDataInputStream(bytes);
		
		source = badis.readShort();
		dest = badis.readShort();
		phase = badis.readInt();
		val = badis.readInt();
		status = badis.readBoolean();
		
		int nProofMessages = badis.readInt();
		for(int i = 0; i < nProofMessages; i++){
			int msgSize = badis.readInt();
			byte[] msg = new byte[msgSize];
			badis.read(msg);
			proofMessages.add(new TurquoisMessage(msg));
		}
		
		setResend(badis.readBoolean());
		
		int macSize = badis.readInt();
		mac = new byte[macSize];
		badis.read(mac);
	}
	
	
	public void setProofMessages(List<TurquoisMessage> msgs){
		this.proofMessages = msgs;
	}
	
	public List<TurquoisMessage> getProofMessages(){
		if(proofMessages != null){
			if(proofMessages.size() == 0)
				return null;
		}
		
		return proofMessages;
	}
	
	public short getSourceId(){
		return source;
	}
	
	public short getDestinationId(){
		return dest;
	}
	
	public int getPhase() {
		return phase;
	}

	public int getVal() {
		return val;
	}

	public boolean isDecided() {
		return status;
	}

	public Object clone(){
		return new TurquoisMessage(source, dest, phase, val, status, authKey.clone(), proofMessages);
	}
	
	public byte[] toByteArray() throws IOException{
		ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
		
		byte[] msgData = messageDataToBytes();
		bados.write(msgData);		
		bados.writeInt(mac.length);
		bados.write(mac);
		
		return bados.toByteArray();
	}
	
	public boolean macIsCorrect(byte[] authKey) throws IOException{
		if(mac == null)
			return false;
		
		return CryptoFunctions.verifyMessageIntegrityMAC(messageDataToBytes(), mac, authKey);
	}
	
	private byte[] messageDataToBytes() throws IOException{
		ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
		
		bados.writeShort(this.source);
		bados.writeShort(this.dest);
		
		// write the data to a different stream to get its size
		bados.writeInt(this.phase);
		bados.writeInt(this.val);
		bados.writeBoolean(this.status);
		
		bados.writeInt(this.proofMessages.size());
		if(proofMessages.size() > 0){
			for(TurquoisMessage m: proofMessages){
				byte[] msgBytes = m.toByteArray();
				bados.writeInt(msgBytes.length);
				bados.write(msgBytes);
			}
		}
		
		bados.writeBoolean(isResend());
		
		return bados.toByteArray();
	}
	
	private void updateMac() throws IOException{
		//mac = CryptoFunctions.createMAC(messageDataToBytes(), authKey);
	}
}
