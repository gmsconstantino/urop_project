package org.wisenet.protocols.myminsens.consensus.turquois;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.wisenet.protocols.myminsens.consensus.turquois.messages.TurquoisMessage;

public class ValidMessagesList{

	private Vector<List<TurquoisMessage>> msgsPerPhase;
	private int msgCounter;
	private Vector<int[]> valuesPerPhaseCounter; //[phase][value] = count
	
	public ValidMessagesList(){
		clear();
	}
	

	public synchronized boolean add(TurquoisMessage e) {
		if(msgsPerPhase.size() <= e.getPhase())
			msgsPerPhase.add(e.getPhase(), new LinkedList<TurquoisMessage>());
		
		if(!isDuplicateMessage(e)){
			msgsPerPhase.get(e.getPhase()).add(e);
			
			if(valuesPerPhaseCounter.size() <= e.getPhase())
				valuesPerPhaseCounter.add(e.getPhase(), new int[3]);
			
			valuesPerPhaseCounter.get(e.getPhase())[e.getVal()] += 1;
			msgCounter++;
			
			return true;
		}
		
		return false;
	}

	public boolean contains(TurquoisMessage m){
		return isDuplicateMessage(m);
	}
	
	public void add(int index, TurquoisMessage element) {
		add(element);
	}

	public boolean addAll(Collection<? extends TurquoisMessage> c) {
		for(TurquoisMessage m: c){
			add(m);
		}
		
		return true;
	}

	public boolean addAll(int index, Collection<? extends TurquoisMessage> c) {
		return addAll(c);
	}

	public void clear() {
		msgCounter = 0;
		valuesPerPhaseCounter = new Vector<int[]>(1);
		valuesPerPhaseCounter.add(0, null);
		
		msgsPerPhase = new Vector<List<TurquoisMessage>>(1);
		msgsPerPhase.add(0, null);
	}

	public boolean isEmpty() {
		return msgCounter == 0;
	}
	
	public Iterator<TurquoisMessage> iterator() {
		List<TurquoisMessage> l = new LinkedList<TurquoisMessage>();
		
		for(List<TurquoisMessage> li: msgsPerPhase){
			l.addAll(li);
		}
		
		return l.iterator();
	}
	
	public int size() {
		return msgCounter;
	}
	
	public List<TurquoisMessage> getMsgsInPhase(int phase){
		return msgsPerPhase.get(phase);
	}
	
	public int getPhaseCounter(int phase){
		if(msgsPerPhase.size() <= phase){
			for(int i = msgsPerPhase.size(); i <= phase; i++)
				msgsPerPhase.add(i, new LinkedList<TurquoisMessage>());
		}
		
		return msgsPerPhase.get(phase).size();
	}
	
	public int getValuePerPhaseCounter(int phase, int value){
		if(valuesPerPhaseCounter.size() <= phase){
			valuesPerPhaseCounter.add(phase, new int[3]);
			return 0;
		}
		
		return valuesPerPhaseCounter.get(phase)[value];
	}
	
	public int getMostReceivedValueInPhase(int phase){
		int val = -1;
		int numValues = 0;
		
		for(int i = 0; i < 3; i++){
			if(getValuePerPhaseCounter(phase, i) > numValues){
				val = i;
				numValues = getValuePerPhaseCounter(phase, i);
			}
		}
		
		return val;
	}
	
	public boolean valGreaterThan(int phase, int val, int threshold){
		try{
			return valuesPerPhaseCounter.get(phase)[val] >= threshold;
		}
		catch(IndexOutOfBoundsException e){
			return false;
		}
	}
	
	public Set<Short> getReceivedParticipants(int phase){
		Set<Short> recPart = new HashSet<Short>();
		
		for(TurquoisMessage m: msgsPerPhase.get(phase)){
			recPart.add((Short) m.getSourceId());
		}
		
		return recPart;
	}
	
	
	private boolean isDuplicateMessage(TurquoisMessage m){				
		if(m.getPhase() < msgsPerPhase.size()){
			@SuppressWarnings("unchecked")
			LinkedList<TurquoisMessage> clone = (LinkedList<TurquoisMessage>)((LinkedList<TurquoisMessage>) msgsPerPhase.get(m.getPhase())).clone();
			
			for(TurquoisMessage tm: clone){
				if((Short) tm.getSourceId() == (Short) m.getSourceId()){
					return true;
				}
			}
		}
		
		return false;
	}
}