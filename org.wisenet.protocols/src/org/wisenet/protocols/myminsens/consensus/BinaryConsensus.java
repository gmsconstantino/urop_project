package org.wisenet.protocols.myminsens.consensus;

import java.util.HashMap;

import org.wisenet.simulator.core.Message;


public interface BinaryConsensus{

	public void init(Boolean proposal);
	
	public Boolean getDecision();
	
	public void receiveMessage(Message m);
	
	public void addParticipant(Short participantId, byte[] participantSecretKey);
	
	public boolean removeParticipant(Short participantId);
	
	public void setParticipants(HashMap<Short, byte[]> participantsKeys);
	
	public void stop();
}
