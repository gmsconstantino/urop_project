package org.wisenet.protocols.myminsens.consensus.mymultivalued;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.wisenet.protocols.myminsens.basestation.communication.CommunicationChannel;
import org.wisenet.protocols.myminsens.consensus.BinaryConsensus;
import org.wisenet.protocols.myminsens.consensus.MultiValuedConsensus;
import org.wisenet.protocols.myminsens.consensus.SentMessagesRecord;
import org.wisenet.protocols.myminsens.consensus.mymultivalued.exceptions.InvalidMVConsensusMsgException;
import org.wisenet.protocols.myminsens.consensus.mymultivalued.exceptions.UndecidedException;
import org.wisenet.protocols.myminsens.consensus.mymultivalued.messages.MVConsensusMsg;
import org.wisenet.protocols.myminsens.utils.KillableThread;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.Simulator;

public class MVConsensus implements MultiValuedConsensus<byte[]>{

	private static final short INIT_PHASE = 1;
	private static final short VECT_PHASE = 2;
	private static final short BIN_PHASE = 3;
	
	
	private byte[] proposedValue = null;
	private byte[] decisionValue = null;
	private short phase;
	private BinaryConsensus binConsensus;
	private boolean decided = false;
	
	private Set<Short> participants;
	private HashMap<Short, byte[]> participantsKeys;
	
	private Short myId;
	
	private ReceivedMessagesList received;
	private SentMessagesRecord<Short, MVConsensusMsg> sentMessagesInPhase;
	
	private CommunicationChannel network;
	
	private SenderTask senderTask;
	private ReceiverTask receiverTask;
	private BinConsensusResultTask binConsensusResultReader;
	
	private Queue<MVConsensusMsg> messagesToDeliver;
	private final long SEND_PERIOD = 1000;
	
	
	public MVConsensus(Simulator sim, Short myId, BinaryConsensus binaryConsensusLayer, CommunicationChannel network){
		binConsensus = binaryConsensusLayer;
		received = new ReceivedMessagesList();
		
		participants = new HashSet<Short>();
		participants.add(myId);
		participantsKeys = new HashMap<Short, byte[]>();
		this.myId = myId;
		
		this.network = network;
		
		messagesToDeliver = new LinkedList<MVConsensusMsg>();
		sentMessagesInPhase = new SentMessagesRecord<Short, MVConsensusMsg>();
		
		senderTask = new SenderTask(SEND_PERIOD);
		receiverTask = new ReceiverTask(50);
		binConsensusResultReader = new BinConsensusResultTask(1000);
	}
	
	
	public void init(byte[] proposal) {
		phase = INIT_PHASE;
		proposedValue = proposal;
		
		receiverTask.start();
		senderTask.start();
	}

	/**
	 * Might throw an UndecidedException indicating that the value is still being decided
	 */
	public byte[] getDecision() {
		if(decided){
			return decisionValue;
		}
		else{
			throw new UndecidedException();
		}
		
	}

	public synchronized void receiveMessage(Message m) {
		if(!(m instanceof MVConsensusMsg))
			throw new InvalidMVConsensusMsgException("Not a MVConsensus message.");
		
		MVConsensusMsg msg = (MVConsensusMsg) m;
//		System.out.println(myId + ": Message received! Phase: " + msg.getTag() + " Value: " + msg.getValue());
		
		if(msg.getTag() > 0 && msg.getTag() < 3){
			if(phase > msg.getTag()){
				// treat late message. Only resends message if it's not already a resent message
				if(!msg.isResend()){
					MVConsensusMsg m2 = sentMessagesInPhase.getMessage(msg.getTag());
					if(m2 != null){
						m2.setResend(true);
						network.broadcastConsensusMsg(m2, participants);
					}
				}
			}
			else{
				// treat normal message
				messagesToDeliver.add(msg);
			}
		}
	}

	public void addParticipant(Short participantId, byte[] participantSecretKey) {
		participants.add(participantId);
		participantsKeys.put(participantId, participantSecretKey);
	}

	public boolean removeParticipant(Short participantId) {
		participantsKeys.remove(participantId);
		
		return participants.remove(participantId);
	}
	
	public BinaryConsensus getBinaryConsensus(){
		return binConsensus;
	}
	
	public Short getId(){
		return myId;
	}
	
	public void stop(){
		senderTask.kill();
		receiverTask.kill();
		binConsensusResultReader.kill();
	}
	
	public void switchToPhase(short newPhase){
		phase = newPhase;
		senderTask.resetSendCounter();
	}
	
	public Set<Short> getReceivedMsgsParticipants(){
		return received.getReceivedParticipants(phase);
	}
	
	
	private int getNumFaultyParticipantsAllowed(){
		return (participants.size()) / 3;
	}
	
	
	private class SenderTask extends KillableThread{
		
		private static final short MAX_SENDS = 5;
		private long period;
		private int sends = 0;
		
		
		public SenderTask(long period){
			super();
			this.period = period;
		}
		
		
		public void run(){
			while(isRunning){
				if(sends < MAX_SENDS){
					if(phase > 0 && phase < 3){
						byte[] proposal = proposedValue;
						
						if(phase == VECT_PHASE){
							proposal = received.getMostReceivedValueInPhase(INIT_PHASE);
						}
						
						MVConsensusMsg msg = new MVConsensusMsg(myId, participants, phase, proposal);
						
						receiveMessage(msg);
						sentMessagesInPhase.add(msg.getTag(), msg);
						network.broadcastConsensusMsg(msg, participants);
					}
					
					sends++;
				}
				
				try {
					sleep(period);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		public void resetSendCounter(){
			sends = 0;
		}
	}
		
	private class ReceiverTask extends Thread{
		
		private long period;
		private boolean isRunning = true;
		
		
		public ReceiverTask(long period){
			this.period = period;
		}
		
		
		public void kill(){
			isRunning = false;
		}
		
		public void run(){
			while(isRunning){
				if(!messagesToDeliver.isEmpty()){
					// it's odd, but sometimes we get null messages...
					MVConsensusMsg m = messagesToDeliver.poll();
					if(m != null){
						if(m.getTag() == MVConsensusMsg.INIT_TAG){
							treatInitMessage(m);
						}
						else if(m.getTag() == MVConsensusMsg.VECT_TAG){
							treatVectMessage(m);
						}
					}
				}
				
				try {
					sleep(period);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		private void treatInitMessage(MVConsensusMsg msg){
			if(phase != INIT_PHASE)
				return;
			
			received.add(msg);
			
			// if received more than n-2f messages with the same value
			if(received.getValueCount(msg.getValue(), INIT_PHASE) >= (participants.size()) - 2 * getNumFaultyParticipantsAllowed()){
				switchToPhase(VECT_PHASE);
			}
		}
		
		private void treatVectMessage(MVConsensusMsg msg){
			if(phase <= VECT_PHASE){
				received.add(msg);
				
				if(received.getNumMessagesInPhase(phase) >= participants.size() - getNumFaultyParticipantsAllowed()){
					switchToPhase(BIN_PHASE);
					senderTask.kill();
					
					byte[] mostReceived = received.getMostReceivedValueInPhase(VECT_PHASE);
					
					// if all messages have the same value and there are more than n-2f messages with the one value
					if(received.getValueCount(mostReceived, VECT_PHASE) >= (participants.size()) - 2 * getNumFaultyParticipantsAllowed()){
						binConsensus.init(true);
//						System.out.println(myId + ": proposing true. Value=" + mostReceived);
					}
					else{
						binConsensus.init(false);
//						System.out.println(myId + ": proposing false");
					}
					
					binConsensusResultReader.start();
				}
			}
			else if(phase == BIN_PHASE){
				return;
			}
			else if(phase == INIT_PHASE){
				messagesToDeliver.add(msg);
			}
		}
	}
	
	private class BinConsensusResultTask extends Thread{
		
		private long period;
		private boolean isRunning = true;
		
		
		public BinConsensusResultTask(long period){
			this.period = period;
		}
		
		
		public void kill(){
			isRunning = false;
		}
		
		public void run(){
			while(isRunning){
				Boolean decision = binConsensus.getDecision();
				
				if(decision != null){
					if(decision){
						decisionValue = received.getMostReceivedValueInPhase(VECT_PHASE);
					}
					else{
						decisionValue = null;
					}
					
					decided = true;
					
					binConsensus.stop();
					this.kill();
				}
				
				try {
					sleep(period);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
