package org.wisenet.protocols.myminsens.consensus.mymultivalued.exceptions;

public class InvalidMVConsensusMsgException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public InvalidMVConsensusMsgException(){
		super();
	}
	
	public InvalidMVConsensusMsgException(String msg){
		super(msg);
	}

}
