package org.wisenet.protocols.myminsens.consensus.mymultivalued;

import java.util.Arrays;

public class ByteArrayWrapper {

	private byte[] data;
	
	
	public ByteArrayWrapper(byte[] b){
		data = b;
	}


	public byte[] getData() {
		return data;
	}
	
	public boolean equals(Object other){
		return Arrays.equals(data, ((ByteArrayWrapper) other).getData());
	}
	
	public int hashCode(){
		return Arrays.hashCode(data);
	}
}
