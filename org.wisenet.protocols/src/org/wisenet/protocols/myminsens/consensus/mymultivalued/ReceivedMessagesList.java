package org.wisenet.protocols.myminsens.consensus.mymultivalued;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import org.wisenet.protocols.myminsens.consensus.mymultivalued.messages.MVConsensusMsg;

public class ReceivedMessagesList{

	public static final int NUM_PHASES = 2;
	
	private Vector<List<MVConsensusMsg>> msgsInPhase;
	private Vector<HashMap<ByteArrayWrapper,Integer>> valuesCount;
	private int msgCounter;
	
	
	public ReceivedMessagesList(){
		clear();
	}
	

	public boolean add(MVConsensusMsg e) {
		if(!isDuplicateMessage(e)){
			msgsInPhase.get(e.getTag()).add(e);
			msgCounter += 1;
			Integer v = valuesCount.get(e.getTag()).get(new ByteArrayWrapper(e.getValue()));
			
			if(v == null){
				v = 0;
			}
				
			v++;
			valuesCount.get(e.getTag()).put(new ByteArrayWrapper(e.getValue()), v);
			
			return true;
		}
		
		return false;
	}

	public void add(int index, MVConsensusMsg element) {
		add(element);
	}

	public boolean addAll(Collection<? extends MVConsensusMsg> c) {
		for(MVConsensusMsg m : c){
			add(m);
		}
		
		return true;
	}

	public boolean addAll(int index, Collection<? extends MVConsensusMsg> c) {
		return addAll(c);
	}

	public void clear() {
		msgsInPhase = new Vector<List<MVConsensusMsg>>(NUM_PHASES + 1);
		valuesCount = new Vector<HashMap<ByteArrayWrapper,Integer>>(NUM_PHASES + 1);
		msgCounter = 0;
		
		msgsInPhase.add(0, null);
		valuesCount.add(0, null);
		
		for(int i = 1; i < NUM_PHASES+1; i++){
			msgsInPhase.add(i, new LinkedList<MVConsensusMsg>());
			valuesCount.add(i, new HashMap<ByteArrayWrapper, Integer>());
		}
	}

	public int getValueCount(byte[] value, short phase){		
		Integer c = valuesCount.get(phase).get(new ByteArrayWrapper(value));
		
		if(c == null)
			return 0;
		else
			return c;
	}
	
	public int getNumMessagesInPhase(short phase){
		return msgsInPhase.get(phase).size();
	}
	
	public boolean allMessagesWithSameValue(short phase){
		return valuesCount.get(phase).size() == 1;
	}
	
	public byte[] getMostReceivedValueInPhase(short phase){
		byte[] highest = null;
		int highestCount = 0;
		
		for(Entry<ByteArrayWrapper, Integer> e: valuesCount.get(phase).entrySet()){
			if(e.getValue() > highestCount){
				highest = e.getKey().getData();
				highestCount = e.getValue();
			}
		}
		
		return highest;
	}
	
	public boolean isEmpty() {
		return msgCounter == 0;
	}
	
	public int size() {
		return msgCounter;
	}
	
	public Set<Short> getReceivedParticipants(int phase){
		Set<Short> receivedParticipants = new HashSet<Short>();
		
		for(MVConsensusMsg m: msgsInPhase.get(phase)){
			receivedParticipants.add((Short) m.getSourceId());
		}
		
		return receivedParticipants;
	}
	
	
	private boolean isDuplicateMessage(MVConsensusMsg m){
		for(MVConsensusMsg mm: msgsInPhase.get(m.getTag())){
			if((Short) m.getSourceId() == (Short) mm.getSourceId())
				return true;
		}
		
		return false;
	}
}
