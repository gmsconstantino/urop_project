package org.wisenet.protocols.myminsens.consensus.mymultivalued.messages;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.wisenet.protocols.common.ByteArrayDataInputStream;
import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.protocols.myminsens.consensus.ResendableMessage;

public class MVConsensusMsg extends ResendableMessage {

	public static final short INIT_TAG = 1;
	public static final short VECT_TAG = 2;
	
	private short source;
	private Set<Short> dests;
	
	private short tag;
	private byte[] value;
	
	
	public MVConsensusMsg(short source, Set<Short> dests, short tag, byte[] value){
		this.source = source;
		this.dests = dests;
		this.tag = tag;
		this.value = value;
	}
	
	public MVConsensusMsg(byte[] data) throws IOException{
		ByteArrayDataInputStream badis = new ByteArrayDataInputStream(data);
		
		source = badis.readShort();
		
		dests = new HashSet<Short>();
		int numDests = badis.readInt();
		for(int i=0; i< numDests; i++)
			dests.add(badis.readShort());
		
		tag = badis.readShort();
		
		int valueSize = badis.readInt();
		value = new byte[valueSize];
		badis.read(value);
		
		setResend(badis.readBoolean());
	}
	
	
	public short getSourceId(){
		return source;
	}
	
	public short getDestinationId(){
		return -1;
	}

	public Set<Short> getDestinations(){
		return dests;
	}
	
	public short getTag() {
		return tag;
	}

	public byte[] getValue() {
		return value;
	}
	
	
	public byte[] toByteArray() throws IOException{
		ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
		
		bados.writeShort(source);
		bados.writeInt(dests.size());
		
		for(Short s: dests)
			bados.writeShort(s);
		
		bados.writeShort(tag);
		bados.writeInt(value.length);
		bados.write(value);
		
		bados.writeBoolean(isResend());
		
		return bados.toByteArray();
	}
}
