package org.wisenet.protocols.myminsens.consensus.mymultivalued.exceptions;

public class UndecidedException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	
	public UndecidedException() {
		super();
	}
	
	public UndecidedException(String msg){
		super(msg);
	}
}
