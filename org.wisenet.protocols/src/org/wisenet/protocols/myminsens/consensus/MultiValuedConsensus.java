package org.wisenet.protocols.myminsens.consensus;

import org.wisenet.simulator.core.Message;

public interface MultiValuedConsensus<V> {

	public void init(V proposal);
	
	public V getDecision();
	
	public void receiveMessage(Message m);
	
	public void addParticipant(Short participantId, byte[] participantSecretKey);
	
	public boolean removeParticipant(Short participantId);
	
	public void stop();
	
	public BinaryConsensus getBinaryConsensus();
}
