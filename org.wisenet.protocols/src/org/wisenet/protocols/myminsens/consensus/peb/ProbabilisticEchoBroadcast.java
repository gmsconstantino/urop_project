package org.wisenet.protocols.myminsens.consensus.peb;

import java.util.HashSet;
import java.util.Set;

import org.wisenet.protocols.myminsens.basestation.communication.CommunicationChannel;
import org.wisenet.protocols.myminsens.consensus.peb.messages.PEBMessage;
import org.wisenet.protocols.myminsens.utils.KillableThread;
import org.wisenet.simulator.core.Message;

public class ProbabilisticEchoBroadcast extends KillableThread{

	private static final long SEND_PERIOD = 200;
	private static final int MAX_SENDS = 30;
	private int sends = 0;
	
	
	private Message m;
	private Set<Short> participants;
	private CommunicationChannel network;
	private short myId;
	
	private Set<Short> received;
	
	
	public ProbabilisticEchoBroadcast(CommunicationChannel network, short myId, Message m, Set<Short> participants){
		this.network = network;
		this.myId = myId;
		this.m = m;
		this.participants = participants;
		
		this.received = new HashSet<Short>();
	}
	
	
//	public synchronized void receivePEBMessage(PEBMessage msg){
//		if(msg.getTag() == PEBMessage.ECHO && participants.contains(msg.getSourceId()) && 
//				!received.contains(msg.getSourceId()) && msg.getMsg() == m)
//		{
//			received.add(msg.getSourceId());
//			
//			if(received.size() > getF()){
//				isRunning = false;
//			}
//		}
//	}
	
	public void run(){
		while(isRunning){
			if(sends < MAX_SENDS){
				
			}
		}
	}
	
	
	private void broadcast(){
		
	}
	
	private int getF(){
		return participants.size() / 3;
	}
}
