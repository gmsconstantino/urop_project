package org.wisenet.protocols.myminsens.consensus.peb.messages;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.wisenet.protocols.myminsens.MINSENSConstants;
import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.messages.data.INSENSMessagePayload;

public class PEBMessage extends INSENSMessagePayload{

	public static final byte INIT = 1;
	public static final byte ECHO = 2;
	
	private byte tag;
	private byte[] msg;
	private Set<Short> dests;
	
	private short source;
	
	
	public PEBMessage(short source, Set<Short> dests, byte tag, byte[] msg){
		super(MINSENSConstants.MSG_PEB);
		
		this.tag = tag;
		this.msg = msg;
		this.dests = dests;
		this.source = source;
		this.dests = dests;
	}

	public PEBMessage(byte[] payload) throws MINSENSException, IOException{
		super(payload);
		
		source = badis.readShort();
		
		dests = new HashSet<Short>();
		int ndests = badis.readShort();
		for(int i = 0; i < ndests; i++)
			dests.add(badis.readShort());
		
		tag = badis.readByte();
		
		int msgSize = badis.readInt();
		msg = new byte[msgSize];
		badis.read(msg);
	}
	

	public byte getTag() {
		return tag;
	}

	public byte[] getMsgBytes() {
		return msg;
	}

	public Set<Short> getDests() {
		return dests;
	}
}
