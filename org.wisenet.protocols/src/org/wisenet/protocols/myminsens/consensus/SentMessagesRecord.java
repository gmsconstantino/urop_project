package org.wisenet.protocols.myminsens.consensus;

import java.util.Hashtable;

/**
 * 
 * @author Jo�o Tavares
 *
 * @param <P> type that represents the phase where the message was sent
 * @param <M> type of the messages
 */
public class SentMessagesRecord<P,M> {

	private Hashtable<P, M> messagesSentPerPhase;
	
	
	public SentMessagesRecord(){
		messagesSentPerPhase = new Hashtable<P, M>();
	}
	
	
	public boolean add(P phase, M message){
		if(messagesSentPerPhase.containsKey(phase))
			return false;
		
		messagesSentPerPhase.put(phase, message);
		return true;
	}
	
	public M getMessage(P phase){
		return messagesSentPerPhase.get(phase);
	}
}
