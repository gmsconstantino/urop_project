/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.myminsens.messages.evaluation;

import org.wisenet.protocols.myminsens.messages.MINSENSDATAMessage;
import org.wisenet.simulator.components.instruments.IInstrumentMessage;

/**
 *
* @author  <MSc Student @di.fct.unl.pt>
 */
public class EvaluationMINSENSDATAMessage extends MINSENSDATAMessage implements IInstrumentMessage {

    /**
     *
     */
    public EvaluationMINSENSDATAMessage() {
        super("A".getBytes());
    }

    /**
     *
     * @param payload
     */
    public EvaluationMINSENSDATAMessage(byte[] payload) {
        super(payload);
    }

    public short getSourceId() {
        return getSource();
    }

    public short getDestinationId() {
        return getDestination();
    }

    public void setSourceId(Object id) {
        setSource((Short) id);
    }

    public void setDestinationId(Object id) {
        setDestination((Short) id);
    }

    public void setUniqueId(Object id) {
        setID((Long) id);
    }

//    public Object getUniqueId() {
//        return getID();
//    }
    public long getUniqueId() {
        return getID();
    }

    /**
     *
     * @param id
     */
    public void setUniqueId(long id) {
        setID(id);
    }
}
