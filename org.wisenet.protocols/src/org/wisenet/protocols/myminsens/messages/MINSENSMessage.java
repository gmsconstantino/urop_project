package org.wisenet.protocols.myminsens.messages;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wisenet.protocols.myminsens.MINSENSConstants;
import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.MINSENSFunctions;
import org.wisenet.simulator.core.Message;

/**
 *
 * @author  <MSc Student @di.fct.unl.pt>
 */
public class MINSENSMessage extends Message {

    /**
     *
     * @param payload
     */
    public MINSENSMessage(byte[] payload) {

        super(payload);
        byte type;
        try {
            type = MINSENSFunctions.getMessageType(payload);

            setMessageColor(type);
        } catch (MINSENSException ex) {
            Logger.getLogger(MINSENSMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     */
    public MINSENSMessage() {
        super();
    }
    
    public MINSENSMessage(MINSENSMessage clone){
    	super(clone);
    }

    private void setMessageColor(byte type) {
        switch (type) {
            case MINSENSConstants.MSG_FEEDBACK:
                setColor(Color.RED);
                break;
            case MINSENSConstants.MSG_DATA:
                setColor(Color.GREEN);
                break;
            case MINSENSConstants.MSG_ROUTE_REQUEST:
                setColor(Color.YELLOW);
                break;
            case MINSENSConstants.MSG_ROUTE_UPDATE:
                setColor(Color.CYAN);
                break;
            case MINSENSConstants.MSG_CONSENSUS:
            	setColor(Color.BLACK);
            	break;
        }
    }
    
    public Object clone(){
    	return new MINSENSMessage(this);
    }
}
