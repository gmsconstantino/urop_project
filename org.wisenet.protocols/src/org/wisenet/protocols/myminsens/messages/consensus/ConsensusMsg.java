package org.wisenet.protocols.myminsens.messages.consensus;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.protocols.myminsens.MINSENSConstants;
import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.messages.data.INSENSMessagePayload;

public class ConsensusMsg extends INSENSMessagePayload{

	public static final byte TURQUOIS_MSG = 1;
	public static final byte MVCONSENSUS_MSG = 2;
	
	private short source;
	private Set<Short> destinations;
	private short immediate;
	
	private long dataMsgId;
	
	private byte consType;
	private byte[] data;
	
	
	public ConsensusMsg(byte[] payload) throws MINSENSException, IOException {
		super(payload);

		destinations = new HashSet<Short>();
		
		source = badis.readShort();
		
		int nDests = badis.readInt();
		for(int i=0; i<nDests; i++)
			destinations.add(badis.readShort());
		
		immediate = badis.readShort();
		dataMsgId = badis.readLong();
		
		consType = badis.readByte();
		int dataSize = badis.readInt();
		data = new byte[dataSize];
		
		badis.read(data);
	}

	public ConsensusMsg(short source, Set<Short> destinations, short immediate, long dataMsgId, byte consType, byte[] data) throws MINSENSException{
		super(new byte[] {MINSENSConstants.MSG_CONSENSUS});
		
		this.source = source;
		this.destinations = destinations;
		this.immediate = immediate;
		this.dataMsgId = dataMsgId;
		this.consType = consType;
		this.data = data;
	}
	

	public short getImmediate() {
		return immediate;
	}

	public void setImmediate(short immediate) {
		this.immediate = immediate;
	}

	public short getSource() {
		return source;
	}

	public Set<Short> getDestinations() {
		return destinations;
	}

	public long getDataMsgId() {
		return dataMsgId;
	}

	public byte getConsType() {
		return consType;
	}

	public byte[] getData() {
		return data;
	}
	
	public byte[] toByteArray() throws IOException{
		ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream(); 
		
		bados.writeByte(MINSENSConstants.MSG_CONSENSUS);
		bados.writeShort(source);
		
		bados.writeInt(destinations.size());
		for(Short s: destinations)
			bados.writeShort(s);
		
		bados.writeShort(immediate);
		bados.writeLong(dataMsgId);
		bados.writeByte(consType);
		
		bados.writeInt(data.length);
		bados.write(data);
		
		return bados.toByteArray();
	}
}