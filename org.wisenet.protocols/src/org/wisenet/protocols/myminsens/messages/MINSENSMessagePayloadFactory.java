/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.myminsens.messages;

import java.io.IOException;
import java.util.Set;
import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.protocols.myminsens.MINSENSConstants;
import org.wisenet.protocols.myminsens.MINSENSFunctions;
import org.wisenet.protocols.myminsens.basestation.INSENSForwardingTable;
import org.wisenet.protocols.myminsens.messages.data.DATAPayload;
import org.wisenet.protocols.myminsens.utils.NeighborInfo;
import org.wisenet.simulator.core.node.Node;

/**
 *
 * 
 */
public class MINSENSMessagePayloadFactory {

    /**
     * Creates a Route Update Message Payload
     * @param src Source node id
     * @param dst Destination node id
     * @param imt Immediate sender node id
     * @param ows current round OWS sequence number
     * @param table forwarding table
     * @param key Private key 
     * @param node
     * @return
     */
    public static byte[] createRUPDPayload(short src, short dst, short imt , Set<Integer> routeIds, long ows, INSENSForwardingTable table, byte[] key, Node node) {
        try {
            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
            bados.writeByte(MINSENSConstants.MSG_ROUTE_UPDATE);
            bados.writeShort(src);
            bados.writeShort(dst);
            bados.writeShort(imt);  // Este tem de mudar de lugar tem q ir cifrado mas se for alterado tem um problema de ataque
            bados.writeInt(routeIds.size());
            for(Integer i: routeIds)
            	bados.writeInt(i);
            
            bados.writeLong(ows);
            table.write(bados);
            byte[] mac = MINSENSFunctions.createMAC(bados.toByteArray(), key, node);
            bados.write(mac);
            return bados.toByteArray();
        } catch (IOException ex) {
        	ex.printStackTrace();
//            Logger.getLogger(MINSENSMessagePayloadFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    /**
     * Creates a Route Request Message payload 
     * @param id source node id
     * @param ows message ows
     * @param parent_mac can be null if not parent mac needded
     * @param key private key to create mac
     * @param node
     * @return
     */
    public static byte[] createREQPayload(short bsId, short id, long ows, byte[] key, byte[] parent_mac, Node node) {
        try {
            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
            bados.writeByte(MINSENSConstants.MSG_ROUTE_REQUEST);
            bados.writeShort(bsId);
            bados.writeShort(id);
            bados.writeLong(ows);
            byte[] data = null;
            if (parent_mac == null) { // no parent mac, so is a first time message
                data = bados.toByteArray();
            } else { // append parent mac to mac, nested mac
                data = new byte[bados.toByteArray().length + parent_mac.length];
                System.arraycopy(bados.toByteArray(), 0, data, 0, bados.toByteArray().length);
                System.arraycopy(parent_mac, 0, data, bados.toByteArray().length, parent_mac.length);
            }
            byte[] mac = MINSENSFunctions.createMAC(data, key, node);
            bados.write(mac);
            return bados.toByteArray();
        } catch (IOException ex) {
        	ex.printStackTrace();
//            Logger.getLogger(MINSENSMessagePayloadFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    /**
     * Creates a Feedback Message Payload
     * @param id Feedback node id
     * @param key Private key of the node
     * @param neighborInfo Neighbor info data
     * @param parent_mac
     * @param node
     * @return
     */
    public static byte[] createFBKPayload(short id, byte[] key, NeighborInfo neighborInfo, byte[] parent_mac, Node node) {
        try {
            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
            bados.writeByte(MINSENSConstants.MSG_FEEDBACK);
            bados.writeShort(id);
            //TODO: Encrypt neighbor info
            byte[] neighborInfoData = neighborInfo.toByteArray();
            byte[] cipherNeighborInfoData = MINSENSFunctions.encryptData(node, neighborInfoData, key);
            bados.writeInt(cipherNeighborInfoData.length); // size of neighbor info data
            bados.write(cipherNeighborInfoData);
            byte[] data = bados.toByteArray();
            byte[] mac = MINSENSFunctions.createMAC(data, key, node);
            bados.write(parent_mac);
            bados.write(mac);
            return bados.toByteArray();
        } catch (IOException ex) {
        	ex.printStackTrace();
//            Logger.getLogger(MINSENSMessagePayloadFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     *
     * @param src
     * @param dst
     * @param imt
     * @param payload
     * @param key
     * @param node
     * @return
     */
    public static byte[] createDATAPayload(short src, short dst, short imt, long msgId, int routeId, Set<Integer> routeIds, Set<Short> targetBSs, long startTime, byte[] payload, byte[] key, Node node) {
        try {
            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
            bados.writeByte(MINSENSConstants.MSG_DATA);
            bados.writeShort(src);
            bados.writeShort(dst);
            bados.writeShort(imt);
            bados.writeLong(msgId);
            bados.writeInt(routeId);
            
            bados.writeInt(routeIds.size());
            for(Integer i: routeIds)
            	bados.writeInt(i);
            
            bados.writeInt(targetBSs.size());
            for(Short s: targetBSs)
            	bados.writeShort(s);
            
            bados.writeLong(startTime);
            
            byte[] cipherPayload = MINSENSFunctions.encryptData(node, payload, key);
            bados.writeInt(cipherPayload.length); // size of neighbor info data
            bados.write(cipherPayload);
            bados.write(payload);
            byte[] mac = MINSENSFunctions.createMAC(bados.toByteArray(), key, node);

            bados.write(mac);
            return bados.toByteArray();
        } catch (IOException ex) {
//            Logger.getLogger(MINSENSMessagePayloadFactory.class.getName()).log(Level.SEVERE, null, ex);
        	ex.printStackTrace();
        }
        return null;
    }
    
    public static byte[] createDATAPayload(short src, short dst, short imt, long msgId, int routeId, Set<Integer> routeIds, Set<Short> targetBSs, long startTime, byte[] cipherPayload, byte[] mac) {
        try {
            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
            bados.writeByte(MINSENSConstants.MSG_DATA);
            bados.writeShort(src);
            bados.writeShort(dst);
            bados.writeShort(imt);
            bados.writeLong(msgId);
            bados.writeInt(routeId);
            
            bados.writeInt(routeIds.size());
            for(Integer i: routeIds)
            	bados.writeInt(i);
            
            bados.writeInt(targetBSs.size());
            for(Short s: targetBSs)
            	bados.writeShort(s);
            
            bados.writeLong(startTime);
            
            bados.writeInt(cipherPayload.length); // size of neighbor info data
            bados.write(cipherPayload);

            bados.write(mac);
            return bados.toByteArray();
        } catch (IOException ex) {
//            Logger.getLogger(MINSENSMessagePayloadFactory.class.getName()).log(Level.SEVERE, null, ex);
        	ex.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param src
     * @param dst
     * @param imt
     * @param ows
     * @param table
     * @param mac
     * @param node
     * @return
     */
    public static byte[] updateRUPDPayload(short src, short dst, short imt, Set<Integer> routeIds, long ows, INSENSForwardingTable table, byte[] mac, Node node) {
        try {
            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
            bados.writeByte(MINSENSConstants.MSG_ROUTE_UPDATE);
            bados.writeShort(src);
            bados.writeShort(dst);
            bados.writeShort(imt);  // Este tem de mudar de lugar tem q ir cifrado mas se for alterado tem um problema de ataque
            
            bados.writeInt(routeIds.size());
            for(Integer i: routeIds)
            	bados.writeInt(i);
            
            bados.writeLong(ows);
            table.write(bados);
            bados.write(mac);
            return bados.toByteArray();
        } catch (IOException ex) {
        	ex.printStackTrace();
//            Logger.getLogger(MINSENSMessagePayloadFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
    public static byte[] createCREQPayload(short source, Set<Short> dests, short imt, long messageId, boolean isResend, byte[] mac) throws IOException{
		ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
		
		bados.writeByte(MINSENSConstants.MSG_CONSENSUS_REQUEST);
		
		bados.writeShort(source);
		
		bados.writeInt(dests.size());
		for(Short s: dests)
			bados.writeShort(s);
		
        bados.writeShort(imt);
        
        bados.writeLong(messageId);
        bados.writeBoolean(isResend);
        
        bados.writeInt(mac.length);
        bados.write(mac);
        
        return bados.toByteArray();
    }
    

    /**
     *
     * @param src
     * @param dst
     * @param imt
     * @param payload
     * @param mac
     * @param node
     * @return
     */
    public static byte[] updateDATAPayloadImmediate(DATAPayload p, short imt) {
    	return createDATAPayload(p.source, p.destination, imt, p.originalMsgId, p.routeId, p.routeIds, p.targetBSs, p.sentTime, p.data, p.mac);
    }
    
    public static byte[] updateDATAPayloadRouteId(DATAPayload p, int routeId) {
    	return createDATAPayload(p.source, p.destination, p.immediate, p.originalMsgId, routeId, p.routeIds, p.targetBSs, p.sentTime, p.data, p.mac);
    }

    public static byte[] createRUPDReplyPayload(short src, short dst, short imt ,Set<Integer> routeIds, int ftUID, long ows, byte[] mac, Node node) {
        try {
            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
            bados.writeByte(MINSENSConstants.MSG_ROUTE_UPDATE_ACK);
            bados.writeShort(src);
            bados.writeShort(dst);
            bados.writeShort(imt);  // Este tem de mudar de lugar tem q ir cifrado mas se for alterado tem um problema de ataque
           
            bados.writeInt(routeIds.size());
            for(Integer i: routeIds)
            	bados.writeInt(i);
            
            bados.writeInt(ftUID);
            bados.writeLong(ows);
            bados.write(mac);
            return bados.toByteArray();
        } catch (IOException ex) {
        	ex.printStackTrace();
//            Logger.getLogger(MINSENSMessagePayloadFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public static byte[] updateRUPDAckPayload(short src, short dst, short imt, Set<Integer> routeIds, int ftUID, long ows, byte[] mac, Node node) {
        try {
            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
            bados.writeByte(MINSENSConstants.MSG_ROUTE_UPDATE_ACK);
            bados.writeShort(src);
            bados.writeShort(dst);
            bados.writeShort(imt);  // Este tem de mudar de lugar tem q ir cifrado mas se for alterado tem um problema de ataque
            
            bados.writeInt(routeIds.size());
            for(Integer i: routeIds)
            	bados.writeInt(i);
            
            bados.writeInt(ftUID);
            bados.writeLong(ows);
            bados.write(mac);
            return bados.toByteArray();
        } catch (IOException ex) {
//            Logger.getLogger(MINSENSMessagePayloadFactory.class.getName()).log(Level.SEVERE, null, ex);
        	ex.printStackTrace();
        }
        return null;

    }
}
