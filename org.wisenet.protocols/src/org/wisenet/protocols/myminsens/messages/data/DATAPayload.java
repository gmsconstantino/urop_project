/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.myminsens.messages.data;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.messages.MINSENSMessagePayloadFactory;
import org.wisenet.simulator.utilities.CryptoFunctions;

/**
 *
 * 
 */
public class DATAPayload extends INSENSMessagePayload {
    public short source;
    public short destination;
    public short immediate;
    public long originalMsgId; // for consensus purposes

    public Set<Integer> routeIds;
    public int routeId;
    public Set<Short> targetBSs;
    
    // for time latency analysis
    public long sentTime;

    public int size;
    public byte[] data;
    public byte[] mac;

    /**
     *
     * @param payload
     * @throws MINSENSException
     */
    public DATAPayload(byte[] payload) throws MINSENSException {
        super(payload);

        try {
            source = badis.readShort();
            destination = badis.readShort();
            immediate = badis.readShort();
            originalMsgId = badis.readLong();
            routeId = badis.readInt();
            
            int nRoutes = badis.readInt();
            routeIds = new HashSet<Integer>();
            for(int i = 0; i < nRoutes; i++)
            	routeIds.add( badis.readInt() );
            
            int nTargetBSs = badis.readInt();
            targetBSs = new HashSet<Short>();
            for(int i = 0; i < nTargetBSs; i++)
            	targetBSs.add(badis.readShort());
            
            sentTime = badis.readLong();
            
            size = badis.readInt();
            data = new byte[size];
            badis.read(data);
            mac = new byte[CryptoFunctions.MAC_SIZE];
            badis.read(mac);
        } catch (IOException ex) {
            Logger.getLogger(DATAPayload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public DATAPayload clone(){
    	Set<Integer> routes = new HashSet<Integer>();
    	Set<Short> bss = new HashSet<Short>();
    	
    	for(Integer i: routeIds)
    		routes.add(i.intValue());
    	for(Short s: targetBSs)
    		bss.add(s.shortValue());
    	
    	try {
			return new DATAPayload(MINSENSMessagePayloadFactory.createDATAPayload(source, destination, immediate, originalMsgId, routeId, routes, bss, sentTime, data.clone(), mac.clone()));
		} catch (MINSENSException e) {
			e.printStackTrace();
		}
    	
    	return null;
    }
}
