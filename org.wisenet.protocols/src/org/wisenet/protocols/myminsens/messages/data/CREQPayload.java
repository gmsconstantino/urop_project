package org.wisenet.protocols.myminsens.messages.data;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.messages.MINSENSMessagePayloadFactory;


public class CREQPayload extends INSENSMessagePayload{
	
	
	private short source;
	private Set<Short> dests;
	private short immediate;
	private long msgId;
	private boolean isResend = false;
	private byte[] mac;
	
	
	public CREQPayload(byte[] payload) throws MINSENSException {
		super(payload);
		
		try{
			source = badis.readShort();
			
			dests = new HashSet<Short>();
			int numDests = badis.readInt();
			for(int i = 0; i < numDests; i++)
				dests.add(badis.readShort());
			
			immediate = badis.readShort();
			msgId = badis.readLong();
			isResend = badis.readBoolean();
			
			int macSize = badis.readInt();
			mac = new byte[macSize];
			badis.read(mac);
		}
		catch(IOException e){
			Logger.getLogger(CREQPayload.class.getName()).log(Level.SEVERE, null, e);
			e.printStackTrace();
		}
	}
	

	public short getSource() {
		return source;
	}

	public Set<Short> getDests() {
		return dests;
	}

	public short getImmediate() {
		return immediate;
	}

	public long getMsgId() {
		return msgId;
	}

	public byte[] getMac() {
		return mac;
	}
	
	public boolean isResend(){
		return isResend;
	}
	
	public void setResend(boolean isResend){
		this.isResend = isResend;
	}
	
	public byte[] toByteArray() throws IOException{
		return MINSENSMessagePayloadFactory.createCREQPayload(source, dests, immediate, msgId, isResend, mac);
	}
	
	public Object clone(){
		try {
			return new CREQPayload(MINSENSMessagePayloadFactory.createCREQPayload(source, dests, immediate, msgId, isResend, mac.clone()));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MINSENSException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
