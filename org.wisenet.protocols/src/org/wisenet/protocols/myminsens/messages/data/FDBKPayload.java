/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.myminsens.messages.data;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.simulator.utilities.CryptoFunctions;

/**
 *
 * @author 
 */
public class FDBKPayload extends INSENSMessagePayload {

    /**
     *
     */
    public short sourceId;
    /**
     *
     */
    public byte[] parent_mac;
    /**
     *
     */
    public byte[] neighborInfo;
    /**
     *
     */
    public int neighborInfoSize;
    /**
     *
     */
    public byte[] mac;

    /**
     *
     * @param payload
     * @throws MINSENSException
     */
    public FDBKPayload(byte[] payload) throws MINSENSException {
        super(payload);
        try {
            sourceId = badis.readShort();
            neighborInfoSize = badis.readInt();
            neighborInfo = new byte[neighborInfoSize];
            badis.read(neighborInfo);
            parent_mac = new byte[CryptoFunctions.MAC_SIZE];
            badis.read(parent_mac);
            mac = new byte[CryptoFunctions.MAC_SIZE];
            badis.read(mac);
        } catch (IOException ex) {
            Logger.getLogger(FDBKPayload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
