/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.myminsens.messages.data;

import org.wisenet.protocols.myminsens.MINSENSException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wisenet.protocols.common.ByteArrayDataInputStream;

/**
 * This is the base message information that every cleans slate message needs
 * @author 
 */
public class INSENSMessagePayload {

    /**
     *
     */
    protected ByteArrayDataInputStream badis;
    /**
     *
     */
    public byte type;

    public INSENSMessagePayload(byte type){
    	this.type = type;
    }
    
    /**
     *
     * @param payload
     * @throws MINSENSException
     */
    public INSENSMessagePayload(byte[] payload) throws MINSENSException {
        try {
            if (payload == null) {
                throw new MINSENSException("Invalid payload: cannot be null");
            }
            badis = new ByteArrayDataInputStream(payload);
            type = badis.readByte();
        } catch (IOException ex) {
            Logger.getLogger(INSENSMessagePayload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
