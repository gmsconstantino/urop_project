/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.myminsens.messages.data;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.basestation.INSENSForwardingTable;
import org.wisenet.simulator.utilities.CryptoFunctions;

/**
 *
 * 
 */
public class RUPDPayload extends INSENSMessagePayload {

    public short source;
    public short destination;
    public short immediate;
    
    public Set<Integer> routeIds;
    public long ows;
    public INSENSForwardingTable forwardingTable;
    public byte[] mac;
    
    /**
     *
     * @param payload
     * @throws MINSENSException
     */
    public RUPDPayload(byte[] payload) throws MINSENSException {
        super(payload);
        
        try {
            source = badis.readShort();
            destination = badis.readShort();
            immediate = badis.readShort();
            int nRoutes = badis.readInt();
            routeIds = new HashSet<Integer>();
            for(int i = 0; i < nRoutes; i++)
            	routeIds.add(badis.readInt());
            ows=badis.readLong();
            forwardingTable=new INSENSForwardingTable();
            forwardingTable.read(badis);
            mac = new byte[CryptoFunctions.MAC_SIZE];
            badis.read(mac);
        } catch (IOException ex) {
            Logger.getLogger(RUPDPayload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
