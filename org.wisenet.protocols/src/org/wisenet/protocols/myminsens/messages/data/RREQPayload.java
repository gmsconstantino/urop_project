/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.myminsens.messages.data;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.simulator.utilities.CryptoFunctions;

/**
 *
 * 
 */
public class RREQPayload extends INSENSMessagePayload {

	
	
	public short bsId;
    /**
     *
     */
    public short sourceId;
    /**
     *
     */
    public long ows;
    /**
     *
     */
    public byte[] mac;

    /**
     *
     * @param payload
     * @throws MINSENSException
     */
    public RREQPayload(byte[] payload) throws MINSENSException {
        super(payload);
        try {
        	bsId = badis.readShort(); 
            sourceId = badis.readShort();
            ows = badis.readLong();
            mac = new byte[CryptoFunctions.MAC_SIZE];
            badis.read(mac);
        } catch (IOException ex) {
            Logger.getLogger(RREQPayload.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
