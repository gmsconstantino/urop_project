package org.wisenet.protocols.myminsens.utils;

public class KillableThread extends Thread{

	protected boolean isRunning = true;
	
	
	public KillableThread(){
		super();
	}
	
	
	public void kill(){
		isRunning = false;
	}
}
