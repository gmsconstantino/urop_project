package org.wisenet.protocols.myminsens.utils;

/**
 *
* @author  <MSc Student @di.fct.unl.pt>
 */
public class NetworkKeyStoreException extends Exception{

	private static final long serialVersionUID = 1L;

	/**
     *
     * @param cause
     */
    public NetworkKeyStoreException(Throwable cause) {
        super(cause);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public NetworkKeyStoreException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     *
     * @param message
     */
    public NetworkKeyStoreException(String message) {
        super(message);
    }

    /**
     *
     */
    public NetworkKeyStoreException() {
    }

}
