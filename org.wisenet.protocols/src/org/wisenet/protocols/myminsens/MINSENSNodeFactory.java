package org.wisenet.protocols.myminsens;

import org.wisenet.protocols.myminsens.EvaluateMINSENSApplication;
import org.wisenet.protocols.myminsens.MINSENSNode;
import org.wisenet.simulator.core.node.factories.AbstractNodeFactory;
import org.wisenet.simulator.core.node.layers.mac.Mica2MACLayer;

public class MINSENSNodeFactory extends AbstractNodeFactory  {

	@Override
	public void setup() {
		setApplicationClass(EvaluateMINSENSApplication.class);
        setRoutingLayerClass(MINSENSRoutingLayer.class);
        setNodeClass(MINSENSNode.class);
        setMacLayer(Mica2MACLayer.class);
        setSetup(true);
		
	}

}
