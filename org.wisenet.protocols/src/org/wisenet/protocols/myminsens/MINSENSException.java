/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */

package org.wisenet.protocols.myminsens;

/**
 *
* @author  <MSc Student @di.fct.unl.pt>
 */
public class MINSENSException extends Exception{

	private static final long serialVersionUID = 1L;

	/**
     *
     * @param cause
     */
    public MINSENSException(Throwable cause) {
        super(cause);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public MINSENSException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     *
     * @param message
     */
    public MINSENSException(String message) {
        super(message);
    }

    /**
     *
     */
    public MINSENSException() {
    }


}
