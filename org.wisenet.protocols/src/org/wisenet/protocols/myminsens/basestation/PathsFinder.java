/*
 *     Wireless Sensor Network Simulator
 *   The next generation for WSN Simulations
 */
package org.wisenet.protocols.myminsens.basestation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.util.VertexPair;

import org.wisenet.protocols.myminsens.basestation.jgrapht.NetworkGraph;

/**
 *
 * @author  <MSc Student @di.fct.unl.pt>
 */
public class PathsFinder {

    public static final int NUMBER_OF_THREADS_FOR_1ST_PATHS = 2;
    public static final int NUMBER_OF_THREADS_FOR_2ND_PATHS = 2;
    private NetworkGraph graph = null;
    private Short start = null;
    private Queue<Short> vertices = null;
    private List<List<Short>> firstPaths = new ArrayList<List<Short>>();
    private List<List<Short>> otherPaths = new ArrayList<List<Short>>();
    private Queue<List<Short>> firstPathsQueue;

    public static synchronized List<DefaultEdge> findPath(NetworkGraph graph, Short start, Short end) {
        try {
            return DijkstraShortestPath.findPathBetween(graph, start, end);
        } catch (Exception e) {
            Logger.getLogger(PathsFinder.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }

    }

    public List<List<Short>> findFirstPaths() {
        vertices = new ArrayDeque<Short>(graph.vertexSet());
        Thread[] workers = new Thread[NUMBER_OF_THREADS_FOR_1ST_PATHS];
        
        for (int i = 0; i < workers.length; i++) {
            workers[i] = new Thread(new FirstFinder());
            workers[i].start();
        }
        
        for (int i = 0; i < workers.length; i++) {
            try {
                workers[i].join();
            } catch (InterruptedException ex) {
                Logger.getLogger(PathsFinder.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return firstPaths;
    }

    /**
     * Extract all vertices from edge path
     * @param graph_thread
     * @param path
     * @param start
     * @param end
     * @return
     */
    public static synchronized List<Short> extractVerticesFromEdges(NetworkGraph graph, List<DefaultEdge> path, Short start, short end) {
        List<Short> list = new ArrayList<Short>();
        
        if (path.isEmpty()) {
            return list;
        }
        
        Short curr = start;
        Short next = null;
        
        for (DefaultEdge defaultEdge : path) {
            VertexPair<Short> vp = new VertexPair<Short>(graph.getEdgeSource(defaultEdge), graph.getEdgeTarget(defaultEdge));
            next = vp.getOther(curr);
            list.add(curr);
            curr = next;
        }

        list.add(end);
        return list;
    }

    public List<List<Short>> getPaths() {
        return firstPaths;
    }

    public Short getStart() {
        return start;
    }

    public Queue<Short> getVertices() {
        return vertices;
    }

    PathsFinder(NetworkGraph graph, short start) {
        this.graph = graph;
        this.start = start;

    }

    private synchronized Short getNextEnd() {
        return vertices.poll();
    }

    private synchronized void addToPaths(List<Short> path) {
        firstPaths.add(path);
    }

    public List<List<Short>> findOtherPaths(List<List<Short>> firstPaths) {
        firstPathsQueue = new ArrayDeque<List<Short>>(firstPaths);
        Thread[] workers = new Thread[NUMBER_OF_THREADS_FOR_2ND_PATHS];
        
        for (int i = 0; i < workers.length; i++) {
            workers[i] = new Thread(new OtherFinder((NetworkGraph) graph.clone()));
            workers[i].start();
        }
        
        for (int i = 0; i < workers.length; i++) {
            try {
                workers[i].join();
            } catch (InterruptedException ex) {
                Logger.getLogger(PathsFinder.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return otherPaths;
    }

//        for (List firstPath : firstPaths) {
//            List secPath = calculateOtherPaths(graph_thread, firstPath);
//            if (!secPath.isEmpty()) {
//                paths2.add(secPath);
//            }
//        }
//        return paths2;
    class FirstFinder implements Runnable {

        public void run() {
            Short end = null;
            do {
                end = getNextEnd();
                if (end != (Short) start && end != null) {

                    List<DefaultEdge> path = (List<DefaultEdge>) findPath(graph, start, end);
                    if (path != null) {
                        addToPaths(extractVerticesFromEdges(graph, path, start, end));
                    }
                }
            } while (end != null);
        }
    }

    class OtherFinder implements Runnable {

        NetworkGraph graph_thread;

        public OtherFinder(NetworkGraph graph) {
            this.graph_thread = graph;
        }

        public void run() {
            List<Short> nextPath = null;
            do {
                nextPath = getNextPath();
                if (nextPath != null && !nextPath.isEmpty()) {

                    List<Short> path = processOtherPath(nextPath);
                    if (path != null && !path.isEmpty()) {
                        addToOtherPaths(path);
                    }
                }
            } while (nextPath != null);
        }

        private Set<DefaultEdge> getAllEdgesFromSet(NetworkGraph graph, Set<Short> set) {
            Set<DefaultEdge> edgesSet = new HashSet<DefaultEdge>();

            for (Short s : set) {
                Set<DefaultEdge> e = new HashSet<DefaultEdge>(graph.incomingEdgesOf(s));
                e.addAll(new HashSet<DefaultEdge>(graph.outgoingEdgesOf(s)));
                edgesSet.addAll(e);
            }
            
            return edgesSet;
        }

        private Set<Short> getAllNeighborsOf(NetworkGraph graph, Set<DefaultEdge> edgesSet, Short first, Short last) {
            Set<Short> neighborsSet = new HashSet<Short>();


            for (DefaultEdge edge : edgesSet) {
                neighborsSet.add(graph.getEdgeSource(edge));
                neighborsSet.add(graph.getEdgeTarget(edge));
            }
            
            neighborsSet.remove(first);
            neighborsSet.remove(last);

            return neighborsSet;
        }

        private List<Short> processOtherPath(List<Short> nextPath) {
            LinkedList<Short> list = new LinkedList<Short>(nextPath);
            Short first = list.removeFirst();
            Short last = list.removeLast();
            
            List<Short> result = new ArrayList<Short>();

            if (list.size() == 0) {
                return result;
            }
            /**
             * Todos os nos do caminho excepto 1 e ultimo
             */
            NetworkGraph g = (NetworkGraph) graph_thread.clone();
            Set<Short> S1 = new HashSet<Short>(list);
            Set<DefaultEdge> edgesS1 = getAllEdgesFromSet(graph_thread, S1);
//            g.removeAllVertices(S1);
//            List<DefaultEdge> pathS1 = (List<DefaultEdge>) findPath(g, first, last);
//
            Set<Short> S2 = getAllNeighborsOf(graph_thread, edgesS1, first, last);
            Set<DefaultEdge> edgesS2 = getAllEdgesFromSet(graph_thread, S2);
//            g.removeAllVertices(S2);
//            List<DefaultEdge> pathS2 = (List<DefaultEdge>) findPath(g, first, last);

            Set<Short> S3 = getAllNeighborsOf(graph_thread, edgesS2, first, last);
            Set<DefaultEdge> edgesS3 = getAllEdgesFromSet(graph_thread, S3);
            g.removeAllVertices(S3);
            List<DefaultEdge> pathS3 = (List<DefaultEdge>) findPath(g, first, last);
            if (pathS3 == null) {
                putBack(g, edgesS3, S3);
                g.removeAllVertices(S2);
                List<DefaultEdge> pathS2 = (List<DefaultEdge>) findPath(g, first, last);
                if (pathS2 == null) {
                    putBack(g, edgesS2, S2);
                    g.removeAllVertices(S1);
                    List<DefaultEdge> pathS1 = (List<DefaultEdge>) findPath(g, first, last);
                    if (pathS1 == null) {
                    } else {
                        result = extractVerticesFromEdges(g, pathS1, first, last);
                    }
                } else {
                    result = extractVerticesFromEdges(g, pathS2, first, last);
                }
            } else {
                result = extractVerticesFromEdges(g, pathS3, first, last);
            }

            return result;
        }

        private void putBack(NetworkGraph g, Set<DefaultEdge> edges, Set<Short> vertices) {
            for (Short s : vertices) {
                g.addVertex(s);
            }
            
            for (DefaultEdge e : edges) {
                Short s = g.getEdgeSource(e);
                Short t = g.getEdgeTarget(e);
                g.addEdge(s, t);
            }
        }
    }

    private synchronized void addToOtherPaths(List<Short> path) {
        otherPaths.add(path);
    }

    private synchronized List<Short> getNextPath() {
        return firstPathsQueue.poll();
    }
}
