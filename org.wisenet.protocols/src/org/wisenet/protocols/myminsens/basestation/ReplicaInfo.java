package org.wisenet.protocols.myminsens.basestation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.wisenet.protocols.myminsens.consensus.mymultivalued.ByteArrayWrapper;
import org.wisenet.protocols.myminsens.messages.data.DATAPayload;

public class ReplicaInfo {

	private Set<Integer> routes;
	private Set<Integer> receivedRoutes;
	private List<DATAPayload> replicas;
	
	// to determine the most received value, which is going to be selected as correct
	private HashMap<ByteArrayWrapper, Integer> valuesCounters;
	private DATAPayload mostReceivedValue;
	private int mostReceivedCount;
	
	
	public ReplicaInfo(Set<Integer> routes){
		this.routes = routes;
		
		receivedRoutes = new HashSet<Integer>();
		replicas = new LinkedList<DATAPayload>();
		valuesCounters = new HashMap<ByteArrayWrapper, Integer>();		
		mostReceivedCount = 0;
	}
	
	
	public boolean addReplica(DATAPayload p){
		// if didn't receive a replica from routeId
		if((routes.contains(p.routeId) || p.routeId == -1) && !receivedRoutes.contains(p.routeId)){
			receivedRoutes.add(p.routeId);
			replicas.add(p);
			
			Integer count = valuesCounters.get(new ByteArrayWrapper(p.data));
			if(count == null){
				count = 0;
				valuesCounters.put(new ByteArrayWrapper(p.data), count);
			}
			count++;
			
			if(count > mostReceivedCount){
				mostReceivedCount = count;
				mostReceivedValue = p;
			}
			
			return true;
		}
		
		return false;
	}
	
	public boolean hasEnoughReplicas(){
		return getNumReplicas() > (getNumMaxReplicas() / 2);
	}
	
	public DATAPayload getOneReplica(){
		return mostReceivedValue;
	}
	
	public byte[] getMostReceivedValue(){
		return mostReceivedValue.data;
	}
	
	public int getNumReplicas(){
		return replicas.size();
	}
	
	public int getNumMaxReplicas(){
		return routes.size();
	}
}
