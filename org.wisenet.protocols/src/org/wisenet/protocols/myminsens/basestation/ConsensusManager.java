package org.wisenet.protocols.myminsens.basestation;

import java.util.Collection;
import java.util.HashMap;

public class ConsensusManager {

	private HashMap<Long, ConsensusInstance> consensusInstances;
	
	
	public ConsensusManager(){
		consensusInstances = new HashMap<Long, ConsensusInstance>();
	}
	
	
	public boolean addConsensusInstance(ConsensusInstance ci){
		if(!consensusInstances.containsKey(ci.getMsgId())){
			consensusInstances.put(ci.getMsgId(), ci);
			return true;
		}
		
		return false;
	}
	
	public boolean consensusInstanceExists(long msgId){
		return consensusInstances.containsKey(msgId);
	}
	
	public ConsensusInstance getConsensusInstance(long msgId){
		return consensusInstances.get(msgId);
	}
	
	public Collection<ConsensusInstance> getConsensusInstances(){
		return consensusInstances.values();
	}
}
