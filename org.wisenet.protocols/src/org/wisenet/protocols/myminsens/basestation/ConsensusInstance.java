package org.wisenet.protocols.myminsens.basestation;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.basestation.communication.CommunicationChannel;
import org.wisenet.protocols.myminsens.consensus.mymultivalued.MVConsensus;
import org.wisenet.protocols.myminsens.consensus.mymultivalued.exceptions.UndecidedException;
import org.wisenet.protocols.myminsens.consensus.mymultivalued.messages.MVConsensusMsg;
import org.wisenet.protocols.myminsens.consensus.turquois.Turquois;
import org.wisenet.protocols.myminsens.consensus.turquois.messages.TurquoisMessage;
import org.wisenet.protocols.myminsens.messages.MINSENSMessagePayloadFactory;
import org.wisenet.protocols.myminsens.messages.consensus.ConsensusMsg;
import org.wisenet.protocols.myminsens.messages.data.CREQPayload;
import org.wisenet.protocols.myminsens.stats.StatsManager;
import org.wisenet.protocols.myminsens.utils.KillableThread;
import org.wisenet.simulator.core.Simulator;


public class ConsensusInstance {

	private static final byte PREPARING_STATE = 1;
	private static final byte RUNNING_STATE = 2;
	private static final byte DONE_STATE = 3;
	
	private static final long MSG_SEND_PERIOD = 500;
	
	private long originalMsgId;
	private byte[] proposal;
	private byte currentState;
	private short myNodeId;
	
	private Set<Short> participants;
	private Set<Short> receivedCREQs;
	private CREQSenderTask creqSender;
	
	private Queue<ConsensusMsg> pendingConsensusMsgs;
	
	private CommunicationChannel network;
	private MVConsensus consensus;
	
	private Simulator sim;
	private long consensusStartTime;
	
	
	public ConsensusInstance(Simulator sim, CommunicationChannel network, Set<Short> participants, short myNodeId, long msgId, long msgSentTime, byte[] proposalValue) throws MINSENSException, IOException{
		this.sim = sim;
		originalMsgId = msgId;
		proposal = proposalValue;
		this.myNodeId = myNodeId;
		
		currentState = PREPARING_STATE;
		
		this.network = network;
		
		this.participants = new HashSet<Short>();
		for(Short s: participants)
			this.participants.add(s);
		
		if(!this.participants.contains(myNodeId)){
			this.participants.add(myNodeId);
		}
		
		receivedCREQs = new HashSet<Short>();
		pendingConsensusMsgs = new LinkedList<ConsensusMsg>();
		
		CREQPayload p = new CREQPayload(MINSENSMessagePayloadFactory.createCREQPayload(myNodeId, participants, myNodeId, msgId, false, new byte[0]));
		receiveCREQMessage(p);
		
		consensusStartTime = sim.getSimulation().getCurrentSimulationTime();
		System.out.println(consensusStartTime - msgSentTime);
		
		StatsManager.getInstance().addCreqStarted(originalMsgId, this.myNodeId, this.participants, consensusStartTime - msgSentTime);
		System.out.println("CREQ Started!!!");
		
		creqSender = new CREQSenderTask(MSG_SEND_PERIOD, p);
		creqSender.start();
		
		Turquois bin = new Turquois(sim, myNodeId, network);
		consensus = new MVConsensus(sim, myNodeId, bin, network);
		
		for(Short id: participants){
			bin.addParticipant(id, null);
			consensus.addParticipant(id, null);
		}
	}
	
	
	public void startConsensus(){
		currentState = RUNNING_STATE;
		consensus.init(proposal);
		
		while(!pendingConsensusMsgs.isEmpty()){
			try {
				receiveConsensusMessage(pendingConsensusMsgs.poll());
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
		}
	}
	
	public synchronized byte[] getResult(){
		try{
			if(consensus != null){
				byte[] res = consensus.getDecision();
				currentState = DONE_STATE;
				long finishTime = sim.getSimulation().getCurrentSimulationTime();
				
				StatsManager manager = StatsManager.getInstance();
				manager.addReceived(originalMsgId, participants, myNodeId, true, finishTime - consensusStartTime);
				
				return res;
			}
			
			return null;
		}
		catch(UndecidedException e){
			return null;
		}
	}
	
	public long getMsgId(){
		return originalMsgId;
	}
	
	public synchronized boolean receiveCREQMessage(CREQPayload p){
		if(participants.contains(p.getSource())){
			if(isReceivingCREQs()){
				if(receivedCREQs.add(p.getSource())){					
					if(hasEnoughCREQs() && !isRunningConsensus()){
						
						StatsManager.getInstance().addCreqFinished(originalMsgId, myNodeId);
						startConsensus();
						creqSender.kill();
					}
					
					return true;
				}
			}
			else{ // resend creq
				if(!p.isResend()){
					receivedCREQs.add(p.getSource());
					
					try {
						network.broadcastCREQMessage(MINSENSMessagePayloadFactory.createCREQPayload(myNodeId, participants, myNodeId, originalMsgId, false, new byte[0]));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
		}
		
		return false;
	}
	
	public synchronized boolean receiveConsensusMessage(ConsensusMsg m) throws IOException{
		if(isRunningConsensus() || isDecided()){			
			if(m.getConsType() == ConsensusMsg.MVCONSENSUS_MSG){
				consensus.receiveMessage(new MVConsensusMsg(m.getData()));
			}
			else if(m.getConsType() == ConsensusMsg.TURQUOIS_MSG){
				consensus.getBinaryConsensus().receiveMessage(new TurquoisMessage(m.getData()));
			}
			else{
				return false;
			}
		}
		else{
			return pendingConsensusMsgs.add(m);
		}
		
		return true;
	}
	
	public boolean isReceivingCREQs(){
		return currentState == PREPARING_STATE;
	}
	
	public boolean isRunningConsensus(){
		return currentState == RUNNING_STATE;
	}
	
	public boolean isDecided(){
		return currentState == DONE_STATE;
	}
	
	public Set<Short> getReceivedParticipants(){
		return consensus.getReceivedMsgsParticipants();
	}
	
	public Set<Short> getReceivedParticipantsBin(){
		return ((Turquois)consensus.getBinaryConsensus()).getReceivedParticipants();
	}
	
	public Set<Short> getParticipants(){
		return participants;
	}
	
	private boolean hasEnoughCREQs(){
		return receivedCREQs.size() > getNplusFoverTwo();
	}
	
	private double getNplusFoverTwo(){
		return (double) ((participants.size() + getNumFaultyProcessesAllowed()) / 2);
	}
	
	private int getNumFaultyProcessesAllowed(){
		return (participants.size()) / 3;
	}
	
	
	private class CREQSenderTask extends KillableThread{
		
		private CREQPayload p;
		private final static int MAX_SENDS = 5;
		private long period;
		private int sends = 0;
		
		public CREQSenderTask(long delay, CREQPayload p){
			super();
			period = delay;
			this.p = p;
		}
		
		public void run(){
			while(isRunning){
				if(sends < MAX_SENDS){
					broadcast(p);
					sends++;
				}
				
				try {
					sleep(period);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		private void broadcast(CREQPayload p){
			try {
				network.broadcastCREQMessage(p.toByteArray());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
