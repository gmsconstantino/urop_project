package org.wisenet.protocols.myminsens.basestation.communication.tcp;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Set;


public class TCPSocketsManager {

	private HashMap<Short, ServerSocket> sockets;
	private HashMap<Short, HashMap<Short, Socket>> clientSockets;
	private static TCPSocketsManager instance = null;
	private int port = 6100;
	
	
	private TCPSocketsManager(){
		sockets = new HashMap<Short, ServerSocket>();
		clientSockets = new HashMap<Short, HashMap<Short,Socket>>();
	}
	
	
	public static TCPSocketsManager getInstance(){
		if(instance == null)
			instance = new TCPSocketsManager();
		
		return instance;
	}
	
	public synchronized boolean createServerSocket(short nodeId){
		boolean ret = false;
		
		try {
			ServerSocket server = new ServerSocket(port++);
			
			ret = sockets.put(nodeId, server) == null;
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ret;
	}
	
	/**
	 * 
	 * @param myId id of the requester node
	 * @param participants ids of all participnat nodes
	 * @return an hash map containing the server socket port for each participant
	 */
	public HashMap<Short, Integer> getServerSocketsPort(short myId, Set<Short> participants){
		HashMap<Short, Integer> res = new HashMap<Short, Integer>();
		
		for(Short participant: participants){
			if(participant != myId && sockets.containsKey(participant)){
				res.put(participant, sockets.get(participant).getLocalPort());
			}
		}
		
		return res;
	}
	
	public ServerSocket getServerSocket(short nodeId){
		if(sockets.containsKey(nodeId))
			return sockets.get(nodeId);
		
		return null;
	}
	
	public int getServerSocketPort(short nodeId){
		ServerSocket ss = getServerSocket(nodeId);
		
		if(ss == null)
			return 0;
		
		return ss.getLocalPort();
	}
	
	public synchronized Socket getMySocketTo(short myId, short nodeId) throws UnknownHostException, IOException{
		if(myId == nodeId)
			return null;
		
		HashMap<Short,Socket> mySockets = clientSockets.get(myId);
		
		if(mySockets == null){
			mySockets = new HashMap<Short, Socket>();
			clientSockets.put(myId, mySockets);
		}
		
		Socket otherSocket = mySockets.get(nodeId);
		
		if(otherSocket == null){
			int ssPort = getServerSocketPort(nodeId);
			otherSocket = new Socket("localhost", ssPort);
			
			mySockets.put(nodeId, otherSocket);
		}
		
		return otherSocket;
	}
}
