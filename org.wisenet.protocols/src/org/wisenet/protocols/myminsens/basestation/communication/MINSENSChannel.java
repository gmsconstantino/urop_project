package org.wisenet.protocols.myminsens.basestation.communication;

import java.io.IOException;
import java.util.Set;

import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.MINSENSRoutingLayer;
import org.wisenet.protocols.myminsens.consensus.mymultivalued.messages.MVConsensusMsg;
import org.wisenet.protocols.myminsens.consensus.peb.messages.PEBMessage;
import org.wisenet.protocols.myminsens.consensus.turquois.messages.TurquoisMessage;
import org.wisenet.protocols.myminsens.messages.consensus.ConsensusMsg;

import org.wisenet.simulator.core.Message;


public class MINSENSChannel implements CommunicationChannel {

	private MINSENSRoutingLayer rl;
	private short myNodeId;
	private long dataMsgId;
	
	
	public MINSENSChannel(MINSENSRoutingLayer rl, short myNodeId, long dataMsgId){
		this.rl = rl;
		this.myNodeId = myNodeId;
		this.dataMsgId = dataMsgId;
	}
	
	
	public synchronized void broadcastConsensusMsg(Message m, Set<Short> destinations){
		byte msgType = -1;
		byte[] data = null;
		
		try{
		
			if(m instanceof TurquoisMessage){
				TurquoisMessage msg = (TurquoisMessage) m;
				msgType = ConsensusMsg.TURQUOIS_MSG;
				data = msg.toByteArray();
			}
			else if(m instanceof MVConsensusMsg){
				MVConsensusMsg msg = (MVConsensusMsg) m;
				msgType = ConsensusMsg.MVCONSENSUS_MSG;
				data = msg.toByteArray();
			}
			
			ConsensusMsg consMsg = new ConsensusMsg((Short) m.getSourceId(), destinations, myNodeId, dataMsgId, msgType, data);
			rl.sendConsensusMessage(consMsg);
			
		} catch (MINSENSException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void broadcastCREQMessage(byte[] creqPayloadBytes) throws IOException{
		rl.sendCREQMessage(creqPayloadBytes);
	}


	public void broadcastPEBMessage(PEBMessage m) {}

	public void close() {/* no streams to close */}
}
