package org.wisenet.protocols.myminsens.basestation.communication;

import java.io.IOException;
import java.util.Set;

import org.wisenet.protocols.myminsens.consensus.peb.messages.PEBMessage;
import org.wisenet.simulator.core.Message;

public interface CommunicationChannel {

	public void broadcastConsensusMsg(Message m, Set<Short> destinations);
	public void broadcastCREQMessage(byte[] creqPayloadBytes) throws IOException;
	public void broadcastPEBMessage(PEBMessage m);
	public void close();
}