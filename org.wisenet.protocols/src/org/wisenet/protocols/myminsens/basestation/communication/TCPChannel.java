package org.wisenet.protocols.myminsens.basestation.communication;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.basestation.BaseStationController;
import org.wisenet.protocols.myminsens.consensus.mymultivalued.messages.MVConsensusMsg;
import org.wisenet.protocols.myminsens.consensus.peb.messages.PEBMessage;
import org.wisenet.protocols.myminsens.consensus.turquois.messages.TurquoisMessage;
import org.wisenet.protocols.myminsens.messages.consensus.ConsensusMsg;
import org.wisenet.protocols.myminsens.messages.data.CREQPayload;
import org.wisenet.simulator.core.Message;


public class TCPChannel implements CommunicationChannel{

	private short myNodeId;
	private long dataMsgId;
	private List<BaseStationController> participantsControllers;
	
	
	public TCPChannel(short myNodeId, long dataMsgId){
		this.myNodeId = myNodeId;
		this.dataMsgId = dataMsgId;
		participantsControllers = TCPSocketsManager.getInstance().getAllServerSocketsExcept(myNodeId);
	}
	
	
	public void broadcastConsensusMsg(Message m, Set<Short> destinations) {
		byte msgType = -1;
		byte[] data = null;
		try{
			if(m instanceof TurquoisMessage){
				TurquoisMessage msg = (TurquoisMessage) m;
				msgType = ConsensusMsg.TURQUOIS_MSG;
				data = msg.toByteArray();
			}
			else if(m instanceof MVConsensusMsg){
				MVConsensusMsg msg = (MVConsensusMsg) m;
				msgType = ConsensusMsg.MVCONSENSUS_MSG;
				data = msg.toByteArray();
			}
			
			ConsensusMsg consMsg = new ConsensusMsg((Short) m.getSourceId(), destinations, myNodeId, dataMsgId, msgType, data);
			
			for(BaseStationController bsc: participantsControllers)
				bsc.receiveConsensusMessage(consMsg);
		}
		catch(IOException e){
			e.printStackTrace();
		} catch (MINSENSException e) {
			e.printStackTrace();
		}
	}

	public void broadcastCREQMessage(byte[] creqPayloadBytes) throws IOException {
		for(BaseStationController bsc: participantsControllers){
			try {
				bsc.receiveCREQMessage(new CREQPayload(creqPayloadBytes));
			} catch (MINSENSException e) {
				e.printStackTrace();
				continue;
			}
		}
	}


	@Override
	public void broadcastPEBMessage(PEBMessage m) {
		// TODO Auto-generated method stub
		
	}
}
