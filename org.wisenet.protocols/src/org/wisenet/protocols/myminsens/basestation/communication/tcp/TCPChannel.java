package org.wisenet.protocols.myminsens.basestation.communication.tcp;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.basestation.communication.CommunicationChannel;
import org.wisenet.protocols.myminsens.consensus.mymultivalued.messages.MVConsensusMsg;
import org.wisenet.protocols.myminsens.consensus.peb.messages.PEBMessage;
import org.wisenet.protocols.myminsens.consensus.turquois.messages.TurquoisMessage;
import org.wisenet.protocols.myminsens.messages.consensus.ConsensusMsg;
import org.wisenet.simulator.core.Message;


public class TCPChannel implements CommunicationChannel{

	public static final short CREQ_MSG = 1;
	public static final short CONSENSUS_MSG = 2;
	public static final short CLOSE_MSG = 3;
	
	
	private short myNodeId;
	private long dataMsgId;
	private HashMap<Short, Socket> participantsSockets;
	
	
	public TCPChannel(short myNodeId, Set<Short> participants, long dataMsgId) throws IOException{
		this.myNodeId = myNodeId;
		this.dataMsgId = dataMsgId;
		participantsSockets = new HashMap<Short, Socket>();
		
		HashMap<Short,Integer> ports = TCPSocketsManager.getInstance().getServerSocketsPort(myNodeId, participants);
		
		for(Entry<Short, Integer> p: ports.entrySet()){
			Socket s = new Socket("localhost", p.getValue());
			participantsSockets.put(p.getKey(), s);
			
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());
			dos.writeShort(myNodeId);
			dos.writeLong(dataMsgId);
		}
	}
	
	
	public void broadcastConsensusMsg(Message m, Set<Short> destinations) {
		byte msgType = -1;
		byte[] data = null;
		try{
			if(m instanceof TurquoisMessage){
				TurquoisMessage msg = (TurquoisMessage) m;
				msgType = ConsensusMsg.TURQUOIS_MSG;
				data = msg.toByteArray();
			}
			else if(m instanceof MVConsensusMsg){
				MVConsensusMsg msg = (MVConsensusMsg) m;
				msgType = ConsensusMsg.MVCONSENSUS_MSG;
				data = msg.toByteArray();
			}
			
			ConsensusMsg consMsg = new ConsensusMsg((Short) m.getSourceId(), destinations, myNodeId, dataMsgId, msgType, data);
			
			for(Socket s: participantsSockets.values()){
				DataOutputStream dos = new DataOutputStream(s.getOutputStream());
				
				dos.writeShort(CONSENSUS_MSG);
				byte[] msgBytes = consMsg.toByteArray();
				
				dos.writeInt(msgBytes.length);
				dos.write(msgBytes);
			}
		}
		catch(IOException e){
			e.printStackTrace();
		} catch (MINSENSException e) {
			e.printStackTrace();
		}
	}

	public void broadcastCREQMessage(byte[] creqPayloadBytes) throws IOException {
		for(Socket s: participantsSockets.values()){
			DataOutputStream dos = new DataOutputStream(s.getOutputStream());
			
			dos.writeInt(creqPayloadBytes.length);
			dos.write(creqPayloadBytes);
		}
	}

	public void broadcastPEBMessage(PEBMessage m) {/* NOT IMPLEMENTED */}

	public void close() {
		for(Socket s: participantsSockets.values()){
			try {
				DataOutputStream dos = new DataOutputStream(s.getOutputStream());
				dos.writeShort(CLOSE_MSG);
				
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}
		}
	}
}
