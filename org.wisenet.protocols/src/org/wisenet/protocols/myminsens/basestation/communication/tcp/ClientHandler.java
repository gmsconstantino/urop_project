package org.wisenet.protocols.myminsens.basestation.communication.tcp;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

import org.wisenet.protocols.myminsens.MINSENSException;
import org.wisenet.protocols.myminsens.basestation.BaseStationController;
import org.wisenet.protocols.myminsens.messages.consensus.ConsensusMsg;
import org.wisenet.protocols.myminsens.messages.data.CREQPayload;

public class ClientHandler extends Thread{

	private short nodeId;
	private long msgId;
	private Socket clientSocket;
	private BaseStationController serverBs;
	private boolean isRunning = true;
	private DataInputStream dis;
	
	
	public ClientHandler(Socket clientSocket, BaseStationController serverBs) throws IOException{
		this.clientSocket = clientSocket;
		this.serverBs = serverBs;
		
		dis = new DataInputStream(this.clientSocket.getInputStream());
		
		nodeId = dis.readShort();
		msgId = dis.readLong();
	}
	
	
	public void run(){
		while(isRunning){
			try {
				short tcpMsgId = dis.readShort();
				int msgSize = dis.readInt();
				byte[] msgData = new byte[msgSize];
				dis.read(msgData);
				
				String msgType = tcpMsgId == TCPChannel.CREQ_MSG ? "CREQ" : "Consensus";
				System.out.println("Received " + msgType + "message!");
				
				switch(tcpMsgId){
				
				case(TCPChannel.CREQ_MSG):
					CREQPayload cp = new CREQPayload(msgData);
					handleCREQMessage(cp);
					break;
				case(TCPChannel.CONSENSUS_MSG):
					ConsensusMsg msg = new ConsensusMsg(msgData);
					handleConsensusMessage(msg);
					break;
				case(TCPChannel.CLOSE_MSG):
					handleClose();
					break;
				}
				
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			} catch (MINSENSException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void handleConsensusMessage(ConsensusMsg m) throws IOException{
		serverBs.receiveConsensusMessage(m);
	}
	
	private void handleCREQMessage(CREQPayload p) throws MINSENSException{
		serverBs.receiveCREQMessage(p);
	}
	
	private void handleClose() throws IOException{
		clientSocket.close();
		isRunning = false;
	}
}
