package org.wisenet.protocols.myminsens.basestation.communication;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import org.wisenet.protocols.myminsens.basestation.BaseStationController;

public class TCPSocketsManager {

	private HashMap<Short, BaseStationController> sockets;
	private static TCPSocketsManager instance = null;
	
	
	/**
	 * This is not really TCP implemented through Sockets, it's only a simulation
	 * using direct function call
	 */
	public TCPSocketsManager(){
		sockets = new HashMap<Short, BaseStationController>();
	}
	
	
	public static TCPSocketsManager getInstance(){
		if(instance == null)
			instance = new TCPSocketsManager();
		
		return instance;
	}
	
	public boolean addSocket(short nodeId, BaseStationController bsc){
		return sockets.put(nodeId, bsc) == null;
	}
	
	public BaseStationController getController(short nodeId){
		return sockets.get(nodeId);
	}
	
	public List<BaseStationController> getAllServerSocketsExcept(short nodeId){
		List<BaseStationController> servs = new LinkedList<BaseStationController>();
		
		for(Entry<Short, BaseStationController> e: sockets.entrySet()){
			if(e.getKey() != nodeId)
				servs.add(e.getValue());
		}
		
		return servs;
	}
}
