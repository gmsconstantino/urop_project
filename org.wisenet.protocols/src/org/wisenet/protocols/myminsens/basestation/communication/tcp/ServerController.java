package org.wisenet.protocols.myminsens.basestation.communication.tcp;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.wisenet.protocols.myminsens.basestation.BaseStationController;

public class ServerController extends Thread{

	private ServerSocket server;
	private BaseStationController bs;
	private boolean isRunning = true;
	
	
	public ServerController(ServerSocket server, BaseStationController bs){
		this.server = server;
		this.bs = bs;
	}
	
	
	public void run(){
		while(isRunning){
			try {
				Socket client = server.accept();
				new ClientHandler(client, bs).start();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
