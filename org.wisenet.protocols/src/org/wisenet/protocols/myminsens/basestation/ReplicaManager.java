package org.wisenet.protocols.myminsens.basestation;

import java.util.HashMap;

import org.wisenet.protocols.myminsens.messages.data.DATAPayload;

public class ReplicaManager {

	private HashMap<Long, ReplicaInfo> replicas;
	
	
	public ReplicaManager(){
		replicas = new HashMap<Long, ReplicaInfo>();
	}
	
	
	public boolean addReplica(DATAPayload dp){
		ReplicaInfo ri = replicas.get(dp.originalMsgId);
		
		if(ri == null){
			ri = new ReplicaInfo(dp.routeIds);
			replicas.put(dp.originalMsgId, ri);
		}
		
		return ri.addReplica(dp);
	}
	
	public boolean hasEnoughReplicas(long msgId){
		if(replicas.containsKey(msgId))
			return replicas.get(msgId).hasEnoughReplicas();
		
		return false;
	}
	
	public DATAPayload getMostReceivedReplica(long msgId){
		if(replicas.containsKey(msgId))
			return replicas.get(msgId).getOneReplica();
		
		return null;
	}
}
