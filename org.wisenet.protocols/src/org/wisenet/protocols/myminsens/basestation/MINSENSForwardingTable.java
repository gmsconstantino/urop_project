/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.myminsens.basestation;

import java.util.AbstractMap.SimpleEntry;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Random;
import org.wisenet.simulator.core.node.layers.routing.ForwardingTableInterface;

/**
 *
 * @author Andr� Guerreiro
 */
public class MINSENSForwardingTable implements ForwardingTableInterface {

    static int counter = 0;
    private int hopDistanceOnLastRoute;// only used to measure route balancement
    int uniqueId = counter++;
    short nodeId;
    Hashtable<Short,INSENSForwardingTable> fTables = new Hashtable<Short,INSENSForwardingTable>();
    int totalRoutesToBs = 0;
    int nextRRRoute = 0;
    short lastUsedBs;
    boolean isSink = false;

    /**
     *
     * @param nodeId
     */
    public MINSENSForwardingTable(short nodeId) {
        this.nodeId = nodeId;
    }

    /**
     *
     */
    public MINSENSForwardingTable() {
    }

    /**
     *
     * @return
     */
    public short getNodeId() {
        return nodeId;
    }

    /**
     *
     * @param nodeId
     */
    public void setNodeId(short nodeId) {
        this.nodeId = nodeId;
    }

    /**
     *
     * @return
     */
    public Set<RoutingTableEntry> getEntries(short bsId) {
        return fTables.get(bsId).getEntries();
    }


    /**
     *
     * @param source
     * @param destination
     * @return
     */
    public short getImmediate(short source, short destination) {
    	for(Short id : fTables.keySet()){
    		Set<RoutingTableEntry> entries = fTables.get(id).getEntries();
	        for (RoutingTableEntry routingTableEntry : entries) {
	            if (routingTableEntry.getSource() == source && routingTableEntry.getDestination() == destination) {
	                return routingTableEntry.getImmediate();
	            }
	        }
    	}
        return -1;
    }

    
    public int getRouteId(short source, short dest, short imt){
    	INSENSForwardingTable table = fTables.get(dest);
    	
    	if(table != null)
    		return table.getRouteId(source, dest, imt);
    	
    	return -1;
    }
    
    /**
     *
     * @param entry
     */
    public void add(Short bsId, INSENSForwardingTable table) {
       fTables.put(bsId, table);
       this.totalRoutesToBs+=table.getNumberOfRoutesToBS();
    }

    public boolean isStableOnBs(Short bsId){
    	return fTables.keySet().contains(bsId);
    }

    /**
     *
     * @param entry
     */
    public void remove(short bsId,RoutingTableEntry entry) {
        INSENSForwardingTable ft = fTables.get(bsId);
        if(ft != null){
        	ft.getEntries().remove(entry);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String out = "";
        out += "ForwardingTable[" + uniqueId + "] : " + nodeId + "\n";
        out += "-----------------------------\n";
        for(Short id : fTables.keySet()){
    		Set<RoutingTableEntry> entries = fTables.get(id).getEntries();
	        for (RoutingTableEntry routingTableEntry : entries) {
	            out += routingTableEntry + "\n";
	        }
        }
	   out += "-----------------------------\n";
        return out;
    }


    /**
     * Evaluate if a route exists in this forwing table
     * @param destination
     * @param source
     * @param immediate
     * @return
     */
    public boolean haveRoute(short destination, short source, short immediate, Set<Integer> routeIds) {
    	for(Short id : fTables.keySet()){
    		Set<RoutingTableEntry> entries = fTables.get(id).getEntries();
	    	for (RoutingTableEntry routingTableEntry : entries) {
	            if (routingTableEntry.getSource() == source && routingTableEntry.getDestination() == destination 
	            		&& routingTableEntry.getImmediate() == immediate
	            		&& routeIds.contains(routingTableEntry.routeId))//my routeId must be in the designated ones in order to forward
	            {
	            	hopDistanceOnLastRoute = routingTableEntry.getHopDistance();
	            	lastUsedBs = id;
	                return true;
	            }
	        }
    	}
    	
//    	INSENSForwardingTable ft = fTables.get(destination);
//    	if(ft != null){
//    		return ft.haveRoute(destination, source, immediate);
//    	}
    	
        return false;
    }
    
    
    public Hashtable<Short,Set<Integer>> getKRandomRoutes(int k){
    	Hashtable<Short,Set<Integer>> result = new Hashtable<Short,Set<Integer>>();
    	
    	for(int i = 0; i < k; i++){
    		SimpleEntry<Short,Integer> route = getRandomRoute();
    		Set<Integer> r = result.get(route.getKey());
    		if( r == null)
    			r = new HashSet<Integer>();
    		r.add(route.getValue());
    		result.put(route.getKey(), r);
    	}
    	return result;
    }

    public Hashtable<Short,Set<Integer>> getKRoutesBalancedByBS(int k){
    	Hashtable<Short,Set<Integer>> result = new Hashtable<Short,Set<Integer>>();
    	int perBS = k/fTables.size();
    	
    	for(INSENSForwardingTable ft: fTables.values() ){
    		Set<Integer> routes = new HashSet<Integer>();
    		Set<Integer> allRoutes = ft.getRouteIdsToBS();
    		int routesCount = 0;
    		
    		for(Integer i: allRoutes){
    			if(routesCount >= perBS)
    				break;
    			
    			routes.add(i);
    		}
    		
    		result.put(ft.getBsId(), routes);
    	}
    	
    	return result;
    }
    
    public Hashtable<Short,Set<Integer>> getAllRoutes(){
    	Hashtable<Short,Set<Integer>> routes = new Hashtable<Short,Set<Integer>>();
    	for(INSENSForwardingTable ft: fTables.values() ){
    		routes.put(ft.getBsId(),ft.getRouteIdsToBS());
    	}
    	return routes;
    }
    
    public SimpleEntry<Short,Integer> getRandomRoute(){
    	Random r = new Random();
    	int bsPos = r.nextInt(fTables.size());
    	SimpleEntry<Short,Integer> result = null;
    	
    	for(Entry<Short, INSENSForwardingTable> e: fTables.entrySet()){
    		if(bsPos == 0){
    			Integer routeId = e.getValue().getRandomRoute();
    			result = new SimpleEntry<Short, Integer>(e.getKey(), routeId);
    		}
    		else
    			bsPos--;
    	}
    	
    	return result;
    }
    
    public SimpleEntry<Short,Integer> getNextRouteRoundRobin(){
    	SimpleEntry<Short,Integer> result = null;
//    	int routeToFind = nextRRRoute;
//    	for( INSENSForwardingTable ft: fTables.values() ){
//    		if( routeToFind <= ft.getNumberOfRoutesToBS()){
//    			result = new SimpleEntry<Short,Integer>(ft.getBsId(),ft.getRouteIdsToBS().get(routeToFind));
//    			break;
//    		}else
//    			routeToFind-= ft.getNumberOfRoutesToBS();
//    	}
//    	nextRRRoute = (nextRRRoute++)%(totalRoutesToBs);
    	return result;
    }
    
    public int getUniqueId() {
        return uniqueId;
    }

	public int getHopDistanceToBS(short bsId) {
		return fTables.get(bsId).getHopDistanceToBS();
	}


	@Override
	public int getHopDistanceOnLastRoute() {
		return hopDistanceOnLastRoute;
	}
	
	public  void registerLastUsedBs(short bsId){
		lastUsedBs  = bsId;
	}

	@Override
	public int getHopDistanceToBS() {
		if(isSink)
			return 0;
		else
			return getHopDistanceToBS(lastUsedBs); // assumes registerLastUsedBs is used before a send()
	}

	@Override
	public void setHopDistanceToBS(int hopDistanceToBS) {
		
	}
	
	public void setAsSink(){
		this.isSink = true;
	}
}
