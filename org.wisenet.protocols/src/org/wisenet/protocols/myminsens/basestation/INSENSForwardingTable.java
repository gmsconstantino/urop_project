/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.protocols.myminsens.basestation;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.wisenet.protocols.common.ByteArrayDataInputStream;
import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.simulator.core.node.layers.routing.ForwardingTableInterface;

/**
 *
 * @author Andr� Guerreiro
 */
public class INSENSForwardingTable implements ForwardingTableInterface {

	private static int counter = 0;
    private int hopDistanceToBS ;//by default, 1hop nodes don't get their
    private int hopDistanceOnLastRoute;// only used to measure route balancement
    private int uniqueId = counter++;
    private short nodeId;
    private short bsId;
    private int routesToBs;
    
    private Set<RoutingTableEntry> entries = new HashSet<RoutingTableEntry>();

    /**
     *
     * @param nodeId
     */
    public INSENSForwardingTable(short nodeId, short bsId) {
        this.nodeId = nodeId;
        this.hopDistanceToBS = Integer.MAX_VALUE;
        this.routesToBs = 0;
        this.bsId = bsId;
    }

    /**
     *
     */
    public INSENSForwardingTable() {
    }

    /**
     *
     * @return
     */
    public short getNodeId() {
        return nodeId;
    }

    /**
     *
     * @param nodeId
     */
    public void setNodeId(short nodeId) {
        this.nodeId = nodeId;
    }
    
    public void increaseRoutesNumber(){
    	this.routesToBs++;
    }
    
    public int getNumberOfRoutesToBS(){
    	return this.routesToBs;
    }
    
    public int getRouteId(short source, short dest, short imt){
    	for(RoutingTableEntry rte: entries){
    		if(rte.getSource() == source && rte.getDestination() == dest && rte.getImmediate() == imt)
    			return rte.getRouteId();
    	}
    	
    	return -1;
    }
    
    public Set<Integer> getRouteIdsToBS(){
    	Set<Integer> lst = new HashSet<Integer>();
    	
    	for(RoutingTableEntry rte: entries){
    		lst.add(rte.getRouteId());
    	}
    	
    	return lst;
    }

    /**
     *
     * @return
     */
    public Set<RoutingTableEntry> getEntries() {
        return entries;
    }

    /**
     *
     * @param badis
     */
    public void read(ByteArrayDataInputStream badis) {
        try {
            uniqueId = badis.readInt();
            hopDistanceToBS = badis.readInt();
            bsId = badis.readShort();
            routesToBs = badis.readInt();
            int nrEntries = badis.readInt();
            for (int i = 0; i < nrEntries; i++) {
                RoutingTableEntry entry = new RoutingTableEntry();
                entry.read(badis);
                entries.add(entry);
            }
        } catch (Exception ex) {
            Logger.getLogger(INSENSForwardingTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param bados
     */
    public void write(ByteArrayDataOutputStream bados) {
        try {
            bados.writeInt(uniqueId);
            bados.writeInt(hopDistanceToBS);
            bados.writeShort(bsId);
            bados.writeInt(routesToBs);
            bados.writeInt(entries.size());
            for (RoutingTableEntry routingTableEntry : entries) {
                routingTableEntry.write(bados);
            }
        } catch (Exception ex) {
            Logger.getLogger(INSENSForwardingTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param source
     * @param destination
     * @return
     */
    public short getImmediate(short source, short destination) {
        for (RoutingTableEntry routingTableEntry : entries) {
            if (routingTableEntry.getSource() == source && routingTableEntry.getDestination() == destination) {
                return routingTableEntry.getImmediate();
            }
        }
        return -1;
    }

    /**
     *
     * @param entry
     */
    public void add(RoutingTableEntry entry) {
        entries.add(entry);
    }

    /**
     *
     * @param source
     * @param destination
     * @param immediate
     */
    public void add(short source, short destination, short immediate) {
        entries.add(new RoutingTableEntry(source, destination, immediate));
    }

    /**
     *
     * @param entry
     */
    public void remove(RoutingTableEntry entry) {
        entries.remove(entry);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String out = "";
        out += "ForwardingTable[" + uniqueId + "] : " + nodeId + "\n";
        out += "-----------------------------\n";
        for (RoutingTableEntry routingTableEntry : entries) {
            out += routingTableEntry + "\n";
        }
        out += "-----------------------------\n";
        return out;
    }

    /**
     *
     * @param forwardingTable
     */
    public void addAll(INSENSForwardingTable forwardingTable) {
        entries.addAll(forwardingTable.getEntries());
    }

    /**
     * Evaluate if a route exists in this forwarding table
     * @param destination
     * @param source
     * @param immediate
     * @return
     */
    public boolean haveRoute(short destination, short source, short immediate) {
        for (RoutingTableEntry routingTableEntry : entries) {
            if (routingTableEntry.getSource() == source && routingTableEntry.getDestination() == destination && routingTableEntry.getImmediate() == immediate) {
            	hopDistanceOnLastRoute = routingTableEntry.getHopDistance();
                return true;
            }
        }
        return false;
    }

    public int getUniqueId() {
        return uniqueId;
    }

	public int getHopDistanceToBS() {
		return hopDistanceToBS;
	}

	public void setHopDistanceToBS(int hopDistanceToBS) {
		this.hopDistanceToBS = hopDistanceToBS;
	}

	@Override
	public int getHopDistanceOnLastRoute() {
		return hopDistanceOnLastRoute;
	}

	public short getBsId() {
		return bsId;
	}
	
	public Integer getRandomRoute(){
		Random r = new Random();
		int routePos = r.nextInt(entries.size());
		Integer res = null;
		
		for(RoutingTableEntry e: entries){
			if(routePos == 0){
				res = e.getRouteId();
				break;
			}
			else
				routePos--;
		}
		
		return res;
	}
}
