package org.wisenet.protocols.cleanslate;

import java.security.MessageDigest;

import org.wisenet.protocols.MAC.tinysec.TinySecExtended;
import org.wisenet.protocols.common.Utils;

/**
 * 
 * @author Tiago Ara�jo
 *
 *  Valores para desafio
 */
public class ChallengeValues {
	
	private MessageDigest mdigest;
	
	//Valor final h(...(h(Ck)))
	private int c0;
	
	//Numero de vezes max que se pode aplicar o hash (k = long n)
	private int k;
	
	//Valor inicial h(Ck)
	private int ck;
	
	/**
	 * {(C0||Id)}Kna-1
	 */
	private byte[] cvSignature;
	
	private TinySecExtended tinySecExt;
	
	
	
	public ChallengeValues() {
		this.c0 = -1;
		this.k = -1;
		this.ck = -1;
		this.cvSignature = null;
		this.mdigest = null;
		this.tinySecExt = null;
	}
	
	
	/**
	 * h^k (Ck)=C0
	 * @param c0
	 * @param k
	 * @param ck
	 * @param cvSignature
	 */
	public ChallengeValues(int c0, int k, int ck, byte[] cvSignature, MessageDigest mdigest, TinySecExtended tinySecExt) {
		this.c0 = c0;
		this.k = k;
		this.ck = ck;
		this.cvSignature = cvSignature;
		this.mdigest = mdigest;
		this.tinySecExt = tinySecExt;
	}
	

	
	public byte[] getSignature(){
		return cvSignature;
	}
	
	public int getCi(int i) throws Exception{
		int iterations = k-i;
		int ci = ck;
		for(int j = 0; j<iterations ; j++){
			byte[] aux = Utils.intToByteArray(ci);
			byte[] aux2 = tinySecExt.createSignature(aux, mdigest);
			ci = Utils.byteArrayToInt(aux2);
		}
		return ci;
	}
	
	public int getC0(){
		return c0;
	}
	
	public MessageDigest getMessageDigest() {
		return mdigest;
	}
	
	public void setSignature(byte[] signature){
		cvSignature=signature;
	}
}