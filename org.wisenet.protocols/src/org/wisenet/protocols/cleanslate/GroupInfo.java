package org.wisenet.protocols.cleanslate;

/**
 * 
 * @author Tiago Ara�jo
 *
 * Cont�m a informa��o b�sica de um grupo
 */
public class GroupInfo implements Comparable<GroupInfo>, Cloneable {
	
	private long groupID;
	
	private int sizeGroup;
	
	public GroupInfo(long groupId, int sizeGroup){		
		this.groupID = groupId;
		this.sizeGroup = sizeGroup;
	}

	public long getGroupID() {
		return groupID;
	}

	public int getSizeGroup() {
		return sizeGroup;
	}
	
	public void setGroupId(long groupID){
		this.groupID = groupID;
	}
	
	public void setGroupSize(int sizeGroup){
		this.sizeGroup = sizeGroup;
	}

	public int compareTo(GroupInfo ngi) {
		if(sizeGroup < ngi.sizeGroup)
			return -1;
		else if (sizeGroup > ngi.sizeGroup)
			return 1;
		else if(sizeGroup == ngi.sizeGroup){
			if(groupID < ngi.groupID)
				return -1;
			else if(groupID > ngi.groupID)
				return 1;
			else return 0;
		}
		return 0;
	}
	
	public String toString() {
		return "[" + groupID + " , " + sizeGroup + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (groupID ^ (groupID >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupInfo other = (GroupInfo) obj;
		if (groupID != other.groupID)
			return false;
		return true;
	}
	
	/**
	 * groupID + sizegroup
	 * @return
	 */
	public int size(){
		return 8 + 4;
	}
	
	public GroupInfo clone() throws CloneNotSupportedException {
		return (GroupInfo)super.clone();
	}
}