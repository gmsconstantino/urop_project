package org.wisenet.protocols.cleanslate;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.wisenet.simulator.components.simulation.AbstractSimulation;
import org.wisenet.simulator.core.Simulator;

import org.wisenet.protocols.cleanslate.utils.UtilsCleanSlate;
import org.wisenet.protocols.common.Utils;



/**
 * 
 * @author Tiago Ara�jo
 * 
 * Classe auxiliar
 */
public class Auxiliary {
	
	public static Map<Long,List<CleanSlateRoutingLayer>> mergesToGroup = new HashMap<Long, List<CleanSlateRoutingLayer>>();
	public static Map<Long,Set<Long>> updates = new HashMap<Long,Set<Long>>();
	public static Map<Long,Integer> counterGroup = new HashMap<Long,Integer>();
	public static Map<Long,Integer> numEdgeNodes = new HashMap<Long,Integer>();
	public static Set<Long> interiorNodes = new HashSet<Long>();
	
	//PARA A GVT
	public static Map<Long,Set<Long>> gvtmessages = new HashMap<Long,Set<Long>>();
	public static Map<Long,CleanSlateRoutingLayer> gvtnodes = new HashMap<Long,CleanSlateRoutingLayer>();
	
	public static Set<Long> gvtfinalmessage = new HashSet<Long>();
	public static Map<Long,CleanSlateRoutingLayer> gvtfinalmsgnodes = new HashMap<Long, CleanSlateRoutingLayer>();
	
	//Para a remo��o de nos
	public static Map<Short,CleanSlateRoutingLayer> nodes = new HashMap<Short, CleanSlateRoutingLayer>();
	public static Set<Long> removenodes = new HashSet<Long>();
	
	
	
	public static void addToGroup(GroupInfo gi, CleanSlateRoutingLayer node){
		
		List<CleanSlateRoutingLayer> list = mergesToGroup.get(gi.getGroupID());
		if(list==null){
			list=new LinkedList<CleanSlateRoutingLayer>();
			list.add(node);
			mergesToGroup.put(gi.getGroupID(), list);
		}
		else{
			if(!list.contains(node))
				list.add(node);
		}
	}
	
	
	public static void lockgroups(GroupInfo g1, GroupInfo g2){
		
		lockgroup(g1,false,true);
		lockgroup(g2,false,false);
	}
	
	
	
	public static void lockgroup(GroupInfo g, boolean aux, boolean aux2){
		
		
		counterGroup.put(g.getGroupID(), 0);
		
		List<CleanSlateRoutingLayer> list = mergesToGroup.get(g.getGroupID());
		Iterator<CleanSlateRoutingLayer> it = list.iterator();
		while(it.hasNext()){
			CleanSlateRoutingLayer next = it.next();
			next.lock = true;
			
			if(aux2) //Alterado (Antes nao tinha)
				next.removeMergeEvent();
			
			if(aux)
				next.mergeDoned = true;
		}
	}
	
	
	public static void completedGroup(GroupInfo gi, AbstractSimulation abstractSimulation){

		boolean res = mergesToGroup.get(gi.getGroupID()).size() == gi.getSizeGroup();
		
		if(res){
			counterGroup.put(gi.getGroupID(), 0);
			launchUpdates(gi, abstractSimulation);
		}
	}
	
	
	private static void launchUpdates(GroupInfo gi, AbstractSimulation abstractSimulation){

		Iterator<CleanSlateRoutingLayer> it = mergesToGroup.get(gi.getGroupID()).iterator();
		updates.put(gi.getGroupID(), new HashSet<Long>());
		while(it.hasNext()){
			CleanSlateRoutingLayer csrl = it.next();
			
			csrl.removeMergeEvent();
			csrl.launchNeighborsUpdate();
		}
		
		Auxiliary.verifyUpdates(gi, abstractSimulation);
	}
	
	
	
	public static void addToCounter(GroupInfo gi){
		
		Integer i = counterGroup.get(gi.getGroupID());
		if(i==null)
			counterGroup.put(gi.getGroupID(), 1);
		else{
			int x = i +1 ;
			counterGroup.put(gi.getGroupID(), x);
		}
	}

	
	public static void verifyUpdates(GroupInfo gi, AbstractSimulation abstractSimulation){
	

		int num = numEdgeNodes.get(gi.getGroupID());
		int c = counterGroup.get(gi.getGroupID());
		
		if(c == num){
			if(updates.get(gi.getGroupID()).isEmpty())
				updates.remove(gi.getGroupID());
			else
				return;
		} else
			return ;
		
		unlockGroup(gi, abstractSimulation);		
	}
	
	
	private static void unlockGroup(GroupInfo gi, AbstractSimulation abstractSimulation) {
	
		
		counterGroup.put(gi.getGroupID(), 0);			
		
		List<CleanSlateRoutingLayer> list = mergesToGroup.get(gi.getGroupID());
		Iterator<CleanSlateRoutingLayer> it = list.iterator();
	
		while(it.hasNext()){
			CleanSlateRoutingLayer next = it.next();
			next.lock = false;
			next.mergeDoned = false;
		}
		
		
		it = list.iterator();
		while(it.hasNext()){
			CleanSlateRoutingLayer csrl = it.next();	
			//app.removeMaliciousNodes();
			if(csrl.isEdgeNode()){
				long time = Utils.randomLongBetween(Simulator.ONE_SECOND*15, Simulator.ONE_SECOND*20, new Random());
				csrl.sendMergingPropose(abstractSimulation.getCurrentSimulationTime() + time);
			}else {
				csrl.removeMergeEvent();
				removeEdgeNode(csrl.myGroupInfo.getGroupID(),csrl.uniqueID);
			}
		}
		

		//FORMAR DE CONTORNAR O PROBLEMA (ATEN��O... eventQueue.size() == 0)
		if(numEdgeNodes.get(gi.getGroupID()) == 0){
			it = list.iterator();
			if(it.hasNext()){
				if(abstractSimulation.getSimulator().eventQueue.size() == 0){
					System.out.println("Inicia remocao de nos");
					while(it.hasNext()){
						CleanSlateRoutingLayer csrlaux = it.next();	
						csrlaux.removeMaliciousNodes();
					}
				}
			}
		}
	}
	
	public static void unlockGroup2(GroupInfo gi){
		
		counterGroup.put(gi.getGroupID(), 0);
		updates.remove(gi.getGroupID());
		
		List<CleanSlateRoutingLayer> list = mergesToGroup.get(gi.getGroupID());
		Iterator<CleanSlateRoutingLayer> it = list.iterator();
	
		while(it.hasNext()){
			CleanSlateRoutingLayer next = it.next();
			next.lock = false;
			next.mergeDoned = false;
		}
	}
	
	public static void clearGroup(GroupInfo gi){
		List<CleanSlateRoutingLayer> list = mergesToGroup.get(gi.getGroupID());
		if(list!=null)
			list.clear();
	}
	
	public static void createWith1Group(long groupID, int num){
		numEdgeNodes.put(groupID, num);
	}
	
	public static void createWith2Groups(long groupID, GroupInfo old1, GroupInfo old2){
		
		
		int x1 = numEdgeNodes.get(old1.getGroupID());
		int x2 = numEdgeNodes.get(old2.getGroupID());
		int newx = x1+x2;
		
		numEdgeNodes.put(groupID, newx);
	}
	
	public static void removeEdgeNode(long groupID, long nodeID){
		
		if(!interiorNodes.contains(nodeID)){
			interiorNodes.add(nodeID);
			
			int x = numEdgeNodes.get(groupID);
			x--;
			numEdgeNodes.put(groupID, x);
		}
	}
	
	
	public static void addEdgeNode(long groupID, long nodeID){
		
		interiorNodes.remove(nodeID);
		
		int x = numEdgeNodes.get(groupID);
		x++;
		numEdgeNodes.put(groupID, x);
	}
	
	
	public static void addUpdateMessage(long groupID, long msgNumb){
		updates.get(groupID).add(msgNumb);
	}
	
	public static void remUpdateMessage(long groupID, long msgNumb){
		try{
			Set<Long> set = updates.get(groupID);
			if(set.contains(msgNumb))
				set.remove(msgNumb);
			else
				System.err.println("ERRO");
		} catch(NullPointerException e) {
			System.err.println("ERRO2 " + groupID);
		}
	}
	
	
	/////////////////  PARA A GVT
	
	
	public static void addGvtMessage(long groupID, long msgNumb){
		
		try {
			gvtmessages.get(groupID).add(msgNumb);
		} catch(NullPointerException e){
			System.err.println("ERRO1 " + groupID + " " + gvtmessages.get(groupID));
		}
	}
	

	
	public static void remBroadcastChallengeMessage(long groupID, long msgNumb){

		Set<Long> set = gvtmessages.get(groupID);
		if(set.contains(msgNumb))
			set.remove(msgNumb);
		else
			System.err.println("ERRO");
	}
	
	public static void verifyBroadcastChallenge(long groupID){
		
		CleanSlateRoutingLayer n = gvtnodes.get(groupID);

		if(gvtmessages.get(groupID).isEmpty() && n.responseMessageSent && n.crm!= null){
			gvtmessages.remove(groupID);
			n.checkingResponseMessage();
		}
	}
	
	
	public static void gvtaddNode(long groupID, CleanSlateRoutingLayer node){
		
		gvtnodes.put(groupID, node);
		Set<Long> set = gvtmessages.get(groupID);
		if(set==null){
			set = new HashSet<Long>();
			gvtmessages.put(groupID, set);
		}
	}
	
	
	//++
	public static void removeAndVerify(long removeGroupID, long groupID1, long groupID2){
		
		Auxiliary.gvtfinalmessage.remove(removeGroupID);
		
		if(!gvtfinalmessage.contains(groupID1) && !gvtfinalmessage.contains(groupID2)){
			CleanSlateRoutingLayer cs1 = gvtfinalmsgnodes.remove(groupID1);
			CleanSlateRoutingLayer cs2 = gvtfinalmsgnodes.remove(groupID2);
			cs1.finishgvt();
			cs2.finishgvt(); 
		}
	}
	
	public static void addGVTFinalMessage(long groupID, CleanSlateRoutingLayer node){
		Auxiliary.gvtfinalmessage.add(node.myGroupInfo.getGroupID());
		Auxiliary.gvtfinalmsgnodes.put(node.myGroupInfo.getGroupID(), node);
	}
	
	
	//NA REMO��O DE UM NO
	
	public static boolean hasLoop(long sourceID, short nexthopID, String address){
		
		CleanSlateRoutingLayer n = nodes.get(nexthopID);
		
		int i = UtilsCleanSlate.preMatching(address,n.address,n.routingTableM.size());
		
		if(i==-1 || i == -2)
			return false;
		else {
			if(nexthopID == sourceID)
				return true;
			
			Short newnexthop = n.routingTableM.get(i).getSecond();
			if(newnexthop==null)
				return false;
			
			return hasLoop(sourceID, newnexthop, address);
		}
	}
	
	public static void removeNode(long groupID, long nodeID){
		
		if(!interiorNodes.remove(nodeID) && !removenodes.contains(nodeID)){
			int x = numEdgeNodes.get(groupID);
			x--;
			numEdgeNodes.put(groupID, x);
			System.out.println("Auxiliary - remove no");
		}
		
		removenodes.add(nodeID);
	}
	
	
	public static void reset() {
		
		mergesToGroup.clear();
		updates.clear();
		counterGroup.clear();
		numEdgeNodes.clear();
		interiorNodes.clear();
		
		//PARA A GVT
		gvtmessages.clear();
		gvtnodes.clear();
		
		gvtfinalmessage.clear();
		gvtfinalmsgnodes.clear();
		
		//Para a remo��o de nos
		nodes.clear();
		removenodes.clear();	
	}	
}