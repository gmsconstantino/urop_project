package org.wisenet.protocols.cleanslate.utils;

import java.io.IOException;
import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;

import org.wisenet.protocols.MAC.tinysec.ConstantsTinySec;
import org.wisenet.protocols.MAC.tinysec.TinySecExtended;
import org.wisenet.protocols.cleanslate.CleanSlateConstants;
import org.wisenet.protocols.cleanslate.GroupInfo;
import org.wisenet.protocols.common.Pair;
import org.wisenet.protocols.common.Utils;

/**
 * 
 * @author Tiago Ara�jo
 *
 * Classe com m�todos auxiliares para o Clean-Slate
 *
 */
public class UtilsCleanSlate {
	
	/**
	 * 
	 * Verificar assinatura 
	 */
	public static boolean verifySignature(byte[] signature, short sourceID, Key keyNaPub, TinySecExtended tinySecExt){
		try {
			byte[] plain = tinySecExt.decrypt(signature, keyNaPub, 
					CleanSlateConstants.PropertiesCipher, ConstantsTinySec.PROVIDER, null);
			byte[] sourceIDbytes = Utils.shortToByteArray(sourceID);
			
			if(MessageDigest.isEqual(sourceIDbytes, plain))
				return true;
			else 
				return false; 
		}catch(Exception e){
			//e.printStackTrace();
		}
		return false;
	}
	
	
	/**
	 * 
	 * Obter o grupo que n�o aquele que � passado em argumento
	 */
	public static GroupInfo getOtherGroup(GroupInfo mygroup, GroupInfo g1, GroupInfo g2){
		try{
			if(g1.equals(mygroup))
				return g2.clone();
			else if (g2.equals(mygroup))
				return g1.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Obter o bit correcto para o endere�o
	 */
	public static Pair<String,String> getBitAddress(long otherID, long myID){
		if(myID < otherID) return new Pair<String,String>("0","1");
		else if(myID > otherID) return new Pair<String,String>("1","0");
		return null;
	}
	
	
	
	public static String calcNewSinkNodeAddress(long group1ID, long group2ID, short uniqueID, String sinknodeaddress){
		
		String newsinknodeaddreess = "";
		
		if(uniqueID == 0 || !sinknodeaddress.isEmpty()){ //COLOCAR SE � UM NO SINK
			Pair<String,String> pair = UtilsCleanSlate.getBitAddress(group1ID, group2ID);

			if(sinknodeaddress.isEmpty())
				newsinknodeaddreess = pair.getFirst();
			else
				newsinknodeaddreess = pair.getFirst() + "." + sinknodeaddress;
		}
		
		return newsinknodeaddreess;
	}
	
	
	/**
	 * Calcular o novo groupID
	 */
	public static int calculateNewGroupID(GroupInfo mygroup,GroupInfo othergroup, TinySecExtended tinySecExt) throws IOException {
		byte[] idmy = Utils.longToByteArray(mygroup.getGroupID());
		byte[] sizemy = Utils.intToByteArray(mygroup.getSizeGroup());
		byte[] idother = Utils.longToByteArray(othergroup.getGroupID());
		byte[] sizeother = Utils.intToByteArray(othergroup.getSizeGroup());
		byte[] res = null;
		
		if(mygroup.getGroupID() < othergroup.getGroupID()){
			
			byte[] res1 = Utils.appendByteArrays(idmy, sizemy);
			byte[] res2 = Utils.appendByteArrays(idother, sizeother);
			res = Utils.appendByteArrays(res1, res2);
			
		}else if(mygroup.getGroupID() > othergroup.getGroupID()){
			
			byte[] res1 = Utils.appendByteArrays(idmy, sizemy);
			byte[] res2 = Utils.appendByteArrays(idother, sizeother);
			res = Utils.appendByteArrays(res2, res1);	
		}
		
		
		try {
			byte[] hash = tinySecExt.createSignature(res, ConstantsTinySec.MSGDIGESTALGORITHM, ConstantsTinySec.PROVIDER);
			return Utils.toHex(hash).hashCode();
		} catch (Exception e) {
			throw new IOException();
		}
	}
	
	
	/**
	 * Actualizar a tabela de encaminhamento principal
	 */
	public static void updateRoutingTable(Vector<Pair<String,Short>> routingTable, short nextHopID, String myBit, String otherBit) {
		Pair<String,Short> newPair = new Pair<String,Short>(otherBit,nextHopID);
		Iterator<Pair<String,Short>> it = routingTable.iterator();
		while(it.hasNext()){
			Pair<String,Short> pair = it.next();
			pair.setFirst(myBit + "." + pair.getFirst());
		}
		
		routingTable.add(newPair);
	}

	
	/**
	 * Actualizar as tabelas de encaminhamento secundarias (L e R) 
	 */
	public static void updateOthersRoutingTables(Vector<Short> routingTableL, Vector<Short> routingTableR, int size, List<Short> nextHops, short nextHopM){
		
		Iterator<Short> it = nextHops.iterator();
		while(it.hasNext()){
			short nexthop = it.next();
			if(nexthop != nextHopM){
				if(routingTableL.size() < size  || routingTableL.get(size-1)==null)
					routingTableL.add(nexthop);
				else if((routingTableR.size() < size || routingTableR.get(size-1)==null) && !routingTableL.get(size-1).equals(nexthop))
					routingTableR.add(nexthop);
				else if(!routingTableL.get(size-1).equals(nexthop))
					break;
			}
		}
	}
	
	
	/**
	 * Actualizar a direc��o
	 */
	public static String updateDirection(String direction){
		
		if(direction.equals("random"))
			return direction;
		
		if(!direction.isEmpty())
			return direction.substring(1);
		return "";
	}
	
	
	/**
	 * 
	 * Obter o pr�ximo next-hop de acordo com a direc��o utilizada
	 */
	public static Short getNextHop(String direction, int index, Vector<Short> l, Vector<Short> r, Vector<Pair<String,Short>> m){
		
		
		if(direction.equals("random"))
			return randomlyDirection(index, l, r, m);
		else
			return notRandomlyDirection(direction,index,l,r,m);		
	}
	
	/**
	 * Obter next-hop com direc��o random
	 */
	private static Short randomlyDirection(int index, Vector<Short> l, Vector<Short> r, Vector<Pair<String,Short>> m){
		
		
		Short nexthopl = l.get(index);
		Short nexthopr = r.get(index);
		
		int i = 1;
		if(nexthopl != null)
			i++;
		if(nexthopr != null)
			i++;
		
		int x = (int)Utils.randomIntBetween(1, i, new Random());
		
		if(x == 1)
			return m.get(index).getSecond();
		else if(x==2) {
			if(nexthopl != null)
				return nexthopl;
			else 
				return nexthopr;
		} 
		else if (x == 3)
			return nexthopr;
		
		return m.get(index).getSecond();
	}
	
	
	/**
	 * Obter next-hop seguindo a direc��o indicada
	 */
	private static Short notRandomlyDirection(String direction, int index, Vector<Short> l, Vector<Short> r, Vector<Pair<String,Short>> m){
		
		if(direction.isEmpty())
			return m.get(index).getSecond();
		else {
			
			char c = direction.charAt(0);
			switch (c) {
			case 'L':
			{
				Short res = l.get(index);
				if(res != null)
					return res;
				else
					return m.get(index).getSecond();	
			}	
			case 'M':
				return m.get(index).getSecond();
			case 'R': {
				Short res = r.get(index);
				if(res != null)
					return res;
				else
					return m.get(index).getSecond();
			}
			default:
				System.err.println("ERROR - INVALID DIRECTION");
				return null;
			}
		}
	}
	
	
	
	public static Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> getNextHopFromPrefixAux(Vector<Pair<String,Short>> routingTable, String prefix){
		
		Pair<Short,Short> resAux = new Pair<Short,Short>(null,null);
			
		ListIterator<Pair<String,Short>> it = routingTable.listIterator(routingTable.size());
		Short toZero = null;
		Short toOne = null;
		
		while(it.hasPrevious() && (resAux.getFirst() == null || resAux.getSecond() == null)){
			Pair<String,Short> pair = it.previous();
			
			String addr = pair.getFirst();
			if(addr.startsWith(prefix)){
				char aux= getChar(prefix, addr);
				
				if(aux=='0'){
					resAux.setFirst(pair.getSecond());
					if(pair.getSecond()==null)
						; 
				}
				else if(aux=='1'){
					resAux.setSecond(pair.getSecond());
					if(pair.getSecond()==null)
						; 
				}
				else if(addr.length() == prefix.length()){
					if(addr.charAt(addr.length()-1)=='0'){
						toZero = pair.getSecond();
						if(pair.getSecond()==null)
							;
					}
					else if(addr.charAt(addr.length()-1)=='1'){
						toOne = pair.getSecond();
						if(pair.getSecond()==null)
							;
					}
				}
			}
		}
		
		
		Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> res = 
			new Pair<Pair<Short,Boolean>,Pair<Short,Boolean>>(
					new Pair<Short,Boolean>(resAux.getFirst(),false), 
					new Pair<Short,Boolean>(resAux.getSecond(),false)
			);
		
		if(resAux.getFirst() == null){
			if(toZero!=null){
				res.getFirst().setFirst(toZero);
				res.getFirst().setSecond(true);
			}
		}
		if(resAux.getSecond() == null){
			if(toOne!=null){
				res.getSecond().setFirst(toOne);
				res.getSecond().setSecond(true);
			}
		}
		
		return res;
	}
	

	
	private static char getChar(String prefix, String addr){
		char aux= '-';
		if(prefix.isEmpty())
			aux = addr.charAt(prefix.length());
		else if(addr.length() > prefix.length()+1)
			aux = addr.charAt(prefix.length()+1);
		return aux;
	}
	
	public static String getPrefixAux(String prefix, Pair<Short,Boolean> pair, int i){
		String prefixRes = "";
		if(pair.getSecond())
			prefixRes = prefix;
		else
			prefixRes = prefix + "." + i;
		return prefixRes;
	}
	
	/**
	 * Verificar se o destino � poss�vel atingir a partir do endere�o passado em argumento
	 */
	public static int preMatching(String destination, String address, int nmerges){
		
		if(address.equals(destination))
			return -2;
		
		StringTokenizer st = new StringTokenizer(destination,".");
		StringTokenizer st2 = new StringTokenizer(address,".");
		
		int z = 0;
		while(st.hasMoreTokens() && st2.hasMoreTokens()){
			
			z++;
			try {
				String str1 = st.nextToken();
				String str2 = st2.nextToken();
				if(!str1.equals(str2))
					return nmerges-z;
			}catch(java.lang.StackOverflowError e){
				return -1;
			}
		}
		
		return -1;
	}
	

	
	@SuppressWarnings("unchecked")
	/**
	 * Fazer uma c�pia de um vector
	 */
	public static Vector<Pair<String,Short>> getCopyVector(Vector<Pair<String,Short>> old){
		Vector<Pair<String,Short>> newVector = new Vector<Pair<String,Short>>();
		Iterator<Pair<String,Short>> it = old.iterator();
		while(it.hasNext()){
			Pair<String,Short> next = it.next();
			newVector.add((Pair<String, Short>) next.clone());
		}
		return newVector;
	}
	

	
	public static boolean verifyAndCheck(byte[] signature, short challengerID, int c0, int ci,
			int iter, Key keyNaPub, byte[] ivector, MessageDigest md, TinySecExtended tinySecExt){
		
		try {

			//Decifrar {C0||ChallengerID}Kna-1
			byte[] signatureDecrp = tinySecExt.decrypt(signature, keyNaPub, 
					CleanSlateConstants.PropertiesCipher, ConstantsTinySec.PROVIDER, ivector);
			
			byte[] c0Byte = Utils.intToByteArray(c0);
			byte[] idByte = Utils.shortToByteArray(challengerID);
			byte[] res = Utils.appendByteArrays(c0Byte, idByte);
			
			if(Arrays.equals(signatureDecrp, res))
				return tinySecExt.verifyOWS(md, ci, c0, iter);
			
		}catch(Exception e){
			return false;
		}
		return false;
	}
	
	/**
	 * Verificar se � o n� desafiador
	 */
	public static boolean chooseChallenger(MessageDigest md, String address, long groupID,TinySecExtended tinySecExt) throws Exception{
		if(address.isEmpty())
			return true;
		
		byte[] x = Utils.longToByteArray(groupID);
		byte[] hash = tinySecExt.createSignature(x, md);
		BigInteger bi = new BigInteger(hash);
		String str = bi.toString(2);
		if(str.charAt(0)=='-')
			str = str.substring(1);
		
		String newaddress = newString(".", address);
		
		if(str.startsWith(newaddress))
			return true;
		
		return false;
	}
	
	
	/**
	 * Verificar se � o n� respondedor
	 */
	public static boolean chooseResponder(MessageDigest md, GroupInfo otherGroup, GroupInfo myGroup, int ci, String address, TinySecExtended tinySecExt) throws Exception{
		//Calcular r = F(Idg, |G|, Idg', |G'|, Ci)
		
		if(address.isEmpty())
			return true;
		
		
		byte[] idother = Utils.longToByteArray(otherGroup.getGroupID());
		byte[] sizeother = Utils.intToByteArray(otherGroup.getSizeGroup());
		byte[] idmy = Utils.longToByteArray(myGroup.getGroupID());
		byte[] sizemy = Utils.intToByteArray(myGroup.getSizeGroup());
		byte[] cibyte = Utils.intToByteArray(ci);
		
		byte[] res1 = Utils.appendByteArrays(idother, sizeother);
		byte[] res2 = Utils.appendByteArrays(idmy, sizemy);
		byte[] res3 = Utils.appendByteArrays(res1, res2);
		byte[] res = Utils.appendByteArrays(res3,cibyte);
		byte[] hash = tinySecExt.createSignature(res, md);
		
		BigInteger bi = new BigInteger(hash);
		String str = bi.toString(2);
		if(str.charAt(0)=='-')
			str = str.substring(1);
		
		String newaddress = newString(".", address); 
		
		if(str.startsWith(newaddress))
			return true;
		
		return false;
	}
	
	
	
	/**
	 * Verificar a assinatura do respondedor
	 */
	public static boolean verifyResponserSignature(short responderID, byte[] signatureRespID, Key keyNaPub, TinySecExtended tinySecExt) throws Exception{
		byte[] idbytes = Utils.shortToByteArray(responderID);
		byte[] plainId = tinySecExt.decrypt(signatureRespID, keyNaPub, 
				CleanSlateConstants.PropertiesCipher, ConstantsTinySec.PROVIDER, null);
		return MessageDigest.isEqual(idbytes, plainId); 
	}
	
	
	private static String newString(String delim, String str){
		StringTokenizer st = new StringTokenizer(str,delim);
		String newString = "";
		
		while(st.hasMoreTokens())
			newString+=st.nextToken();
			
		return newString;
	}
	
	
	/**
	 * Verificar a GVT
	 */
	public static boolean verifyGVT(List<GroupInfo> mergeTable, short responderID, GroupInfo responderGroup, TinySecExtended tinySecExt) throws IOException{
		Iterator<GroupInfo> it = mergeTable.iterator();
		GroupInfo aux = new GroupInfo(responderID,1);
		
		while(it.hasNext()){
			GroupInfo nextGroup = it.next();
			int newGroupID = calculateNewGroupID(aux, nextGroup,tinySecExt);
			int newGroupSize = aux.getSizeGroup() + nextGroup.getSizeGroup();
			aux = new GroupInfo(newGroupID,newGroupSize);
		}
		
		return aux.equals(responderGroup);
	}
	
	
	
	///////////Substituir edge node
	
	/**
	 * Substituir edge node
	 */
	public static Pair<Vector<String>,Vector<Integer>> replaceNode(Short nodeID, Vector<Short> routingTableL, Vector<Short> routingTableR, Vector<Pair<String,Short>> routingTableM){
		
			
		Vector<String> vect1 = new Vector<String>();
		Vector<Integer> vect2 = new Vector<Integer>();
		Pair<Vector<String>,Vector<Integer>> res = new Pair<Vector<String>,Vector<Integer>>(vect1,vect2);
		
		if(nodeID == null)
			return res;
		
		Iterator<Pair<String,Short>> it1 = routingTableM.iterator();
		Iterator<Short> it2 = routingTableL.iterator();
		Iterator<Short> it3 = routingTableR.iterator();
		int i = 0;
		while(it1.hasNext()){
			Pair<String,Short> next1 = it1.next();
			Short next2 = it2.next();
			Short next3 = it3.next();
			if(nodeID.equals(next1.getSecond())){
				if(next2 != null){
					routingTableL.set(i, null);
					next1.setSecond(next2);
				}else if(next3 != null){
					routingTableR.set(i, null);
					next1.setSecond(next3);
				}else {
					vect1.add(next1.getFirst());
					vect2.add(i);
					next1.setSecond(null);
				}
			}
			
			Short l = routingTableL.get(i);
			if(l!= null && nodeID.equals(l))
				routingTableL.set(i,null);
			
			Short r = routingTableL.get(i);
			if(r != null && nodeID.equals(r))
				routingTableR.set(i,null);
		
			i++;
		}
		
		return res;
	}
	
	
	public static Integer containsAddress(Vector<String> vect1, Vector<Integer> vect2, String addressaux){
		
		Iterator<String> it1 = vect1.iterator();
		Iterator<Integer> it2 = vect2.iterator();
		
		int i = 0;
		while(it1.hasNext() && it2.hasNext()){
			String address = it1.next();
			int index = it2.next();
			
			if(address.equals(addressaux)){
				vect1.removeElementAt(i);
				vect2.removeElementAt(i);
				return index;
			}
			i++;
		}
		return null;
	}
}