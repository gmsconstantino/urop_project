package org.wisenet.protocols.cleanslate.utils.rabin;

// Class by Robin Abraham

import java.math.*;
import java.util.*;

public class RabinEncrypt {

	 
	public static int getSize(String plainText){
		
		KeySet keys = new KeySet(256);
		String cipher = encipher(plainText,keys.getN());
		
		return cipher.getBytes().length;
	}

	
	private static String encipher(String plainText, BigInteger n) {
		
		String ciphertext = "";
		StringTokenizer t = new StringTokenizer(plainText, "\n");
		BigInteger two = BigInteger.valueOf(2);		
		while ( t.hasMoreTokens()) {
		    //System.out.println("******************************");
	    	String s = new String();
	    	s = t.nextToken();
			// add extra bits to the end of plaintext
		    byte[] temp = s.getBytes();
	    	byte[] rabinbytes = new byte[temp.length + 1];
					   
		    System.arraycopy(temp, 0, rabinbytes, 0, temp.length);	
						
	   		 System.arraycopy(rabinbytes, (rabinbytes.length - 2), rabinbytes, (rabinbytes.length - 1), 1);
		  
	   		 BigInteger m = new BigInteger(rabinbytes);
	   		 //System.out.println("n used: "+ n.toString());
		     BigInteger c = m.modPow(two, n); 		  		     
			 ciphertext += c.toString() ;
			 ciphertext += "\n";
		}
		
		return ciphertext;
		//System.out.println("full cipher text : " + ciphertext);
    }
}
	

