package org.wisenet.protocols.cleanslate.utils.rabin;

// Class by Robin Abraham
// Generates keys

import java.math.*;
import java.util.*;


public class KeySet {

// I generate the prime numbers for the key using the methods available from BigInteger class.

//    private static int BITS;
    private static BigInteger p;  
    private static BigInteger q;
    private static BigInteger n;
    private static int Size;
    BigInteger three = BigInteger.valueOf(3);    
    BigInteger four = BigInteger.valueOf(4);
    
    public KeySet(int size) {
    	if(p == null && q == null && n == null){
			Size = size;
			long time = System.currentTimeMillis();
			p = getPrime(time); 
			while((p.mod(four)).compareTo(three) != 0){
			    time += 8;
			    p = getPrime(time);
			    //System.out.println("For p : " + time);
			}
			time = System.currentTimeMillis();
			q = getPrime(time);
			while((q.mod(four)).compareTo(three) != 0) {
				time += 12;
			    q = getPrime(time);
			    //System.out.println("For q : " + time);		    
			}
			n = p.multiply(q);
    	}
	  
		//String s = n.toString();
	  
	  
    }
   
    private static BigInteger getPrime(long n) {
		Random num = new Random(n);
		BigInteger prime = new BigInteger(Size, 100, num);
		return prime;
    }
	
    
    public BigInteger getP() {
		return p;
    }

    public BigInteger getQ() {
		return q;
    }
   
    public BigInteger getN() {
		return n;
    }
}

