package org.wisenet.protocols.cleanslate;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Tiago Ara�jo
 *
 */
public class MaliciousNodesExt extends MaliciousNodes {
	
	private Set<Short> removedNodes;
	
	//Ids e grupos de nos maliciosos conhecidos a partir de outros
	protected Set<Short> newids;
	private Set<GroupInfo> newgroups;
	
	
	public MaliciousNodesExt(){
		super();
		this.newids = new HashSet<Short>();
		this.newgroups = new HashSet<GroupInfo>();
		this.removedNodes = new HashSet<Short>();
	}
	
	public void addNewMaliciousNode(short id){
		this.newids.add(id);
		this.newgroups.add(null);
	}
	
	public void addNewMaliciousNode(short id, GroupInfo gi){
		this.newids.add(id);
		this.newgroups.add(gi);
	}
	
	public boolean isRemoved(short nodeID){
		return removedNodes.contains(nodeID);
	}
	
	public void addRemovedMaliciousNode(short nodeID){
		removedNodes.add(nodeID);
	}
	
	public MaliciousNodes getMaliciousNodes(){
		return super.getMaliciousNodes();
	}
	
	public boolean containsMaliciousNode(short id){
		return newids.contains(id) || super.contains(id);
	}
}