package org.wisenet.protocols.cleanslate;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.wisenet.protocols.cleanslate.CleanSlateRoutingLayer.ReplaceRequestMessageEvent;
import org.wisenet.protocols.common.Pair;


/**
 * 
 * @author Tiago Ara�jo
 *
 */
public class RemoveNode {
	
	private ReplaceRequestMessageEvent replaceevent;
	private List<Pair<Vector<String>,Vector<Integer>>> replacesMaliciousNode;
	private List<Short> maliciousNodesID;	//Id do no a ser removido
	
	public RemoveNode(){
		this.replacesMaliciousNode = new LinkedList<Pair<Vector<String>,Vector<Integer>>>();
		this.maliciousNodesID = new LinkedList<Short>();
	}
	
	public void setReplaceRequestvent(ReplaceRequestMessageEvent replaceevent){
		this.replaceevent = replaceevent;
	}
	
	public ReplaceRequestMessageEvent getReplaceRequestEvent(){
		return replaceevent;
	}
	
	//Malicious
	
	public void setReplacesMaliciousNodes(ReplaceResult result){
		this.replacesMaliciousNode = result.getReplaces();
		this.maliciousNodesID = result.getMaliciousNodesID();
	}
	
	
	public List<Pair<Vector<String>,Vector<Integer>>> getReplacesMalicious(){
		return replacesMaliciousNode;
	}
	
	public List<Short> getMaliciousNodeID(){
		return maliciousNodesID;
	}
	
	public Pair<Vector<String>,Vector<Integer>> getEntryToReplace(short maliciousNodeID){
		
		Iterator<Short> it = maliciousNodesID.iterator();
		int i=-1;
		while(it.hasNext()){
			i++;
			long next = it.next();
			if(next == maliciousNodeID)
				break;
		}
	
		if(i==-1 || i>=maliciousNodesID.size())
			return null;
		
		return replacesMaliciousNode.get(i);
	}
	
	public void removeNodeID(short nodeID){
		Iterator<Short> it = maliciousNodesID.iterator();
		int i=-1;
		while(it.hasNext()){
			i++;
			long next = it.next();
			if(next == nodeID)
				break;
		}
		
		if(!(i==-1 || i>=maliciousNodesID.size())){
			maliciousNodesID.remove(i);
			replacesMaliciousNode.remove(i);
		}
	}
	
	public boolean isEmpty(){
		return maliciousNodesID.isEmpty() && replacesMaliciousNode.isEmpty();
	}
	
	public List<Vector<String>> getAddresses(){
		List<Vector<String>> res = new LinkedList<Vector<String>>();
		
		Iterator<Pair<Vector<String>,Vector<Integer>>> it = replacesMaliciousNode.iterator();
		while(it.hasNext()){
			Pair<Vector<String>,Vector<Integer>> next = it.next();
			res.add(next.getFirst());
		}
		return res;
	}
}