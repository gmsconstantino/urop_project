package org.wisenet.protocols.cleanslate;

import org.wisenet.protocols.common.Pair;

/**
 * 
 * @author Tiago Ara�jo
 * 
 * Informa��o de grupos abortados
 */
public class AbortInfo {

	private Pair<Short,GroupInfo> gvtfail;
	private Pair<Short,GroupInfo> responserSignfail;
	private Pair<Short,GroupInfo> challengefail;
	
	public AbortInfo(){
		this.gvtfail = null;
		this.responserSignfail = null;
		this.challengefail = null;
	}
	
	public void setGVTFail(Pair<Short,GroupInfo> gvtfail){
		this.gvtfail = gvtfail;
	}
	
	public Pair<Short,GroupInfo> getGVTFail(){
		return gvtfail;
	}
	
	public void setResponserSignFail(Pair<Short,GroupInfo> responserSignFail){
		this.responserSignfail = responserSignFail;
	}
	
	public Pair<Short,GroupInfo> getResponserSignFail(){
		return responserSignfail;
	}
	
	public void setChallengeFail(Pair<Short,GroupInfo> challengeFail){
		this.challengefail = challengeFail;
	}
	
	public Pair<Short,GroupInfo> getChallengeFail(){
		return challengefail;
	}
	
	public boolean isNull() {
		return (gvtfail == null && responserSignfail == null && challengefail == null);
	}
	
	public void reset(){
		this.gvtfail = null;
		this.responserSignfail = null;
		this.challengefail = null;
	}
}