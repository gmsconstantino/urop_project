package org.wisenet.protocols.cleanslate;

import org.wisenet.simulator.core.Simulator;


/**
 * 
 * @author Tiago Ara�jo
 *
 * Constantes do Clean-Slate
 * 
 */
public class CleanSlateConstants {

	// - CRIPTOGRAFIA
	public static String[] PropertiesKey = {"RSA","256"};  //USAR O RSA? ou Rabin?
	public static String[] PropertiesCipher = {"RSA","NONE","PKCS1Padding"};
	
			//Tudo o resto � usado da camada TinySec
	
	
	
	//FASE HELLO
	
	//Apos receber uma mensagem HELLO, intervalo aleatorio para delay no envio de mensagem HELLO 
	public static int TIMENEIGHBORPROTOCOLMAX = Simulator.ONE_SECOND*2;
	public static int TIMENEIGHBORPROTOCOLMIN = 0;
	
	//Para 400 nos funciona
	public static int TIMETOINITACKS = Simulator.ONE_SECOND*3000;
	
	//Usar 50 para 500 nos AJUSTAR!  (10 s)
	public static int TIMETOINITMERGES = Simulator.ONE_SECOND*5000;
	
	
		// FASE SEGUINTE
	
	//Intervalo aleatorio para tempo de delay ao enviar um ack
	public static int TIMETOACKMESSAGEMIN = 0;
	public static int TIMETOACKMESSAGEMAX = Simulator.ONE_SECOND;
	
	//Intervalo aleatorio para entrega fiavel de mensagens (a retransmissao)
	public static int TIMETORETRANSMITMAX = Simulator.ONE_SECOND*3;
	public static int TIMETORETRANSMITMIN = Simulator.ONE_SECOND;
	
	public static int ARRIVEDMSGSSIZE = 50000; 
	public static int DUPLICATESSIZE =  50000;
	
	public static int MAXATTEMPTS = 50;
	public static int NRWAITINGSMAX = 200;
	
	
	/** 
	 * Tempo que se espera para retransmitir uma mensagem, 
	 * quando se verifica imediatamente que ela n�o pode ser enviada.
	 */
	public static int TIMETONEXTTRANSMISSION = Simulator.ONE_SECOND;
	
	
	/////////////////////////////////////////////////////////
}