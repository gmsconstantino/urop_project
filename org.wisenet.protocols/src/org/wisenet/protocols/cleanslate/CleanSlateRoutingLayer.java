package org.wisenet.protocols.cleanslate;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import org.wisenet.protocols.MAC.tinysec.TinySecExtended;
import org.wisenet.protocols.cleanslate.messages.Accepted2Message;
import org.wisenet.protocols.cleanslate.messages.AcceptedMessage;
import org.wisenet.protocols.cleanslate.messages.AckInvitation2Message;
import org.wisenet.protocols.cleanslate.messages.AckInvitationMessage;
import org.wisenet.protocols.cleanslate.messages.AckMessage;
import org.wisenet.protocols.cleanslate.messages.ActionMessage;
import org.wisenet.protocols.cleanslate.messages.CleanSlateMessage;
import org.wisenet.protocols.cleanslate.messages.DataMessage;
import org.wisenet.protocols.cleanslate.messages.DoMergeMessage;
import org.wisenet.protocols.cleanslate.messages.HelloMessage;
import org.wisenet.protocols.cleanslate.messages.HoneyBeeMessage;
import org.wisenet.protocols.cleanslate.messages.MergePropMessage;
import org.wisenet.protocols.cleanslate.messages.MessageTypes;
import org.wisenet.protocols.cleanslate.messages.NoMergeMessage;
import org.wisenet.protocols.cleanslate.messages.RejectedMessage;
import org.wisenet.protocols.cleanslate.messages.ReliableMessage;
import org.wisenet.protocols.cleanslate.messages.ReplaceReplyMessage;
import org.wisenet.protocols.cleanslate.messages.ReplaceRequestMessage;
import org.wisenet.protocols.cleanslate.messages.UpdateGroupMessage;
import org.wisenet.protocols.cleanslate.messages.WaitingMessage;
import org.wisenet.protocols.cleanslate.messages.gvt.BroadcastChallengeMessage;
import org.wisenet.protocols.cleanslate.messages.gvt.ChallengeRequestMessage;
import org.wisenet.protocols.cleanslate.messages.gvt.ChallengeResponseMessage;
import org.wisenet.protocols.cleanslate.messages.gvt.ChooseChallengerMessage;
import org.wisenet.protocols.cleanslate.messages.gvt.ChooseResponderMessage;
import org.wisenet.protocols.cleanslate.messages.gvt.FinalResponseMessage;
import org.wisenet.protocols.cleanslate.messages.gvt.ResponseToEdgeNodeMessage;
import org.wisenet.protocols.cleanslate.utils.UtilsCleanSlate;
import org.wisenet.protocols.common.Pair;
import org.wisenet.protocols.common.Utils;
import org.wisenet.simulator.core.Application;
import org.wisenet.simulator.core.Event;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.node.Node;
import org.wisenet.simulator.core.node.layers.routing.RoutingLayer;


public class CleanSlateRoutingLayer extends RoutingLayer {
	
	protected short uniqueID;
	protected GroupInfo myGroupInfo;
	
	protected Set<Long> pendingMsgs;
	protected Set<Short> pendingInvitations;
	protected Set<Long> arrivedMsgs;
	private List<Long> arrivedMsgs_sorted;
	protected int currwaiting;
	
	protected long mergeProposeMsg;
	protected long acceptedMsg;
	
	private List<Long> duplicateMsgs_sorted;
	protected Set<Long> acceptedMsgs;
	
	protected Long mergeWith;
	
	protected Node parentAux;
	protected List<Short> nexthops;
	
	//Para a GVT
	protected boolean hasChallenger;
	protected short edgenodeIDtochallenger;
	protected GroupInfo otherGroup;
	protected boolean firstGroup;
	protected boolean responseMessageSent;
	protected ChallengeResponseMessage crm;
	protected FinalResponseMessage frm;
	protected AbortInfo mergeabortInfo;
	protected Set<GroupInfo> rejectedGroups;
	
	protected RemoveNode removenode;
	
	
	///
	
	//routing table
	protected Vector<Pair<String,Short>> routingTableM;
	protected Vector<Pair<String,Short>> oldRoutingTableM;
	protected Vector<Short> routingTableL;
	protected Vector<Short> routingTableR;
	
	//merge table
	protected List<GroupInfo> merges;
	protected List<GroupInfo> myLastGroups;
	
	
	//network address
	protected String address;
	protected String oldAddress;
	protected String sinknodeaddress;
	protected String sinknodeaddressaux;
	
	
	//Fase de descoberta de vizinhos
	protected Set<Short> neighborsNodes;
	protected Set<Short> bidirecNeighbors;
	
	//Vizinhos que s�o conhecidos
	protected Set<GroupInfo> neighborsGroups;
	protected Set<GroupInfo> directedNeighborsGroups;
	
	//Actualiza��es
	protected Set<GroupInfo> newGroups;
	protected Set<GroupInfo> removeGroups;
	
	//Evitar duplicados
	protected Set<Long> duplicates;
	protected boolean duplicateNeighbors;
	
	protected Event lastMergeEvent;
	protected boolean lock;
	protected boolean mergeDoned;
	
	protected MaliciousNodesExt maliciousNodes;
	
	//private Node parent_data;
	
	protected boolean groupaborted;
	
	
	
	public CleanSlateRoutingLayer() {
		super();		
		
		this.neighborsNodes = new HashSet<Short>();
		this.neighborsGroups = new TreeSet<GroupInfo>();
		this.directedNeighborsGroups = new HashSet<GroupInfo>();
		this.newGroups = new HashSet<GroupInfo>();
		this.merges = new LinkedList<GroupInfo>();
		this.address = new String();
		this.oldAddress = new String();
		this.sinknodeaddress = "";
		this.sinknodeaddressaux = null;
		this.routingTableM = new Vector<Pair<String,Short>>();
		this.oldRoutingTableM = new Vector<Pair<String,Short>>();
		this.routingTableL = new Vector<Short>();
		this.routingTableR = new Vector<Short>();
		this.removeGroups = new HashSet<GroupInfo>();
		this.duplicateNeighbors = false;
		this.myGroupInfo = null;
		this.myLastGroups = new LinkedList<GroupInfo>();
		this.lastMergeEvent = null;
		this.duplicates = new HashSet<Long>();
		this.lock = false;
		this.mergeDoned = false;
		this.bidirecNeighbors = new HashSet<Short>();
		
		//this.parent_data = null;
		
		this.maliciousNodes = new MaliciousNodesExt();
		this.groupaborted = false;
		
		
		this.pendingMsgs = new HashSet<Long>();
		this.arrivedMsgs = new HashSet<Long>();
		this.arrivedMsgs_sorted = new LinkedList<Long>();
		this.duplicateMsgs_sorted = new LinkedList<Long>();
		this.pendingInvitations = new HashSet<Short>();
		this.acceptedMsgs = new HashSet<Long>();
		this.mergeWith = null;
		this.parentAux = null;
		this.nexthops = new LinkedList<Short>();
		
		//GVT
		this.hasChallenger = false;
		this.otherGroup = null;
		this.firstGroup = false;
		this.responseMessageSent = false;
		this.crm = null;
		this.mergeabortInfo = new AbortInfo();
		this.frm = null;
		this.removenode = new RemoveNode();
		this.rejectedGroups =  new HashSet<GroupInfo>();
		this.currwaiting = 0;
		Debug.reset();
	}
	
	
	/**
	 * Recep��o de mensagem segura
	 */
	protected CleanSlateMessage securityMessageReceived(Message message){
		
		if(message instanceof DataMessage)
			return DataMessage.buildMessage(message);
		
		return (CleanSlateMessage)message;
	}
	
	protected void isHelloMessage(HelloMessage nm){
		
		
		if(UtilsCleanSlate.verifySignature(nm.getSignature(),nm.getSourceId(),getHostNode().keyNaPub,getNode().getTinySecExt())){// && getParentSignalStrength()> REQUIREDSIGNAL){
			if(!bidirecNeighbors.contains(nm.getSourceId()))
				bidirecNeighbors.add((Short) nm.getSourceId());
			
			if(nm.getInvitations().contains(uniqueID) && duplicateNeighbors)
				pendingInvitations.add((Short) nm.getSourceId());
			
			if(!duplicateNeighbors){
				Auxiliary.addToGroup(myGroupInfo, this);
				duplicateNeighbors = true;
				
				long waittime = Utils.randomLongBetween(CleanSlateConstants.TIMENEIGHBORPROTOCOLMIN, CleanSlateConstants.TIMENEIGHBORPROTOCOLMAX, new Random()); 
				new EventSendHelloMessage(getHostNode().getSimulator().getSimulationTime() + waittime);
				
				//Passado x tempo apos enviar o HELLO, enviar os Acks de forma fiavel
				long time = getHostNode().getSimulator().getSimulationTime() + waittime + CleanSlateConstants.TIMETOINITACKS;
				new EventAckInvitationMessage(time);
				
				//Esperar y tempo para enviar propostas
				long time2 = getHostNode().getSimulator().getSimulationTime() + waittime + CleanSlateConstants.TIMETOINITACKS + CleanSlateConstants.TIMETOINITMERGES;
				sendMergingPropose(time2);
				
			}
		}
	}

	
	protected void isAckInvitationMessage(AckInvitationMessage aim){
		boolean arrived = receiveReliableAckInvitationMessage(aim);
		if(arrived)
			return;
		
		if(aim.getDestinations().contains(uniqueID)){
			
			if(UtilsCleanSlate.verifySignature(aim.getSignature(),aim.getSourceId(),getHostNode().keyNaPub,getNode().getTinySecExt())){
				neighborsNodes.add((Short) aim.getSourceId());
				neighborsGroups.add(new GroupInfo((Short) aim.getSourceId(), 1));
				directedNeighborsGroups.add(new GroupInfo((Short) aim.getSourceId(), 1));
				Auxiliary.createWith1Group(myGroupInfo.getGroupID(), 1);
				Auxiliary.interiorNodes.remove(uniqueID);
			}	
		}
	}
	
	
	protected void isAckInvitation2Message(AckInvitation2Message ack2){
		if(UtilsCleanSlate.verifySignature(ack2.getSignature(),ack2.getSourceId(),getHostNode().keyNaPub,getNode().getTinySecExt())){
			if(ack2.getDestinationId() == (Short) uniqueID && pendingInvitations.contains(ack2.getSourceId())){
				
				Debug.receiveAckInvite();
				pendingInvitations.remove((Short) ack2.getSourceId());
				neighborsNodes.add((Short) ack2.getSourceId());
				neighborsGroups.add(new GroupInfo((Short) ack2.getSourceId(), 1));
				directedNeighborsGroups.add(new GroupInfo((Short) ack2.getSourceId(), 1));
				getHostNode().parents2.add(getNode().getParentNode());
				Auxiliary.createWith1Group(myGroupInfo.getGroupID(), 1);
				Auxiliary.interiorNodes.remove(uniqueID);
			}
		}
	}

	
	//////////////////////////////////////////////////////////////////
	
	
	/**
	 * Verificar proposta
	 */
	protected boolean verifyPropose(GroupInfo othergroup){
		if(neighborsGroups.isEmpty())
			return false;
		GroupInfo gi = ((TreeSet<GroupInfo>)neighborsGroups).first();
		if(othergroup.equals(gi))
			return true;
		return false;
	}
	
	
	/**
	 * Registar informa��o vinda de outros grupos
	 */
	protected void collectsGroupInformation1(GroupInfo group, long sourceID, Set<GroupInfo> oldGroups, Set<GroupInfo> removeGroups){
		try{
			if(!myGroupInfo.equals(group) && neighborsNodes.contains(sourceID) && !myLastGroups.contains(group) && 
					!merges.contains(group) && !oldGroups.contains(myGroupInfo) && !directedNeighborsGroups.contains(group)){
				
//				if(!removeGroups.contains(group) && (rejectedGroup == null || !group.equals(rejectedGroup))){
				if(!removeGroups.contains(group) && !this.rejectedGroups.contains(group))
					newGroups.add(group.clone());
				
				removeGroups.addAll(removeGroups);
			}
		}catch(CloneNotSupportedException e){
			System.err.println(e.getMessage());
		}
	}
	
	
	/**
	 * Registar informa��o vinda de outros grupos
	 */
	protected void collectsGroupInformation2(GroupInfo old1, GroupInfo old2, long sourceID){
		if(!old1.equals(myGroupInfo) && !old2.equals(myGroupInfo) && neighborsNodes.contains(sourceID)){
			
			try{
				long id = UtilsCleanSlate.calculateNewGroupID(old1,old2,getNode().getTinySecExt());  //Origem � o G' e Destino o G
				int size = old1.getSizeGroup() + old2.getSizeGroup();
				GroupInfo newgroup = new GroupInfo(id, size);
				
				if(!myGroupInfo.equals(newgroup) && !directedNeighborsGroups.contains(newgroup) && !removeGroups.contains(newgroup)){
					
					if(newGroups.contains(old1))
						newGroups.remove(old1);
					if(newGroups.contains(old2))
						newGroups.remove(old2);
					
					newGroups.add(newgroup);
				}
			}catch(IOException e){
				System.err.println(e.getMessage());
			}
		}
	}
	
	
	protected Set<GroupInfo> getOldGroupsAfterMerge(){	
		return new HashSet<GroupInfo>(removeGroups);
	}
	
	
	protected Set<GroupInfo> getMergesAndLastGroups(){
		
		Set<GroupInfo> removeGroups = new HashSet<GroupInfo>();
		removeGroups.addAll(new HashSet<GroupInfo>(myLastGroups));
		removeGroups.addAll(new HashSet<GroupInfo>(merges));	
		return removeGroups;
	}
	
	
	protected void sendAckResponse(Object message){
		sendMessageScheduled(message, CleanSlateConstants.TIMETOACKMESSAGEMIN, CleanSlateConstants.TIMETOACKMESSAGEMAX);
	}
	
	
	private void auxMethodToRejectMessage(MergePropMessage m, GroupInfo mygroup){
		
		Set<GroupInfo> removeGroups = this.getMergesAndLastGroups(); 
		Set<GroupInfo> oldGroups = this.getOldGroupsAfterMerge();
		
		RejectedMessage rejectedMessage = new RejectedMessage(mygroup,m.getOriginGroupInfo(),uniqueID, (Short) m.getSourceId(),removeGroups,oldGroups,m.getRandomValue(),m.getRandomValue());
		sendAckResponse(rejectedMessage);
	}
	
	
	private void auxMethodToRejectMessage1(MergePropMessage m, GroupInfo mygroup){
			
		Set<GroupInfo> removeGroups = this.getMergesAndLastGroups(); 
		Set<GroupInfo> oldGroups = this.getOldGroupsAfterMerge();
		try {
			removeGroups.add(myGroupInfo.clone());
		} catch(CloneNotSupportedException e){
			
		}
		
		RejectedMessage rejectedMessage = new RejectedMessage(mygroup,m.getOriginGroupInfo(),uniqueID, (Short) m.getSourceId(),removeGroups,oldGroups,m.getRandomValue(),m.getRandomValue());
		sendAckResponse(rejectedMessage);
	}

	
	
	private synchronized void acceptedToMerge(MergePropMessage m){
						
		try {			
			if(neighborsNodes.contains(m.getSourceId()) && (m.getDestinationGroupInfo().equals(myGroupInfo)) && (verifyPropose(m.getOriginGroupInfo()))){
				removeMergeEvent();   //Acrescentado
				acceptedMsgs.add(m.getRandomValue());
				sinknodeaddressaux = m.getSinkNodeAddress();
				String newsinknodeaddress = UtilsCleanSlate.calcNewSinkNodeAddress(m.getOriginGroupInfo().getGroupID(), myGroupInfo.getGroupID(), uniqueID, sinknodeaddress);
				acceptedMsg = new Random().nextLong();
				GroupInfo myoldgroup = myGroupInfo.clone();
				Auxiliary.lockgroups(myoldgroup, m.getOriginGroupInfo());
				sendReliableMessage(new AcceptedMessage(m.getOriginGroupInfo(),m.getDestinationGroupInfo(),uniqueID, (Short)m.getSourceId(),acceptedMsg,newsinknodeaddress));	
			}
		}catch(CloneNotSupportedException e){
			e.printStackTrace();
		}
	}
	
	
	protected void isMergePropMessage(MergePropMessage m){
		
		if(arrivedMsgs.size() > CleanSlateConstants.ARRIVEDMSGSSIZE)
			removeArrivedMsgs((int)arrivedMsgs.size()/5);
		
		if(duplicates.size() >  CleanSlateConstants.DUPLICATESSIZE)
			removeDuplicateMsgs((int)duplicates.size()/5);
		
		if(acceptedMsgs.contains(m.getRandomValue()))
			return ;
				
		
		if(neighborsNodes.contains(m.getSourceId())){
			GroupInfo mygroup = null;
			try{
				mygroup = myGroupInfo.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace(); return ;
			}

			
			if(m.getDestinationGroupInfo().equals(myGroupInfo)){	
			
				if(groupaborted){
					auxMethodToRejectMessage1(m, mygroup);
					return ;
				}
						
				if(lock && mergeDoned){
					
					this.collectsGroupInformation1(m.getOriginGroupInfo(),(Short)m.getSourceId(),this.getOldGroupsAfterMerge(),new HashSet<GroupInfo>()); //
					Set<GroupInfo> removeGroups = this.getMergesAndLastGroups(); 
					WaitingMessage waitingMessage = new WaitingMessage(mygroup,m.getOriginGroupInfo(),uniqueID, (Short)m.getSourceId(),m.getRandomValue(),removeGroups);
					sendAckResponse(waitingMessage);
					return ;
				}
				
				if(lock && !mergeDoned && verifyPropose(m.getOriginGroupInfo())){

					if(!nexthops.contains(m.getSourceId()))
						nexthops.add((Short)m.getSourceId());
					
					String newsinknodeaddress = UtilsCleanSlate.calcNewSinkNodeAddress(m.getOriginGroupInfo().getGroupID(), myGroupInfo.getGroupID(), uniqueID, sinknodeaddress);
					Accepted2Message a2m = new Accepted2Message(myGroupInfo, m.getOriginGroupInfo(), uniqueID, (Short)m.getSourceId(), m.getRandomValue(), newsinknodeaddress);
					sendAckResponse(a2m);
					return ;
					
				}else if(lock && !mergeDoned && !verifyPropose(m.getOriginGroupInfo()))
					this.collectsGroupInformation1(m.getOriginGroupInfo(),(Short)m.getSourceId(),this.getOldGroupsAfterMerge(),new HashSet<GroupInfo>()); 
				
					
				
				if(!lock && directedNeighborsGroups.isEmpty() && neighborsGroups.isEmpty()){
					
//					if(removeMaliciousEvent!=null){
//						getSimulator().removeEvent(removeMaliciousEvent);
//						removeMaliciousEvent = null;
//					}

					Set<GroupInfo> removeGroups = this.getMergesAndLastGroups();
					WaitingMessage waitingMessage = new WaitingMessage(mygroup,m.getOriginGroupInfo(),uniqueID, (Short)m.getSourceId(),m.getRandomValue(),removeGroups);
					sendAckResponse(waitingMessage);

					this.collectsGroupInformation1(m.getOriginGroupInfo(),(Short)m.getSourceId(),this.getOldGroupsAfterMerge(),new HashSet<GroupInfo>());
					
					RejectedMessage rm = new RejectedMessage(null,myGroupInfo,uniqueID,uniqueID,getOldGroupsAfterMerge(),new TreeSet<GroupInfo>(),new Random().nextLong(),new Random().nextLong());	
					isRejectedMessage(rm,true);
				}
				else if(verifyPropose(m.getOriginGroupInfo()))
					acceptedToMerge(m);
				else{					
					this.collectsGroupInformation1(m.getOriginGroupInfo(),(Short)m.getSourceId(),this.getOldGroupsAfterMerge(),new HashSet<GroupInfo>());
					Set<GroupInfo> removeGroups = this.getMergesAndLastGroups(); 
					WaitingMessage waitingMessage = new WaitingMessage(mygroup,m.getOriginGroupInfo(),uniqueID, (Short)m.getSourceId(),m.getRandomValue(),removeGroups);
					sendAckResponse(waitingMessage);
				}
			}
			else if(myLastGroups.contains(m.getDestinationGroupInfo()) && lock && mergeDoned && mergeWith != null 
					&& mergeWith == m.getOriginGroupInfo().getGroupID()){  //FOI ACRESCENTADO
				
				if(!nexthops.contains(m.getSourceId()))
					nexthops.add((Short)m.getSourceId());
				
				Accepted2Message a2m = new Accepted2Message(myGroupInfo,m.getOriginGroupInfo(), uniqueID, (Short)m.getSourceId(), m.getRandomValue(), "");
				sendAckResponse(a2m);
			}
			else if(myLastGroups.contains(m.getDestinationGroupInfo()))
				this.auxMethodToRejectMessage(m, mygroup);
			
		} 
	}

	
	protected void isAccepted2Message(Accepted2Message m){
		
		if(lock && (myGroupInfo.equals(m.getDestinationGroupInfo()) && uniqueID != (Short)m.getSourceId() && !mergeDoned) ||
				(myLastGroups.contains(m.getDestinationGroupInfo()) && mergeDoned && mergeWith != null 
				&& mergeWith == m.getSourceGroup().getGroupID())){

			if(!nexthops.contains(m.getSourceId()))
				nexthops.add((Short) m.getSourceId());
		}		

		if(m.getDestination().equals(uniqueID) && myGroupInfo.equals(m.getDestinationGroupInfo()))
			pendingMsgs.remove(mergeProposeMsg);
	}
	
	
	protected void isAcceptedMessage(AcceptedMessage m){
		
		if(myGroupInfo.equals(m.getDestinationGroupInfo()) && !m.getDestination().equals(uniqueID) && lock){
			
			if(!nexthops.contains(m.getSourceId()))
				nexthops.add((Short)m.getSourceId());
		}

		boolean arrived = receiveReliableResponseMessage(m); //Para garantir que a � uma mensagem com fiabilidade
		if(arrived)
			return;
				
		if(m.getDestination().equals(uniqueID) && myGroupInfo.equals(m.getDestinationGroupInfo())){
			removeMergeEvent(); //Acrescentado
			sinknodeaddressaux = m.getSinkNodeAddress();
			parentAux = getHostNode().getParentNode();
			this.firstGroup = true;
			this.chooseChallengerGVT(m.getSourceGroup(),(Short)m.getSourceId());
		} else
			collectsGroupInformation2(m.getDestinationGroupInfo(), m.getSourceGroup(), (Short)m.getSourceId());		
	}
	
	
	protected void updateMsgsVariables(){
		pendingMsgs.remove(mergeProposeMsg);
		insertArrivedMsg(mergeProposeMsg);
	}
	
	
	protected synchronized void isRejectedMessage(RejectedMessage m, boolean aux){
		
		if(!aux){
			 if(!m.getDestinationGroupInfo().equals(myGroupInfo))
				 return ;
			 
			boolean arrived = receiveReliableResponseMessage(m);
			if(arrived)
				return;
		}else if(m.getDestination().equals(uniqueID) && myGroupInfo.equals(m.getDestinationGroupInfo()))
			pendingMsgs.remove(mergeProposeMsg);
		
		if(m.getDestination().equals(uniqueID) && myGroupInfo.equals(m.getDestinationGroupInfo()) && !lock && !myGroupInfo.equals(m.getSourceGroup())){

			if(!duplicates.contains(m.getRemoveDuplValue())){
				currwaiting=0;
				Auxiliary.lockgroup(myGroupInfo,true,true);
				
				this.insertDuplicateMsg(m.getRemoveDuplValue());
				this.updateMsgsVariables();
				pendingMsgs.remove(acceptedMsg);
				
				Auxiliary.clearGroup(myGroupInfo);
				
				if(m.hasRejectedGroup())
					rejectedGroups.add(m.getRejectedGroup());
				
				removeGroups.addAll(new HashSet<GroupInfo>(m.getRemoveGroups()));
				neighborsGroups.clear(); 
				
				
				if(m.getSourceGroup() != null && !directedNeighborsGroups.contains(m.getSourceGroup()))
					newGroups.add(m.getSourceGroup());
				
				Auxiliary.addToGroup(myGroupInfo, this);
				Auxiliary.completedGroup(myGroupInfo, this.getNode().getSimulator().getSimulation());
			}
			
			try{
				if(merges.size() > 0){
					Set<GroupInfo> oldGroups = this.getOldGroupsAfterMerge();
					Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, "");
					if(pair.getFirst().getFirst()!=null)  //FAZ BROADCAST FIAVEL PARA O GRUPO
						sendReliableMessage(new NoMergeMessage(myGroupInfo.clone(),"0", uniqueID,pair.getFirst().getFirst(),m.getRemoveDuplValue(),m.getRemoveGroups(),oldGroups,new Random().nextLong(),m.getRejectedGroup()));
					if(pair.getSecond().getFirst()!=null)
						sendReliableMessage(new NoMergeMessage(myGroupInfo.clone(),"1",uniqueID,pair.getSecond().getFirst(),m.getRemoveDuplValue(),m.getRemoveGroups(),oldGroups,new Random().nextLong(),m.getRejectedGroup()));
				}			
			}catch(CloneNotSupportedException e){
				System.err.println(e.getMessage());
			}
		}else
			collectsGroupInformation1(m.getSourceGroup(),(Short)m.getSourceId(),m.getOldGroups(),m.getRemoveGroups());
	}
	
	
	protected void isNoMergeMessage(NoMergeMessage m){
		
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;
		
		if(m.getDestination().equals(uniqueID) && myGroupInfo.equals(m.getDestinationGroupInfo())){
			
			if(!duplicates.contains(m.getRemoveDuplValue())){
				currwaiting=0;
				insertDuplicateMsg(m.getRemoveDuplValue());
				this.updateMsgsVariables();
				
				if(m.hasRejectedGroup())
					rejectedGroups.add(m.getRejectedGroup());
				
				removeGroups.addAll(m.getRemoveGroups());
				neighborsGroups.clear();
				
				Auxiliary.addToGroup(myGroupInfo, this);
				Auxiliary.completedGroup(myGroupInfo, getNode().getSimulator().getSimulation());
			}
		
			
			try {
				Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, m.getPrefix());
				if(m.getPrefix().length() <= address.length()-2){ 
					
					Set<GroupInfo> oldGroups = this.getOldGroupsAfterMerge();
					String prefix = "";
					if(pair.getFirst().getFirst()!=null){  //FAZ BROADCAST FIAVEL PARA O GRUPO
						prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getFirst(), 0);
						sendReliableMessage(new NoMergeMessage(myGroupInfo.clone(), prefix , uniqueID,pair.getFirst().getFirst(),m.getRemoveDuplValue(),m.getRemoveGroups(),oldGroups,new Random().nextLong(),m.getRejectedGroup()));
					}
					if(pair.getSecond().getFirst()!=null){
						prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getSecond(), 1);
						sendReliableMessage(new NoMergeMessage(myGroupInfo.clone(),prefix , uniqueID,pair.getSecond().getFirst(),m.getRemoveDuplValue(),m.getRemoveGroups(),oldGroups,new Random().nextLong(),m.getRejectedGroup()));
					}
				}
			}catch(CloneNotSupportedException e){
				System.err.println(e.getMessage());
			}	
		} else
			collectsGroupInformation1(m.getSourceGroup(),(Short)m.getSourceId(),m.getOldGroups(),m.getRemoveGroups());
	}
	

	
	protected void isDoMergeMessage(DoMergeMessage m){
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;
		
		if(m.getDestination().equals(uniqueID)){
			
			if(myGroupInfo.equals(m.getMyOldGroup())){
				try {
					
					TinySecExtended tinySecExt = getNode().getTinySecExt();
					if(UtilsCleanSlate.verifyResponserSignature(m.getResponderID(), m.getResponderSignature(),getHostNode().keyNaPub,tinySecExt)){
						if(UtilsCleanSlate.verifyGVT(m.getMergeTable(),m.getResponderID(),m.getOtherGroup(),tinySecExt)){
							oldRoutingTableM = UtilsCleanSlate.getCopyVector(routingTableM);
							oldAddress = address;
							doMerge(myGroupInfo, m.getOtherGroup(), (Short)m.getSourceId(),m.getSinkNodeAddress());
							mergeDoned = true;
						} else {
							rejectedGroups.add(crm.getResponderGroup().clone());
							maliciousNodes.addMaliciousNode(crm.getResponderID(),crm.getResponderGroup());
							maliciousNodes.addMaliciousNode(uniqueID,myGroupInfo);
							return ;
						}
							
					}else {
						rejectedGroups.add(crm.getResponderGroup().clone());
						maliciousNodes.addMaliciousNode(crm.getResponderID(),crm.getResponderGroup());
						maliciousNodes.addMaliciousNode(uniqueID,myGroupInfo);
						return ;
					}
				} catch(Exception e){
					e.printStackTrace();
				}
			}
			

			Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(oldRoutingTableM, m.getPrefix());
			if(m.getPrefix().length() <= oldAddress.length()-2){ 
				String prefix ="";
				if(pair.getFirst().getFirst()!=null){  //FAZ BROADCAST FIAVEL PARA O GRUPO
					prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getFirst(), 0);
					sendReliableMessage(new DoMergeMessage(m.getMyOldGroup(),m.getOtherGroup(), prefix , pair.getFirst().getFirst(),
							uniqueID,new Random().nextLong(),m.getSinkNodeAddress(),m.getResponderID(),
							m.getResponderSignature(),m.getMergeTable()));
				}
				if(pair.getSecond().getFirst()!=null){
					prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getSecond(), 1);
					sendReliableMessage(new DoMergeMessage(m.getMyOldGroup(), m.getOtherGroup(), prefix , pair.getSecond().getFirst(),
							uniqueID,new Random().nextLong(),m.getSinkNodeAddress(),m.getResponderID(),
							m.getResponderSignature(),m.getMergeTable()));
				}
			}
		}
		else 
			collectsGroupInformation2(m.getMyOldGroup(), m.getOtherGroup(), (Short)m.getSourceId());
	}
	
	
	protected synchronized void isWaitingMessage(WaitingMessage m){
		
		if(!m.getDestinationGroupInfo().equals(myGroupInfo))
			 return ;
		
		boolean arrived = receiveReliableResponseMessage(m);
		if(arrived)
			return;
		
		if(m.getDestination().equals(uniqueID) && myGroupInfo.equals(m.getDestinationGroupInfo()) && !lock){
			
			///////
			/*if(!newGroups.isEmpty()){
				GroupInfo gimin = (Collections.min(newGroups));
				GroupInfo gimin2 = ((TreeSet<GroupInfo>)neighborsGroups).first();
				if(gimin.compareTo(gimin2) == -1){
					RejectedMessage rm = new RejectedMessage(null,myGroupInfo,uniqueID, uniqueID,getOldGroupsAfterMerge(),
							new TreeSet<GroupInfo>(),new Random().nextLong(),new Random().nextLong());
					isRejectedMessage(rm,true);
					return ;
				}
			}*/
			///
			
			if(currwaiting<CleanSlateConstants.NRWAITINGSMAX){
				currwaiting++;
				long t = Utils.randomLongBetween(Simulator.ONE_SECOND, Simulator.ONE_SECOND*m.getSourceGroup().getSizeGroup()*2, new Random());
				sendMergingPropose(getHostNode().getSimulator().getSimulationTime() + t);
			}
			else {
				rejectedGroups.add(m.getSourceGroup());
				RejectedMessage rm = new RejectedMessage(null,myGroupInfo,uniqueID, uniqueID,getOldGroupsAfterMerge(),
						new TreeSet<GroupInfo>(),new Random().nextLong(),new Random().nextLong());
				isRejectedMessage(rm,true);
				return ;
			}
		}
		else
			collectsGroupInformation1(m.getSourceGroup(),(Short)m.getSourceId(),new HashSet<GroupInfo>(),m.getRemoveGroups());
	}
	
	
	protected void isUpdateGroupMessage(UpdateGroupMessage m){
		
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;
		
		if(m.getDestination().equals(uniqueID) && myGroupInfo.equals(m.getDestinationGroup())){
			
			Set<GroupInfo> dirneighbors = new HashSet<GroupInfo>(m.getDirectedNeighbors());
			dirneighbors.removeAll(merges);
			neighborsGroups.addAll(dirneighbors);
			newGroups.removeAll(m.getRemoveGroups());
			removeGroups.addAll(m.getRemoveGroups());
			
			neighborsGroups.removeAll(m.getRemoveGroups());
			directedNeighborsGroups.removeAll(m.getRemoveGroups());
			
			
			directedNeighborsGroups.removeAll(rejectedGroups);
			neighborsGroups.removeAll(rejectedGroups);
			newGroups.removeAll(rejectedGroups);
				
			mergeWith = null;
				
			Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, m.getPrefix());
			if(m.getPrefix().length() <= address.length()-2){ 
				String prefix = "";
				
				if(pair.getFirst().getFirst()!=null){  //FAZ BROADCAST FIAVEL PARA O GRUPO
					prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getFirst(), 0);
					sendReliableMessage(new UpdateGroupMessage(uniqueID, pair.getFirst().getFirst(), m.getDestinationGroup(),prefix , m.getDirectedNeighbors(),m.getGroup(),m.getRemoveGroups(),new Random().nextLong()));
				}
				if(pair.getSecond().getFirst()!=null){
					prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getSecond(), 1);
					sendReliableMessage(new UpdateGroupMessage(uniqueID, pair.getSecond().getFirst(), m.getDestinationGroup(), prefix ,m.getDirectedNeighbors(),m.getGroup(),m.getRemoveGroups(),new Random().nextLong()));
				}
			}
			
			Auxiliary.verifyUpdates(myGroupInfo, getNode().getSimulator().getSimulation());
		} else
			collectsGroupInformation1(m.getGroup(),(Short)m.getSourceId(),new HashSet<GroupInfo>(),m.getRemoveGroups());
	}
	
	
	protected void isAckMessage(AckMessage ack){
		
		if(ack.getDestination().equals(uniqueID) && pendingMsgs.contains(ack.getRandomValue())){
			
			pendingMsgs.remove(ack.getRandomValue());
			
			if(ack.getAckType().equals("update")){
				Auxiliary.remUpdateMessage(myGroupInfo.getGroupID(), ack.getAuxNumber());
				Auxiliary.verifyUpdates(myGroupInfo, getNode().getSimulator().getSimulation());
			} 
			else if(ack.getAckType().equals("gvt")){
				Auxiliary.remBroadcastChallengeMessage(myGroupInfo.getGroupID(), ack.getAuxNumber());
				Auxiliary.verifyBroadcastChallenge(myGroupInfo.getGroupID());
			}
		}
	}
	
	
	
	protected void isDataMessage(DataMessage dm){
		
		if(dm.getNextHop() == uniqueID && !maliciousNodes.containsMaliciousNode(dm.getForwardBy())){
			
			//parent_data = getHostNode().getParentNode();
			
			int i = UtilsCleanSlate.preMatching(dm.getDestination(),address,routingTableM.size());
			if(dm.getDestination().equals(address)){
				CleanSlateMessage msgapp = new CleanSlateMessage(MessageTypes.DATAMESSAGE,dm.getPayload());
				Application app = getNode().getApplication();
				if(app!=null)	app.receiveMessage(msgapp);	
			}
			else if(i==-1)
				; //System.out.println("ERROR - Invalid address " + getNode().getId());
			else {				
				Short nexthop = UtilsCleanSlate.getNextHop(dm.getDirection(),i, routingTableL, routingTableR,routingTableM);
				String dir = UtilsCleanSlate.updateDirection(dm.getDirection());
				
				if(nexthop != null){
					
					try {
						DataMessage newdm = dm.copyMessage();
						newdm.setForwardBy(uniqueID);
						newdm.setNextHop(nexthop);
						newdm.setDirection(dir);
						newdm.packetSecurity(true);
						immediateSendMessage(newdm); //� utilizado assim
						//sendMessageScheduled(newdm, 0, Simulator.ONE_SECOND*5);
					}
					catch(CloneNotSupportedException e){
						e.printStackTrace();
					}
				}else
					System.out.println("ERRO - No removido");
			}
		}
		else if(dm.getNextHop() == uniqueID && maliciousNodes.containsMaliciousNode((Short)dm.getSourceId()))
			System.out.println("No malicioso a enviar dados");
	}
	
	
	protected boolean sendData(ActionMessage msg){
		String direction = "random";
		String destination = "";
		if(msg.getDestination().equals("sinknode"))
			destination = sinknodeaddress;
		if (!destination.isEmpty()){
			int i = UtilsCleanSlate.preMatching(destination,address,routingTableM.size());
			if(address.equals(destination)){
				CleanSlateMessage msgapp = new CleanSlateMessage(MessageTypes.DATAMESSAGE,msg.getData());
				Application app = getNode().getApplication();
				if(app!=null)	app.receiveMessage(msgapp);
			}
			else if(i==-1)
				; //System.out.println("ERROR - Invalid address " + destination);
			else{
				Pair<String,Short> entry = routingTableM.get(i);
				if(entry.getSecond()!=null && !maliciousNodes.containsMaliciousNode(entry.getSecond())){
					DataMessage dm = new DataMessage(destination,entry.getSecond(),msg.getData(), direction);
					dm.setForwardBy(getNode().getId());
					dm.packetSecurity(true);
					
					return immediateSendMessage(dm); 
				}else
					System.out.println("ERRO - No removido da rede");
			}
		}else
			; //System.out.println("ERROR - Invalid address (unknown sink node)");
		
		return false;
	}
	
	
	
	class EventRemoveMaliciousNodes extends Event {
		
		//long ADDTIME = Simulator.ONE_SECOND*90;
		
		public EventRemoveMaliciousNodes(long time) {
			super(time);
			getHostNode().getSimulator().addEvent(this);
		}
		
		public void execute()
		{
			sendHoneyBeeMessage();
		}
		
		/*public void incTime(){
			super.time+=ADDTIME;
		}*/
		
	}
	
	
	protected void removeMaliciousNodes(){
		//if(neighborsGroups.isEmpty() && directedNeighborsGroups.isEmpty() && newGroups.isEmpty()){
			long time = Utils.randomLongBetween(Simulator.ONE_SECOND*20, Simulator.ONE_SECOND*40, new Random());
			new EventRemoveMaliciousNodes(getHostNode().getSimulator().getSimulationTime()+time);
		//}
	}
	
	
	/**
	 * Lan�ar a actualiza�ao do proprio grupo acerca dos grupos vizinhos
	 */
	protected void launchNeighborsUpdate(){
		
		this.hasChallenger = false;
		this.firstGroup = false;
		this.crm = null;
		this.responseMessageSent = false;
		this.frm = null;
		this.mergeabortInfo.reset();
		this.sinknodeaddressaux = null;
		this.nexthops.clear();
	
		
		if(this.isEdgeNode() || !newGroups.isEmpty()){

			int num = Auxiliary.numEdgeNodes.get(myGroupInfo.getGroupID());
			int x = num/2;
			long randTime = Utils.randomLongBetween(0, Simulator.ONE_SECOND*myGroupInfo.getSizeGroup()*x, new Random());
			long time = getHostNode().getSimulator().getSimulationTime() + randTime;
			
			if(!this.isEdgeNode() && !newGroups.isEmpty())
				Auxiliary.addEdgeNode(myGroupInfo.getGroupID(),uniqueID);
			
			new EventNeighboringUpdate(time);
		} else
			Auxiliary.removeEdgeNode(myGroupInfo.getGroupID(),uniqueID);
	}

	
	
	/**
	 * Retorna true se esta mensagem j� tinha chegado.
	 */
	private boolean receiveReliableMessage(ReliableMessage m){
		if(m.getDestination().equals(uniqueID)){
			
			
			boolean res = false;
			if(arrivedMsgs.contains(m.getRandomValue()))
				res = true;
			else
				insertArrivedMsg(m.getRandomValue());
			
			if(m.getType() == MessageTypes.UPDATEGROUPMESSAGE){
				UpdateGroupMessage ugm = (UpdateGroupMessage)m;
				sendAckResponse(new AckMessage(uniqueID, (Short)m.getSourceId(),m.getRandomValue(),"update",ugm.getAuxNumber()));
			}
			else if(m.getType() == MessageTypes.BROADCASTCHALLENGEMESSAGE){
				BroadcastChallengeMessage bcm = (BroadcastChallengeMessage)m;
				//Auxiliary.settest.add(bcm.getAuxNumber());
				sendAckResponse(new AckMessage(uniqueID, (Short)m.getSourceId(),m.getRandomValue(),"gvt",bcm.getAuxNumber()));
			}
			else if(m.getType() == MessageTypes.CHOOSECHALLENGERMESSAGE){
				ChooseChallengerMessage ccm = (ChooseChallengerMessage)m;
				//Auxiliary.settest.add(ccm.getAuxNumber());
				sendAckResponse(new AckMessage(uniqueID, (Short)m.getSourceId(),m.getRandomValue(),"gvt",ccm.getAuxNumber()));
			}
			else if(m.getType() == MessageTypes.CHOOSERESPONDERMESSAGE){
				ChooseResponderMessage crm = (ChooseResponderMessage)m;
				//Auxiliary.settest.add(crm.getAuxNumber());
				sendAckResponse(new AckMessage(uniqueID, (Short)m.getSourceId(),m.getRandomValue(),"gvt",crm.getAuxNumber()));
			}
			else if(m.getType() == MessageTypes.RESPONSETOEDGENODEMESSAGE){
				ResponseToEdgeNodeMessage renm = (ResponseToEdgeNodeMessage)m;
				//Auxiliary.settest.add(renm.getAuxNumber());
				sendAckResponse(new AckMessage(uniqueID, (Short)m.getSourceId(),m.getRandomValue(),"gvt",renm.getAuxNumber()));
			}
			else
				sendAckResponse(new AckMessage(uniqueID, (Short)m.getSourceId(),m.getRandomValue()));
			
			return res;
		}else 
			return true;
	}
	
	
	protected boolean receiveReliableAckInvitationMessage(AckInvitationMessage m){
		
		if(m.getDestinations().contains(uniqueID)){
			
			boolean res = false;
			if(arrivedMsgs.contains(m.getRandomValue()))
				res = true;
			else
				insertArrivedMsg(m.getRandomValue());
			
			AckInvitation2Message ack2Message = new AckInvitation2Message(uniqueID, (Short)m.getSourceId(), getHostNode().signatureID);
			sendAckResponse(ack2Message);
			
			return res;
		}else 
			return true;
	}
	
	
	private boolean receiveReliableResponseMessage(ReliableMessage m){
		
		
		if(m.getDestination().equals(uniqueID)){

			if(!(m instanceof AcceptedMessage) && !m.getDestinationGroupInfo().equals(myGroupInfo))
					return true;
			
			boolean res = false;
			if(arrivedMsgs.contains(m.getRandomValue()))
				res = true;
			else {
				pendingMsgs.remove(mergeProposeMsg);
				insertArrivedMsg(m.getRandomValue());
			}
			
			if(m instanceof AcceptedMessage){
				
				AcceptedMessage am = (AcceptedMessage)m;
				if(!res || (myLastGroups.size() > 0 && myLastGroups.get(myLastGroups.size()-1).equals(am.getDestinationGroupInfo()))){
					AckMessage ackMessage = new AckMessage(getHostNode().getId(), (Short)m.getSourceId(),m.getRandomValue());
					sendAckResponse(ackMessage);
				}
			}
			
			return res;
		}else 
			return true;
	}
	
	
	
	private void sendReliableMessage(ReliableMessage message){
		pendingMsgs.add(message.getRandomValue());
		new PeriodicSendMessageEvent(getHostNode().getSimulator().getSimulationTime(),message);
	}
	
	
	/**
	 * Actualizar as tabelas de encaminhamento
	 */
	protected void updateRoutingTables(short nextHopID, Pair<String,String> pair){
		
		//Adicionar entrada na tabela
		UtilsCleanSlate.updateRoutingTable(routingTableM, nextHopID, pair.getFirst(), pair.getSecond());
		UtilsCleanSlate.updateOthersRoutingTables(routingTableL, routingTableR, routingTableM.size(), nexthops, nextHopID);
		
		if(routingTableL.size()< routingTableM.size())
			routingTableL.add(null);
		else if(routingTableL.size() == routingTableM.size()){
			if(routingTableL.get(routingTableL.size()-1) == routingTableM.get(routingTableM.size()-1).getSecond()){
				routingTableL.remove(routingTableL.size()-1);
				routingTableL.add(null);
			}
		}
		
		if(routingTableR.size() < routingTableM.size())
			routingTableR.add(null);
		else if(routingTableR.size() == routingTableM.size()){
			if(routingTableR.get(routingTableR.size()-1) == routingTableM.get(routingTableM.size()-1).getSecond()){
				routingTableR.remove(routingTableR.size()-1);
				routingTableR.add(null);
			}
		}
	}
	
	
	/**
	 * Opera�ao de merge
	 */
	protected void doMerge(GroupInfo groupg, GroupInfo groupg2, short nextHopID, String newsinknodeaddress){
		try {
		
			removeMergeEvent(); //ACRESCENTADO
			
			this.updateMsgsVariables();
			GroupInfo otherGroup = UtilsCleanSlate.getOtherGroup(myGroupInfo,groupg2, groupg);
		
			mergeWith = groupg.getGroupID();
			merges.add(otherGroup);  
			removeGroups.add(otherGroup);
			neighborsGroups.clear();
			newGroups.remove(otherGroup);
			//rejectedProposes.clear();
			
			//Calcular endere�o  First � o mybit, Second � o otherbit 
			Pair<String,String> pair = UtilsCleanSlate.getBitAddress(otherGroup.getGroupID(), myGroupInfo.getGroupID());
			
			if(address.isEmpty())
				address = pair.getFirst();
			else
				address = pair.getFirst() + "." + address;
			

			//Calcular novo ID e size
			myLastGroups.add(myGroupInfo.clone());
			removeGroups.add(myGroupInfo.clone());
			long newGroupID = UtilsCleanSlate.calculateNewGroupID(groupg,groupg2,getNode().getTinySecExt());
			myGroupInfo = new GroupInfo(newGroupID,groupg.getSizeGroup() +  groupg2.getSizeGroup());
			
			Auxiliary.addToGroup(myGroupInfo, this);
			Auxiliary.completedGroup(myGroupInfo, getNode().getSimulator().getSimulation());
			this.updateRoutingTables(nextHopID,pair);
			
			
			if(uniqueID == 0) //SE FOR O SINK NODE
				sinknodeaddress = address;
			
			if(sinknodeaddress.isEmpty() && !newsinknodeaddress.isEmpty() && uniqueID != 0)
				sinknodeaddress = newsinknodeaddress;
			else if(!sinknodeaddress.isEmpty() && newsinknodeaddress.isEmpty() && uniqueID != 0)
				sinknodeaddress = pair.getFirst() + "." + sinknodeaddress;
			
			nexthops.clear();
			rejectedGroups.clear();
			

			currwaiting=0;
				
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}catch(CloneNotSupportedException e1){
			System.err.println(e1.getMessage());
		}
	}
	
	
	protected boolean isEdgeNode(){
		return !directedNeighborsGroups.isEmpty();
	}
	
	protected void removeArrivedMsgs(int n){
		for(int i=0; i< n; i++){
			long num = arrivedMsgs_sorted.remove(0);
			arrivedMsgs.remove(num);
		}
	}
	
	protected void removeDuplicateMsgs(int n){
		for(int i=0; i< n; i++){
			long num = duplicateMsgs_sorted.remove(0);
			duplicates.remove(num);
		}
	}
	
	
	protected void sendMergingPropose(long time){
		new EventMerging(time);		
	}
	
	protected void insertArrivedMsg(long number){
		arrivedMsgs.add(number);
		arrivedMsgs_sorted.add(number);
	}
	
	protected void insertDuplicateMsg(long number){
		duplicates.add(number);
		duplicateMsgs_sorted.add(number);
	}
	
		//////////////////// GVT //////////////////////////////
	
	
	/**
	 * Escolher o desafiador do grupo
	 */
	private void chooseChallengerGVT(GroupInfo sourceGroup, short otheredgenodeID){
		try {
			Auxiliary.gvtaddNode(myGroupInfo.getGroupID(), this);
			Auxiliary.addGVTFinalMessage(myGroupInfo.getGroupID(), this);
			
			if(UtilsCleanSlate.chooseChallenger(getHostNode().challengev.getMessageDigest(),address,
					myGroupInfo.getGroupID(),getNode().getTinySecExt())){
				hasChallenger = true;
				int i = merges.size(); //Calcular o ci = h^(k-i)(Ck)
				if(i>0)
					this.sendBroadcastChallengeMessage(sourceGroup,uniqueID,otheredgenodeID);
				else{
					ChallengeRequestMessage crm = new ChallengeRequestMessage(uniqueID,otheredgenodeID,myGroupInfo.clone(), getHostNode().challengev.getCi(i), new Random().nextLong());
					sendReliableMessage(crm);
				}
			}
			else {
				long randValue = new Random().nextLong();
				this.insertDuplicateMsg(randValue);
				Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, "");
				if(pair.getFirst().getFirst()!=null)
					sendReliableMessage(new ChooseChallengerMessage("0", uniqueID,pair.getFirst().getFirst(),randValue,new Random().nextLong(),sourceGroup,uniqueID,otheredgenodeID,myGroupInfo.getGroupID()));
				if(pair.getSecond().getFirst()!=null)
					sendReliableMessage(new ChooseChallengerMessage("1",uniqueID,pair.getSecond().getFirst(),randValue,new Random().nextLong(),sourceGroup,uniqueID,otheredgenodeID,myGroupInfo.getGroupID()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}			
	}
	
	
	protected void isChooseChallengerMessage(ChooseChallengerMessage m){
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;
		
		if(m.getDestination().equals(uniqueID) && !hasChallenger){
			try {
				if(!duplicates.contains(m.getRemoveDuplValue())){
					
					this.insertDuplicateMsg(m.getRemoveDuplValue());
					removeMergeEvent();
					pendingMsgs.remove(mergeProposeMsg);
					
					if(UtilsCleanSlate.chooseChallenger(getHostNode().challengev.getMessageDigest(),address, myGroupInfo.getGroupID(),getNode().getTinySecExt())){
						this.sendBroadcastChallengeMessage(m.getOtherGroup(),m.getMyEdgeNodeID(),m.getOtherEdgeNodeID());
						return ;
					}
				}
			
				Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, m.getPrefix());
				if(m.getPrefix().length() <= address.length()-2){ 
					
					String prefix = "";
					if(pair.getFirst().getFirst()!=null){  //FAZ BROADCAST FIAVEL PARA O GRUPO
						prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getFirst(), 0);
						sendReliableMessage(new ChooseChallengerMessage(prefix , uniqueID,pair.getFirst().getFirst(),m.getRemoveDuplValue(),new Random().nextLong(),m.getOtherGroup(),m.getMyEdgeNodeID(),m.getOtherEdgeNodeID(),myGroupInfo.getGroupID()));				
					}
					if(pair.getSecond().getFirst()!=null){
						prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getSecond(), 1);
						sendReliableMessage(new ChooseChallengerMessage(prefix , uniqueID,pair.getSecond().getFirst(),m.getRemoveDuplValue(),new Random().nextLong(),m.getOtherGroup(),m.getMyEdgeNodeID(),m.getOtherEdgeNodeID(),myGroupInfo.getGroupID()));
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Enviar o desafio para todo o grupo, para que este o conhe�a
	 */
	private void sendBroadcastChallengeMessage(GroupInfo sourceGroup, short myedgenodeID, short otheredgenodeID) throws Exception {

		
		int i = merges.size(); //Calcular o ci = h^(k-i)(Ck)
		long randValue = new Random().nextLong();
		this.insertDuplicateMsg(randValue);
		
		if(uniqueID == myedgenodeID){	 
			ChallengeRequestMessage crm = new ChallengeRequestMessage(uniqueID,otheredgenodeID,myGroupInfo.clone(), getHostNode().challengev.getCi(i), new Random().nextLong());
			sendReliableMessage(crm);
		}
		
		Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, "");
		
		
		if(pair.getFirst().getFirst()!=null){
			
			BroadcastChallengeMessage bcm = new BroadcastChallengeMessage("0", uniqueID,
				pair.getFirst().getFirst(),randValue,new Random().nextLong(),getHostNode().challengev.getC0(),
				getHostNode().challengev.getCi(i),i,uniqueID,getHostNode().challengev.getSignature(),sourceGroup,myedgenodeID,otheredgenodeID,myGroupInfo.getGroupID());
			sendReliableMessage(bcm);
		}
		if(pair.getSecond().getFirst()!=null){
			
			BroadcastChallengeMessage bcm = new BroadcastChallengeMessage("1",uniqueID,
				pair.getSecond().getFirst(),randValue,new Random().nextLong(),getHostNode().challengev.getC0(),
				getHostNode().challengev.getCi(i), i,uniqueID, getHostNode().challengev.getSignature(),sourceGroup,myedgenodeID,otheredgenodeID, myGroupInfo.getGroupID());
			sendReliableMessage(bcm);
		}	
	}
	
	protected void isBroadcastChallengeMessage(BroadcastChallengeMessage m) {
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;
		
		if(m.getDestination().equals(uniqueID)){
			if(!duplicates.contains(m.getRemoveDuplValue())){
				
				this.insertDuplicateMsg(m.getRemoveDuplValue());
				removeMergeEvent();
				pendingMsgs.remove(mergeProposeMsg);
				hasChallenger = true;
				
				if(!UtilsCleanSlate.verifyAndCheck(m.getSignature(),m.getChallengerID(),m.getC0(),
						m.getCi(),m.getI(),getHostNode().keyNaPub,getHostNode().getIVector(),
						getHostNode().challengev.getMessageDigest(),getNode().getTinySecExt()))
				mergeabortInfo.setChallengeFail(new Pair<Short,GroupInfo>(m.getChallengerID(),myGroupInfo));
				
				
				if(uniqueID == m.getMyEdgeNodeID()){ 	//Se esta e se for edge node entao envia para o grupo
					try {
						ChallengeRequestMessage crm = new ChallengeRequestMessage(uniqueID,m.getOtherEdgeNodeID(),myGroupInfo.clone(), m.getCi(),new Random().nextLong());
						sendReliableMessage(crm);
					} catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
			
			Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, m.getPrefix());
			if(m.getPrefix().length() <= address.length()-2){ 
				String prefix = "";
				if(pair.getFirst().getFirst()!=null){ 
					prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getFirst(), 0);	
					BroadcastChallengeMessage bcm = new BroadcastChallengeMessage(prefix , uniqueID,
						pair.getFirst().getFirst(),m.getRemoveDuplValue(),new Random().nextLong(),m.getC0(),
						m.getCi(),m.getI(),m.getChallengerID(),m.getSignature(),m.getOtherGroup(),m.getMyEdgeNodeID(),m.getOtherEdgeNodeID(),myGroupInfo.getGroupID());	
					sendReliableMessage(bcm);
				}
				if(pair.getSecond().getFirst()!=null){
					prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getSecond(), 1);
					BroadcastChallengeMessage bcm = new BroadcastChallengeMessage(prefix , uniqueID,
						pair.getSecond().getFirst(),m.getRemoveDuplValue(),new Random().nextLong(),m.getC0(),
						m.getCi(),m.getI(),m.getChallengerID(),m.getSignature(),m.getOtherGroup(),m.getMyEdgeNodeID(),m.getOtherEdgeNodeID(),myGroupInfo.getGroupID());
					sendReliableMessage(bcm);
				}
			}
		}
	}
	
	
	protected void isChallengeRequestMessage(ChallengeRequestMessage m){
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;		
		
		
		if(m.getDestination().equals(uniqueID)){
			try{
				removeMergeEvent();
				
				pendingMsgs.remove(mergeProposeMsg);
				pendingMsgs.remove(acceptedMsg);
				if(!firstGroup)
					this.chooseChallengerGVT(m.getSourceGroup(),(Short)m.getSourceId());
				
				this.otherGroup = m.getSourceGroup();
				this.edgenodeIDtochallenger = (Short)m.getSourceId();
				
				if(UtilsCleanSlate.chooseResponder(getHostNode().challengev.getMessageDigest(),m.getSourceGroup(), myGroupInfo, 
						m.getCi(),address,getNode().getTinySecExt())){
					this.responseMessageSent = true;
					ChallengeResponseMessage cr = new ChallengeResponseMessage(uniqueID, (Short)m.getSourceId(), myGroupInfo.clone(), uniqueID , getHostNode().signatureID, new LinkedList<GroupInfo>(merges), new Random().nextLong());
					sendReliableMessage(cr);
					Auxiliary.verifyBroadcastChallenge(myGroupInfo.getGroupID());
				}	
				else{					//Envia para o resto do grupo
					long randValue = new Random().nextLong();
					this.insertDuplicateMsg(randValue);
					Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, "");
					if(pair.getFirst().getFirst()!=null){  
						ChooseResponderMessage crm = new ChooseResponderMessage("0", uniqueID,pair.getFirst().getFirst(),
							randValue,new Random().nextLong(),m.getSourceGroup(),m.getCi(),address,myGroupInfo.getGroupID());
						sendReliableMessage(crm);
					}
					if(pair.getSecond().getFirst()!=null){
						ChooseResponderMessage crm = new ChooseResponderMessage("1",uniqueID,pair.getSecond().getFirst(),
							randValue,new Random().nextLong(),m.getSourceGroup(),m.getCi(),address,myGroupInfo.getGroupID());
						sendReliableMessage(crm);
					}
				}
			} catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void isChooseResponderMessage(ChooseResponderMessage m){
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;
		
		
		if(m.getDestination().equals(uniqueID)){
			try {
				if(!duplicates.contains(m.getRemoveDuplValue())){
					this.insertDuplicateMsg(m.getRemoveDuplValue());
					removeMergeEvent();
					pendingMsgs.remove(mergeProposeMsg);
					
					if(UtilsCleanSlate.chooseResponder(getHostNode().challengev.getMessageDigest(),m.getOtherGroup(), 
							myGroupInfo, m.getCi(),address,getNode().getTinySecExt())){
						int i = UtilsCleanSlate.preMatching(m.getMyEdgeNodeAddress(),address,routingTableM.size());
						Pair<String,Short> entry = routingTableM.get(i);
						ResponseToEdgeNodeMessage rten = new ResponseToEdgeNodeMessage(m.getMyEdgeNodeAddress(), uniqueID, entry.getSecond(),
							myGroupInfo.clone(),uniqueID,getHostNode().signatureID, new LinkedList<GroupInfo>(merges),new Random().nextLong(),myGroupInfo.getGroupID());
						sendReliableMessage(rten);
						return ;
					}
				}
				
				
				Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, m.getPrefix());
				if(m.getPrefix().length() <= address.length()-2){ 
					
					String prefix = "";
					if(pair.getFirst().getFirst()!=null){  //FAZ BROADCAST FIAVEL PARA O GRUPO
						prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getFirst(), 0);
						sendReliableMessage(new ChooseResponderMessage(prefix , uniqueID,pair.getFirst().getFirst(),m.getRemoveDuplValue(),new Random().nextLong(),m.getOtherGroup(),m.getCi(),m.getMyEdgeNodeAddress(),myGroupInfo.getGroupID()));				
					}
					if(pair.getSecond().getFirst()!=null){
						prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getSecond(), 1);
						sendReliableMessage(new ChooseResponderMessage(prefix , uniqueID,pair.getSecond().getFirst(),m.getRemoveDuplValue(),new Random().nextLong(),m.getOtherGroup(),m.getCi(),m.getMyEdgeNodeAddress(),myGroupInfo.getGroupID()));
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}	
	}
	
	
	/**
	 * Inciar o merge
	 */
	private void initDoMerge(){
		try {	
			GroupInfo myoldgroup = myGroupInfo.clone();
			oldRoutingTableM = UtilsCleanSlate.getCopyVector(routingTableM);
			oldAddress = address;
			
			
			doMerge(myoldgroup,crm.getResponderGroup(), (Short)crm.getSourceId(),sinknodeaddressaux); //Confirmar!
			mergeDoned = true;
			
			if(parentAux != null)
				getHostNode().parents.add(parentAux);
			parentAux = null;
			
			Auxiliary.createWith2Groups(myGroupInfo.getGroupID(), myoldgroup,otherGroup); //Tirar?
			
			if(merges.size() > 1){
				Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(oldRoutingTableM, "");
				if(pair.getFirst().getFirst()!=null){  //FAZ BROADCAST FIAVEL PARA O GRUPO{
					DoMergeMessage m = new DoMergeMessage(myoldgroup.clone(),crm.getResponderGroup(), "0", pair.getFirst().getFirst(),
						uniqueID,new Random().nextLong(),sinknodeaddressaux,crm.getResponderID(),crm.getSignatureResponserID(),crm.getMergeTable());
					sendReliableMessage(m);
				}
				if(pair.getSecond().getFirst()!=null){
					DoMergeMessage m = new DoMergeMessage(myoldgroup.clone(), crm.getResponderGroup(), "1", pair.getSecond().getFirst(),
						uniqueID,new Random().nextLong(),sinknodeaddressaux,crm.getResponderID(),crm.getSignatureResponserID(),crm.getMergeTable());
					sendReliableMessage(m);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
	
	protected void checkingResponseMessage(){
		try {
			
			if(mergeabortInfo.getChallengeFail()!=null){
				FinalResponseMessage m = new FinalResponseMessage(uniqueID,(Short)crm.getSourceId(), new Random().nextLong(), FinalResponseMessage.ABORT,myGroupInfo.getGroupID());
				sendReliableMessage(m);
			}
			
			TinySecExtended tinySecExt = getNode().getTinySecExt();

			if(UtilsCleanSlate.verifyResponserSignature(crm.getResponderID(), crm.getSignatureResponserID(),getHostNode().keyNaPub,tinySecExt)){
				
				if(UtilsCleanSlate.verifyGVT(crm.getMergeTable(),crm.getResponderID(),crm.getResponderGroup(),tinySecExt)){
					if(mergeabortInfo.getChallengeFail()==null){
						FinalResponseMessage m = new FinalResponseMessage(uniqueID,(Short)crm.getSourceId(), new Random().nextLong(), FinalResponseMessage.ACCEPT,myGroupInfo.getGroupID());
						sendReliableMessage(m);
					}
				}
				else{
					if(mergeabortInfo.getChallengeFail()==null){
						mergeabortInfo.setGVTFail(new Pair<Short,GroupInfo>(crm.getResponderID(),crm.getResponderGroup()));
						FinalResponseMessage m = new FinalResponseMessage(uniqueID,(Short)crm.getSourceId(), new Random().nextLong(), FinalResponseMessage.ABORT,myGroupInfo.getGroupID());
						sendReliableMessage(m);
					}
				}
					
			}else{
				if(mergeabortInfo.getChallengeFail()==null){
					mergeabortInfo.setResponserSignFail(new Pair<Short,GroupInfo>(crm.getResponderID(),crm.getResponderGroup()));
					FinalResponseMessage m = new FinalResponseMessage(uniqueID,(Short)crm.getSourceId(), new Random().nextLong(), FinalResponseMessage.ABORT,myGroupInfo.getGroupID());
					sendReliableMessage(m);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	protected void isChallengeResponseMessage(ChallengeResponseMessage m){
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;
		
		if(m.getDestination().equals(uniqueID)){
			this.crm = m;
			Auxiliary.verifyBroadcastChallenge(myGroupInfo.getGroupID());
		}
	}
	
	
	protected void finishgvt(){

		if(!mergeabortInfo.isNull() || (frm.getFinalResponseType() == FinalResponseMessage.ABORT)){
			try {
				
				//removeGroups.add(crm.getResponderGroup().clone());
				if(mergeabortInfo.getResponserSignFail() != null){
					maliciousNodes.addMaliciousNode(mergeabortInfo.getResponserSignFail().getFirst(),mergeabortInfo.getResponserSignFail().getSecond());
					maliciousNodes.addMaliciousNode(uniqueID,myGroupInfo);
				}
				
				if(mergeabortInfo.getGVTFail()!=null){
					maliciousNodes.addMaliciousNode(mergeabortInfo.getGVTFail().getFirst(),mergeabortInfo.getGVTFail().getSecond());
					maliciousNodes.addMaliciousNode(uniqueID,myGroupInfo);
					//sendHoneyBeeMessage(mergeabortInfo,true);
				}
				
				if(mergeabortInfo.getChallengeFail() != null){
					maliciousNodes.addMaliciousNode(mergeabortInfo.getChallengeFail().getFirst(),mergeabortInfo.getChallengeFail().getSecond());
					maliciousNodes.addMaliciousNode(uniqueID,myGroupInfo);
				}
					
				Auxiliary.unlockGroup2(myGroupInfo);
				RejectedMessage rm = new RejectedMessage(null,myGroupInfo,uniqueID, uniqueID,getOldGroupsAfterMerge(),
						new TreeSet<GroupInfo>(),new Random().nextLong(),new Random().nextLong(),crm.getResponderGroup().clone());
				isRejectedMessage(rm,true);
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		else if(frm.getFinalResponseType() == FinalResponseMessage.ACCEPT)
			this.initDoMerge();
		
		//mergeabort = false;
	}
	
	/*
	private void initAbortGroup(){
		
		Auxiliary.lockgroup(myGroupInfo,true,true);
		
		groupaborted=true;
		lock = false;
		mergeDoned = false;
		long value = new Random().nextLong();
		this.insertDuplicateMsg(value);
		this.updateMsgsVariables();
		pendingMsgs.remove(acceptedMsg);
		Auxiliary.clearGroup(myGroupInfo);
		CleanSlateApplication.i++;
		
		//if(!maliciousNodes.contains(uniqueID)){
			//Auxiliary.addToGroup(myGroupInfo, node);
			//Auxiliary.completedGroup(myGroupInfo);
		//}
		
		try{
			if(merges.size() > 0){
				
				Pair<Pair<Long,Boolean>,Pair<Long,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, "");
				if(pair.getFirst().getFirst()!=null)  //FAZ BROADCAST FIAVEL PARA O GRUPO
					sendReliableMessage(new AbortMessage(myGroupInfo.clone(),"0", uniqueID,pair.getFirst().getFirst(),value,new Random().nextLong()));
				if(pair.getSecond().getFirst()!=null)
					sendReliableMessage(new AbortMessage(myGroupInfo.clone(),"1",uniqueID,pair.getSecond().getFirst(),value,new Random().nextLong()));
			}			
		}catch(CloneNotSupportedException e){
			System.err.println(e.getMessage());
		}
	}
	*/
	
	
	/*private void isAbortMessage(AbortMessage m){
		
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;
		
		if(m.getDestination() == uniqueID && myGroupInfo.equals(m.getDestinationGroupInfo())){
			
			if(!duplicates.contains(m.getRemoveDuplValue())){
				groupaborted=true;
				lock = false;
				mergeDoned = false;
				insertDuplicateMsg(m.getRemoveDuplValue());
				this.updateMsgsVariables();
				pendingMsgs.remove(acceptedMsg);
				//Auxiliary.addToGroup(myGroupInfo, node);
				//Auxiliary.completedGroup(myGroupInfo);
			}
		
			
			try {
				Pair<Pair<Long,Boolean>,Pair<Long,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, m.getPrefix());
				if(m.getPrefix().length() <= address.length()-2){ 
					
					String prefix = "";
					if(pair.getFirst().getFirst()!=null){  //FAZ BROADCAST FIAVEL PARA O GRUPO
						prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getFirst(), 0);
						sendReliableMessage(new AbortMessage(myGroupInfo.clone(), prefix , uniqueID,pair.getFirst().getFirst(),m.getRemoveDuplValue(),new Random().nextLong()));
					}
					if(pair.getSecond().getFirst()!=null){
						prefix = UtilsCleanSlate.getPrefixAux(m.getPrefix(), pair.getSecond(), 1);
						sendReliableMessage(new AbortMessage(myGroupInfo.clone(),prefix , uniqueID,pair.getSecond().getFirst(),m.getRemoveDuplValue(),new Random().nextLong()));
					}
				}
			}catch(CloneNotSupportedException e){
				System.err.println(e.getMessage());
			}
		}
		
	}*/
	
	protected void isFinalResponseMessage(FinalResponseMessage m){
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;
		
		if(m.getDestination().equals(uniqueID)){
			frm = m;
			Auxiliary.removeAndVerify(m.getSourceGroupID(),myGroupInfo.getGroupID(), m.getSourceGroupID());
		}
	}
	
	protected void isResponseToEdgeNodeMessage(ResponseToEdgeNodeMessage m){
		boolean arrived = receiveReliableMessage(m);
		if(arrived)
			return;
		
		if(m.getDestination().equals(uniqueID)){
			
			if(m.getDestinationAddress().equals(address)){
				this.responseMessageSent = true;
				ChallengeResponseMessage cr = new ChallengeResponseMessage(uniqueID, edgenodeIDtochallenger , m.getResponderGroup(), m.getResponderID(), m.getSignatureResponserID(),m.getMergeTable() , new Random().nextLong());
				sendReliableMessage(cr);
				Auxiliary.verifyBroadcastChallenge(myGroupInfo.getGroupID());			
			}
			else {
				int i = UtilsCleanSlate.preMatching(m.getDestinationAddress(),address,routingTableM.size());
				short nexthop = UtilsCleanSlate.getNextHop("",i, routingTableL, routingTableR,routingTableM);
				ResponseToEdgeNodeMessage rten = new ResponseToEdgeNodeMessage(m.getDestinationAddress(), uniqueID, nexthop, m.getResponderGroup(), m.getResponderID(), m.getSignatureResponserID(), m.getMergeTable(), new Random().nextLong(),myGroupInfo.getGroupID());
				sendReliableMessage(rten);
			}
		}
	}
	
	
	protected ReplaceResult replaceNodes(List<Short> maliciousNodes){
		
		List<Pair<Vector<String>,Vector<Integer>>> replaces = new LinkedList<Pair<Vector<String>,Vector<Integer>>>();
		List<Short> nodesIDList = new LinkedList<Short>();
		
		Iterator<Short> it = maliciousNodes.iterator();
		while(it.hasNext()){
			
			short nodeid = it.next();
			
			Pair<Vector<String>,Vector<Integer>> replacesMalicious = UtilsCleanSlate.replaceNode(nodeid, 
					routingTableL,routingTableR,routingTableM);
			
			if(!replacesMalicious.getFirst().isEmpty()){
				replaces.add(replacesMalicious);
				nodesIDList.add(nodeid);
			}
		}
		return new ReplaceResult(replaces, nodesIDList);
	}
	
	////////////////////////////////////// SE NOS SAO REMOVIDOS DA REDE  /////
	
	private boolean removeNodeInNetwork(List<Short> maliciousNodesID){
		
		
		ReplaceResult replaceResult = this.replaceNodes(maliciousNodesID);
		
		if(!replaceResult.isEmpty()){
			removenode.setReplacesMaliciousNodes(replaceResult);
			ReplaceRequestMessage rrm = new ReplaceRequestMessage(uniqueID,replaceResult.getMaliciousNodesID(),replaceResult.getAddresses());
			new ReplaceRequestMessageEvent(getHostNode().getSimulator().getSimulationTime(),rrm);
			return true;
		}
		
		return false;
	}
	
	
	private Vector<Pair<String,Short>> newNextHops(Iterator<String> addressesIt, short sourceID, short removedNodeID){
		
		Vector<Pair<String,Short>> nexthops = new Vector<Pair<String,Short>>();
		
		while(addressesIt.hasNext()){
			String address = addressesIt.next();
			
			int i = UtilsCleanSlate.preMatching(address,address,routingTableM.size());
			if(i==-2)
				nexthops.add(new Pair<String,Short>(address,uniqueID));
			else if(i!=-1){
				Short nexthop = routingTableM.get(i).getSecond(); //Aqui
				if(nexthop != null && nexthop != removedNodeID && nexthop != sourceID && !Auxiliary.hasLoop(sourceID,nexthop,address))
					nexthops.add(new Pair<String,Short>(address,nexthop));
				else {
					Short nexthopl = routingTableL.get(i);
					if(nexthopl!=null && nexthopl != removedNodeID && nexthopl != sourceID && !Auxiliary.hasLoop(sourceID,nexthop,address))
						nexthops.add(new Pair<String,Short>(address,nexthopl));
					else {
						Short nexthopr = routingTableR.get(i);
						if(nexthopr!=null && nexthopr != removedNodeID && nexthopr!=sourceID && !Auxiliary.hasLoop(sourceID,nexthop,address))
							nexthops.add(new Pair<String,Short>(address,nexthopr));
					}
				}
			}
		}
		return nexthops;
	}
	
	
	protected NewHopsResult calcNewNextHops(short sourceID, List<Short> maliciousNodeIDs, List<Vector<String>> addresses){
		
		List<Vector<Pair<String,Short>>> newhops = new LinkedList<Vector<Pair<String,Short>>>();
		List<Short> newMaliciousNodesID = new LinkedList<Short>();
		
		Iterator<Short> it = maliciousNodeIDs.iterator();
		Iterator<Vector<String>> it2 = addresses.iterator();
		while(it.hasNext() && it2.hasNext()){
			
			short nodeID = it.next();
			Vector<String> vect = it2.next();
			Vector<Pair<String,Short>> newnexthops = this.newNextHops(vect.iterator(),sourceID,nodeID);
			
			if(!newnexthops.isEmpty()){
				newhops.add(newnexthops);
				newMaliciousNodesID.add(nodeID);
			}
			
		}
		
		return new NewHopsResult(newhops, newMaliciousNodesID);
	}
	
	
	protected void isReplaceRequestMessage(ReplaceRequestMessage m){
				
		if(neighborsNodes.contains((Short)m.getSourceId()) && !m.getMaliciousNodesID().contains(uniqueID)){
			
			NewHopsResult newnexthops = this.calcNewNextHops((Short)m.getSourceId(),m.getMaliciousNodesID(), m.getAddresses());
			
			
			if(!newnexthops.isEmpty()){
				ReplaceReplyMessage rrm = new ReplaceReplyMessage(uniqueID,(Short)m.getSourceId(), newnexthops);
				sendMessageScheduled(rrm, 0, Simulator.ONE_SECOND*2);
			}
		}
	}
	
	
	private void updateReplaces(NewHopsResult newhopsresult, short sourceID){
		
		Iterator<Short> it1 = newhopsresult.getMaliciousNodesID().iterator();
		Iterator<Vector<Pair<String,Short>>> it2 = newhopsresult.getNewHops().iterator();
		while(it1.hasNext() && it2.hasNext()){
			
			short maliciousNodeID = it1.next();
			Vector<Pair<String,Short>> vectnewhops = it2.next();
			Pair<Vector<String>,Vector<Integer>> pair = removenode.getEntryToReplace(maliciousNodeID);
			
			if(pair != null){
				Iterator<Pair<String,Short>> it3 = vectnewhops.iterator();
				while(it3.hasNext()){
					Pair<String,Short> newhop = it3.next();
					Integer index = UtilsCleanSlate.containsAddress(pair.getFirst(), pair.getSecond(), newhop.getFirst()); 
					if(index != null){
						removenode.getReplaceRequestEvent().resetAttempts();
						Pair<String,Short> entry = routingTableM.get(index);
						if(newhop.getSecond() != null && neighborsNodes.contains(newhop.getSecond()))
							entry.setSecond(newhop.getSecond());
						else 
							entry.setSecond(sourceID);
						
						if(pair.getFirst().isEmpty() && pair.getSecond().isEmpty())
							removenode.removeNodeID(maliciousNodeID);
					}
				}
			}
		}
	}
	
	
	protected void isReplaceReplyMessage(ReplaceReplyMessage m) {

		if(uniqueID == (Short)m.getDestinationId() && !removenode.getMaliciousNodeID().contains((Short)m.getSourceId())){

			this.updateReplaces(m.getNewNextHops(),(Short)m.getSourceId());			

			if(removenode.isEmpty())
				removenode.getReplaceRequestEvent().stop();
			else{
				removenode.getReplaceRequestEvent().getPendingMessage().setAddressesMalicious(new LinkedList<Vector<String>>(removenode.getAddresses()));
				removenode.getReplaceRequestEvent().getPendingMessage().setMaliciousNodesID(new LinkedList<Short>(removenode.getMaliciousNodeID()));
			}
		}
	}

	protected class ReplaceRequestMessageEvent extends Event {
		
		protected long timetoretransmit = Simulator.ONE_SECOND*2;
		protected int ATTEMPTSMAX = 5;
		
		protected int attempts;
		protected ReplaceRequestMessage pendingMessage;
		
		public ReplaceRequestMessageEvent(long time, ReplaceRequestMessage message) {
			super(time);
			this.pendingMessage = message;
			this.attempts = 0;
			removenode.setReplaceRequestvent(this);
			getHostNode().getSimulator().addEvent(this);
		}
		
		public ReplaceRequestMessageEvent(long time, ReplaceRequestMessage message, int attempts) {
			super(time);
			this.pendingMessage = message;
			this.attempts = attempts;
			removenode.setReplaceRequestvent(this);
			getHostNode().getSimulator().addEvent(this);
		}
		
		
		public void execute()
		{	
			if(attempts != -1 && attempts < ATTEMPTSMAX){
				send(pendingMessage);
				new ReplaceRequestMessageEvent(getHostNode().getSimulator().getSimulationTime() + timetoretransmit,pendingMessage,++attempts);
			}
		}
		
		public void stop(){
			this.attempts = -1;
		}
		
		public void resetAttempts(){
			this.attempts = 0;
		}
		
		public ReplaceRequestMessage getPendingMessage(){
			return pendingMessage;
		}
	}
	
	
	protected void sendHoneyBeeMessage(){
		
		
		if(!maliciousNodes.isEmpty()){

			long x = new Random().nextLong();
			insertDuplicateMsg(x);
			
			this.removeInitiatorAndMaliciousNodes(maliciousNodes);
			HoneyBeeMessage hbm = new HoneyBeeMessage(uniqueID,getHostNode().signatureID, maliciousNodes.getMaliciousNodes(),x);
			sendMessageScheduled(hbm, Simulator.ONE_SECOND, Simulator.ONE_SECOND*3);
		}
	}
	
	
	protected void removeInitiatorAndMaliciousNodes(MaliciousNodes maliciousnodes){
		
		
		Iterator<Short> it1 = maliciousnodes.getIds().iterator();
		Iterator<GroupInfo> it2 = maliciousnodes.getGroups().iterator();
		
		while(it1.hasNext() && it2.hasNext()){
			
			short id = it1.next();
			GroupInfo gi = it2.next();
			
			if(!maliciousNodes.isRemoved(id)){
				neighborsNodes.remove(id);
				neighborsGroups.remove(new GroupInfo(id,1));
				directedNeighborsGroups.remove(new GroupInfo(id,1));
				newGroups.remove(new GroupInfo(id,1));
				removeGroups.add(new GroupInfo(id,1));
				
				if(myGroupInfo.equals(gi)){
					int newsize = myGroupInfo.getSizeGroup()-1;
					myGroupInfo.setGroupSize(newsize);
				}
				maliciousNodes.addRemovedMaliciousNode(id);
				
				if(!maliciousNodes.contains(id))
					maliciousNodes.addNewMaliciousNode(id);
			}
		}
	}
	
	
	protected void isHoneyBeeMessage(HoneyBeeMessage m){
		
		if(!m.getMaliciousNodes().contains(uniqueID)  && !duplicates.contains(m.getRandomValue()) && 
				UtilsCleanSlate.verifySignature(m.getSignature(), m.getInitiatiorNodeID(), getHostNode().keyNaPub,getNode().getTinySecExt())){
			
			insertDuplicateMsg(m.getRandomValue());
			
			this.removeInitiatorAndMaliciousNodes(m.getMaliciousNodes());
			boolean done = removeNodeInNetwork(new LinkedList<Short>(m.getMaliciousNodes().getIds()));

			if(done)
				sendMessageScheduled(m, Simulator.ONE_SECOND*3, Simulator.ONE_SECOND*5);
			else
				sendMessageScheduled(m, Simulator.ONE_SECOND, Simulator.ONE_SECOND*3);
		}	
	}
	
	
	
	
	
	//////////////////////////////////////////////////////////////////////////////////////
	
    //Helper Methods
    
    
    /**
     * Permite agendar o envio de uma mensagem, tendo como tempo m�nimo de espera 
     * o minthreshold e o tempo m�ximo o maxthreshold.
     */
	protected void sendMessageScheduled(Object message, int minthreshold, int maxthreshold){
		long taux = Utils.randomLongBetween(minthreshold, maxthreshold, new Random());
		long t = getNode().getSimulator().getSimulationTime() + taux;
		new SendDelayedMessage(t,message);
	}
	
	
	/**
	 * Permite enviar imediatamente uma mensagem, contudo se a mensagem n�o pode ser enviada
	 * tenta enviar novamente, esperando por um tempo entre o minthreshold e o maxthreshold
	 */
	protected boolean immediateSendMessage(Object message){
		 send(message);
		 return true;
	 }


	//EVENTS
	
	
	class SendDelayedMessage extends Event {
		
		private Object message;
		
		public SendDelayedMessage(long time, Object message) {
			super(time);
			this.message = message;
			getNode().getSimulator().addEvent(this);
		}
		
		//Cria a mensagem de feedback quando for para envi�-la
		public void execute()
		{
			immediateSendMessage(message);
		}	
	} 
	
	
	//EVENTS PARA FASE HELLO
	
	protected class EventSendHelloMessage extends Event {
		
		
		public EventSendHelloMessage(long time) {
			super(time);
			getHostNode().getSimulator().addEvent(this);
		}
		
		public void execute()
		{
			HelloMessage newMsg = new HelloMessage(uniqueID, getHostNode().signatureID,new HashSet<Short>(bidirecNeighbors));
			bidirecNeighbors.clear();
			immediateSendMessage(newMsg);
		}	
	}
	
	protected class EventAckInvitationMessage extends Event {
		
		long timeaux = Simulator.ONE_SECOND*5;
		private int attempts;
		private Set<Short> oldpendings;
		private int MAXATTEMPTS = 3;
		protected short rvalue;
		
		public EventAckInvitationMessage(long time) {
			super(time);
			this.attempts = 0;
			this.oldpendings = new HashSet<Short>();
			this.rvalue = (short)new Random().nextLong();
			getHostNode().getSimulator().addEvent(this);
		}
		
		public EventAckInvitationMessage(long time, int attempts, Set<Short> old, short rvalue) {
			super(time);
			this.attempts = attempts;
			this.oldpendings = old;
			this.rvalue = rvalue;
			getHostNode().getSimulator().addEvent(this);
		}
		
		
		public void execute()
		{
			if(!pendingInvitations.isEmpty()){
				AckInvitationMessage aim = new AckInvitationMessage(uniqueID, new HashSet<Short>(pendingInvitations), rvalue, getHostNode().signatureID);
				send(aim);
				long val = Utils.randomLongBetween(CleanSlateConstants.TIMETORETRANSMITMIN, CleanSlateConstants.TIMETORETRANSMITMAX, new Random());
				
				
				if(!oldpendings.equals(pendingInvitations)){
					oldpendings = new HashSet<Short>(pendingInvitations);
					attempts = 0;
				}else
					attempts++;
				
				if(attempts<MAXATTEMPTS)
					new EventAckInvitationMessage(getHostNode().getSimulator().getSimulationTime() + val, ++attempts,this.oldpendings,rvalue);
			}
		}	
	}

	
	
	//EVENTOS PARA AS RESTANTES FASES
	
	/**
	 * Evento para enviar proposta de merge
	 */
	protected class EventMerging extends Event {
		
		public EventMerging(long time) {
			super(time);
			removeMergeEvent();
			lastMergeEvent = this;
			getHostNode().getSimulator().addEvent(this);
			
		}
		
		public void execute()
		{	
			try {
				Debug.initMergePropose();
				lastMergeEvent = null;
				
				if(isEdgeNode() && !mergeDoned){
					GroupInfo gi = ((TreeSet<GroupInfo>)neighborsGroups).first(); 
					if(directedNeighborsGroups.contains(gi)){
						String newsinknodeaddress = UtilsCleanSlate.calcNewSinkNodeAddress(gi.getGroupID(),myGroupInfo.getGroupID(),uniqueID,sinknodeaddress);
						MergePropMessage m = new MergePropMessage(gi.clone(),myGroupInfo.clone(),uniqueID,new Random().nextLong(),newsinknodeaddress);
						
						mergeProposeMsg = m.getRandomValue();
						sendReliableMessage(m);
					}
				}else if(myGroupInfo.getSizeGroup() == 1)
					Auxiliary.removeEdgeNode(myGroupInfo.getGroupID(), uniqueID); 
			} catch (CloneNotSupportedException e) {
				System.err.println(e.getMessage());
			}
		}	
	}
	
	
	/**
	 * 
	 * Evento para n� enviar a sua informa��o dos vizinhos para todo o grupo,
	 * broadcast fi�vel.
	 *
	 */
	protected class EventNeighboringUpdate extends Event {
		
		public EventNeighboringUpdate(long time){
			super(time);
			getHostNode().getSimulator().addEvent(this);
		}
		
		public void execute(){
			
			
			acceptedMsgs.clear();
			pendingMsgs.remove(mergeProposeMsg); 
			insertArrivedMsg(mergeProposeMsg);
			
			newGroups.removeAll(merges);
			newGroups.removeAll(removeGroups);
			
			directedNeighborsGroups.addAll(new HashSet<GroupInfo>(newGroups));
			neighborsGroups.addAll(new TreeSet<GroupInfo>(directedNeighborsGroups));
			neighborsGroups.removeAll(removeGroups);
			directedNeighborsGroups.removeAll(removeGroups);
			newGroups.clear();
			
			
			directedNeighborsGroups.removeAll(rejectedGroups);
			neighborsGroups.removeAll(rejectedGroups);

			
			if(!isEdgeNode()){	//Torna-se Edge Node logo apos um merge (� verificado agora nesta fase)
				removeMergeEvent();
				Auxiliary.removeEdgeNode(myGroupInfo.getGroupID(),getHostNode().getId());
				Auxiliary.verifyUpdates(myGroupInfo, getNode().getSimulator().getSimulation());
				return;
			}
			
			Auxiliary.addToCounter(myGroupInfo);
			
			if(myGroupInfo.getSizeGroup() == 1)
				Auxiliary.verifyUpdates(myGroupInfo, getNode().getSimulator().getSimulation());
			else {
				try{
					Set<GroupInfo> removeGroups = getMergesAndLastGroups();
					Pair<Pair<Short,Boolean>,Pair<Short,Boolean>> pair = UtilsCleanSlate.getNextHopFromPrefixAux(routingTableM, "");

					if(pair.getFirst().getFirst()!=null) 
						sendReliableMessage(new UpdateGroupMessage(uniqueID,pair.getFirst().getFirst(),myGroupInfo,"0", new HashSet<GroupInfo>(directedNeighborsGroups),myGroupInfo.clone(),removeGroups,new Random().nextLong()));
					if(pair.getSecond().getFirst()!=null)
						sendReliableMessage(new UpdateGroupMessage(uniqueID,pair.getSecond().getFirst(),myGroupInfo,"1", new HashSet<GroupInfo>(directedNeighborsGroups),myGroupInfo.clone(),removeGroups,new Random().nextLong()));
					
				}catch(CloneNotSupportedException e){
					System.err.println(e.getMessage());
				}
			}
		}
	}
	
	
	/**
	 * 
	 * @author Tiago Ara�jo
	 *
	 * Envio peri�dico de mensagens, para garantir a fiabilidade 
	 */
	protected class PeriodicSendMessageEvent extends Event {
		
		protected long timetoretransmitmin = CleanSlateConstants.TIMETORETRANSMITMIN;
		protected long timetoretransmitmax = CleanSlateConstants.TIMETORETRANSMITMAX;
		protected int attempts;

		protected ReliableMessage pendingMessage;
		
		public PeriodicSendMessageEvent(long time, ReliableMessage message) {
			super(time);
			this.pendingMessage = message;
			this.attempts = 0;
			if((message instanceof MergePropMessage && isEdgeNode()) || !(message instanceof MergePropMessage))
				getHostNode().getSimulator().addEvent(this);
		}
		
		public PeriodicSendMessageEvent(long time, ReliableMessage message, int attempts) {
			super(time);
			this.pendingMessage = message;
			this.attempts = attempts;
			if((message instanceof MergePropMessage && isEdgeNode()) || !(message instanceof MergePropMessage))
				getHostNode().getSimulator().addEvent(this);
		} 
		
		
		public void execute()
		{
			
			if(this.attempts > 0 && this.attempts % 25 == 0)
				timetoretransmitmax *= 2;
			
			if(pendingMsgs.contains(pendingMessage.getRandomValue())){
				send(pendingMessage);
				
				if(attempts > CleanSlateConstants.MAXATTEMPTS){
					if(pendingMessage instanceof MergePropMessage){
						MergePropMessage mpm = (MergePropMessage)pendingMessage;
						if(mpm.getDestinationGroupInfo().size()==1)
							neighborsNodes.remove(mpm.getDestinationGroupInfo().getGroupID()); //Confirmar!
					
						
						rejectedGroups.add(mpm.getDestinationGroupInfo());
						RejectedMessage rm = new RejectedMessage(null,myGroupInfo,uniqueID,uniqueID,getOldGroupsAfterMerge(),new TreeSet<GroupInfo>(),new Random().nextLong(),new Random().nextLong());
						isRejectedMessage(rm, true);
					}
					return;
				}
				
				long val = Utils.randomLongBetween(timetoretransmitmin, timetoretransmitmax, new Random());		
				new PeriodicSendMessageEvent(getHostNode().getSimulator().getSimulationTime() + val,pendingMessage, ++attempts);
			}
		}	
	}	
	
    protected CleanSlateNode getHostNode() {
    	return (CleanSlateNode)getNode();
    }	

	protected void onReceiveMessage(Object message) {
		
		final CleanSlateMessage msg = securityMessageReceived((Message) message);
		
    	if(msg.getType() == MessageTypes.HELLOMESSAGE)
			isHelloMessage((HelloMessage)msg);
		else if(msg.getType() == MessageTypes.ACKINVITATION2MESSAGE)
			isAckInvitation2Message((AckInvitation2Message)msg);
		else if(msg.getType() == MessageTypes.ACKINVITATIONMESSAGE)
			isAckInvitationMessage((AckInvitationMessage)msg);
		else if(msg.getType() == MessageTypes.MERGEPROPMESSAGE)
			isMergePropMessage((MergePropMessage)msg); //Proposta
		else if(msg.getType() == MessageTypes.DOMERGEMESSAGE)
			isDoMergeMessage((DoMergeMessage)msg);
		else if(msg.getType() == MessageTypes.NOMERGEMESSAGE)
			isNoMergeMessage((NoMergeMessage)msg);
		else if(msg.getType() == MessageTypes.ACKMESSAGE)
			isAckMessage((AckMessage)msg);
		else if(msg.getType() == MessageTypes.UPDATEGROUPMESSAGE)
			isUpdateGroupMessage((UpdateGroupMessage)msg);
		else if (msg.getType() == MessageTypes.ACCEPTED2MESSAGE)
			isAccepted2Message((Accepted2Message)msg);
		else if(msg.getType() == MessageTypes.ACCEPTEDMESSAGE)  //Os restantes s�o respostas � proposta
			isAcceptedMessage((AcceptedMessage)msg);
		else if(msg.getType() == MessageTypes.REJECTEDMESSAGE)
			isRejectedMessage((RejectedMessage)msg,false);
		else if(msg.getType() == MessageTypes.WAITINGMESSAGE)
			isWaitingMessage((WaitingMessage)msg);
		else if(msg.getType() == MessageTypes.DATAMESSAGE)
			isDataMessage((DataMessage)msg);
		else if(msg.getType() == MessageTypes.BROADCASTCHALLENGEMESSAGE)
			isBroadcastChallengeMessage((BroadcastChallengeMessage) msg);
		else if(msg.getType() == MessageTypes.CHOOSECHALLENGERMESSAGE)  //PARA A GVT
			isChooseChallengerMessage((ChooseChallengerMessage)msg);
		else if(msg.getType() == MessageTypes.CHOOSERESPONDERMESSAGE)
			isChooseResponderMessage((ChooseResponderMessage)msg);
		else if(msg.getType() == MessageTypes.CHALLENGEREQUESTMESSAGE)
			isChallengeRequestMessage((ChallengeRequestMessage)msg);
		else if(msg.getType() == MessageTypes.RESPONSETOEDGENODEMESSAGE)
			isResponseToEdgeNodeMessage((ResponseToEdgeNodeMessage) msg);
		else if(msg.getType() == MessageTypes.CHALLENGERESPONSEMESSAGE)
			isChallengeResponseMessage((ChallengeResponseMessage)msg);
		else if(msg.getType() == MessageTypes.FINALRESPONSEMESSAGE)
		    isFinalResponseMessage((FinalResponseMessage)msg);
//        		else if(message instanceof AbortMessage)
//        			isAbortMessage((AbortMessage)message);
		else if(msg.getType() == MessageTypes.REPLACEREQUESTMESSAGE)
			isReplaceRequestMessage((ReplaceRequestMessage)msg);
		else if(msg.getType() == MessageTypes.REPLACEREPLYMESSAGE)
			isReplaceReplyMessage((ReplaceReplyMessage)msg);
		else if(msg.getType() == MessageTypes.HONEYBEEMESSAGE)
			isHoneyBeeMessage((HoneyBeeMessage) msg);
	}


	@Override
	protected boolean onSendMessage(Object message, Application app) {
		ActionMessage m = (ActionMessage)message;
		
		if(m.getType() == ActionMessage.SENDDATA)
			return this.sendData(m);
		
		return false;
	}


	@Override
	protected void onRouteMessage(Object message) {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void startupAttacks() {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void sendMessageToAir(Object message) {
		getNode().getMacLayer().sendMessage(message, CleanSlateRoutingLayer.this);
	}


	@Override
	public void newRound() {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void onStartUp() {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void onStable(boolean oldValue) {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void initAttacks() {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected Message encapsulateMessage(Message m) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected String getRoutingTable() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void sendMessageDone() {
		// TODO Auto-generated method stub
		
	}
}