package org.wisenet.protocols.cleanslate.basestation;

import java.awt.Color;

import org.wisenet.protocols.cleanslate.CleanSlateNode;
import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.radio.RadioModel;
import org.wisenet.simulator.gui.IDisplayable;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * 
 */
public class CleanSlateBSNode extends CleanSlateNode implements IDisplayable {
	
	enum State {
		PROTOCOLSTARTED,
		PROTOCOLCOMPLETED,
		NOTHING
	}
	
	public CleanSlateBSNode(Simulator sim, RadioModel radioModel){
		super(sim,radioModel);
        //setEnableFunctioningEnergyConsumption(false);
        setRadius(2);
        setPaintingPaths(false);
        setBaseColor(Color.GREEN);
	}
	
	
	@Override
    public void init(){
        setSinkNode(true);
        getGraphicNode().setMarkColor(Color.CYAN);
        getGraphicNode().mark();
		super.init();
    }
	
	/*public void initNeighborProtocol(Class<?> applicationClass){
		((BSApplication)getApplication(applicationClass)).initNeighborProtocol();
	}*/
}