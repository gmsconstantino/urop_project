package org.wisenet.protocols.cleanslate.basestation;


import org.wisenet.protocols.cleanslate.Auxiliary;
import org.wisenet.protocols.cleanslate.CleanSlateRoutingLayer;
import org.wisenet.protocols.cleanslate.messages.ActionMessage;
import org.wisenet.protocols.cleanslate.messages.CleanSlateMessage;
import org.wisenet.protocols.cleanslate.messages.DataMessage;
import org.wisenet.protocols.cleanslate.messages.MessageTypes;
import org.wisenet.protocols.cleanslate.utils.UtilsCleanSlate;
import org.wisenet.simulator.core.Application;


/**
 * 
 * @author Tiago Ara�jo
 *
 */
public class CleanSlateBSRoutingLayer extends CleanSlateRoutingLayer{
	
	
	public CleanSlateBSRoutingLayer() {
		super();
		Auxiliary.reset();
	}
	
	protected void isDataMessage(DataMessage dm){
		
		if(dm.getNextHop() == uniqueID && !maliciousNodes.containsMaliciousNode(dm.getForwardBy())){
			
			//parent_data = getHostNode().getParentNode();
			
			int i = UtilsCleanSlate.preMatching(dm.getDestination(),address,routingTableM.size());
			if(dm.getDestination().equals(address)){
				CleanSlateMessage msgapp = new CleanSlateMessage(MessageTypes.DATAMESSAGE,dm.getPayload());
				Application app = getNode().getApplication();
				if(app!=null)	app.receiveMessage(msgapp);
			}
			else if(i==-1)
				; //System.out.println("ERROR - Invalid address");
			else {				
				Short nexthop = UtilsCleanSlate.getNextHop(dm.getDirection(),i, routingTableL, routingTableR,routingTableM);
				String dir = UtilsCleanSlate.updateDirection(dm.getDirection());
				
				if(nexthop != null){
					
					try {
						DataMessage newdm = dm.copyMessage();
						newdm.setForwardBy(uniqueID);
						newdm.setNextHop(nexthop);
						newdm.setDirection(dir);
						newdm.packetSecurity(true);
						immediateSendMessage(newdm);
					}
					catch(CloneNotSupportedException e){
						e.printStackTrace();
					}
				}else
					System.out.println("ERRO - No removido");
			}
		}
		else if(dm.getNextHop() == uniqueID && maliciousNodes.containsMaliciousNode((Short) dm.getSourceId()))
			System.out.println("No malicioso esta a enviar dados");
		
	}
		
	@Override
	public boolean sendMessage(Object message, Application app){

		ActionMessage m = (ActionMessage)message;
		if(m.getType() == ActionMessage.INITROUTINGPROTOCOL)
			new EventSendHelloMessage(getNode().getSimulator().getSimulationTime());
		else if(m.getType() == ActionMessage.SENDDATA)
			return sendData(m);
		return false;
	}
}