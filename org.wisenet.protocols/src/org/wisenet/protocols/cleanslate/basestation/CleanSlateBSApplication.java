package org.wisenet.protocols.cleanslate.basestation;

import java.security.KeyPair;
import java.util.Collection;
import java.util.Iterator;

import org.wisenet.protocols.MAC.tinysec.ConstantsTinySec;
import org.wisenet.protocols.MAC.tinysec.TinySecExtended;
import org.wisenet.protocols.cleanslate.CleanSlateApplication;
import org.wisenet.protocols.cleanslate.CleanSlateConstants;
import org.wisenet.protocols.cleanslate.CleanSlateNode;
import org.wisenet.protocols.cleanslate.NetworkAuthority;
import org.wisenet.protocols.cleanslate.messages.ActionMessage;
import org.wisenet.protocols.common.Pair;
import org.wisenet.simulator.core.node.Node;


public class CleanSlateBSApplication extends CleanSlateApplication {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5483307776829655595L;

	
	@Override
    public void run() {
        
		if(!getNode().getRoutingLayer().isStable()){
	    	preInit();
	    	initHelloProtocol();
		}else
			sendDataMessage();
    }
    
    
    /**
     * Fase de pr�-configura��o dos segredos criptogr�ficos
     */
    private void preInit(){
    	
    	TinySecExtended tinySecExt = getNode().getTinySecExt();
    	KeyPair kp = tinySecExt.createKeyPair(CleanSlateConstants.PropertiesKey[0], new Integer(CleanSlateConstants.PropertiesKey[1]), ConstantsTinySec.PROVIDER);
		NetworkAuthority na = new NetworkAuthority(kp,tinySecExt);
		
		Collection<Node> nodes = this.getNode().getSimulator().getNodes();
		int k = (int)(Math.log(nodes.size())/Math.log(2));
		Iterator<Node> it = nodes.iterator();
		
    	while(it.hasNext()){
    		Node n = it.next();
			try {//gerar o id para cada n�
    			getCleanSlateNode(n).setIVector(na.getIVector());
    			getCleanSlateNode(n).setKeyNaPub(na.getPublicKey());
				Pair<Short,byte[]> pair = na.generateID(n.getId());
				getCleanSlateNode(n).setPairId(pair);
				getCleanSlateNode(n).setChallengeValues(na.generateChallangeValues(k,pair.getFirst()));
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
    }
    
   
	
    /**
     * Metodo que inicia o protocolo de descoberta de vizinhos
     */
	private void initHelloProtocol(){
		
		ActionMessage m =  new ActionMessage(ActionMessage.INITROUTINGPROTOCOL);
		sendMessage(m);
	}
	
	
    private CleanSlateNode getCleanSlateNode(Node n) {
    	return (CleanSlateNode) n;
    }
}