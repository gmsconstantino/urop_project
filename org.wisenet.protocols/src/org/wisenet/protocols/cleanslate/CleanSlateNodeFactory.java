package org.wisenet.protocols.cleanslate;

import org.wisenet.simulator.core.node.factories.AbstractNodeFactory;
import org.wisenet.simulator.core.node.layers.mac.Mica2MACLayer;


/**
 * 
 * @author Tiago Ara�jo
 *
 */
public class CleanSlateNodeFactory extends AbstractNodeFactory{
	
    public void setup() {
        setApplicationClass(CleanSlateApplication.class);
        setRoutingLayerClass(CleanSlateRoutingLayer.class);
        setNodeClass(CleanSlateNode.class);
        setMacLayer(Mica2MACLayer.class);
        setSetup(true);
    }
}