package org.wisenet.protocols.cleanslate;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.wisenet.protocols.common.Pair;


/**
 * 
 * @author Tiago Ara�jo
 *
 * Resultado de novos hops determinados 
 *
 */
public class NewHopsResult {
	
	private List<Vector<Pair<String,Short>>> newhops;
	private List<Short> maliciousNodesID;
	
	public NewHopsResult(List<Vector<Pair<String,Short>>> newhops, List<Short> maliciousNodesID){
		this.newhops = newhops;
		this.maliciousNodesID = maliciousNodesID;
	}
	
	public List<Vector<Pair<String,Short>>> getNewHops(){
		return newhops;
	}
	
	public List<Short> getMaliciousNodesID(){
		return maliciousNodesID;
	}
	
	public boolean isEmpty() {
		return newhops.isEmpty();
	}
	
	public int size(){
		
		int size = 0;
		Iterator<Vector<Pair<String,Short>>> it = newhops.iterator();
		while(it.hasNext()){
			Iterator<Pair<String,Short>> it2 = it.next().iterator();
			while(it2.hasNext()){
				Pair<String,Short> pair = it2.next();
				size += pair.getFirst().getBytes().length + 2;
			}	
		}
		return size;
	}
}