package org.wisenet.protocols.cleanslate;

import java.awt.Color;
import java.awt.Graphics;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.wisenet.protocols.MAC.tinysec.ConstantsTinySec;
import org.wisenet.protocols.common.Pair;
import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.node.Mica2SensorNode;
import org.wisenet.simulator.core.node.Node;
import org.wisenet.simulator.core.radio.RadioModel;
import org.wisenet.simulator.core.ui.ISimulationDisplay;
import org.wisenet.simulator.gui.IDisplayable;



/**
 * 
 * @author Tiago Ara�jo
 *
 */
public class CleanSlateNode extends Mica2SensorNode implements IDisplayable {
	
	//To Graphics
    private String showLabel = "";
    private boolean received = false;
	
    
    //Valores entregues pela NA:
    
    /** Network Authority (NA) oferece chave publica a cada n� */
    public PublicKey keyNaPub;
    /** Id assinado pela NA*/
	protected byte[] signatureID;
	private byte[] ivector;
	protected ChallengeValues challengev;
	
	public List<Node> parents; //Liga��es entre grupos
	public List<Node> parents2; //Liga��es entre vizinhos
	public List<Node> wormholes; //Liga��es de wormhole
    
	public CleanSlateNode(Simulator sim, RadioModel radioModel) {
	    super(sim, radioModel);
	    //setEnableFunctioningEnergyConsumption(false);
	    setRadius(2);
	    setPaintingPaths(false);
	    setBaseColor(Color.GREEN);
		this.parents = new LinkedList<Node>();
		this.parents2 = new LinkedList<Node>();
		this.wormholes = new LinkedList<Node>();
		this.challengev = new ChallengeValues();
	}
	 
	
	public byte[] getSignatureID(){
		return signatureID;
	}
	
	public void setSignatureID(byte[] sign){
		this.signatureID = sign;
	}
	
	public ChallengeValues getChallengeValues(){
		return challengev;
	}
	 
    @Override
    public void init(){
    	super.init();
    }
    
    @Override
    public synchronized  void  displayOn(ISimulationDisplay disp) {
        super.displayOn(disp);
        
        
        if(received){
        	Color c = Color.orange;
            getGraphicNode().setBackcolor(c);
            getGraphicNode().paint(disp);
        }

        Graphics g = disp.getGraphics();
        int _x = disp.x2ScreenX(this.getX());
        int _y = disp.y2ScreenY(this.getY());
        
        if (!showLabel.isEmpty()) {
            g.setColor(Color.BLACK);
            g.drawRect(_x, _y-10, 50, 20);
            g.setColor(Color.YELLOW);
            g.fillRect(_x, _y-10, 50, 20);
            g.setColor(Color.BLACK);
            g.drawString(showLabel, _x + 5, _y);
            showLabel="";
        }

    	Iterator<Node> wormholesit = wormholes.iterator();
		while(wormholesit.hasNext()){
			Node maliciousnode = wormholesit.next();
			g.setColor( Color.orange);
			int x1 = disp.x2ScreenX(maliciousnode.getX());
			int y1 = disp.y2ScreenY(maliciousnode.getY());
			g.drawLine(_x,_y,x1,y1);
		}
        
		Iterator<Node> it = parents.iterator();
		List<Short> ids = new LinkedList<Short>();
		while(it.hasNext()){
			Node parent = it.next();
			ids.add(parent.getId());
			g.setColor( Color.magenta);
			int x1 = disp.x2ScreenX(parent.getX());
			int y1 = disp.y2ScreenY(parent.getY());
			g.drawLine(_x,_y,x1,y1);
		}
    }
 

	public byte[] getIVector() {
		return this.ivector;
	}
		
	public void setIVector(byte[] ivector){
		this.ivector = ivector;
	}
	
	public void setKeyNaPub(PublicKey pub){
		this.keyNaPub = pub;
	}
	
	public void setChallengeValues(ChallengeValues cv){
		this.challengev = cv;
	}


	
	// ID, {ID}Kna-1
	public void setPairId(Pair<Short,byte[]> id){
		try {
			byte[] plainId = tinySecExt.decrypt(id.getSecond(), keyNaPub, 
					CleanSlateConstants.PropertiesCipher, ConstantsTinySec.PROVIDER, null);
			
			byte[] idbytes = new byte[1];
			idbytes[0] = id.getFirst().byteValue();
			
			if(MessageDigest.isEqual(idbytes, plainId)){
				getMyRoutingLayer().uniqueID = id.getFirst(); 
				Auxiliary.nodes.put(id.getFirst(), getMyRoutingLayer());
				this.signatureID = id.getSecond();
				getMyRoutingLayer().myGroupInfo = new GroupInfo(id.getFirst(), 1);
				Auxiliary.createWith1Group(getMyRoutingLayer().myGroupInfo.getGroupID(), 0);
			} 
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
    
    public void setReceived(boolean received){
    	this.received = received;
    }

    public String getShowLabel() {
        return showLabel;
    }

    public void setShowLabel(String data) {
        this.showLabel = data;
    }
	
    
    private CleanSlateRoutingLayer getMyRoutingLayer() {
    	return (CleanSlateRoutingLayer) this.getRoutingLayer();
    }


	@Override
	public short getUniqueID() {
		return getId();
	}
}