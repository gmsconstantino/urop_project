package org.wisenet.protocols.cleanslate;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Tiago Ara�jo
 *
 * Informa�ao de n�s maliciosos 
 */
public class MaliciousNodes {
	
	//Os ids e grupos q o proprio no descobre
	protected Set<Short> ids;
	private Set<GroupInfo> groups;
	
	public MaliciousNodes(){
		this.ids = new HashSet<Short>();
		this.groups = new HashSet<GroupInfo>();
	}
	
	public void addMaliciousNode(short id){
		this.ids.add(id);
		this.groups.add(null);
	}
	
	public void addMaliciousNode(short id, GroupInfo gi){
		this.ids.add(id);
		this.groups.add(gi);
	}
	
	public boolean contains(short nodeid){
		return ids.contains(nodeid);
	}
	
	public void clear() {
		this.ids.clear();
		this.groups.clear();
	}
	
	public Set<Short> getIds(){
		return ids;
	}
	
	public Set<GroupInfo> getGroups(){
		return groups;
	}
	
	public MaliciousNodes getMaliciousNodes(){
		return this;
	}
	
	public boolean isEmpty(){
		return ids.isEmpty() && groups.isEmpty();
	}
	
	/**
	 * ids + groups
	 * @return
	 */
	public int size(){
		int size = ids.size()*2;
		if(!groups.isEmpty())
			size += groups.iterator().next().size();
		
		return size;
	}
}