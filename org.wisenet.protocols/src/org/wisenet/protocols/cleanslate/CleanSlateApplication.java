package org.wisenet.protocols.cleanslate;

import java.io.Serializable;

import org.wisenet.protocols.cleanslate.messages.ActionMessage;
import org.wisenet.simulator.core.Application;
import org.wisenet.simulator.core.Message;

/**
 * 
 * @author Tiago Araœjo
 * 
 *  Camada aplicação Clean-Slate
 *
 */
public class CleanSlateApplication extends Application implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5865711361520263634L;

	 @Override
	 public void run() {
		//if(getNode().getRoutingLayer().isStable())
			this.sendDataMessage();
		//}
	 }
	    
	    
    protected void sendDataMessage(){
    	byte[] msgBytes = "ola".getBytes();
		ActionMessage msg = new ActionMessage(ActionMessage.SENDDATA,"sinknode",msgBytes);
		//msg.setSourceNodeId(getHostNode().getId());
		sendMessage(msg);
    }


	@Override
	protected void onMessageReceived(Object message) {
		 Message m = (Message) message;
	     String str = new String(m.getPayload());
	     ((CleanSlateNode) getNode()).setShowLabel(str);
	}
}