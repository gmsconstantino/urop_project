package org.wisenet.protocols.cleanslate.messages;

import org.wisenet.protocols.cleanslate.GroupInfo;


/**
 * 
 * @author Tiago Ara�jo
 *
 * Esta mensagem � uma resposta � proposta,
 * e indica que foi aceite a proposta para mergir os grupos.
 */
public class AcceptedMessage extends ReliableMessage {
	
	private GroupInfo sourceGroup;
	private String sinknodeaddress;

	
	public AcceptedMessage(GroupInfo destinationGroup, GroupInfo sourceGroup, short source, 
			short destination, long randomValue, String sinknodeaddress){
		super(source,destination, destinationGroup,randomValue);
		super.setType(MessageTypes.ACCEPTEDMESSAGE);
		this.sourceGroup = sourceGroup;
		this.sinknodeaddress = sinknodeaddress;
	}
	
	public AcceptedMessage(int type, GroupInfo destinationGroup, GroupInfo sourceGroup, short source, 
			short destination, long randomValue, String sinknodeaddress){
		super(source,destination, destinationGroup,randomValue);
		super.setType(MessageTypes.ACCEPTEDMESSAGE);
		this.sourceGroup = sourceGroup;
		this.sinknodeaddress = sinknodeaddress;
	}
	
	
	public GroupInfo getSourceGroup(){
		return this.sourceGroup;
	}
	
	public String getSinkNodeAddress(){
		return sinknodeaddress;
	}
	
	
	/**
	 * sourceGroup + sinknodeaddress + super
	 */
	@Override
	public int size() {
		int size = sourceGroup.size() + sinknodeaddress.getBytes().length;
		return size + super.size();
	}
}