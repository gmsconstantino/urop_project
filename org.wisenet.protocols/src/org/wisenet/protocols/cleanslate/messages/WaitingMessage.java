package org.wisenet.protocols.cleanslate.messages;

import java.util.Set;

import org.wisenet.protocols.cleanslate.GroupInfo;

/**
 * 
 * @author Tiago Ara�jo
 *
 * Mensagem de resposta � proposta. 
 * Indica que o grupo que faz a proposta deve esperar e voltar a tentar.
 */
public class WaitingMessage extends ReliableMessage {
	
	private GroupInfo sourceGroup;
	private Set<GroupInfo> removeGroups;
	
	
	public WaitingMessage(GroupInfo sourceGroup, GroupInfo destinationGroup, short sourceID, short destinationID, 
			long randomValue, Set<GroupInfo> removeGroups){
		super(sourceID,destinationID,destinationGroup, randomValue);
		super.setType(MessageTypes.WAITINGMESSAGE);
		this.sourceGroup = sourceGroup;
		this.removeGroups = removeGroups;
	}
	
	
	public GroupInfo getSourceGroup(){
		return sourceGroup;
	}
	
	public Set<GroupInfo> getRemoveGroups(){
		return removeGroups;
	}
	
	/**
	 * sourceGroup + removeGroups + super
	 */
	@Override
	public int size(){
		
		int size = sourceGroup.getSizeGroup();
		
		if(!removeGroups.isEmpty())
			size += removeGroups.iterator().next().size()*removeGroups.size();
		
		return size + super.size();
	}
}