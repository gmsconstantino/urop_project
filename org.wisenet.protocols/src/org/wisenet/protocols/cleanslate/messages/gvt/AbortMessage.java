package org.wisenet.protocols.cleanslate.messages.gvt;

import org.wisenet.protocols.cleanslate.GroupInfo;
import org.wisenet.protocols.cleanslate.messages.MessageTypes;
import org.wisenet.protocols.cleanslate.messages.ReliableMessage;


/**
 * 
 * @author Tiago Ara�jo
 *
 *  
 */
public class AbortMessage extends ReliableMessage {
	
	
	private long removeDuplValue;
	
	public AbortMessage(GroupInfo destinationGroup, String prefix, short sourceID, short destinationID, long removeDuplValue, long randomValue){
		super(sourceID,destinationID,destinationGroup, randomValue);
		super.setType(MessageTypes.ABORTMESSAGE);
		super.setPrefix(prefix);
		this.removeDuplValue = removeDuplValue;
	}

	
	
	public long getRemoveDuplValue(){
		return removeDuplValue;
	}
	
	
	
	/**
	 * removeDuplValue + super
	 */
	@Override
	public int size(){
				
		return 8 + super.size();
	}
}