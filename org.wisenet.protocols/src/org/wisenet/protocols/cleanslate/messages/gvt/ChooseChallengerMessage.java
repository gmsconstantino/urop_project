package org.wisenet.protocols.cleanslate.messages.gvt;

import java.util.Random;

import org.wisenet.protocols.cleanslate.Auxiliary;
import org.wisenet.protocols.cleanslate.GroupInfo;
import org.wisenet.protocols.cleanslate.messages.MessageTypes;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem que � enviada para o pr�prio grupo, de forma a escolher um n�
 * do grupo que seja o respons�vel por fazer o pedido de desafio.
 *
 */
public class ChooseChallengerMessage extends ChooseMessage{

	private short myedgenodeID;
	private short otheredgenodeID;
	
	private long auxnumber; //nao conta para a energia
	
	
	public ChooseChallengerMessage(String prefix, short sourceID, short destinationID, long removeDuplValue, long randomValue,
			GroupInfo sourceGroup, short myedgenodeID, short otheredgenodeID, long mygroupID){
		super(prefix,sourceID,destinationID,removeDuplValue,randomValue,sourceGroup);
		super.setType(MessageTypes.CHOOSECHALLENGERMESSAGE);
		this.myedgenodeID = myedgenodeID;
		this.otheredgenodeID = otheredgenodeID;
		this.auxnumber = new Random().nextLong();
		Auxiliary.addGvtMessage(mygroupID, auxnumber);
	}
	
	public short getMyEdgeNodeID(){
		return myedgenodeID;
	}
	
	public short getOtherEdgeNodeID(){
		return otheredgenodeID;
	}
	
	public long getAuxNumber(){
		return auxnumber;
	}
	
	
	/**
	 * 	myedgenodeID + otheredgenodeID + super
	 */
	@Override
	public int size(){
		return 2 + 2 + super.size();
	}
}