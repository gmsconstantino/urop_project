package org.wisenet.protocols.cleanslate.messages.gvt;

import org.wisenet.protocols.cleanslate.GroupInfo;
import org.wisenet.protocols.cleanslate.messages.MessageTypes;



/**
 * 
 * @author Tiago Ara�jo
 *
 * Depois de ser escolhido o n� que realiza o pedido de desafio,
 * o pr�prio grupo dever� saber qual foi o n� escolhido e o desafio.
 * Ent�o esta mensagem � enviada para o pr�prio grupo para anunciar
 * essa informa��o.
 * 
 */
public class BroadcastChallengeMessage extends ChooseChallengerMessage {
	
	
	//PARA A GVT
	private int c0;
	private int ci;
	private int i;
	private byte[] signature;
	private short challengerID;
	
	
	public BroadcastChallengeMessage(String prefix, short sourceID, short destinationID, 
			long removeDuplValue, long randomValue,int c0,  int ci, int i, 
			short challengerID, byte[] signature, GroupInfo sourceGroup, short myedgenodeID, short otheredgenodeID, long mygroupID){
		
		super(prefix,sourceID,destinationID,removeDuplValue,randomValue,sourceGroup,myedgenodeID,otheredgenodeID,mygroupID);
		super.setType(MessageTypes.BROADCASTCHALLENGEMESSAGE);
		this.c0 = c0;
		this.ci = ci;
		this.i = i;
		this.challengerID = challengerID;
		this.signature = signature;
	}
	
	public int getC0(){
		return c0;
	}
	
	public int getCi(){
		return ci;
	}
	
	public int getI(){
		return i;
	}
	
	public short getChallengerID(){
		return challengerID;
	}
	
	public byte[] getSignature(){
		return signature;
	}
	
	
	/**
	 * 	c0 + ci + i + signature + challengerID
	 */
	@Override
	public int size() {
		return 4 + 4 + 4 + signature.length + 2 + super.size();
	}
}