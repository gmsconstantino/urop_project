package org.wisenet.protocols.cleanslate.messages.gvt;

import org.wisenet.protocols.cleanslate.GroupInfo;
import org.wisenet.protocols.cleanslate.messages.MessageTypes;
import org.wisenet.protocols.cleanslate.messages.ReliableMessage;


/**
 * 
 * @author Tiago Ara�jo
 *
 * Mensagem que faz o pedido de desafio ao outro grupo.
 */
public class ChallengeRequestMessage extends ReliableMessage{
	
	
	
	private int ci;
	private GroupInfo sourceGroup;
	
	public ChallengeRequestMessage(short sourceID, short destinationID, GroupInfo sourceGroup, int ci, long randomValue){
		super(sourceID, destinationID, randomValue);
		super.setType(MessageTypes.CHALLENGEREQUESTMESSAGE);
		this.ci = ci;
		this.sourceGroup = sourceGroup;
	}
	
	
	public int getCi(){
		return ci;
	}
	
	public GroupInfo getSourceGroup(){
		return sourceGroup;
	}
	
	
	/**
	 * sourceGroup + sinknodeaddress + super
	 */
	@Override
	public int size() {
		return 4 + sourceGroup.size() + super.size();
	}
}