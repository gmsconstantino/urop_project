package org.wisenet.protocols.cleanslate.messages.gvt;

import org.wisenet.protocols.cleanslate.GroupInfo;
import org.wisenet.protocols.cleanslate.messages.MessageTypes;
import org.wisenet.protocols.cleanslate.messages.ReliableMessage;


/**
 * 
 * @author Tiago Ara�jo
 *
 * As classes ChooseResponderMessage e ChooseChallengerMessage s�o uma extens�o desta classe 
 */
public class ChooseMessage extends ReliableMessage {
	

	private long removeDuplValue;
	private GroupInfo otherGroup;
	
	public ChooseMessage(String prefix, short sourceID, short destinationID, long removeDuplValue, long randomValue, GroupInfo otherGroup){
		super(sourceID,destinationID,randomValue);
		super.setPrefix(prefix);
		super.setType(MessageTypes.CHOOSEMESSAGE);
		this.removeDuplValue = removeDuplValue;
		this.otherGroup = otherGroup;		
	}

	
	public long getRemoveDuplValue(){
		return removeDuplValue;
	}
	
	public GroupInfo getOtherGroup(){
		return otherGroup;
	}
	
	/**
	 * removeDuplValue + otherGroup + super
	 */
	@Override
	public int size(){
		
		int size = 8 + otherGroup.size();
		return size + super.size();
	}
}