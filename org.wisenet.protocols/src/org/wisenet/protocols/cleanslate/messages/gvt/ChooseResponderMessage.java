package org.wisenet.protocols.cleanslate.messages.gvt;

import java.util.Random;

import org.wisenet.protocols.cleanslate.Auxiliary;
import org.wisenet.protocols.cleanslate.GroupInfo;
import org.wisenet.protocols.cleanslate.messages.MessageTypes;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem enviada para o grupo de forma a seleccionar o responder,
 * ou seja, o n� que ir� responder ao pedido de desafio.
 *
 */
public class ChooseResponderMessage extends ChooseMessage{

	private int ci;
	private String myedgenodeaddress;
	
	private long auxnumber; //(nao conta para a energia)
	
	public ChooseResponderMessage(String prefix, short sourceID, short destinationID, 
			long removeDuplValue, long randomValue, GroupInfo othergroup, int ci, String myedgenodeaddress, long mygroupID){
		
		super(prefix,sourceID,destinationID,removeDuplValue,randomValue,othergroup);
		super.setType(MessageTypes.CHOOSERESPONDERMESSAGE);
		this.ci = ci;
		this.myedgenodeaddress = myedgenodeaddress;
		this.auxnumber = new Random().nextLong();
		Auxiliary.addGvtMessage(mygroupID, auxnumber);
	}

	
	
	public int getCi(){
		return ci;
	}
	
	public String getMyEdgeNodeAddress(){
		return myedgenodeaddress;
	}
	
	public long getAuxNumber(){
		return auxnumber;
	}
	
	
	
	/**
	 * ci + myedgenodeaddress + super
	 */
	@Override
	public int size() {
		return 2 + myedgenodeaddress.getBytes().length + super.size();
	}
}