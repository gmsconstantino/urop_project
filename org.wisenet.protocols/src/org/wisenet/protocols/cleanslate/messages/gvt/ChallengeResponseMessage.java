package org.wisenet.protocols.cleanslate.messages.gvt;

import java.util.List;

import org.wisenet.protocols.cleanslate.GroupInfo;
import org.wisenet.protocols.cleanslate.messages.MessageTypes;
import org.wisenet.protocols.cleanslate.messages.ReliableMessage;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem com a resposta ao pedido de desafio que � enviada
 * para o outro grupo.
 *
 */
public class ChallengeResponseMessage extends ReliableMessage {
	
	private short responderID;
	private byte[] signatureRespID;
	private List<GroupInfo> mergeTable;
	private GroupInfo responderGroup;
	
	//Envia para o Grupo G
	//Signature � a assinatura do id do n� responser
	public ChallengeResponseMessage(short sourceID, short dest, GroupInfo responderGroup, short responderID, byte[] signatureRespID, List<GroupInfo> mergeTable, long randValue){
		super(sourceID,dest,randValue);
		super.setType(MessageTypes.CHALLENGERESPONSEMESSAGE);
		this.responderID = responderID;
		this.signatureRespID = signatureRespID;
		this.mergeTable = mergeTable;
		this.responderGroup = responderGroup;
	}
	
	public short getResponderID(){
		return responderID;
	}
	
	public byte[] getSignatureResponserID(){
		return signatureRespID;
	}
	
	public List<GroupInfo> getMergeTable(){
		return mergeTable;
	}
	
	public GroupInfo getResponderGroup(){
		return responderGroup; 
	}
}