package org.wisenet.protocols.cleanslate.messages.gvt;

import org.wisenet.protocols.cleanslate.messages.MessageTypes;
import org.wisenet.protocols.cleanslate.messages.ReliableMessage;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem que indica a decis�o final, visto que o processo da GVT
 * decorre simultaneamente, ent�o no final ambos os grupos t�m que saber
 * se correu tudo bem.
 *
 */
public class FinalResponseMessage extends ReliableMessage{
	
	public static int ACCEPT = 1;
	public static int ABORT = 2;
	
	private int type;
	private long sourceGroupID;
	
	public FinalResponseMessage(short sourceID, short destinationID, long randomValue, int type, long sourcegroupID){
		super(sourceID,destinationID,randomValue);
		super.setType(MessageTypes.FINALRESPONSEMESSAGE);
		this.type = type;
		this.sourceGroupID = sourcegroupID;
	}


	public int getFinalResponseType(){
		return this.type;
	}
	
	public long getSourceGroupID(){
		return sourceGroupID;
	}
	
	
	/**
	 * type + sourceGroupID + super
	 */
	@Override
	public int size() {
		return 4 + 8 + super.size();
	}	
}