package org.wisenet.protocols.cleanslate.messages.gvt;

import java.util.List;
import java.util.Random;

import org.wisenet.protocols.cleanslate.Auxiliary;
import org.wisenet.protocols.cleanslate.GroupInfo;
import org.wisenet.protocols.cleanslate.messages.MessageTypes;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem que contem a resposta e � enviada at� ao edge node que recebeu o pedido de desafio.
 *
 */
public class ResponseToEdgeNodeMessage extends ChallengeResponseMessage  {
	
	private String destinationAddress;
	
	private long auxnumber; //(nao contabilizado para a energia)!!
	
	public ResponseToEdgeNodeMessage(String destinationAddress, short sourceID, short nextHop, GroupInfo responderGroup, 
			short responderID, byte[] signatureResponder, List<GroupInfo> mergeTable, long randomValue, long mygroupID){
		super(sourceID, nextHop, responderGroup, responderID, signatureResponder, mergeTable, randomValue);
		super.setType(MessageTypes.RESPONSETOEDGENODEMESSAGE);
		this.destinationAddress = destinationAddress;
		this.auxnumber = new Random().nextLong();
		Auxiliary.addGvtMessage(mygroupID, auxnumber);
	}
	
	public String getDestinationAddress(){
		return destinationAddress;
	}
	
	public long getAuxNumber(){
		return auxnumber;
	}
	
	
	/**
	 * destinationAddress + super
	 */
	@Override
	public int size() {
		return destinationAddress.getBytes().length + super.size();
	}
}