package org.wisenet.protocols.cleanslate.messages;

import java.util.Set;

import org.wisenet.protocols.cleanslate.GroupInfo;

/**
 * 
 * @author Tiago Ara�jo
 * 
 * Esta mensagem � uma resposta � proposta.
 * A mensagem indica que a proposta foi rejeitada.
 *
 */
public class RejectedMessage extends ReliableMessage {
	
	private GroupInfo sourceGroup;
	private Set<GroupInfo> oldGroups;
	private Set<GroupInfo> removeGroups;
	private long removeDuplValue;
	private GroupInfo rejectedGroup;

	
	public RejectedMessage(GroupInfo sourceGroup, GroupInfo destinationGroup, short sourceID, short destinationID , 
			Set<GroupInfo> removeGroups, Set<GroupInfo> oldgroups, long randomValue, long removeDuplValue){
		super(sourceID,destinationID,destinationGroup,randomValue);
		super.setType(MessageTypes.REJECTEDMESSAGE);
		this.sourceGroup = sourceGroup;
		this.removeGroups = removeGroups;
		this.oldGroups = oldgroups;
		this.removeDuplValue = removeDuplValue;
		this.rejectedGroup = null;
	}
	
	public RejectedMessage(GroupInfo sourceGroup, GroupInfo destinationGroup, short sourceID, 
			short destinationID , Set<GroupInfo> removeGroups, Set<GroupInfo> oldgroups, 
			long randomValue, long removeDuplValue,GroupInfo rejectedGroup){
		super(sourceID,destinationID,destinationGroup,randomValue);
		super.setType(MessageTypes.REJECTEDMESSAGE);
		this.sourceGroup = sourceGroup;
		this.removeGroups = removeGroups;
		this.oldGroups = oldgroups;
		this.removeDuplValue = removeDuplValue;
		this.rejectedGroup = rejectedGroup;
	}
	
	public GroupInfo getRejectedGroup(){
		return rejectedGroup;
	}
	
	public boolean hasRejectedGroup(){
		return rejectedGroup!=null;
	}
	
	public GroupInfo getSourceGroup(){
		return sourceGroup;
	}

	
	public Set<GroupInfo> getRemoveGroups(){
		return removeGroups;
	}
	
	public Set<GroupInfo> getOldGroups(){
		return oldGroups;
	}
	
	public long getRemoveDuplValue(){
		return removeDuplValue;
	}
	
	
	/**
	 * 	sourceGroup + oldGroups + removeGroups + removeDuplValue + rejectedGroup;
	 */
	@Override
	public int size(){
		int size = sourceGroup.size() + 8;
		
		if(!oldGroups.isEmpty())
			size += oldGroups.iterator().next().size()*oldGroups.size();
		if(!removeGroups.isEmpty())
			size += removeGroups.iterator().next().size()*removeGroups.size();
		
		if(rejectedGroup != null)
			size += rejectedGroup.size();
		
		return size + super.size();
	}
}