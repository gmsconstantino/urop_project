package org.wisenet.protocols.cleanslate.messages;

import org.wisenet.protocols.cleanslate.GroupInfo;


/**
 * 
 * @author Tiago Ara�jo
 *
 * Mensagem fi�vel
 */
public class ReliableMessage extends CleanSlateMessage{
	
	private Number destination;
	private GroupInfo destinationGroup;
	private long randomValue;
	private String prefix;
	
	
	public ReliableMessage(short source, Number destination, GroupInfo destinationGroup, long randomValue){
		super(MessageTypes.RELIABLEMESSAGE);
		super.setSourceId(source);
		this.destination = destination;
		this.destinationGroup = destinationGroup;
		this.randomValue = randomValue;
	}
	
	
	public ReliableMessage(short source, Number destination, long randomValue){
		super(MessageTypes.RELIABLEMESSAGE);
		super.setSourceId(source);
		this.destination = destination;
		this.destinationGroup = null;
		this.randomValue = randomValue;
	}
	
	
	public void setPrefix(String prefix){
		this.prefix = prefix;
	}
	
	public String getPrefix(){
		return prefix;
	}
	
	public Number getDestination(){
		return destination;
	}
	
	public void setDestination(short dest){
		this.destination = dest;
	}
	
	
	public long getRandomValue(){
		return randomValue;
	}
	
	public GroupInfo getDestinationGroupInfo(){
		return destinationGroup;
	}
	
	
	/**
	 * destination + destinationGroup + randomValue + prefix + super
	 */
	@Override
	public int size(){
		
		int size = 0;
		if(destination.getClass().equals(Integer.class))
			size += 4;
		else if(destination.getClass().equals(Long.class))
			size+=8;
		if(destinationGroup!=null)
			size += destinationGroup.size();
		
		size += 8;
		if(prefix!=null)
			size += prefix.getBytes().length;
		
		return size + super.size();
	}
}