package org.wisenet.protocols.cleanslate.messages;

import java.util.Random;
import java.util.Set;

import org.wisenet.protocols.cleanslate.Auxiliary;
import org.wisenet.protocols.cleanslate.GroupInfo;


/**
 * 
 * @author Tiago Ara�jo
 *
 *	Esta mensagem ocorre ap�s uma rejei�ao ou aceita�ao de uma proposta,
 *	provocando a actualiza�ao consistente do pr�prio grupo. 
 *	Os n�s constituintes do grupo passam a conhecer todos os grupos 
 *	que circundam o seu grupo.
 */
public class UpdateGroupMessage extends ReliableMessage  {
	
	private Set<GroupInfo> directedNeighbors;
	private GroupInfo destinationGroup;
	private GroupInfo group; //So para informa�ao dos vizinhos
	private Set<GroupInfo> removeGroups;
	private long auxnumber; //� auxiliar (nao conta para a energia)
	
	public UpdateGroupMessage(short sourceID, short destinationID, GroupInfo destGroup, String prefix, 
			Set<GroupInfo> directedNeighbors, GroupInfo group, Set<GroupInfo> removeGroups, long randomValue){
		super(sourceID, destinationID,randomValue);
		super.setType(MessageTypes.UPDATEGROUPMESSAGE);
		super.setPrefix(prefix);
		this.destinationGroup = destGroup;
		this.directedNeighbors = directedNeighbors;
		this.group = group;
		this.removeGroups = removeGroups;
		this.auxnumber = new Random().nextLong();

		try{
			Auxiliary.addUpdateMessage(group.getGroupID(), auxnumber);
		} catch(NullPointerException e){
			System.err.println("ENVIA PARA " + destinationID + " " + sourceID);
		}
	}
	
	
	public Set<GroupInfo> getDirectedNeighbors(){
		return directedNeighbors;
	}
	
	public GroupInfo getGroup(){
		return group;
	}
	
	public GroupInfo getDestinationGroup(){
		return destinationGroup;
	}
	
	public Set<GroupInfo> getRemoveGroups(){
		return removeGroups;
	}
	
	public long getAuxNumber(){
		return auxnumber;
	}
	
	
	/**
	 * directedNeighbors + destinationGroup + group + removeGroups + auxnumber + super
	 */
	@Override
	public int size(){
		
		int size = 0;
		if(!directedNeighbors.isEmpty())
			size += directedNeighbors.iterator().next().size()*directedNeighbors.size();
		size += destinationGroup.size() + group.size();
		
		if(!removeGroups.isEmpty())
			size += removeGroups.iterator().next().size()*removeGroups.size();
		
		return size + super.size();
	}
}