package org.wisenet.protocols.cleanslate.messages;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;


public class ReplaceRequestMessage extends CleanSlateMessage {
	
	//malicious node
	private List<Short> maliciousNodesID;
	private List<Vector<String>> addresses;
	
	
	public ReplaceRequestMessage(short source , List<Short> maliciousNodesID, List<Vector<String>> addresses){
		super(MessageTypes.REPLACEREQUESTMESSAGE);
		super.setSourceId(source);
		this.maliciousNodesID = maliciousNodesID;
		this.addresses = addresses;
	}
	
	
	
	//Malicious node id
	
	public List<Short> getMaliciousNodesID(){
		return maliciousNodesID;
	}
	
	public List<Vector<String>> getAddresses(){
		return addresses;
	}
	
	
	public void setAddressesMalicious(List<Vector<String>> addresses){
		this.addresses = addresses;
	}
	
	public void setMaliciousNodesID(List<Short> nodesID){
		this.maliciousNodesID = nodesID;
	}
	
	
	/**
	 * maliciousNodesID + addresses + super
	 */
	@Override
	public int size(){
		int size = maliciousNodesID.size()*2; 
		Iterator<Vector<String>> it1 = addresses.iterator();
		while(it1.hasNext()){
			Iterator<String> it2 = it1.next().iterator();
			while(it2.hasNext())
				size += it2.next().getBytes().length;
		}
		
		return size + super.size();
	}	
}