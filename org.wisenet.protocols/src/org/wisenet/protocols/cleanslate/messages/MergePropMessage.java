package org.wisenet.protocols.cleanslate.messages;

import org.wisenet.protocols.cleanslate.GroupInfo;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem � enviada periodicamente, representando uma proposta de um grupo para outro. 
 * Esta mensagem � enviada at� ser obtida uma resposta.
 *
 */
public class MergePropMessage extends ReliableMessage{
	
	
	/** Informa��o do grupo que enviou a proposta */
	private GroupInfo groupOrigin;
	private String sinknodeaddress;

	
	public MergePropMessage(GroupInfo groupDest, GroupInfo groupOrigin, short sourceID, long randomValue){
		super(sourceID,groupDest.getGroupID(),groupDest,randomValue);
		super.setType(MessageTypes.MERGEPROPMESSAGE);
		this.groupOrigin = groupOrigin;
		this.sinknodeaddress = "";
	}
	
	public MergePropMessage(GroupInfo groupDest, GroupInfo groupOrigin, short sourceID, long randomValue, String sinknodeaddress){
		super(sourceID,groupDest.getGroupID(),groupDest,randomValue);
		super.setType(MessageTypes.MERGEPROPMESSAGE);
		this.groupOrigin = groupOrigin;
		this.sinknodeaddress = sinknodeaddress;
	}
	
	
	public GroupInfo getOriginGroupInfo(){
		return this.groupOrigin;
	}
	
	
	public String getSinkNodeAddress(){
		return sinknodeaddress;
	}
	
	
	/**
	 * 	groupOrigin + sinknodeaddress + super
	 */
	@Override
	public int size(){
		int size = groupOrigin.size() + sinknodeaddress.getBytes().length;
		return size + super.size();
	}
}