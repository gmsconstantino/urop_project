package org.wisenet.protocols.cleanslate.messages;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem de Ack para o "AckInvitationMessage".
 *
 */
public class AckInvitation2Message  extends CleanSlateMessage{
	
	private byte[] signature;
	
	public AckInvitation2Message(short sourceID, short destID,  byte[] signature){
		super(MessageTypes.ACKINVITATION2MESSAGE);
		super.setDestinationId(destID);
		super.setSourceId(sourceID);
		this.signature = signature;
	}
	
	public byte[] getSignature(){
		return signature;
	}
	
	/**
	 * signature + super
	 */
    @Override
    public int size() {
    	return signature.length + super.size();
    }
}