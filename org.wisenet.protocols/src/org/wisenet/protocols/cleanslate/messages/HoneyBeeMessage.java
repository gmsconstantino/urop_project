package org.wisenet.protocols.cleanslate.messages;

import org.wisenet.protocols.cleanslate.MaliciousNodes;


/**
 * 
 * @author Tiago Ara�jo
 *
 * Mensagem que indica um n� malicioso detectado bem como o n� que anunciou essa informa�ao
 */
public class HoneyBeeMessage extends CleanSlateMessage {
	
	private short initiatiornodeID;
	private byte[] signature;
	
	private MaliciousNodes maliciousnode;
	
	private long randomValue;
	
	public HoneyBeeMessage(short initiatiornodeID, byte[] signature, MaliciousNodes maliciousNodes, long randomValue){
		super(MessageTypes.HONEYBEEMESSAGE);
		this.initiatiornodeID = initiatiornodeID;
		this.signature = signature;
		this.maliciousnode = maliciousNodes;
		this.randomValue = randomValue;
	}
	
	public short getInitiatiorNodeID(){
		return initiatiornodeID;
	}
	
	public byte[] getSignature(){
		return signature;
	}
	
	
	public MaliciousNodes getMaliciousNodes(){
		return maliciousnode;
	}

	
	public long getRandomValue(){
		return randomValue;
	}
	
	
	/**
	 * initiatiornodeID + signature + maliciousnode + randomValue; + super
	 */
	@Override
    public int size() {
		int size = 2 + signature.length + maliciousnode.size() + 8;
    	return size + super.size();
    }
}