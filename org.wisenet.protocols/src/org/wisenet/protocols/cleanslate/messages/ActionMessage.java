package org.wisenet.protocols.cleanslate.messages;

/**
 * 
 * @author Tiago Ara�jo
 *
 * Define a ac��o a executar: 
 * 1.Iniciar a fase de configura��o do protocolo
 * 2.Enviar mensagem para ser encaminhada
 */
public class ActionMessage {
	
	
	public static int INITROUTINGPROTOCOL = 1;
	public static int SENDDATA = 2;
	
	private int msgType;
	private byte[] data;
	private String dst;
	
	/**
	 * 
	 * @param source
	 * @param dest
	 * @param data
	 */
	public ActionMessage(int msgType, String dst, byte[] data){
		this.data = data;
		this.msgType = msgType;
		this.dst = dst;
	}
	
	public ActionMessage(int msgType){
		this.msgType = msgType;
		this.data = null;
		this.dst = "";
	}
	
	public int getType(){
		return msgType;
	}
	
	public String getDestination(){
		return dst;
	}
	
	public byte[] getData(){
		return data;
	}
}