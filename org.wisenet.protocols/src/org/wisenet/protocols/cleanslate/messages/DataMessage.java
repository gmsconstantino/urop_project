package org.wisenet.protocols.cleanslate.messages;

import java.io.IOException;

import org.wisenet.protocols.common.ByteArrayDataInputStream;
import org.wisenet.protocols.common.ByteArrayDataOutputStream;
import org.wisenet.simulator.core.Message;


/**
 * 
 * @author Tiago Ara�jo
 *
 * Esta mensagem � uma mensagem de dados.
 */
public class DataMessage extends CleanSlateMessage{
	
	private String destination;
	private String direction;
	private short nextHop;
	private short forwardBy;
	private boolean security = true;
	
	public DataMessage(String destination, short nextHop, byte[] data, String direction){
		super(MessageTypes.DATAMESSAGE,data);
		this.destination = destination;
		this.nextHop = nextHop;
		this.direction = direction;
	}
	
	
	public String getDestination(){
		return this.destination;
	}
	
	
	public short getNextHop(){
		return nextHop;
	}
	
	public void setNextHop(short nexthop){
		this.nextHop = nexthop;
	}
	
	
	public String getDirection(){
		return direction;
	}
	
	public void setDirection(String direction){
		this.direction = direction;
	}
	
	
	/**
	 * 
	 * @return
	 * @throws CloneNotSupportedException
	 */
	public DataMessage copyMessage() throws CloneNotSupportedException {
		
		DataMessage clone = (DataMessage)(super.clone());
		return clone;
	}
	 
	 
	 /**
	 * destination + 2 + direction + super
	 */
	@Override
    public int size() {
		int size = destination.getBytes().length + 2 + direction.getBytes().length;
    	return size + super.size();
    }
	
	
	
    /**
     * Isto � necessario para o caso de ser utilizado um pacote TinySec
     * ou utilizando uma mensagem normal. No caso de ser utilizado um pacote TinySec
     * � preciso tornar quase (excepto headers) toda a mensagem num array de bytes.
     * 
     * Cria o payload adequado para o pacote TinySec
     * Payload: neighborsForwarders.size + neighborsForwarders + originForwarder + randomValue + data
     * Os restantes dados fazem parte do cabe�alho do pacote tinySec
     * @return
     */
    @Override
    public byte[] getPayload() {
    	if(security){
	        try {
	            ByteArrayDataOutputStream bados = new ByteArrayDataOutputStream();
	            bados.writeUTF(destination);
	            bados.writeShort(nextHop);
	            bados.writeUTF(direction);
	            bados.write(super.getPayload());
	            bados.close();
	            return bados.toByteArray();
	        } catch (IOException ex) {
	            System.err.println("ERRO");
	        }
    	}else
    		return super.getPayload();
    		
    	return null;
    }
    
    
    
    
    // AUXILIAR - M�TODO ESTATICO
    
    /**
     * Constroi a DataMessage a partir do payload de uma BaseMessage
     * Deve incluir a parte auxiliar
     */
    public static DataMessage buildMessage(Message bm){
    	try {
	    	//short source = bm.getSourceNodeId();
	    	short forwardBy = ((DataMessage) bm).getForwardBy();
	    	//byte type = bm.getType();
	    	//short dst = bm.getDestinationNodeId();
	    	
	    	ByteArrayDataInputStream badis = new ByteArrayDataInputStream(bm.getPayload());
	    	
	    	String destination = badis.readUTF();
	    	short nexthop = badis.readShort();
	    	String direction = badis.readUTF();
	    	
	    	int r = badis.available();
	    	byte[] data = new byte[r];
	    	badis.read(data);
	    	
	    	DataMessage dm = new DataMessage(destination,nexthop,data,direction);
	    	
	    	dm.setForwardBy(forwardBy);
	    	dm.setUniqueId(bm.getMessageNumber());
	    	
	    	return dm;
	    	
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return null;
    }


	public short getForwardBy() {
		return forwardBy;
	}

	public void setForwardBy(short forwardBy) {
		this.forwardBy = forwardBy;
	}
	
	public void packetSecurity(boolean sec){
		this.security = sec;
	}
}