package org.wisenet.protocols.cleanslate.messages;

import java.util.List;

import org.wisenet.protocols.cleanslate.GroupInfo;


/**
 * 
 * @author Tiago Ara�jo
 *
 *	Mensagem com o objectivo de fazer merge,
 *	os n�s que recebem esta mensagem sabem imediatamente
 *	que ocorre um merge entre os dois grupos. 
 *
 */
public class DoMergeMessage extends ReliableMessage{
	
	private GroupInfo myoldgroup;
	private GroupInfo othergroup;
	private String sinknodeaddress;
	
	//GVT
	private short responderID;
	private byte[] signatureResponderID;
	private List<GroupInfo> mergeTable;
	
	public DoMergeMessage(GroupInfo myoldgroup, GroupInfo othergroup, String prefix, short nextHopID, 
			short sourceID, long randomValue, String sinknodeaddress, short responderID, byte[] signatureResponderID, List<GroupInfo> mergeTable){
		super(sourceID,nextHopID,randomValue);
		super.setType(MessageTypes.DOMERGEMESSAGE);
		super.setPrefix(prefix);
		this.myoldgroup = myoldgroup;
		this.othergroup = othergroup;
		this.sinknodeaddress = sinknodeaddress;
		this.responderID = responderID;
		this.signatureResponderID = signatureResponderID;
		this.mergeTable = mergeTable;
		
	}
	

	public GroupInfo getMyOldGroup() {
		return myoldgroup;
	}

	public GroupInfo getOtherGroup() {
		return othergroup;
	}
	
	public String getSinkNodeAddress(){
		return sinknodeaddress;
	}
	
	public short getResponderID(){
		return responderID;
	}
	
	public byte[] getResponderSignature(){
		return signatureResponderID;
	}
	
	public List<GroupInfo> getMergeTable(){
		return mergeTable;
	}
	
	/**
	 * 	myoldgroup + othergroup + sinknodeaddress + responderID + signatureResponderID + mergeTable + super
	 */
	@Override
	public int size(){
		
		int size = myoldgroup.size() + othergroup.size() + sinknodeaddress.getBytes().length
		+ 2 + signatureResponderID.length;
		
		if(!mergeTable.isEmpty())
			size += mergeTable.get(0).size()*mergeTable.size();
		
		return size + super.size();
	}
}