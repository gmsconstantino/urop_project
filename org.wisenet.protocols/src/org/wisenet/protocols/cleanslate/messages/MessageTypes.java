package org.wisenet.protocols.cleanslate.messages;

/**
 * 
 * @author Tiago Ara�jo
 *
 * Tipos de mensagens
 */
public class MessageTypes {
	
	
	//FASE HELLO
	public static byte HELLOMESSAGE = 1; 
	public static byte ACKINVITATION2MESSAGE = 2; 
	public static byte ACKINVITATIONMESSAGE = 3; 
	
	public static byte ACCEPTED2MESSAGE = 4; 
	public static byte ACCEPTEDMESSAGE = 5; 
	public static byte ACKMESSAGE = 6; 
	public static byte DATAMESSAGE = 7; 
	public static byte DOMERGEMESSAGE = 8; 
	public static byte MERGEPROPMESSAGE = 9; 
	public static byte NOMERGEMESSAGE = 10; 
	public static byte REJECTEDMESSAGE = 11; 
	public static byte UPDATEGROUPMESSAGE = 12; 
	public static byte WAITINGMESSAGE = 13; 
	
	//GVT
	public static byte ABORTMESSAGE = 14; 
	public static byte BROADCASTCHALLENGEMESSAGE = 15; 
	public static byte CHALLENGEREQUESTMESSAGE = 16; 
	public static byte CHALLENGERESPONSEMESSAGE = 17;
	public static byte CHOOSECHALLENGERMESSAGE = 18; 
	public static byte CHOOSERESPONDERMESSAGE = 19; 
	public static byte FINALRESPONSEMESSAGE = 20; 
	public static byte RESPONSETOEDGENODEMESSAGE = 21; 
	
	//REMO�AO DE NOS
	public static byte HONEYBEEMESSAGE = 22; 
	public static byte REPLACEREPLYMESSAGE = 23; 
	public static byte REPLACEREQUESTMESSAGE = 24; 
	
	//OTHERS
	public static byte RELIABLEMESSAGE = 25; 
	public static byte CHOOSEMESSAGE = 26; 
}