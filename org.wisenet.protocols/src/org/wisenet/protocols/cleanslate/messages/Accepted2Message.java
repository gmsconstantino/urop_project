package org.wisenet.protocols.cleanslate.messages;

import org.wisenet.protocols.cleanslate.GroupInfo;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem que indica que foi aceite a proposta de merge.
 * No entanto, esta mensagem � enviada se a proposta j� foi aceite por outro n�.
 *
 */
public class Accepted2Message extends AcceptedMessage {

	
	public Accepted2Message(GroupInfo sourcegroup, GroupInfo destinationgroup, short source, short destination, 
			long randomValue, String sinknodeaddress){
		super(destinationgroup,sourcegroup,source,destination,randomValue,sinknodeaddress);
		super.setType(MessageTypes.ACCEPTED2MESSAGE);
	}
}