package org.wisenet.protocols.cleanslate.messages;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem de ack para mensagens fi�veis
 *
 */
public class AckMessage extends ReliableMessage {
	
	private String type;
	
	private long auxNumber; // auxiliar, nao conta para o tamanho da mensagem
	
	public AckMessage(short sourceID, short destinationID, long randomValue){
		super(sourceID,destinationID,randomValue);
		super.setType(MessageTypes.ACKMESSAGE);
		this.type = "";
	}
	
	public AckMessage(short sourceID, short destinationID, long randomValue, String type, long auxNumber){
		super(sourceID,destinationID,randomValue);
		super.setType(MessageTypes.ACKMESSAGE);
		this.type = type;
		this.auxNumber = auxNumber;
	}
	
	public void setAckType(String type){
		this.type = type;
	}
	
	public String getAckType(){
		return type;
	}
	
	public long getAuxNumber(){
		return auxNumber;
	}
	
	/**
	 * type + super
	 */
	@Override
	public int size() {
		return type.getBytes().length + super.size();
	}
}