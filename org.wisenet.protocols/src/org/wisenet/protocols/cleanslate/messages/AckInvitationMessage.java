package org.wisenet.protocols.cleanslate.messages;

import java.util.Set;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Esta mensagem funciona como ack � mensagem de hello.
 *
 */
public class AckInvitationMessage extends CleanSlateMessage {
	
	
	private byte[] signature;
	private Set<Short> destinations;
	private long randomValue;
	
	public AckInvitationMessage(short sourceID, Set<Short> destinations, long randomValue,  byte[] signature){
		super(MessageTypes.ACKINVITATIONMESSAGE);
		super.setSourceId(sourceID);
		this.destinations = destinations;
		this.randomValue = randomValue;
		this.signature = signature;
	}

	
	public byte[] getSignature(){
		return signature;
	}
	
	public Set<Short> getDestinations(){
		return destinations;
	}
	
	
	public long getRandomValue(){
		return randomValue;
	}
	
	/**
	 * signature + destinations + randomValue + super
	 */
    @Override
    public int size() {
    	int size = signature.length + destinations.size()*2 + 8;
    	return size + super.size();
    }
}