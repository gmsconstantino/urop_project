package org.wisenet.protocols.cleanslate.messages;

import java.util.Set;

import org.wisenet.protocols.cleanslate.GroupInfo;


/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem que indica ao grupo que n�o ocorreu um merge,
 * ou seja que a proposta de merge foi rejeitada.
 *
 */
public class NoMergeMessage extends ReliableMessage {
	

	private Set<GroupInfo> oldGroups;
	private Set<GroupInfo> removeGroups;
	private long removeDuplValue;
	private GroupInfo rejectedGroup;
	

	public NoMergeMessage(GroupInfo destinationGroup, String prefix, short sourceID, short destinationID, 
			long removeDuplValue, Set<GroupInfo> removeGroups, Set<GroupInfo> oldGroups, long randomValue, 
			GroupInfo rejectedGroup){
		super(sourceID,destinationID,destinationGroup, randomValue);
		super.setType(MessageTypes.NOMERGEMESSAGE);
		super.setPrefix(prefix);
		this.removeGroups = removeGroups;
		this.oldGroups = oldGroups;
		this.removeDuplValue = removeDuplValue;
		this.rejectedGroup = rejectedGroup;
	}

	public boolean hasRejectedGroup(){
		return rejectedGroup != null;
	}
	
	public GroupInfo getRejectedGroup(){
		return rejectedGroup;
	}
	
	public GroupInfo getSourceGroup(){
		return super.getDestinationGroupInfo();
	}

	
	public Set<GroupInfo> getRemoveGroups(){
		return removeGroups;
	}
	
	public Set<GroupInfo> getOldGroups(){
		return oldGroups;
	}
	
	public long getRemoveDuplValue(){
		return removeDuplValue;
	}
	
	/**
	 * 	oldGroups + removeGroups + removeDuplValue + rejectedGroup + super
	 */
	@Override
	public int size(){
		
		int size = 0;
		if(!oldGroups.isEmpty())
			size += oldGroups.iterator().next().size()*oldGroups.size();
		if(!removeGroups.isEmpty())
			size += removeGroups.iterator().next().size()*removeGroups.size();
		
		size += 8;
		if(rejectedGroup != null)
			size += rejectedGroup.size();
		
		return size + super.size();
	}	
}