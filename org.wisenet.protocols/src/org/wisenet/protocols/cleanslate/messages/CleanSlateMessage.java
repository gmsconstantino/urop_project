package org.wisenet.protocols.cleanslate.messages;

import org.wisenet.simulator.core.Message;


/**
 * 
 * @author Tiago Ara�jo
 *
 * Mensagem Clean-Slate gen�rica
 */
public class CleanSlateMessage extends Message{
	
	private byte msgType;
	
	public CleanSlateMessage(byte type, byte[] payload){
		super(payload); //COM PAYLOAD
		setType(type);
	}
	
	public CleanSlateMessage(byte type){
		super(); //SEM PAYLOAD
		setType(type);
	}

	
	public void setType(byte type){
		this.msgType = type;
	}
	
	public byte getType(){
		return msgType;
	}
	
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
	
    /**
     * type + super
     */
    @Override
    public int size() {
        return 4 + super.size();
    }
}