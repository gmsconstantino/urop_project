package org.wisenet.protocols.cleanslate.messages;

import org.wisenet.protocols.cleanslate.NewHopsResult;

/**
 * 
 * @author Tiago Ara�jo
 * 
 * Mensagem que indica os novos nexthops a serem usados, visto que foram removidos n�s da rede.
 */
public class ReplaceReplyMessage extends CleanSlateMessage {
	
	
	private NewHopsResult newnexthops;
	
	
	public ReplaceReplyMessage(short sourceID, short destination, NewHopsResult newnexthops){
		super(MessageTypes.REPLACEREPLYMESSAGE);
		super.setSourceId(sourceID);
		super.setDestinationId(destination);
		this.newnexthops = newnexthops;
	}
	
	public NewHopsResult getNewNextHops(){
		return newnexthops;
	}
	
	
	/**
	 * NewHopsResult + super
	 */
	@Override
	public int size(){
		return newnexthops.size() + super.size();
	}
}