package org.wisenet.protocols.cleanslate.messages;

import java.util.Set;


/**
 *
 * @author Tiago Ara�jo
 * 
 * Mensagem de hello que d� o n� a conhecer-se a outros n�s.
 *
 */
public class HelloMessage extends CleanSlateMessage {
	
	
	private byte[] signature;
	
	/**
	 * Indica os n�s que j� s�o conhecidos, 
	 * por parte do n� que envia a mensagem Hello.
	 */
	private Set<Short> invitations;
	
	
	public HelloMessage(short src, byte[] signature, Set<Short> invitations){
		super(MessageTypes.HELLOMESSAGE);
		super.setSourceId(src);
		this.signature = signature;
		this.invitations = invitations;
	}
	
	
	public byte[] getSignature(){
		return signature;
	}

	
	public Set<Short> getInvitations(){
		return this.invitations;
	}
	

	/**
	 * signature + invitations + super
	 */
    @Override
    public int size() {
    	int size = signature.length + invitations.size()*2;
    	return size + super.size();
    }
}