package org.wisenet.protocols.cleanslate;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.wisenet.protocols.common.Pair;


public class ReplaceResult {
	
	private List<Pair<Vector<String>,Vector<Integer>>> replaces;
	private List<Short> maliciousNodesID;
	
	public ReplaceResult(List<Pair<Vector<String>,Vector<Integer>>> replaces, List<Short> maliciousNodesID){
		this.replaces = replaces;
		this.maliciousNodesID = maliciousNodesID;
	}
	
	public List<Pair<Vector<String>,Vector<Integer>>> getReplaces() {
		return replaces;
	}
	
	public List<Short> getMaliciousNodesID(){
		return maliciousNodesID;
	}
	
	public boolean isEmpty() {
		return replaces.isEmpty() && maliciousNodesID.isEmpty();
	}
	
	public List<Vector<String>> getAddresses(){
		List<Vector<String>> res = new LinkedList<Vector<String>>();
		
		Iterator<Pair<Vector<String>,Vector<Integer>>> it = replaces.iterator();
		while(it.hasNext()){
			Pair<Vector<String>,Vector<Integer>> next = it.next();
			res.add(next.getFirst());
		}
		return res;
	}
}