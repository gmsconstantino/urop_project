package org.wisenet.protocols.cleanslate;

import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.wisenet.protocols.MAC.tinysec.ConstantsTinySec;
import org.wisenet.protocols.MAC.tinysec.TinySecExtended;
import org.wisenet.protocols.common.Pair;
import org.wisenet.protocols.common.Utils;

import com.sun.corba.se.impl.javax.rmi.CORBA.Util;

/**
 * 
 * @author Tiago Ara�jo
 * 
 * Representa a autoridade de rede
 *
 */
public class NetworkAuthority{
	
	private PrivateKey kpriv;
	
	private PublicKey kpub;
	
	private byte[] ivector;
	
	private TinySecExtended tinySecExt;
	
	
	public NetworkAuthority(KeyPair kp, TinySecExtended tinySecExt){
		kpriv = kp.getPrivate();
		this.kpub = kp.getPublic();
		this.ivector = null;//SecurityFunctions.createNonce(8);
		this.tinySecExt = tinySecExt;
	}
	

	/**
	 * Gerar a assinatura para um identificador
	 * {id}Kna-1
	 */
	public Pair<Short,byte[]> generateID(short id) throws Exception {
		byte[] idbytes = Utils.shortToByteArray(id);
		byte[] signatureId = tinySecExt.encrypt(idbytes, kpriv, 
				CleanSlateConstants.PropertiesCipher, ConstantsTinySec.PROVIDER, null);
		
		return new Pair<Short,byte[]>(id,signatureId);
	}
	
	
	/**
	 * Criar os valores de desafio
	 */
	public ChallengeValues generateChallangeValues(int k, short uniqueId) throws Exception {
		try {
			MessageDigest hashFunction = MessageDigest.getInstance(ConstantsTinySec.MSGDIGESTALGORITHM,ConstantsTinySec.PROVIDER);
			int[] seq = tinySecExt.generateOneWaySeq(hashFunction, tinySecExt.getIVector(8).hashCode(), k+1);
			
			byte[] c0 = Utils.intToByteArray(seq[seq.length-1]);
			byte[] id = Utils.shortToByteArray(uniqueId);
			byte[] res = Utils.appendByteArrays(c0, id);
			
			byte[] signature = tinySecExt.encrypt(res, kpriv, 
					CleanSlateConstants.PropertiesCipher, ConstantsTinySec.PROVIDER,ivector);
		
			return new ChallengeValues(seq[seq.length-1], k, seq[0], signature, hashFunction,tinySecExt);
			
		} catch (NoSuchAlgorithmException e) {
			System.err.println(e.getMessage());
		} catch (NoSuchProviderException e) {
			System.err.println(e.getMessage());
		}
		return null;
	}
	
	
	public byte[] getIVector() {
		return this.ivector;
	}
	
		
	
	public PublicKey getPublicKey(){
		return kpub;
	}	
}