package org.wisenet.protocols.cleanslate;

/**
 * 
 * @author Tiago Ara�jo
 *
 */
public class Debug {
	
	private static boolean init = false;
	
	public Debug(){
		
	}
	
	public static void initMergePropose(){
		init = true;
	}
	
	public static void receiveAckInvite(){
		if(init)
			System.err.println("J� INICIOU A FASE DE MERGING");
	}
	
	public static void reset(){
		init = false;
	}
	
}