package org.wisenet.protocols.common;

import java.io.IOException;
import java.util.Random;

public class Utils {

	private static ByteArrayDataOutputStream bados;
	private static ByteArrayDataInputStream badis;
	
	public synchronized static byte[] intToByteArray(int i) throws IOException{
		bados = new ByteArrayDataOutputStream();
		bados.writeInt(i);
		
		return bados.toByteArray();
	}
	
	public synchronized static int byteArrayToInt(byte[] i) throws IOException{
		badis = new ByteArrayDataInputStream(i);
		
		return badis.readInt();
	}
	
	public synchronized static byte[] shortToByteArray(short s) throws IOException{
		bados = new ByteArrayDataOutputStream();
		bados.writeShort(s);
		
		return bados.toByteArray();
	}
	
	public synchronized static byte[] longToByteArray(long s) throws IOException{
		bados = new ByteArrayDataOutputStream();
		bados.writeLong(s);
		
		return bados.toByteArray();
	}
	
	public static byte[] appendByteArrays(byte[] b1, byte[] b2){
		byte[] res = new byte[b1.length + b2.length];
		int c = 0;
		
		for(int i = 0; i < b1.length; i++)
			res[c++] = b1[i];
		for(int i = 0; i < b2.length; i++)
			res[c++] = b2[i];
		
		return res;
	}
	
	public static long randomLongBetween(long l, long h, Random r){
		return (long) randomDoubleBetween(l, h, r);
	}
	
	public static int randomIntBetween(int l, int h, Random r){
		return (int) randomDoubleBetween(l, h, r);
	}
	
	public static double randomDoubleBetween(double l, double h, Random r){
		return l + r.nextDouble() * h;
	}
}
