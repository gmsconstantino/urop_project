package org.wisenet.protocols.myflooding;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.node.Mica2SensorNode;
import org.wisenet.simulator.core.radio.RadioModel;
import org.wisenet.simulator.gui.IDisplayable;

public class MyFloodNode extends Mica2SensorNode implements IDisplayable{
	
	public MyFloodNode(Simulator sim, RadioModel radioModel) {
		super(sim, radioModel);
	}
	
	@Override
	public void init(){
		try {
			getGraphicNode().setRadius(2);
			getGraphicNode().setMarkColor(Color.BLACK);
			
			if (getId() == 1) {
				setBaseColor(Color.YELLOW);
				this.setSinkNode(true);
            }
			
            super.init();
        } catch (Exception ex) {
            Logger.getLogger(MyFloodNode.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	@Override
	public short getUniqueID() {
		return (Short) getId();
	}
	
}
