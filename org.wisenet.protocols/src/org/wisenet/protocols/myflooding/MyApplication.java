package org.wisenet.protocols.myflooding;

import javax.swing.JOptionPane;

import org.wisenet.protocols.myflooding.messages.MyFloodMessage;
import org.wisenet.simulator.core.Application;
import org.wisenet.simulator.core.Message;

public class MyApplication extends Application {

	@Override
	public void run() {
		short sink = askForSink();

		// for(int i = 0; i < 10; i++){
		// MyFloodMessage m = newHELLOMsg(sink);
		//
		// System.out.println("APP: Node " + getNode().getId() +
		// ": Sent msgID: " + m.getId());
		//
		// DelayedMessageEvent d = new DelayedMessageEvent(
		// getNode().getSimulator().getSimulationTime(),
		// generateRandomLongBetween(LOWERMSGDELAY, UPPERMSGDELAY), m,
		// getNode(), false);
		//
		// getNode().getSimulator().addEvent(d);
		// }

		sendMessage(newHELLOMsg(sink));

	}

	@Override
	protected void onMessageReceived(Object message) {
		if (message == null)
			System.err.println("message null " + getNode().getId());
		else {
			MyFloodMessage m = (MyFloodMessage) message;
			System.out.println("APP Node "+getNode().getId()+": Received from node " + m.getSourceId()
					+ ":  \"" + new String(m.getPayload()) + "\"  MsgID: "
					+ m.getMessageNumber());

//			System.out.println("RECEIVED! Hops: "+ ((Message) message).getTotalHops());
		}
	}

	private MyFloodMessage newHELLOMsg(short destination) {
		return new MyFloodMessage(getNode().getId(), destination,
				"HELLO!".getBytes());
	}

	private short askForSink() {
		String res = "0";

		res = JOptionPane.showInputDialog("Choose destination node ID");
		return Short.valueOf(res);
	}
}
