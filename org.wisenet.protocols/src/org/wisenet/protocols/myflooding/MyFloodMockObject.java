package org.wisenet.protocols.myflooding;


import org.wisenet.simulator.core.Application;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.Simulator;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class MyFloodMockObject extends MyFloodRoutingLayer implements Runnable  {

	int numbertosend = 20;
	int timeout = 750;
	Message message;
	boolean wait = true;
	
	public MyFloodMockObject() {
		super();
	}
	
	@Override
	protected boolean onSendMessage(Object message, Application app) {
		this.message = (Message) message;		
		new Thread(this).start();		
		return true;
	}

	@Override
	public void run() {
		System.out.println("runable");
		
		for(int i=0; i<numbertosend;i++){
			message.setUniqueId(message.getUniqueId()+i);
			send(encapsulateMessage((Message) message));
			sendDataToAnalyse(message,"send",System.currentTimeMillis(),Simulator.getSimulationTime());
			getController().addMessageSentCounter((short)0);
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}		
	}


}
