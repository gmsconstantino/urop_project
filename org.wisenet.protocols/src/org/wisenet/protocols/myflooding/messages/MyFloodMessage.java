package org.wisenet.protocols.myflooding.messages;

import org.wisenet.simulator.core.Message;

public class MyFloodMessage extends Message{

	private static long globalUniqueId = 0;
	
//	private short source;
//	private short destination;
	private Message encapsulatedMessage;
	
	
	public MyFloodMessage(){
		super("NULL".getBytes());
	}
	
	public MyFloodMessage(short source, short destination, byte[] payload) {
		super(payload);
		
//		this.source = source;
//		this.destination = destination;
		this.uniqueId = globalUniqueId++;
		
		sourceId = source;
		destinationId = destination;
	}

	
//	public short getSourceId() {
//		return source;
//	}
//
//	public short getDestinationId() {
//		return destination;
//	}

//	public long getUniqueId() {
//		return uniqueId;
//	}
//	
//	public void setSourceId(Object id){
//		source = (Short) id;
//	}
//	
//	public void setDestinationId(Object id){
//		destination = (Short) id;
//	}
//	
//	public void setUniqueId(long id){
//		uniqueId = id;
//	}
	
	public long getMessageNumber() {
        return globalUniqueId;
    }
	
	public void setPayload(byte[] payload){
		this.payload = payload;
	}
	
	public byte[] getPayload(){
		return payload;
	}
	
	public Message getEncapsulatedMessage(){
		return encapsulatedMessage;
	}
	
	public void setEncapsulatedMessage(Message msg){
		encapsulatedMessage = msg;
	}
}
