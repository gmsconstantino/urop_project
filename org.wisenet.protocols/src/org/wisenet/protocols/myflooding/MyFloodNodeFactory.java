package org.wisenet.protocols.myflooding;

import org.wisenet.simulator.core.node.factories.AbstractNodeFactory;
import org.wisenet.simulator.core.node.layers.mac.Mica2MACLayer;

public class MyFloodNodeFactory extends AbstractNodeFactory{

	public MyFloodNodeFactory(){}
	
	@Override
	public void setup() {
		setApplicationClass(MyApplication.class);
		setRoutingLayerClass(MyFloodRoutingLayer.class);
		setNodeClass(MyFloodNode.class);
		setMacLayer(Mica2MACLayer.class);
		
		setSetup(true);
	}

}
