package org.wisenet.protocols.myflooding;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.HashSet;

import org.wisenet.protocols.myflooding.messages.MyFloodMessage;
import org.wisenet.simulator.core.Application;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.node.layers.routing.RoutingLayer;
import org.wisenet.simulator.core.node.layers.routing.RoutingLayerController;

public class MyFloodRoutingLayer extends RoutingLayer{

	/**
	 * Node data
	 */
	private HashSet<Long> receivedMsgs = new HashSet<Long>();
	
	public MyFloodRoutingLayer(){
		super();
	}
	
	
	@Override
	protected String getRoutingTable() {return null;}

	@Override
	protected void onReceiveMessage(Object message) {
		if(message instanceof MyFloodMessage){
			MyFloodMessage m = (MyFloodMessage) message;
			
			if(receivedMsgs.contains((Long) m.getUniqueId())){
				// DROP MSG!
			}
			else {
				receivedMsgs.add((Long) m.getUniqueId());
				
				if((Short) m.getDestinationId() == getNode().getId()){
					// reached destination
					getNode().getApplication().receiveMessage(m.getEncapsulatedMessage());
					done(m);
					// Send data to DataCollector
					sendDataToAnalyse(m,"received",System.currentTimeMillis(),Simulator.getSimulationTime());
					getController().addMessageReceivedCounter((short)0);
				}
				else {
					routeMessage(m);
				}
			}
		}		
	}
	
	protected void sendDataToAnalyse(Object message,String operation, long time, long simtime){
		try {
			String servidor = "localhost";
			int port = 8888;
			InetAddress serverAddress = InetAddress.getByName(servidor);
			DatagramSocket socket = new DatagramSocket();
			String data = "";
			
			// Merge data to send
			MyFloodMessage m = (MyFloodMessage) message;
			data+="operation="+operation+"&";
			data+="id="+m.getUniqueId()+"&";
			data+="hops="+m.getTotalHops()+"&";
			data+="size="+m.size()+"&";
			data+="time="+Long.toString(time)+"&";
			data+="simtime="+simtime;
			
			byte[] requestData = data.getBytes();
			DatagramPacket echoRequest = new DatagramPacket(requestData,
					requestData.length);
			echoRequest.setAddress(serverAddress);
			echoRequest.setPort(port);
			socket.send(echoRequest);
			socket.close();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected boolean onSendMessage(Object message, Application app) {
		sendDataToAnalyse(message,"send",System.currentTimeMillis(),Simulator.getSimulationTime());
		send(encapsulateMessage((Message) message));
		getController().addMessageSentCounter((short)0);
		return true;
	}

	@Override
	protected void onRouteMessage(Object message) {
		send(message);
	}

	@Override
	protected void startupAttacks() {}

	@Override
	protected void sendMessageToAir(Object message) {
		getNode().getMacLayer().sendMessage(message, this);
	}

	@Override
	public void newRound() {}

	@Override
	public void sendMessageDone(){
		getNode().getApplication().sendMessageDone();
	}

	@Override
	protected void onStartUp() {
		setStable(true);
	}

	@Override
	protected void onStable(boolean oldValue) {}

	@Override
	protected void initAttacks() {}

	@Override
	protected Message encapsulateMessage(Message m) {
		MyFloodMessage msg = new MyFloodMessage();
		
		msg.setUniqueId(m.getUniqueId());
		msg.setSourceId(m.getSourceId());
		msg.setDestinationId(m.getDestinationId());
		msg.setPayload(m.getPayload());
		msg.setEncapsulatedMessage(m);
		
		return msg;
	}
	
	public short getUniqueId() {
        return getNode().getUniqueID();
    }
}
