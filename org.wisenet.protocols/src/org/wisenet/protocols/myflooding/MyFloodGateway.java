package org.wisenet.protocols.myflooding;


import org.wisenet.protocols.gateway.Gateway;
import org.wisenet.protocols.gateway.GatewayServer;
import org.wisenet.protocols.myflooding.messages.MyFloodMessage;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class MyFloodGateway extends MyFloodRoutingLayer implements Gateway{

	GatewayServer s; 
	short dest = 1;
	
	public MyFloodGateway() {
		super();
		s = new GatewayServer(this);
		// s.setDaemon(true);
	}
	
	
	@Override
	public void onReceiveMessageGateway(byte[] message) {
		MyFloodMessage msg = new MyFloodMessage(getNode().getId(), dest, (byte[]) message);
			
		onSendMessage(msg, getNode().getApplication());
	}


}
