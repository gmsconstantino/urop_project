/*
 * SensorSampler.java
 *
 * Copyright (c) 2008-2010 Sun Microsystems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package org.urop.rtt;

import java.io.IOException;

import com.sun.spot.io.j2me.radiogram.*;
import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.Utils;
import javax.microedition.io.*;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * This application is the 'on SPOT' portion of the RTTSunSpot. 
 * This application help the host calculating RTT Time.
 * 
 *
 * @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
 */
public class RTTDeviceApplication extends MIDlet {

	private static final int HOST_PORT = 67;

	protected void startApp() throws MIDletStateChangeException {
		// Listen for downloads/commands over USB connection
		new com.sun.spot.service.BootloaderListenerService().getInstance().start();

		startReceiverThread();
		
		ITriColorLED led = (ITriColorLED)Resources.lookup(ITriColorLED.class, "LED7");
		led.setRGB(255, 0, 0);
		while(true){
			Utils.sleep(1000);
			led.setOn();
            Utils.sleep(1000);
            led.setOff();
		}
		
		
	}

	/**
	 * The receiver thread blocks on the receive function
	 * so you don't have to sleep between each receive.
	 */
	public void startReceiverThread() {
		new Thread() {
			public void run() {
				String tmp = null;
				RadiogramConnection receiverConnection = null;
				RadiogramConnection senderConnection = null;
				Datagram dgSender;
		        Datagram dgReceived;
				ITriColorLED led = (ITriColorLED)Resources.lookup(ITriColorLED.class, "LED1");
				led.setRGB(255, 255, 255);
                led.setOn();
				try {
					// Receiver Connection
					receiverConnection = (RadiogramConnection) Connector.open("radiogram://:"+HOST_PORT);
					// The Connection is a broadcast so we specify it in the creation string
					senderConnection = (RadiogramConnection) Connector.open("radiogram://broadcast:"+(HOST_PORT+1));
					// Then, we ask for a datagram with the maximum size allowed
					dgReceived = receiverConnection.newDatagram(receiverConnection.getMaximumLength());
					dgSender = senderConnection.newDatagram(senderConnection.getMaximumLength());
				} catch (IOException e) {
					System.out.println("Could not open radiogram receiver connection");
					e.printStackTrace();
					return;
				}
				
				while(true){
					try {
						dgReceived.reset();
						// Receive datagram
						receiverConnection.receive(dgReceived);
		                dgSender.reset();
		                dgSender.write(dgReceived.getData());
						// Replay datagram
						senderConnection.send(dgSender);
						System.out.println("Spot: Send");
						tmp = dgSender.readUTF();
						System.out.println("Received: " + tmp + " from " + dgReceived.getAddress());
					} catch (IOException e) {
						System.out.println("Nothing received");
						
						led.setRGB(255, 255, 255);
		                led.setOn();
					}
				}
			}
		}.start();
	}

	protected void pauseApp() {
		// This will never be called by the Squawk VM
	}

	protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
		// Only called if startApp throws any exception other than
		// MIDletStateChangeException
		ITriColorLED led = (ITriColorLED)Resources.lookup(ITriColorLED.class, "LED7");
		led.setOff();
	}
}
