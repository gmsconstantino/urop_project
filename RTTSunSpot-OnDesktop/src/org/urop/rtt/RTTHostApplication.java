/*
 * SendDataDemoHostApplication.java
 *
 * Copyright (c) 2008-2009 Sun Microsystems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package org.urop.rtt;

import java.io.PrintWriter;

import com.sun.spot.io.j2me.radiogram.*;

import com.sun.spot.peripheral.ota.OTACommandServer;
import com.sun.spot.util.Utils;

import javax.microedition.io.*;


/**
 * This application is the 'on Desktop' portion of the RTTHostApplication. 
 * This host application calculate RTT Time and display it.
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class RTTHostApplication {
    // Broadcast port on which we listen for sensor samples
	private static final int SAMPLE_PERIOD = 1 * 1000; // in milliseconds
    private static final int HOST_PORT = 67;
    private final int readsRTTTimes = 250;
        
    private void run() throws Exception {
    	RadiogramConnection receiverConnection = null;
		RadiogramConnection senderConnection = null;
        Datagram dgSender;
        Datagram dgReceived;
        String ourAddress = System.getProperty("IEEE_ADDRESS");
        System.out.println("Starting sensor sampler application on "+ ourAddress + " ...");
        
        try {
        	receiverConnection = (RadiogramConnection) Connector.open("radiogram://:"+(HOST_PORT+1));
			// The Connection is a broadcast so we specify it in the creation string
			senderConnection = (RadiogramConnection) Connector.open("radiogram://broadcast:"+HOST_PORT);
			dgSender = senderConnection.newDatagram(senderConnection.getMaximumLength());
			dgReceived = receiverConnection.newDatagram(receiverConnection.getMaximumLength());
        } catch (Exception e) {
             System.err.println("setUp caught " + e.getMessage());
             throw e;
        }
        
        PrintWriter pw = new PrintWriter("result.txt");

		int i = 0;
		while (i < readsRTTTimes) {
			Utils.sleep(SAMPLE_PERIOD);
			try {
				System.out.println("Send message.");
				dgSender.reset();
				dgSender.writeUTF("HELLO");
				long sendTime = System.currentTimeMillis();
				senderConnection.send(dgSender);
				
				dgReceived.reset();
				receiverConnection.receive(dgReceived);
				long receivedTime = System.currentTimeMillis();
				System.out.println("Received message.");
				pw.write(""+(receivedTime - sendTime)+"\n");
			} catch (Exception e) {
				System.err.println("Caught " + e
						+ " while collecting/sending sensor sample.");
			}
			i++;
		}
		
		senderConnection.close();
		receiverConnection.close();
		pw.close();
		System.out.println("Done.");
    }
    
    /**
     * Start up the host application.
     *
     * @param args any command line arguments
     */
    public static void main(String[] args) throws Exception {
        // register the application's name with the OTA Command server & start OTA running
        OTACommandServer.start("SendDataDemo");

        RTTHostApplication app = new RTTHostApplication();
        app.run();
    }
}
