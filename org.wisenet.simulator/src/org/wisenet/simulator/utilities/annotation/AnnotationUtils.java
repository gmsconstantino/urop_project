package org.wisenet.simulator.utilities.annotation;

import java.lang.reflect.Field;
import java.util.Vector;
import org.wisenet.simulator.utilities.annotation.Annotated;
import org.wisenet.simulator.utilities.annotation.EnergyModelParameter;

/**
 *
* @author Pedro Marques da Silva <MSc Student @di.fct.unl.pt>
 */
public class AnnotationUtils {

    /**
     *
     * @param c
     * @return
     */
    public static boolean isAnnotated(Class c) {
        Class[] interfaces = c.getInterfaces();
        for (int i = 0; i < interfaces.length; i++) {
            Class class1 = interfaces[i];
            if (class1.getName().equals(Annotated.class.getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param o
     * @return
     */
    public static Vector readEnergyModelParametersFields(Object o) {
        Vector inputParameters = new Vector();
        if (isAnnotated(o.getClass())) {

            for (Field f : o.getClass().getDeclaredFields()) {
                EnergyModelParameter p = f.getAnnotation(EnergyModelParameter.class);
                if (p != null) {
                    inputParameters.add(f);
                }

            }

        }
        return inputParameters;
    }

    /**
     *
     * @param o
     * @param aClass
     * @return
     */
    public static Vector readParametersFields(Object o, Class aClass) {
        Vector inputParameters = new Vector();
        if (isAnnotated(o.getClass())) {

            for (Field f : o.getClass().getDeclaredFields()) {
                Object p = f.getAnnotation(aClass);
                if (p != null) {
                    inputParameters.add(f);
                }

            }

        }
        return inputParameters;

    }
}
