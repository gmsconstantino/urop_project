package org.wisenet.simulator.components.evaluation;

public class MessagePassageEntry {

	private short reportingNode;
	private short previousHopId;
	private int hopDistanceToBS;
	
	
	public MessagePassageEntry(short reportingNode, short previousHopId,
			int hopDistanceToBS) {
		super();
		this.reportingNode = reportingNode;
		this.previousHopId = previousHopId;
		this.hopDistanceToBS = hopDistanceToBS;
	}


	public short getReportingNode() {
		return reportingNode;
	}


	public short getPreviousHopId() {
		return previousHopId;
	}


	public int getHopDistanceToBS() {
		return hopDistanceToBS;
	}

	public boolean equals( MessagePassageEntry other ){
		return (reportingNode == other.getReportingNode() 
				&& previousHopId == other.getPreviousHopId()
				&& hopDistanceToBS == other.getPreviousHopId());
	}
	
	
	
	
}
