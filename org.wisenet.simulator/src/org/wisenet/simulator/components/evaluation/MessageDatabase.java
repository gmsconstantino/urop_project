package org.wisenet.simulator.components.evaluation;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.node.layers.routing.RoutingLayer;

/**
 *
 * @author posilva
 */
public class MessageDatabase {

    Hashtable<Long, MessageTableEntry> messagesTable = new Hashtable<Long, MessageTableEntry>();
    Hashtable<Short, List<SendersTableEntry>> senderNodesTable = new Hashtable<Short, List<SendersTableEntry>>();
    Hashtable<Short, ReceiversTableEntry> receiverNodesTable = new Hashtable<Short, ReceiversTableEntry>();
    List<Integer> messageCountByHops = new LinkedList<Integer>();
    
    //key is messageId, value it is a list MessagePassageEntry of the nodes who routed the message 
    Hashtable<Long,List<MessagePassageEntry>> seenMessageNodes = new Hashtable<Long,List<MessagePassageEntry>>();
    long totalNumberOfMessagesSent = 0;
    /**
     *
     */
    protected boolean debugEnabled = false;
    
    
    /**
     * Registers a message sent event
     * @param message
     *              the message sent
     * @param routing
     */
    public synchronized void registerMessageSent(Message message, RoutingLayer routing) {
        totalNumberOfMessagesSent++;
        MessageTableEntry me;
        
        /* if message source id is the same as the sender then process */
        if (message.getSourceId() == routing.getUniqueId()) {
            /* if message not already processed */
            if (!messagesTable.keySet().contains(message.getUniqueId())) {
                log("Message " + message.getUniqueId() + " Sent by " + message.getSourceId() + " to " + message.getDestinationId());
                /* process message */
                me = new MessageTableEntry(message, routing);
                me.setSimulationTimeReceived(Simulator.getSimulationTime());
                messagesTable.put(message.getUniqueId(), me);
                
                /* if senders aren't processed then */
                if (!senderNodesTable.keySet().contains(routing.getUniqueId())) {
                	
                	List<SendersTableEntry> l = new LinkedList<SendersTableEntry>();
                	l.add(new SendersTableEntry(message.getUniqueId()));
                	senderNodesTable.put(routing.getUniqueId(), l);
                	
                    /*process sender*/
//                    senderNodesTable.put(routing.getUniqueId(), new SendersTableEntry());
                    log("Sender" + routing.getUniqueId() + " registered");
                }else{
                	List<SendersTableEntry> l = senderNodesTable.get(routing.getUniqueId());
                	l.add(new SendersTableEntry(message.getUniqueId()));
                }
                
                
                //if not the message wasn't sent and the forwarding table won't exist
//                if(routing.isStable()){
//                	//so that the sender counts to the statistics
//                    LinkedList<Object> list = new LinkedList<Object>();
//                    list.add(new MessagePassageEntry((Short)routing.getUniqueId(),(Short)routing.getNode().getId(),routing.getForwardingTable().getHopDistanceToBS()));
//                    
////                    list.add(routing.getUniqueId());
//                    
//                    seenMessageNodes.put(message.getUniqueId(), list);
//                    int senderHopDistance = routing.getForwardingTable().getHopDistanceToBS();
//                    while (messageCountByHops.size() < senderHopDistance)
//                    	messageCountByHops.addLast(0);
//                    
//                    
//                    int currentCount = messageCountByHops.get(senderHopDistance-1);
//                    messageCountByHops.set(senderHopDistance-1, currentCount+1);
//                }
            }
        }
        // senão descarta mensagem
    }
    

    
    public synchronized void registerMessagePassage(Message message, RoutingLayer routing){

        totalNumberOfMessagesSent++;
		List<MessagePassageEntry> nodesWhoSaw = seenMessageNodes.get(message.getUniqueId());
		if(nodesWhoSaw == null){
			nodesWhoSaw = new LinkedList<MessagePassageEntry>();
			seenMessageNodes.put(message.getUniqueId(), nodesWhoSaw);
		}
		MessagePassageEntry m = new MessagePassageEntry((Short)routing.getUniqueId(),routing.getLastMessageSenderId(),routing.getForwardingTable().getHopDistanceToBS());
		if (!nodesWhoSaw.contains(m)){
			nodesWhoSaw.add(m);
			//retirar daqui do if, para baixo para apanhar retransmissoes
			int senderHopDistance = routing.getForwardingTable().getHopDistanceOnLastRoute();
			//baseStation also registers passage...but should not count on messageCountByHops
			if( senderHopDistance > 0){
				while (messageCountByHops.size() < senderHopDistance)
		        	((LinkedList<Integer>) messageCountByHops).push(0);		        	
		        int currentCount = messageCountByHops.get(senderHopDistance-1);
		        messageCountByHops.set(senderHopDistance-1, currentCount+1);
			}
	        
		}
    	
    	
    }
    
    
    
    /**
     * Registers a succeeded received message
     * @param message
     *              the message received
     * @param routing
     */
    public synchronized void registerMessageReceived(Message message, RoutingLayer routing) {

//        if (message.getDestinationId() == routing.getUniqueId()) {
            if (!receiverNodesTable.contains(routing.getUniqueId())) {
                receiverNodesTable.put(routing.getUniqueId(), new ReceiversTableEntry()); // TODO: NOT NULL
            }

            MessageTableEntry messageEntry = messagesTable.get(message.getUniqueId());
            if (messageEntry != null) {
                messageEntry.arrived();
                log("MESSAGE " + message.getUniqueId() + " takes " + message.getTotalHops() + " HOPS");
                messageEntry.setMessage(message);
                messageEntry.setSimulationTimeReceived(Simulator.getSimulationTime());
                messageEntry.incrementCounter();
                RoutingLayer senderId = messageEntry.getSenderRouting();
                List<SendersTableEntry> senderEntrys = senderNodesTable.get(senderId.getUniqueId());
                for(SendersTableEntry s : senderEntrys){
                	if (s.getId() == message.getUniqueId()){
                		s.arrived();
                	}
                }
//                if (senderEntry != null) {
//                    senderEntry.arrived();
//                }
            }
//        }
    }

    
    public Message getOriginalMessage(long messageId){
//    	return messagesTable.get(messageId);
    	MessageTableEntry mEntry = messagesTable.get(messageId);
    	if (mEntry == null)
    		return null;
    	else
    		return mEntry.getMessage();
    }
    
    /**
     *
     * @return
     */
    public synchronized int getTotalSenderNodes() {
        int count = 0;
        for(  Object o : senderNodesTable.values()){
        	LinkedList<SendersTableEntry> list = (LinkedList<SendersTableEntry>)o;
        	count+= list.size();
        }
    	return count;
    	
//        return senderNodesTable.size();
    }

    /**
     *
     * @return
     */
    public synchronized int getTotalCoveredNodes() {
        int t = 0;
        for (Object object : senderNodesTable.values()) {
        	LinkedList<SendersTableEntry> list = (LinkedList<SendersTableEntry>)object;
        	for(SendersTableEntry s : list){
        		if(s.isArrived())
        			t++;
        	}
//            SendersTableEntry e = (SendersTableEntry) object;
//            if (e.isArrived()) {
//                t++;
//            }
        }
        return t;
    }

    /**
     *
     * @return
     */
    public synchronized long getTotalMessagesReceived() {
        long t = 0;
        for (MessageTableEntry e : messagesTable.values()) {
            if (e.isArrived()) {
                t++;
            }
        }
        return t;
    }

    /**
     *
     * @return
     */
    public synchronized long getTotalNumberOfUniqueMessagesSent() {
        return messagesTable.size();
    }

    
    public synchronized List<Integer> getMessageCountByHops(){
    	return messageCountByHops;
    }
    
    
    
    
    public synchronized Hashtable<Long, List<MessagePassageEntry>> getSeenMessageNodes() {
		return seenMessageNodes;
	}


	/**
     *
     * @param msg
     */
    protected void log(String msg) {
        if (debugEnabled) {
            System.out.println(getClass().getSimpleName() + " - " + msg);
        }
    }

    public double getCoveragePercent() {
    	if(getTotalSenderNodes() > 0)
    		return 100 * (getTotalCoveredNodes()) / getTotalSenderNodes();
    	else
    		return 0;
    }

    public double getReliabilityPercent() {
    	if(getTotalNumberOfUniqueMessagesSent() > 0)
    		return 100 * (getTotalMessagesReceived()) / getTotalNumberOfUniqueMessagesSent();
    	else
    		return 0;
    }

    public double getLatencyMax() {

        DescriptiveStatistics stats = new DescriptiveStatistics();
        for (MessageTableEntry e : messagesTable.values()) {
            stats.addValue(e.getMessage().getTotalHops());
        }

        return stats.getMax();

    }

    public double getLatencyAvg() {
        DescriptiveStatistics stats = new DescriptiveStatistics();
        for (MessageTableEntry e : messagesTable.values()) {
            stats.addValue(e.getMessage().getTotalHops());
        }
        return stats.getMean();
    }

    public double getLatencyMin() {
        DescriptiveStatistics stats = new DescriptiveStatistics();
        for (MessageTableEntry e : messagesTable.values()) {
            stats.addValue(e.getMessage().getTotalHops());
        }
        return stats.getMin();
    }

    void reset() {
        messagesTable.clear();
        senderNodesTable.clear();
        receiverNodesTable.clear();
        totalNumberOfMessagesSent = 0;
    }

    // reliability control
    /**
     *
     */
    public class MessageTableEntry {

        boolean arrived = false;
        int count = 0;
        long simulationTimeSent = 0L;
        long simulationTimeReceived = 0L;
        private Message message;
        private RoutingLayer senderRouting;

        public long getSimulationTimeReceived() {
            return simulationTimeReceived;
        }

        public void setSimulationTimeReceived(long simulationTimeReceived) {
            this.simulationTimeReceived = simulationTimeReceived;
        }

        public long getSimulationTimeSent() {
            return simulationTimeSent;
        }

        public void setSimulationTimeSent(long simulationTimeSent) {
            this.simulationTimeSent = simulationTimeSent;
        }

        private MessageTableEntry(Message message, RoutingLayer routing) {
            this.message = message;
            this.senderRouting = routing;
        }

        /**
         *
         * @return
         */
        public RoutingLayer getSenderRouting() {
            return senderRouting;
        }

        /**
         *
         * @return
         */
        public boolean isArrived() {
            return arrived;
        }

        /**
         *
         */
        public void arrived() {
            this.arrived = true;
        }

        /**
         *
         * @return
         */
        public Message getMessage() {
            return message;
        }

        /**
         *
         * @return
         */
        public int getCount() {
            return count;
        }

        /**
         *
         */
        public void incrementCounter() {
            count++;
        }

        public void setMessage(Message message) {
            this.message = message;
        }
    }
// coverage control

    /**
     *
     */
    public class SendersTableEntry {

        boolean arrived = false;
        long id;

        /**
         *
         */
        public SendersTableEntry() {
        }
        
        public SendersTableEntry(long id){
        	this.id = id;
        }

        
        public long getId(){
        	return this.id;
        }
        /**
         *
         * @return
         */
        public boolean isArrived() {
            return arrived;
        }

        /**
         *
         */
        public void arrived() {
            this.arrived = true;
        }
    }

    /**
     *
     */
    public class ReceiversTableEntry {

        /**
         *
         */
        public ReceiversTableEntry() {
        }
    }

    /**
     *
     * @return
     */
    public boolean isDebugEnabled() {
        return debugEnabled;
    }

    /**
     *
     * @param debugEnabled
     */
    public void setDebugEnabled(boolean debugEnabled) {
        this.debugEnabled = debugEnabled;
    }

    /**
     *
     * @return
     */
    public long getTotalNumberOfMessagesSent() {
        return totalNumberOfMessagesSent;
    }
}
