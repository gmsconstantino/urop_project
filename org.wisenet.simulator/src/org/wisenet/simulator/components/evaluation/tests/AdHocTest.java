package org.wisenet.simulator.components.evaluation.tests;

import java.util.Collection;
import org.wisenet.simulator.core.node.Node;
import org.wisenet.simulator.core.Event;
import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.components.evaluation.tests.events.TestChildEndEvent;

/**
 *
 * 
 */
public class AdHocTest extends BaseTest {

	
	AverageAdHocTest aggreggatorTest;
    /**
     *
     * @param inputParameters
     */
    public AdHocTest(TestInputParameters inputParameters, AverageAdHocTest t)  {
        super(inputParameters);
        aggreggatorTest = t;
    }

    /**
     *
     */
    public AdHocTest() {
        super();
    }
    
    
    @Override
    public void execute() {
    	buildTestConditions();
    	
    	// notify aggreggatorTest of this Test's end
    	TestChildEndEvent endEvent = new TestChildEndEvent();
        endEvent.setTest(this);
        endEvent.setTime(testTime);
        testEvents.add(endEvent);
    	
    	
    	// registers test events into the simulator
        if (testEvents.size() > 2) {
            log("insert " + testEvents.size() + " events into the simulator");
            for (Event event : testEvents) {
                getSimulation().getSimulator().addEvent(event);
            }
        }
    }
    
    
    private void buildTestConditions() {
        // create events to send messages
        if (!sourceNodes.isEmpty()) {
            for (int k = 0; k < inputParameters.getNumberOfMessagesPerNode(); k++) {
                for (Object s : sourceNodes) {
                    Node srcNode = (Node) s;
                    boolean keepSeqN = false;
                    for (Object r : receiverNodes) {
                        Node rcvNode = (Node) r;
                        for (int i = 0; i <= inputParameters.getNumberOfRetransmissions(); i++) {
                        	testTime += (inputParameters.getIntervalBetweenMessagesSent() * Simulator.ONE_SECOND);
                        	DefaultTestExecutionEvent event = createEvent(srcNode, rcvNode, testTime, keepSeqN);
                            testEvents.add(event);
                            keepSeqN = true; // so that each node sends a different msgId
                        }
                    }
                }
            }
        }

    }
    
    public void notifyAggreggatorOfEnd(){
    	aggreggatorTest.childTestFinished();
    }

    /**
     *
     */
    @Override
    public void prepare() {
    	//clear previous settings
    	for (Object node : simulation.getSimulator().getNodes()) {
            Node n = (Node) node;
            n.setSource(false);
//            n.setReceiver(false);
            n.getRoutingLayer().setUnderAttack(false);

        }
    	
    	
    	int senders = 0;
    	int attacked = 0;
        Collection<Node> nodes = simulation.getSimulator().getNodes();
       //for hand picked nodes
        for (Node node : nodes) {
            if (node.isSource()) {
                sourceNodes.add(node);
                senders++;
            }
            if (node.isReceiver()) {
                receiverNodes.add(node);
            }
            if (node.getRoutingLayer().isUnderAttack()) {
                attackNodes.add(node);
                attacked++;
            }
        }
        //fill the rest with random nodes
        while(senders < this.inputParameters.numberOfSenderNodes){
        	Node n = simulation.getSimulator().getRandomNode();
        	if( !n.isSinkNode() && !sourceNodes.contains(n)){
        		sourceNodes.add(n);
        		n.setSource(true);
        		senders++;
        	}
        }
        while(attacked < this.inputParameters.numberOfAttackNodes){
        	Node n = simulation.getSimulator().getRandomNode();
        	if( !n.isSinkNode() && n.getRoutingLayer().isStable() && !attackNodes.contains(n)){
        		attackNodes.add(n);
        		n.getRoutingLayer().setUnderAttack(true);
        		attacked++;
        	}
        }
        
        prepared = true;
    }
}
