/*
 *  Wireless Sensor Network Simulator
 *  The next generation for WSN Simulations
 */
package org.wisenet.simulator.components.evaluation.tests.events;

import org.wisenet.simulator.core.Event;
import org.wisenet.simulator.core.Simulator;

/**
 *
 * @author Pedro Marques da Silva <MSc Student @di.fct.unl.pt>
 */
public class TestEndEvent extends AbstractTestEvent {

	private static final int WAIT_IN_SECONDS = 20;
	
    public void execute() {
    	// test if is the last event
    	if (this.getTest().getSimulation().getSimulator().getNumberOfRemainEvents() == 0)
    		getTest().endTest();
    	else {
	    	// update self time value after the current last event in queue
	    	Event last = (Event) this.getTest().getSimulation().getSimulator().getEventQueue().getLast();
	    	if (last != null) {
		    	this.setTime(last.getTime() + Simulator.ONE_SECOND * WAIT_IN_SECONDS);
		    	this.getTest().getSimulation().getSimulator().addEvent(this);
	    	} else {
		    	// something strange (not empty and last is null hummm) but let's continue
		    	this.setTime(this.getTime() + Simulator.ONE_SECOND * WAIT_IN_SECONDS);
	    	}
    	}
    }
}
