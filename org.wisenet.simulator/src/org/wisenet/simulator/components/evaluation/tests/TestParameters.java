/*
 *  Wireless Sensor Network Simulator
 *  The next generation for WSN Simulations
 */
package org.wisenet.simulator.components.evaluation.tests;

import org.wisenet.simulator.common.ObjectParameters;

/**
 *
 * @author Pedro Marques da Silva <MSc Student @di.fct.unl.pt>
 */
public class TestParameters extends ObjectParameters {
    @Override
    protected void setSupportedParameters() {
        // for now nothing to do
    }
}
