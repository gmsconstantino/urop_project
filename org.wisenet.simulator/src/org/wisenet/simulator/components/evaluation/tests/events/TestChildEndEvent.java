package org.wisenet.simulator.components.evaluation.tests.events;

import org.wisenet.simulator.components.evaluation.tests.AdHocTest;


public class TestChildEndEvent extends AbstractTestEvent {

    @Override
    public void execute() {
        ((AdHocTest)getTest()).notifyAggreggatorOfEnd();
    }
}
