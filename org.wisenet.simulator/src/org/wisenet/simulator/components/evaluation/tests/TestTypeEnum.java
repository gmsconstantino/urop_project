/*
 *  Wireless Sensor Network Simulator
 *  The next generation for WSN Simulations
 */
package org.wisenet.simulator.components.evaluation.tests;

/**
 *
 * @author Pedro Marques da Silva <MSc Student @di.fct.unl.pt>
 */
public enum TestTypeEnum {

    /**
     * 
     */
    OnDemand,
    /**
     *
     */
    Temporized,
    /**
     * 
     */
    OnEmptyQueue,
    /**
     *
     */
    OnCondition;
}
