package org.wisenet.simulator.components.evaluation.tests;

import java.util.Collection;

import org.wisenet.simulator.core.node.Node;



/**
*
* @author Andr� Guerreiro
*/
public class RouteBalancementTest extends BaseTest {

	
	/**
    *
    * @param inputParameters
    */
   public RouteBalancementTest(TestInputParameters inputParameters) {
       super(inputParameters);
       
   }
   
   public RouteBalancementTest() {
       super();
   }
   
   @Override
   public void prepare() {
	   
	   int numberOfSenderNodes = inputParameters.getNumberOfSenderNodes();
	   //set the random sender nodes(only stable ones)
	   for(int i = 0; i < numberOfSenderNodes; i++){
		   Node n = simulation.getSimulator().getRandomNode();
		   while(sourceNodes.contains(n) || !n.getRoutingLayer().isStable() || n.isSinkNode())
			   n = simulation.getSimulator().getRandomNode();
		   sourceNodes.add(n);
		   n.setSource(true);
		   
	   }
	  
	   /*
	    * set one sink nodes as receiver
	    * 
	    * works for multi-basestation protocols assuming that the protocol
	    * overwrites the destination and may transform one message in
	    * several to send to different base-stations
	    */
       Collection<Node> nodes = simulation.getSimulator().getNodes();
       receiverNodes.clear();
       for (Node node : nodes) {
           if (node.isSinkNode()){ 
               receiverNodes.add(node);
               break;
           }
       }
       prepared = true;
   }
   
}
