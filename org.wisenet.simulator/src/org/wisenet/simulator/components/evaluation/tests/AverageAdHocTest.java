package org.wisenet.simulator.components.evaluation.tests;

import java.util.LinkedList;

import org.wisenet.simulator.components.evaluation.tests.events.TestEndEvent;
import org.wisenet.simulator.components.evaluation.tests.events.TestStartEvent;
import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.node.Node;

public class AverageAdHocTest extends AbstractTest  {

	private int runningChildTests;
	private LinkedList<AdHocTest> childTests;
	
	public AverageAdHocTest(TestInputParameters inputParameters) {
        super(inputParameters);
        
    }
	
	@Override
	public boolean verifyPreConditions() {
		return true;
	}

	@Override
	public void beforeStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterFinish() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void execute() {
		if (getSimulation() == null) {
            log("no simulation defined");
            return;
        }
		testEvents.clear();
        testTime = getSimulation().getTime() + (Simulator.ONE_SECOND * 5);
        // Starts Test
        TestStartEvent startEvent = new TestStartEvent();
        startEvent.setTest(this);
        startEvent.setTime(testTime);
        getSimulation().getSimulator().addEvent(startEvent);
        testTime += Simulator.ONE_SECOND * 5;
        
        //meter aqui os varios adhocs e arranjar forma de fazer a m�dia dos resultados
        AdHocTest t = null;
        for(int i = 0; i < getTimesToRun(); i++){
        	t = new AdHocTest(inputParameters,this);
        	t.setDebugEnabled(false);
            t.setBatchMode(false);
            t.setSimulation(simulation);
        	t.prepare();
        	t.execute();
        	runningChildTests++;
        }
        this.sourceNodes = t.sourceNodes;
        this.receiverNodes = t.receiverNodes;
        this.attackNodes = t.attackNodes;
        
	}
	
	public void childTestFinished(){
		runningChildTests--;
		if(runningChildTests == 0){
			// Ends Test
	        TestEndEvent endEvent = new TestEndEvent();
	        endEvent.setTest(this);
	        endEvent.setTime(testTime);
	        getSimulation().getSimulator().addEvent(endEvent);
		}
		
	}

	@Override
	public void prepare() {
		//clear previous settings
    	for (Object node : simulation.getSimulator().getNodes()) {
            Node n = (Node) node;
            n.setSource(false);
//            n.setReceiver(false);
            n.getRoutingLayer().setUnderAttack(false);
            n.getRoutingLayer().clearDataConsensusStructures();

        }
    	this.prepared = true;
		
	}

}
