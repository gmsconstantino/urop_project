package org.wisenet.simulator.components.evaluation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;

import org.wisenet.simulator.components.evaluation.tests.AbstractTest;
import org.wisenet.simulator.components.evaluation.tests.TestResults;
import org.wisenet.simulator.core.Message;
import org.wisenet.simulator.core.energy.GlobalEnergyDatabase;
import org.wisenet.simulator.core.node.layers.routing.RoutingLayer;

/**
 *
 * 
 *  */
public class EvaluationManager {

    MessageDatabase messageDatabase = new MessageDatabase();
    GlobalEnergyDatabase energyDatabase;
    List<Message> allMessages = new ArrayList<Message>();
    AbstractTest test;
    private boolean started;
    private int countAttackedMessages;
    private TestResults testResult;
    private Set<Long> attackedMessagesIdsSet;

    /**
     * Notify test start 
     * @param test
     *          a test instance that started
     */
    public void startTest(AbstractTest test) {
        if (!started) {
            countAttackedMessages = 0;
            attackedMessagesIdsSet = new HashSet<Long>();
            this.test = test;
            energyDatabase = test.getSimulation().getEnergyController().createDatabase(test.getName(), true);
            started = true;
        }
    }

    /**
     * Notify test end
     */
    public void endTest() {
        if (started) {
            energyDatabase = test.getSimulation().getEnergyController().getDatabase(test.getName());
            createResult();
        }
    }

    /**
     *
     * @param message
     * @param routing
     */
    public void registerMessageSent(Object message, RoutingLayer routing) {
        messageDatabase.registerMessageSent((Message) message, routing);
    }
    
    
    public void registerMessagePassage(Object message, RoutingLayer routing) {
        messageDatabase.registerMessagePassage((Message) message, routing);
    }


    /**
     *
     * @param message
     * @param routing
     */
    public void registerMessageReceivedDone(Object message, RoutingLayer routing) {
        // Message received right
        messageDatabase.registerMessageReceived((Message) message, routing);
    }

    /**
     *
     * @return
     */
    public GlobalEnergyDatabase getEnergyDatabase() {
        return energyDatabase;
    }

    /**
     * 
     * @return
     */
    public MessageDatabase getMessageDatabase() {
        return messageDatabase;
    }
    
    public Message getOriginalMessage(long messageId){
    	return messageDatabase.getOriginalMessage(messageId);
    }
    
    

    /**
     *
     * @return
     */
    public boolean isStarted() {
        return started;
    }

    public double getEnergyAvgPerNode() {
        DescriptiveStatistics stats = new DescriptiveStatistics();
        for (Object o : getEnergyDatabase().getNodesEnergy().values()) {
            double singleTestConsumption = (Double)o/test.getTimesToRun();
        	stats.addValue(singleTestConsumption);
//        	stats.addValue((Double)o);
        }
        return stats.getMean();
    }

    public double getTotalEnergy() {
        return getEnergyDatabase().getTotalEnergySpent()/test.getTimesToRun();
    }

    public double getLatencyMin() {
        return getMessageDatabase().getLatencyMin();
    }

    public double getLatencyMax() {
        return getMessageDatabase().getLatencyMax();
    }

    public double getLatencyAvg() {
        return getMessageDatabase().getLatencyAvg();
    }

    public void incrementAttackedMessages() {
        countAttackedMessages++;
    }

    private void createResult() {
        int nRuns = test.getTimesToRun();
    	testResult = new TestResults();
        testResult.setTestName(test.getName());
        testResult.setTotalOfMessagesAttacked(getCountAttackedMessages()/nRuns);
        testResult.setTotalNodes(test.getSimulation().getSimulator().getNodes().size());
        testResult.setTotalStableNodes(RoutingLayer.getController().getTotalStableNodes());
        testResult.setTotalSenderNodes(test.getSourceNodes().size());
        testResult.setTotalReceiverNodes(test.getReceiverNodes().size());
        testResult.setTotalAttackNodes(test.getAttackNodes().size());
        testResult.setAvgNeighboorsPerNode(test.getSimulation().getAverageNeighborsPerNode());
        testResult.setTotalOfMessagesSent(messageDatabase.getTotalNumberOfUniqueMessagesSent()/nRuns);
//        testResult.setMessagesPerNode(test.getInputParameters().getNumberOfMessagesPerNode());
//        testResult.setMessagesInterval(test.getInputParameters().getIntervalBetweenMessagesSent());
//        testResult.setMessagesRetransmissions(test.getInputParameters().getNumberOfRetransmissions());
        testResult.setReliabilityMessagesSent((int) messageDatabase.getTotalNumberOfUniqueMessagesSent()/nRuns);
        testResult.setReliabilityMessagesReceived((int) messageDatabase.getTotalMessagesReceived()/nRuns);
        testResult.setReliabilityPercent(messageDatabase.getReliabilityPercent());
        testResult.setCoverageSenderNodes(messageDatabase.getTotalSenderNodes()/nRuns);
        testResult.setCoverageReceivedNodes(messageDatabase.getTotalCoveredNodes()/nRuns);
        testResult.setCoveragePercent(messageDatabase.getCoveragePercent());
        testResult.setMinHopsPerMessage(messageDatabase.getLatencyMin());
        testResult.setMaxHopsPerMessage(messageDatabase.getLatencyMax());
        testResult.setAvgHopsPerMessage(messageDatabase.getLatencyAvg());
        testResult.setTotalEnergySpent(getTotalEnergy());
        testResult.setAverageEnergyPerNode(getEnergyAvgPerNode());
    }
    

    public TestResults getTestResult() {
        return testResult;
    }

    public int getCountAttackedMessages() {
        return attackedMessagesIdsSet.size();
    }

    public void countAttackedMessages(Message message) {
        attackedMessagesIdsSet.add(message.getUniqueId());
    }
    
    public double getLocalDataConsensusResult(){
    	Set<Short> sinks = test.getSimulation().getSimulator().getSinkNodesIds();
    	for(Short id: sinks){
    		return test.getSimulation().getSimulator().getNode(id).getRoutingLayer().getLocalDataConsensusResult();
    	}
        
        return 0;
    }
    
    
    public double getGlobalDataConsensusResult(){
    	Set<Short> sinks = test.getSimulation().getSimulator().getSinkNodesIds();
    	for(Short id: sinks){
    		return test.getSimulation().getSimulator().getNode(id).getRoutingLayer().getGlobalDataConsensusResult();
    	}
        
        return 0;
    }
    
    public boolean routingUsingLocalDataConsensus(){
    	Set<Short> sinks = test.getSimulation().getSimulator().getSinkNodesIds();
    	for(Short id: sinks){
    		return test.getSimulation().getSimulator().getNode(id).getRoutingLayer().routingUsingLocalDataConsensus();
    	}
    	
    	return false;
    }
    
    public boolean routingUsingGlobalDataConsensus(){
    	Set<Short> sinks = test.getSimulation().getSimulator().getSinkNodesIds();
    	for(Short id: sinks){
    		return test.getSimulation().getSimulator().getNode(id).getRoutingLayer().routingUsingGlobalDataConsensus();
    	}
    	
    	return false;
    }
}
