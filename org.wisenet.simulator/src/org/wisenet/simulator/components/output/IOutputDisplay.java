/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */

package org.wisenet.simulator.components.output;

/**
 *
* @author Pedro Marques da Silva <MSc Student @di.fct.unl.pt>
 */
public interface IOutputDisplay {
    /**
     *
     * @param text
     */
    public void showOutput(String text);
}
