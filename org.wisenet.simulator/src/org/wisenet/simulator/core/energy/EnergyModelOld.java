/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.simulator.core.energy;

import java.lang.reflect.Field;
import java.util.Vector;
import org.wisenet.simulator.utilities.Utilities;
import org.wisenet.simulator.utilities.annotation.AnnotationUtils;
import org.wisenet.simulator.utilities.annotation.Annotated;
import org.wisenet.simulator.utilities.annotation.EnergyModelParameter;

/**
 *
* @author Pedro Marques da Silva <MSc Student @di.fct.unl.pt>
 */
public class EnergyModelOld implements Annotated {

    private static void getEnergyModelDefaultValues(EnergyModel energyModel) throws SecurityException {
        Vector fields = AnnotationUtils.readEnergyModelParametersFields(energyModel);
        for (Object object : fields) {
            try {
                Field field = (Field) object;
                EnergyModelParameter p = field.getAnnotation(EnergyModelParameter.class);
                double v = p.value();
                field.setAccessible(true);
                field.set(energyModel, v);
            } catch (IllegalArgumentException ex) {
                Utilities.handleException(ex);
            } catch (IllegalAccessException ex) {
                Utilities.handleException(ex);
            }
        }
    }
    /**
     * Valores Baseados em
     */
    @EnergyModelParameter(label = "Total energy (Joules)", value = 9360.0)
    double totalEnergy;
    // baseado no paper 3 - Evaluation of security Mechanisms  in WSN // skipjack
    @EnergyModelParameter(label = "Encrypt energy (Joules/Byte)", value = 0.000001788)
    double encryptEnergy;
    @EnergyModelParameter(label = "Decrypt energy (Joules/Byte)", value = 0.000001788)
    double decryptEnergy;
    //Energy Analysis of public key cryptography for WSN PAPER 2
    @EnergyModelParameter(label = "Digest energy (Joules/Byte)", value = 0.0000059) // SHA1
    double digestEnergy;
    @EnergyModelParameter(label = "Sign energy (Joules/Byte)", value = 0.0000059)
    double signatureEnergy;
    @EnergyModelParameter(label = "Verify Digest energy (Joules/Byte)", value = 0.0000059)
    double verifyDigestEnergy;
    @EnergyModelParameter(label = "Verify signature energy (Joules/Byte)", value = 0.0000059)
    double verifySignatureEnergy;
    @EnergyModelParameter(label = "CPU Transition to ON energy (Joules)", value = 0.000000001)
    double cpuTransitionToActiveEnergy;
    @EnergyModelParameter(label = "Transciver Transition to ON energy (Joules)", value = 0.000000002)
    double txTransitionToActiveEnergy;
    @EnergyModelParameter(label = "Transmission energy (Joules/Byte)", value = 0.00000592)
    double transmissionEnergy;
    @EnergyModelParameter(label = "Reception energy (Joules/Byte)", value = 0.00000286)
    double receptionEnergy;
    @EnergyModelParameter(label = "Idle State energy (Joules)", value = 0.0000059)
    double idleEnergy;
    @EnergyModelParameter(label = "Sleep State energy (Joules)", value = 0.0000075)
    double sleepEnergy;
    @EnergyModelParameter(label = "Simple processing energy (Joules)", value = 0.0138)
    double processingEnergy;

    /**
     *
     * @return
     */
    public double getProcessingEnergy() {
        return processingEnergy;
    }

    /**
     *
     * @param processingEnergy
     */
    public void setProcessingEnergy(double processingEnergy) {
        this.processingEnergy = processingEnergy;
    }

    /**
     *
     * @return
     */
    public double getCpuTransitionToActiveEnergy() {
        return cpuTransitionToActiveEnergy;
    }

    /**
     *
     * @param cpuTransitionToActiveEnergy
     */
    public void setCpuTransitionToActiveEnergy(double cpuTransitionToActiveEnergy) {
        this.cpuTransitionToActiveEnergy = cpuTransitionToActiveEnergy;
    }

    /**
     *
     * @return
     */
    public double getDecryptEnergy() {
        return decryptEnergy;
    }

    /**
     *
     * @param decryptEnergy
     */
    public void setDecryptEnergy(double decryptEnergy) {
        this.decryptEnergy = decryptEnergy;
    }

    /**
     *
     * @return
     */
    public double getDigestEnergy() {
        return digestEnergy;
    }

    /**
     *
     * @param digestEnergy
     */
    public void setDigestEnergy(double digestEnergy) {
        this.digestEnergy = digestEnergy;
    }

    /**
     *
     * @return
     */
    public double getEncryptEnergy() {
        return encryptEnergy;
    }

    /**
     *
     * @param encryptEnergy
     */
    public void setEncryptEnergy(double encryptEnergy) {
        this.encryptEnergy = encryptEnergy;
    }

    /**
     *
     * @return
     */
    public double getIdleEnergy() {
        return idleEnergy;
    }

    /**
     *
     * @param idleEnergy
     */
    public void setIdleEnergy(double idleEnergy) {
        this.idleEnergy = idleEnergy;
    }

    /**
     *
     * @return
     */
    public double getReceptionEnergy() {
        return receptionEnergy;
    }

    /**
     *
     * @param receptionEnergy
     */
    public void setReceptionEnergy(double receptionEnergy) {
        this.receptionEnergy = receptionEnergy;
    }

    /**
     *
     * @return
     */
    public double getSignatureEnergy() {
        return signatureEnergy;
    }

    /**
     *
     * @param signatureEnergy
     */
    public void setSignatureEnergy(double signatureEnergy) {
        this.signatureEnergy = signatureEnergy;
    }

    /**
     *
     * @return
     */
    public double getSleepEnergy() {
        return sleepEnergy;
    }

    /**
     *
     * @param sleepEnergy
     */
    public void setSleepEnergy(double sleepEnergy) {
        this.sleepEnergy = sleepEnergy;
    }

    /**
     *
     * @return
     */
    public double getTotalEnergy() {
        return totalEnergy;
    }

    /**
     *
     * @param totalEnergy
     */
    public void setTotalEnergy(double totalEnergy) {
        this.totalEnergy = totalEnergy;
    }

    /**
     *
     * @return
     */
    public double getTransmissionEnergy() {
        return transmissionEnergy;
    }

    /**
     *
     * @param transmissionEnergy
     */
    public void setTransmissionEnergy(double transmissionEnergy) {
        this.transmissionEnergy = transmissionEnergy;
    }

    /**
     *
     * @return
     */
    public double getTxTransitionToActiveEnergy() {
        return txTransitionToActiveEnergy;
    }

    /**
     *
     * @param txTransitionToActiveEnergy
     */
    public void setTxTransitionToActiveEnergy(double txTransitionToActiveEnergy) {
        this.txTransitionToActiveEnergy = txTransitionToActiveEnergy;
    }

    /**
     *
     * @return
     */
    public double getVerifyDigestEnergy() {
        return verifyDigestEnergy;
    }

    /**
     *
     * @param verifyDigestEnergy
     */
    public void setVerifyDigestEnergy(double verifyDigestEnergy) {
        this.verifyDigestEnergy = verifyDigestEnergy;
    }

    /**
     *
     * @return
     */
    public double getVerifySignatureEnergy() {
        return verifySignatureEnergy;
    }

    /**
     *
     * @param verifySignatureEnergy
     */
    public void setVerifySignatureEnergy(double verifySignatureEnergy) {
        this.verifySignatureEnergy = verifySignatureEnergy;
    }

    /**
     *
     * @return
     */
    public EnergyModelOld getInstanceWithDefaultValues() {
//        getEnergyModelDefaultValues(this);
        return this;
    }

    /**
     *
     * @return
     */
    public static EnergyModel getDefaultInstance() {
        EnergyModel energyModel = new EnergyModel();
        getEnergyModelDefaultValues(energyModel);
        return energyModel;
    }
}
