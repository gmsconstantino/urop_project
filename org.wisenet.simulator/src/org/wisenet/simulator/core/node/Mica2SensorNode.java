/*
 ***  Wireless Sensor Network Simulator
 * The next generation for WSN Simulations
 */
package org.wisenet.simulator.core.node;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.util.AbstractMap.SimpleEntry;

import org.wisenet.simulator.core.Simulator;
import org.wisenet.simulator.core.radio.RadioModel;
import org.wisenet.simulator.core.ui.ISimulationDisplay;

/**
 *
 * @author Pedro Marques da Silva <MSc Student @di.fct.unl.pt>
 */
public abstract class Mica2SensorNode extends SensorNode {

    private Color paintingPathColor = Color.LIGHT_GRAY;

    ;
    private boolean paintingPaths = true;

    /**
     *
     * @param sim
     * @param radioModel
     */
    public Mica2SensorNode(Simulator sim, RadioModel radioModel) {
        super(sim, radioModel);
    }

    /**
     *
     * @param radioModel
     */
    @Override
    public void configureMACLayer(RadioModel radioModel) {

        if (macLayer == null) {
            throw new IllegalStateException("MacLayer cannot be null");
        }
        getMacLayer().setNode(this);
        getMacLayer().setRadioModel(radioModel);
        getMacLayer().setNeighborhood(radioModel.createNeighborhood());
    }

    @Override
    public void init() {
        super.init();
    }

    /**
     *
     * @param disp
     */
    @Override
    public void displayOn(ISimulationDisplay disp) {
        Graphics g = disp.getGraphics();

        int _x = disp.x2ScreenX(this.getX());
        int _y = disp.y2ScreenY(this.getY());

        super.displayOn(disp);

        if (turnedOn) {
            Color c = g.getColor();
            if (getMacLayer().isSending()) {
                c = getSendingColor();
            } else if (getMacLayer().isReceiving()) {
                if (getMacLayer().isCorrupted()) {
                    c = getReceivingCorruptedColor();
                } else {
                    c = getReceivingNotCorruptedColor();
                }
            } else {
                c = getBaseColor();
            }

            getGraphicNode().setBackcolor(c);
            getGraphicNode().paint(disp);

            if (paintingPaths) {
                paintPath2Parent(g, _x, _y);
            }
            if(nextHops.size() >0 ){
            	paintPath2NextHops(g, _x, _y);
            }

        } else {
            getGraphicNode().setBackcolor(Color.WHITE);
            getGraphicNode().paint(disp);
        }
    }

    private void paintPath2Parent(Graphics g, int _x, int _y) {
        Color oldcolor = g.getColor();
        if (getParentNode() != null) {
            Color c = getMessageColor();
            g.setColor(c);
            int x1 = getParentNode().getGraphicNode().getX();
            int y1 = getParentNode().getGraphicNode().getY();
            g.drawLine(_x, _y, x1, y1);
        }
        g.setColor(oldcolor);
    }
    
    private void paintPath2NextHops(Graphics g, int _x, int _y){
    	Color oldcolor = g.getColor();
    	for(SimpleEntry s : nextHops){
//    		g.setColor(getNextHopColor());
    		int x1 = (Integer)s.getKey();
    		int y1 = (Integer)s.getValue();
//    		g.drawLine(_x, _y, x1, y1);
//    		
    		Graphics2D g2d = (Graphics2D)g;
    	    Stroke oldStroke = g2d.getStroke();
    		int width = 3;
    	    g2d.setStroke(new BasicStroke(width));
    	    g2d.setColor(getNextHopColor());
    	    g2d.drawLine(_x, _y, x1, y1);
    		g2d.setStroke(oldStroke);
    		
    	}
    	g.setColor(oldcolor);
    }

    /**
     *
     * @return
     */
    public boolean isPaintingPaths() {
        return paintingPaths;
    }

    /**
     *
     * @param paintingPaths
     */
    public void setPaintingPaths(boolean paintingPaths) {
        this.paintingPaths = paintingPaths;
    }

    /**
     *
     * @return
     */
    public Color getPaintingPathColor() {
        return paintingPathColor;
    }

    /**
     *
     * @param paintingPathColor
     */
    public void setPaintingPathColor(Color paintingPathColor) {
        this.paintingPathColor = paintingPathColor;
    }
}
