package org.wisenet.simulator.core.node.layers.routing;

public interface ForwardingTableInterface {

	
	public int getHopDistanceToBS();
	
	public void setHopDistanceToBS(int hopDistanceToBS);
	
	public int getHopDistanceOnLastRoute();
}
