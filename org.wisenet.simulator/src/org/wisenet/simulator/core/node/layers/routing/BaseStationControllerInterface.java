package org.wisenet.simulator.core.node.layers.routing;

import java.util.List;
import java.util.Vector;

public interface BaseStationControllerInterface {

	public List<List<List<Short>>> getPaths();
	
	/*
	 * allPaths is used on consensus routing protocols
	 * to maintain every BS paths on each BS.
	 */
	public Vector<List<Short>> getAllPaths();
}
