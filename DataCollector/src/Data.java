import java.util.HashMap;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class Data extends Thread {

	int id;
	String data;
	HashMap<String, String> params;

	public Data(String data,int id) {
		this.data = data;
		this.id = id;
		params = new HashMap<String, String>();
	}
	
	public void run() {
		// Set data on structures		
		String[] params = data.split("&");
		for(String p : params){
			String[] tmp = p.split("=");
			this.params.put(tmp[0], tmp[1]);
		}
		System.out.println("End Worker "+id);
	}

	public String toString(){
		return params.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Data other = (Data) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (id != other.id)
			return false;
		if (params == null) {
			if (other.params != null)
				return false;
		} else if (!params.equals(other.params))
			return false;
		return true;
	}
	
	
}
