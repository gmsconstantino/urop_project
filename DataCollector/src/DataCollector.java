
import java.io.FileWriter;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
*
* @author Constantino Gomes <BSc Student @campus.fct.unl.pt>
*/
public class DataCollector extends Thread{
	
	public final int PORT = 8888;
	private DatagramSocket socket;
	private volatile boolean running = true;
	private static int idWorker;
	
	List<Data> objectsData;
	
	public DataCollector() {
		idWorker = 0;
		objectsData = new ArrayList<Data>();
		
		try {
			socket = new DatagramSocket(PORT);
			socket.setSoTimeout(1000);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public void terminate() {
        running = false;
    }
	
	public void run() {
		while (running) {
			byte[] buffer = new byte[65536];
			DatagramPacket echoRequest = new DatagramPacket(buffer,
					buffer.length);
			try {
				socket.receive(echoRequest);				
				String data = new String(echoRequest.getData());
				Data d = new Data(data,idWorker);
				objectsData.add(d);
				d.start();
				System.out.println("Start worker "+idWorker);
				idWorker++;
			} catch (SocketTimeoutException e) {
		        // no response received after 1 second
		    } catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void toCSVFile(List<ArrayList<Data>> data) throws IOException{
		String nameFile = JOptionPane.showInputDialog("Nome do Ficheiro:");
		FileWriter pwSend = new FileWriter(nameFile+"_send.csv");
		FileWriter pwReceived = new FileWriter(nameFile+"_recd.csv");
		
		pwSend.append("id\toperation\ttime\tsimtime\thops\tsize\n");
		pwReceived.append("id\toperation\ttime\tsimtime\thops\tsize\n");
		FileWriter pw = null;
		for (ArrayList<Data> darr : data) {
			for(Data d : darr){
				if(d.params.get("operation").equals("send"))
					pw = pwSend;
				else
					pw = pwReceived;
				
				pw.append(d.params.get("id")+"\t");
				pw.append(d.params.get("operation")+"\t");
				pw.append(d.params.get("time")+"\t");
				pw.append(d.params.get("simtime")+"\t");
				pw.append(d.params.get("hops")+"\t");
				pw.append(d.params.get("size")+"\n");
			}
		}
		pwSend.close();
		pwReceived.close();
	}

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);
		
		DataCollector collector = new DataCollector();
		collector.start();	
		
		System.out.println("Press Enter to Stop jobs.");
		in.nextLine();
		collector.terminate();
		try {
			collector.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Terminate and join thread collector.");		
		System.out.println("Analyse data:");
		List<Data> visited = new ArrayList<Data>();
		int i;
		ArrayList<Data> arr= null;
		List<ArrayList<Data>> toPrint = new ArrayList<ArrayList<Data>>();
		for(Data d : collector.objectsData){
			if(visited.contains(d))
				continue;

			int msgid = Integer.parseInt(d.params.get("id"));
			i = 0;
			try{
				ListIterator<Data> it = collector.objectsData.listIterator(d.id+1);
				Data dit = null;
				arr = new ArrayList<Data>();
				arr.add(d);
				while (i<5 && it.hasNext()) {
					dit = (Data) it.next();
					int msg = Integer.parseInt(dit.params.get("id"));
					if(msgid == msg){
						arr.add(dit);
						visited.add(dit);
					}					
					i++;
				}
				if(arr.size()>1){
					if(arr.size()>2){
						long tmp = Long.parseLong(arr.get(0).params.get("time"));
						int index = 0;
						for(int j=0;j<arr.size();j++){
							if(arr.get(1).params.get("operation").equals("send"))
								continue;
							
							long time = Long.parseLong(arr.get(j).params.get("time"));
							if(index!=j && time >= tmp)
								arr.remove(j);
							else{
								arr.remove(index);
								tmp = time;
								index = j;
							}
							if(j>=arr.size())
								break;
						}
					}
					toPrint.add(arr);
				}
			}catch(IndexOutOfBoundsException e){

			}
		}
		System.out.println("Saving data.");
		collector.toCSVFile(toPrint);
		
		System.out.println("Done.");
	}

}


//List<Integer> idReceived = new ArrayList<Integer>();
//HashMap<Integer, ArrayList<Data>> dataById = new HashMap<Integer, ArrayList<Data>>();
//for(Data d : collector.objectsData){
//	System.out.println(d.toString());
//	
//	int id = Integer.parseInt(d.params.get("id"));
//	ArrayList<Data> arr;
//	if(dataById.containsKey(id)){
//		arr = dataById.get(id);
//		idReceived.add(id);
//	}else
//		arr = new ArrayList<Data>();
//	arr.add(d);
//	dataById.put(id, arr);
//}
//collector.toCSVFile(idReceived, dataById);